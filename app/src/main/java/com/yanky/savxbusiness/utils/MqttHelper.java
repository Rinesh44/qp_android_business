package com.yanky.savxbusiness.utils;

import com.yanky.savxbusiness.model.MqttPayload;

import java.io.UnsupportedEncodingException;

public class MqttHelper {
    private static final String BASE_TOPIC = "savx/session/%s/%s";

    public static String getShareTopic(String sessionId){
        return String.format(BASE_TOPIC, sessionId, "share");
    }

    public static byte[] getBytes(MqttPayload payload){
        if(null == payload){
            throw new NullPointerException("Empty payload");
        }
        String data = SerDe.toJson(payload);
        try {
            return data.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static MqttPayload parseFrom(byte[] payload){
        //Convert directly to string
        try {
            return SerDe.toObject(new String(payload, "UTF-8"), MqttPayload.class);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

}