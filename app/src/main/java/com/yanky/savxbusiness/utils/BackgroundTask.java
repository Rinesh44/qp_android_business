package com.yanky.savxbusiness.utils;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

/**
 * Author : Dipak Malla
 * Date : 1/19/17.
 */

public class BackgroundTask extends Thread {
    private Handler handler;
    @Override
    public void run(){
        try{
            Looper.prepare();
            handler = new Handler();
            Looper.loop();
        }catch (Throwable throwable){
            Log.e("Background_Task", "Error", throwable);
        }
    }
    public Handler getHandler(){
        return this.handler;
    }
}
