package com.yanky.savxbusiness.utils;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.widget.RelativeLayout;

/**
 * Created by Leesa Shakya on 08/10/18.
 * leezshk@gmail.com
 */

public class ViewUtils {
    public static RelativeLayout.LayoutParams getDialogParams(Context context){
        DisplayMetrics metrics = new DisplayMetrics(); //get metrics of screen
        ((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width = (int) (metrics.widthPixels * 0.8); //set width to 80% of total

        return new RelativeLayout.LayoutParams(width,
                RelativeLayout.LayoutParams.MATCH_PARENT);
    }
}
