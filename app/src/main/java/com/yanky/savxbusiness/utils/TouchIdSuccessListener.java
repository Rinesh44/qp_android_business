package com.yanky.savxbusiness.utils;

public interface TouchIdSuccessListener {
    void onTouchIDSuccess();
}
