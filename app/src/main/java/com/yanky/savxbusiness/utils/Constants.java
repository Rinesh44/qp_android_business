package com.yanky.savxbusiness.utils;

public class Constants {
    public static final String PASSCODE = "passcode";
    public static final String USER_ID = "user_id";
    public static final String USER_TOKEN = "user_token";
    public static final String IS_KEY_BACKUP = "is_key_backup";
    public static final String LOGGED_IN = "logged_in";
    public static final String VALIDATED = "validated";
    public static final String FULL_NAME = "fullname";
    public static final String DOB = "dob";
    public static final String GENDER = "gender";
    public static final String PHONE_OR_EMAIL = "phone_or_email";
    public static final String PASSCODE_LONG = "passcode_long";
    public static final String DEFAULT_WALLET_ADDRESS = "default_wallet_address";
    public static final String TOUCHID_ENABLED = "touch_id_enabled";
    public static final String SELECTED_CURRENCY = "selected_currency";
    public static final String SELECTED_COUNTRY_CODE = "selected_country_code";
    public static final String IS_KEY_IMPORTED = "is_key_imported";
    public static final String IS_FOR_WAITING_USER = "is_for_waiting_user";
    public static final String IS_FROM_LOGIN = "is_from_login";
    public static final String FIRST_TIME_LAUNCH = "first_time_launch";

    public static final String TRADE = "Trade";
    public static final String TRANSACTION = "Transaction";
    public static final String ACHLOADFUND = "Ach Fund Load";
    public static final String WALLETREQUESTCREATED = "Wallet Request created";
    public static final String WALLETREQUEST = "Wallet Request";
    public static final String FUNDREQUESTCREATED = "Fund Request created";
    public static final String FUNDREQUEST = "Fund Request";
    public static final String INVOICE = "Invoice";
    public static final String PAYMENTRECEIVED = "Payment Received";
    public static final String MORETRANSACTION = "moreTransaction";
    public static final String MOREPAYMENTREQUEST = "morePaymentRequest";
    public static final String MOREWALLETREQUEST = "moreWalletRequest";
    public static final String MOREAUTOPAYMENT = "moreAutoPayment";
    public static final String WALLETS = "Wallets";
    public static final String PAYMENTRECEIVEDHOLD = "Payment Received on hold";
    public static final String MORE = "More";

}
