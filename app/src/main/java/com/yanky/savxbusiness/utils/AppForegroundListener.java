package com.yanky.savxbusiness.utils;

public interface AppForegroundListener {
    void onAppForeground();
}
