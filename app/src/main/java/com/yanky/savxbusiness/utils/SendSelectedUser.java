package com.yanky.savxbusiness.utils;

import com.yanky.savxbusiness.realm.models.User;

public interface SendSelectedUser {
    void getUser(User user);
}
