package com.yanky.savxbusiness.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scrounger.countrycurrencypicker.library.Country;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.realm.models.TransactionHolder;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection;

public class HeaderRecyclerViewSection extends StatelessSection {
    private static final String TAG = HeaderRecyclerViewSection.class.getSimpleName();
    private String title;
    private List<TransactionHolder> list;
    private Context mContext;

    private RecyclerViewClickListener clickListener;
    private int headerPosition;
    private ItemViewHolder iHolder;
    private int headerCount = 0;

    public void setClickListener(RecyclerViewClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }


    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        public TextView headerTitle;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            headerTitle = (TextView) itemView.findViewById(R.id.header_id);
        }
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView heading, amount, date;


        public ItemViewHolder(View itemView) {
            super(itemView);
            heading = (TextView) itemView.findViewById(R.id.tv_heading);
            amount = (TextView) itemView.findViewById(R.id.tv_amount);
            date = (TextView) itemView.findViewById(R.id.tv__date);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            AppUtils.showLog(TAG, "adapterPos: " + getAdapterPosition());
            AppUtils.showLog(TAG, "headerPosition " + headerPosition);
            if (clickListener != null)
                if (headerPosition == 0) clickListener.onClick(view, getAdapterPosition() - 1);
                else clickListener.onClick(view, getAdapterPosition() - headerPosition);
        }
    }

    public HeaderRecyclerViewSection(Context mContext, String title, List<TransactionHolder> list) {
        super(R.layout.transaction_header, R.layout.transaction_row);
        this.mContext = mContext;
        this.title = title;
        this.list = list;
    }

    @Override
    public int getContentItemsTotal() {
        return list.size();
    }


    @Override
    public RecyclerView.ViewHolder getItemViewHolder(View view) {
        LinearLayout linearLayout = (LinearLayout) view;
//        CardView cardView = linearLayout.findViewById(R.id.cv_transaction_holder);
        LinearLayout layout = (LinearLayout) LayoutInflater.from(mContext).inflate(R.layout.transaction_raw, linearLayout, false);
        linearLayout.addView(layout);
        return new ItemViewHolder(linearLayout);
    }

    @Override
    public void onBindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
        iHolder = (ItemViewHolder) holder;
        TransactionHolder transactionHolder = list.get(position);


        if (list.size() == 1) {
            iHolder.itemView.setBackground(mContext.getResources().getDrawable(R.drawable.round_corner));
        } else if (position == list.size() - 1) {
            iHolder.itemView.setBackground(mContext.getResources().getDrawable(R.drawable.rounded_bottomsheet_inverse));
            headerCount++;
        } else if (position == 0) {
            iHolder.itemView.setBackground(mContext.getResources().getDrawable(R.drawable.rounded_bottomsheet));
        }


        String desc = TransactionDescriptionHelper.getDescriptionFor(transactionHolder, mContext);
        String status = TransactionDescriptionHelper.getStatus(transactionHolder, mContext);
        AppUtils.showLog(TAG, "status" + status);

        iHolder.heading.setText(desc);
        StringBuilder amountBuilder = new StringBuilder();
        int flagId;

        try {
            if (transactionHolder.getTransactiontype().equals("Load Fund")) {
                Country countryWithCurrency = Country.getCountryWithCurrency(transactionHolder.getToCountryCode(), mContext);
                assert countryWithCurrency != null;
                String currencySymbol = Objects.requireNonNull(countryWithCurrency.getCurrency()).getSymbol();
                if (currencySymbol.length() > 1) {
                    currencySymbol = currencySymbol.substring(currencySymbol.length() - 1);
                }
                amountBuilder.append(transactionHolder.getToCurrency());
                amountBuilder.append(" ");
                amountBuilder.append(currencySymbol);


//                flagId = Country.getCountry(transactionHolder.getToCountryCode(), mContext).getFlagId();
            } else {
                Country countryWithCurrency = Country.getCountryWithCurrency(transactionHolder.getFromCountryCode(), mContext);
                String currencySymbol = Objects.requireNonNull(countryWithCurrency.getCurrency()).getSymbol();
                if (currencySymbol.length() > 1) {
                    currencySymbol = currencySymbol.substring(currencySymbol.length() - 1);
                }
                amountBuilder.append(transactionHolder.getFromCurrency());
                amountBuilder.append(" ");
                amountBuilder.append(currencySymbol);


//                flagId = Country.getCountry(transactionHolder.getFromCountryCode(), mContext).getFlagId();
            }

            amountBuilder.append(" ");
            amountBuilder.append(String.format("%.0f", Double.valueOf(transactionHolder.getAmount()) / 100));
            iHolder.amount.setText(amountBuilder);


        /*    if (flagId != 0) {
                Drawable drawable = mContext.getResources().getDrawable(flagId);
                Drawable resized = resize(drawable);
                iHolder.amount.setCompoundDrawablePadding(10);
                iHolder.amount.setCompoundDrawablesWithIntrinsicBounds(resized, null, null, null);
            }*/

            iHolder.date.setText(getDate(transactionHolder.getTimestamp()));

            if (status.equals("INCOMING")) {
                iHolder.amount.setTextColor(mContext.getResources().getColor(R.color.green));
            } else {
                iHolder.amount.setTextColor(mContext.getResources().getColor(R.color.reds));
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public RecyclerView.ViewHolder getHeaderViewHolder(View view) {
        return new HeaderViewHolder(view);
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder) {
        HeaderViewHolder hHolder = (HeaderViewHolder) holder;
        hHolder.headerTitle.setText(title);
        headerPosition = hHolder.getAdapterPosition();
    }


    private Drawable resize(Drawable image) {
        Bitmap b = ((BitmapDrawable) image).getBitmap();
        Bitmap bitmapResized = Bitmap.createScaledBitmap(b, 30, 30, false);
        return new BitmapDrawable(mContext.getResources(), bitmapResized);
    }

    private String getDate(long time) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("E, d MMM yyyy, h:mm a");
            return sdf.format(new Date(time));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getHeader() {
        return title;
    }

}