package com.yanky.savxbusiness.utils;

import com.yanky.savxbusiness.entities.WalletProto;

import java.util.List;

public interface UpdateUnpaidInvoice {
    void updateUnpaidInvoiceRecyclerview();
    void updateUnpaidInvoiceHolder(List<WalletProto.InvoiceHolder> unpaidInvoiceHolder);
}
