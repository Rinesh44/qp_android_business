package com.yanky.savxbusiness.utils;

import com.yanky.savxbusiness.entities.WalletProto;

import java.util.List;

public interface UpdatePaidInvoice {
    void updatePaidInvoiceRecyclerView();

    void updatePaidInvoiceHolder(List<WalletProto.InvoiceHolder> invoiceHolders);
}
