package com.yanky.savxbusiness.utils;

import com.yanky.savxbusiness.realm.models.Wallet;

public interface SendSelectedWallet {
    void getWallet(Wallet wallet);
}
