package com.yanky.savxbusiness.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.yanky.savxbusiness.realm.models.TransactionHolder;
import com.yanky.savxbusiness.realm.models.TransactionOnHoldHolder;
import com.yanky.savxbusiness.repo.WalletsRepo;

public class TransactionDescriptionHelper {
    private static final String TAG = "TransactionDescriptionH";


    public static String getDescriptionFor(TransactionHolder transactionHolder, Context context) {
        String fromUser = transactionHolder.getFromUserFullName();
        String toUser = transactionHolder.getToUserFullName();
        String textDesc = "";
        boolean incoming = false;
        boolean toSelf = false;

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        String userId = preferences.getString(Constants.USER_ID, null);
        AppUtils.showLog(TAG, userId);
        if (transactionHolder.getToUserId().equals(userId) && transactionHolder.getFromUserId().equals(userId)) {
            toSelf = true;
        } else if (transactionHolder.getToUserId().equals(userId)) {
            //incoming
            incoming = true;
        } else {
            //outgoing
            incoming = false;
        }

        try {

            switch (transactionHolder.getTransactiontype()) {
                case "Unknown":
                    textDesc = "Transaction";
                    return textDesc;


                case "Invoice":
                    textDesc = transactionHolder.getWalletName();
                    return textDesc;

                case "Wallet Request":
                    if (incoming) {
                        String walletName = WalletsRepo.getInstance().getWalletNameForAddress(transactionHolder.getToWalletAddress());
                        textDesc = "Fund received on wallet " + walletName;
                    } else {
                        String walletName = WalletsRepo.getInstance().getWalletNameForAddress(transactionHolder.getFromPubKey());
                        textDesc = "Fund transfered from wallet " + walletName;
                    }
                    return textDesc;

                case "Request Fund":
                    double transactionAmount = Double.valueOf(transactionHolder.getAmount());
                    if (incoming) {
                        textDesc = fromUser + " sent " + transactionHolder.getFromCurrency() + " " + String.format("%.2f", transactionAmount / 100);
                    } else {
                        textDesc = "Sent " + transactionHolder.getFromCurrency() + " " + String.format("%.2f", transactionAmount / 100) +
                                " to " + toUser;
                    }
                    return textDesc;

                case "Load Fund":
                    textDesc = "Loaded fund from " + transactionHolder.getNameOnAccount();
                    return textDesc;

                case "Withdraw Fund":
                    textDesc = "Withdraw fund";
                    return textDesc;


                case "Fund Transfer":
                    double amount = Double.valueOf(transactionHolder.getAmount());
                    if (!toSelf) {
                        if (incoming) {
                            textDesc = fromUser + " sent " + transactionHolder.getFromCurrency() + " " + String.format("%.2f", amount / 100);
                        } else {
                            textDesc = "Sent " + transactionHolder.getFromCurrency() + " " + String.format("%.2f", amount / 100) + " to " +
                                    toUser;
                        }
                    } else {
                        textDesc = "You transfered " + String.format("%.2f", amount / 100) +
                                " to your wallet " + WalletsRepo.getInstance().getWalletNameForAddress(transactionHolder.getToPubKey())
                                + " from wallet " + WalletsRepo.getInstance().getWalletNameForAddress(transactionHolder.getFromPubKey());
                        return textDesc;

                    }
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return textDesc;

    }


    public static String getDescriptionFor(TransactionOnHoldHolder transactionHolder, Context context) {
        String fromUser = transactionHolder.getFromUserFullName();
        String toUser = transactionHolder.getToUserFullName();
        String textDesc = "";
        boolean incoming = false;
        boolean toSelf = false;

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        String userId = preferences.getString(Constants.USER_ID, null);
        AppUtils.showLog(TAG, userId);
        if (transactionHolder.getToUserId().equals(userId) && transactionHolder.getFromUserId().equals(userId)) {
            toSelf = true;
        } else if (transactionHolder.getToUserId().equals(userId)) {
            //incoming
            incoming = true;
        } else {
            //outgoing
            incoming = false;
        }

        try {

            switch (transactionHolder.getTransactiontype()) {
                case "Unknown":
                    textDesc = "Transaction";
                    return textDesc;


                case "Invoice":
                    textDesc = transactionHolder.getWalletName();
                    return textDesc;

                case "Wallet Request":
                    if (incoming) {
                        String walletName = WalletsRepo.getInstance().getWalletNameForAddress(transactionHolder.getToWalletAddress());
                        textDesc = "Fund received on wallet " + walletName;
                    } else {
                        String walletName = WalletsRepo.getInstance().getWalletNameForAddress(transactionHolder.getFromPubKey());
                        textDesc = "Fund transfered from wallet " + walletName;
                    }
                    return textDesc;

                case "Request Fund":
                    double transactionAmount = Double.valueOf(transactionHolder.getAmount());
                    if (incoming) {
                        textDesc = fromUser + " sent " + transactionHolder.getFromCurrency() + " " + String.format("%.2f", transactionAmount / 100);
                    } else {
                        textDesc = "Sent " + transactionHolder.getFromCurrency() + " " + String.format("%.2f", transactionAmount / 100) +
                                " to " + toUser;
                    }
                    return textDesc;

                case "Load Fund":
                    textDesc = "Loaded fund from " + transactionHolder.getNameOnAccount();
                    return textDesc;

                case "Withdraw Fund":
                    textDesc = "Withdraw fund";
                    return textDesc;


                case "Fund Transfer":
                    double amount = Double.valueOf(transactionHolder.getAmount());
                    if (!toSelf) {
                        if (incoming) {
                            textDesc = fromUser + " sent " + transactionHolder.getFromCurrency() + " " + String.format("%.2f", amount / 100);
                        } else {
                            textDesc = "Sent " + transactionHolder.getFromCurrency() + " " + String.format("%.2f", amount / 100) + " to " +
                                    toUser;
                        }
                    } else {
                        textDesc = "You transfered " + String.format("%.2f", amount / 100) +
                                " to your wallet " + WalletsRepo.getInstance().getWalletNameForAddress(transactionHolder.getToPubKey())
                                + " from wallet " + WalletsRepo.getInstance().getWalletNameForAddress(transactionHolder.getFromPubKey());
                        return textDesc;

                    }
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return textDesc;

    }

    public static String getStatus(TransactionHolder transactionHolder, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String userId = preferences.getString(Constants.USER_ID, null);

        if (transactionHolder.getToUserId().equals(userId)) {
            return "INCOMING";
        } else {
            return "OUTGOING";
        }
    }

    public static String getStatus(TransactionOnHoldHolder transactionHolder, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String userId = preferences.getString(Constants.USER_ID, null);

        if (transactionHolder.getToUserId().equals(userId)) {
            return "INCOMING";
        } else {
            return "OUTGOING";
        }
    }


    public static String getShortDesc(TransactionHolder transactionHolder, Context context) {
        String fromUser = transactionHolder.getFromUserFullName();
        String toUser = transactionHolder.getToUserFullName();
        String textDesc = "";
        boolean incoming = false;
        boolean toSelf = false;

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        String userId = preferences.getString(Constants.USER_ID, null);
        AppUtils.showLog(TAG, userId);
        if (transactionHolder.getToUserId().equals(userId) && transactionHolder.getFromUserId().equals(userId)) {
            toSelf = true;
        } else if (transactionHolder.getToUserId().equals(userId)) {
            //incoming
            incoming = true;
        } else {
            //outgoing
            incoming = false;
        }

        AppUtils.showLog(TAG, "transactionType: " + transactionHolder.getTransactiontype());


        switch (transactionHolder.getTransactiontype()) {
            case "Unknown":
                textDesc = "Transaction";
                return textDesc;


            case "Invoice":
                textDesc = "Invoice paid from wallet " + transactionHolder.getWalletName();
                return textDesc;

            case "Wallet Request":
                if (incoming) {
                    String walletName = WalletsRepo.getInstance().getWalletNameForAddress(transactionHolder.getToWalletAddress());
                    textDesc = "Fund received on wallet " + walletName;
                } else {
                    String walletName = WalletsRepo.getInstance().getWalletNameForAddress(transactionHolder.getFromPubKey());
                    textDesc = "Fund transfered from wallet " + walletName;
                }
                return textDesc;

            case "Request Fund":
                double transactionAmount = Double.valueOf(transactionHolder.getAmount());
                if (incoming) {
                    textDesc = fromUser + " sent " + transactionHolder.getFromCurrency() + " " + String.format("%.2f", transactionAmount / 100);
                } else {
                    textDesc = "Sent " + transactionHolder.getFromCurrency() + " " + String.format("%.2f", transactionAmount / 100) +
                            " to " + toUser;
                }
                return textDesc;

            case "Load Fund":
                textDesc = "Loaded fund on wallet " + transactionHolder.getWalletName();
                return textDesc;

            case "Withdraw Fund":
                textDesc = "Withdraw fund";
                return textDesc;

            case "Fund Transfer":
                double amount = Double.valueOf(transactionHolder.getAmount());
                if (!toSelf) {
                    if (incoming) {
                        textDesc = fromUser + " sent " + transactionHolder.getFromCurrency() + " " + String.format("%.2f", amount / 100);
                    } else {
                        textDesc = "Sent " + transactionHolder.getFromCurrency() + " " + String.format("%.2f", amount / 100) + " to " +
                                toUser;
                    }
                } else {
                    textDesc = "You transfered " + String.format("%.2f", amount / 100) +
                            " to your wallet " + WalletsRepo.getInstance().getWalletNameForAddress(transactionHolder.getToPubKey())
                            + " from wallet " + WalletsRepo.getInstance().getWalletNameForAddress(transactionHolder.getFromPubKey());
                    return textDesc;

                }
        }
        return textDesc;

    }


}

