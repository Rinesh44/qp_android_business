package com.yanky.savxbusiness.utils;

import com.yanky.savxbusiness.realm.models.TransactionOnHoldHolder;

public interface SwipeButtonListener {
    void transferBalance();

    void accept();

    void reject(String requestId);

    void reject();

    void cancel(String requestId);

    void edit(int pos);

    void delete(String id, int pos);

    void claim(TransactionOnHoldHolder transactionOnHoldHolder);

    void createNewWallet(TransactionOnHoldHolder transactionOnHoldHolder);
}
