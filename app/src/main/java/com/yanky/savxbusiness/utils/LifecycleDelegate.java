package com.yanky.savxbusiness.utils;

public interface LifecycleDelegate {
    void onAppBackgrounded();

    void onAppForegrounded();
}
