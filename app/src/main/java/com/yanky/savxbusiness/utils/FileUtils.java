package com.yanky.savxbusiness.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Author Dipak Malla
 * Created on 10/3/18.
 * Email: dpakmalla@gmail.com
 */
public class FileUtils {
    public static boolean write(byte[] data, File path){
        OutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(path);
            outputStream.write(data);
            outputStream.flush();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            close(outputStream);
        }
        return false;
    }

    public static byte[] toBytes(InputStream stream) throws IOException {
        return toBytes(stream, 4096);
    }
    public static byte[] open(File path) {
        InputStream inputStream = null;
        try{
            inputStream = new FileInputStream(path);
            return toBytes(inputStream);
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            if(null != inputStream){
                try {
                    inputStream.close();
                } catch (IOException ignore) {}
            }
        }
        return null;
    }
    public static boolean delete(File file){
        if(null == file){
            return false;
        }
        try{
            return file.delete();
        }catch (Throwable ignore){
        }
        return false;
    }
    public static boolean delete(String path){
        if(null == path){
            return false;
        }
        return delete(new File(path));
    }
    public static String getFileNameFromPath(String path){
        if(null == path){
            return null;
        }
        File file = new File(path);
        return file.getName();
    }
    public static String getExtensionFromFileName(String fileName){
        if(null == fileName){
            return null;
        }
        String[] parts = fileName.split("\\.");
        if(parts.length <= 1){
            return null;
        }else{
            return parts[parts.length - 1];
        }
    }
    public static boolean isImage(String mime){
        if(null == mime){
            return false;
        }
        switch (mime){
            case "image/gif":
            case "image/x-icon":
            case "image/jpeg":
            case "image/jpg":
            case "image/png":
            case "image/svg+xml":
            case "image/tiff":
                return true;
        }
        return false;
    }
    public static String getExtension(String mime){
        if(null == mime){
            return null;
        }
        switch (mime){
            case "image/gif":
                return "gif";
            case "image/x-icon":
                return "icon";
            case "image/jpeg":
            case "image/jpg":
                return "jpg";
            case "image/png":
                return "png";
            case "image/svg+xml":
                return "svg";
            case "image/tiff":
                return "tiff";
        }
        return null;
    }
    public static boolean isVideo(String mime){
        if(null == mime){
            return false;
        }
        switch (mime){
            case "video/x-msvideo":
            case "video/mpeg":
            case "video/ogg":
            case "video/webm":
            case "video/3gpp":
            case "video/3gpp2":
            case "video/mp4":
            case "application/x-mpegURL":
            case "video/x-flv":
            case "video/MP2T":
            case "video/quicktime":
            case "video/video/x-ms-wmv":
                return true;
        }
        return false;
    }
    public static byte[] toBytes(InputStream stream, int bufferSize) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = null;
        try{
            byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] buffer = new byte[bufferSize];
            int read;
            while ((read = stream.read(buffer)) != -1){
                byteArrayOutputStream.write(buffer, 0, read);
            }
            byteArrayOutputStream.flush();
            return byteArrayOutputStream.toByteArray();
        } finally {
            close(byteArrayOutputStream);
        }
    }
    public static InputStream toInputStream(byte[] bytes){
        return new ByteArrayInputStream(bytes);
    }

    public static void reset(InputStream stream){
        try {
            stream.reset();
        } catch (IOException ignore) {}
    }

    public static void close(InputStream stream){
        if(null != stream){
            try {
                stream.close();
            } catch (IOException ignore) {}
        }
    }
    public static void close(OutputStream stream){
        if(null != stream){
            try {
                stream.close();
            } catch (IOException ignore) {}
        }
    }
}
