package com.yanky.savxbusiness.activities.account;

import com.yanky.savxbusiness.QuiqpayApp;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.entities.AccountProto;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.entities.TxProto;
import com.yanky.savxbusiness.realm.models.CountryDetails;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.CallbackWrapper;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;


/**
 * Created by Leesa Shakya on 27/09/18.
 * leezshk@gmail.com
 */

public class LoginPresenterImpl implements LoginPresenter, LoginInteractor.OnLoginFinishedListener {
    public String TAG = LoginPresenterImpl.class.getSimpleName();
    private final LoginActivity activity;
    private final Endpoints endpoints;
    private final LoginInteractorImpl interactor;
    private final String deviceId;
    private final String deviceInfo;
    private List<CountryDetails> countryDetailsList = new ArrayList<>();

    LoginPresenterImpl(LoginActivity activity, Endpoints endpoints, LoginInteractorImpl interactor,
                       String deviceId, String deviceInfo) {
        this.activity = activity;
        this.endpoints = endpoints;
        this.interactor = interactor;
        this.deviceId = deviceId;
        this.deviceInfo = deviceInfo;
    }

    @Override
    public void getCountryCode() {
        endpoints.getCountry().enqueue(new CallbackWrapper<>(activity, new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
            @Override
            public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                ReqResProto.BaseResponse baseResponse = response.body();
                if (baseResponse == null) {
                    AppUtils.showLog(TAG, "baseResponse is null");
                    activity.onCountryCodeFail(null);
                    return;
                }

                if (baseResponse.getError()) {
                    activity.onCountryCodeFail(baseResponse.getMsg());
                    return;
                }

                AppUtils.showLog(TAG, "COUNTRYLIST count:" + baseResponse.getCountryDetailsList().size());
                mapCountryDetails(baseResponse.getCountryDetailsList());
                activity.onCountryCodeSuccess(countryDetailsList);
            }

            @Override
            public void onFailureResult() {
                activity.onCountryCodeFail(null);
            }
        }));
    }

    private void mapCountryDetails(List<TxProto.CountryDetail> DetailsList) {
        for (TxProto.CountryDetail countryDetail : DetailsList
        ) {
            CountryDetails countryDetails = new CountryDetails();
            countryDetails.setCountryCode(countryDetail.getCountryCode());
            countryDetails.setDialCode(countryDetail.getDialCode());
            countryDetails.setName(countryDetail.getName());

            countryDetailsList.add(countryDetails);
        }

    }

    @Override
    public void validateEmailPhone(String emailPhone) {
        interactor.validateEmailPhone(emailPhone, this);
    }

    @Override
    public void validatePassword(String password) {
        interactor.validatePassword(password, this);
    }

    @Override
    public void onLogin(String emailPhone, String password) {
        interactor.onLogin(emailPhone, password, this);
    }

    @Override
    public void checkUserStatus(AccountProto.User profile, boolean isKeyBackedUp) {
        if (profile.getUserStatus() != AccountProto.UserStatus.VERIFIED) {
            AppUtils.showLog(TAG, "Status Pending");
            onStatusPending();
        } else {
            AppUtils.showLog(TAG, "Status Verified");
            onStatusVerified(profile, isKeyBackedUp);
        }

    }

    @Override
    public void onStatusPending() {
        activity.onVerificationFailure();
    }

    @Override
    public void onStatusVerified(AccountProto.User profile, boolean isKeyBackedUp) {
        if (isKeyBackedUp) {
            AppUtils.showLog(TAG, "Key Backed Up");
            activity.enterPasscode(isKeyBackedUp, profile.getUserId());
        } else {
            AppUtils.showLog(TAG, "Key Not Backed Up");
            String userId = profile.getUserId();
            activity.onVerificationSuccess(userId);
        }
    }


    @Override
    public void emptyEmailPhone() {
        activity.emptyEmailPhone();
    }

    @Override
    public void validEmailPhone() {
        activity.validEmailPhone();
    }

    @Override
    public void invalidEmailPhone() {
        activity.invalidEmailPhone();
    }

    @Override
    public void emptyPassword() {
        activity.emptyPassword();
    }

    @Override
    public void validPassword() {
        activity.validPassword();
    }

    @Override
    public void onValidationSuccess(String emailPhone, String password) {
        activity.showLoading();
        final AccountProto.LoginRequest loginRequest = AccountProto.LoginRequest.newBuilder()
                .setEmailPhone(emailPhone)
                .setPassword(password)
                .setDeviceId(deviceId)
                .setDeviceInfo(deviceInfo)
                .setPushToken(QuiqpayApp.getFirebaseToken())
                .setDeviceType(AccountProto.DeviceType.ANDROID)
                .setThirdPartyLogin(AccountProto.ThirdPartyLogin.UNKNOWN_TPLOGIN)
                .build();

        endpoints.login(loginRequest).enqueue(new CallbackWrapper<>(activity,
                new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
                    @Override
                    public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                        activity.hideLoading();
                        ReqResProto.BaseResponse baseResponse = response.body();

                        if (baseResponse == null) {
                            activity.loginFailure(null);
                            return;
                        }

                        System.out.println("base response: " + baseResponse.toString());

                        if (baseResponse.getError()) {
                            activity.loginFailure(baseResponse.getMsg());
                            return;
                        }
                        activity.loginSuccess(baseResponse.getLoginResponse());
                    }

                    @Override
                    public void onFailureResult() {
                        activity.hideLoading();
                        activity.loginFailure(null);
                    }
                }));
    }
}
