package com.yanky.savxbusiness.activities.keys;

import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.yanky.savxbusiness.activities.account.LoginActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.crypto.Crypto;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.entities.WalletProto;
import com.yanky.savxbusiness.realm.RealmDatabase;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.CallbackWrapper;
import com.yanky.savxbusiness.utils.Constants;

import io.realm.Realm;
import retrofit2.Response;

public class EnterPasscodePresenterImpl implements EnterPasscodePresenter {
    private String TAG = EnterPasscodeActivity.class.getSimpleName();
    private final EnterPasscodeActivity activity;
    private final Endpoints endpoints;
    public boolean firstLogin;


    EnterPasscodePresenterImpl(EnterPasscodeActivity activity, Endpoints endpoints) {
        this.activity = activity;
        this.endpoints = endpoints;
    }


    @Override
    public void validatePasscodeOne(boolean iskeyBackedUp, String passcode, Crypto crypto, String userId, String token
            , SharedPreferences preferences) {
        AppUtils.showLog(TAG, "validatePasscodeOne()");
        if (!iskeyBackedUp) {
            AppUtils.showLog(TAG, "key not backed up");
            backupKeys(passcode, crypto, userId, token, preferences);
        } else {
            AppUtils.showLog(TAG, "key backed up");
            String refId = crypto.generateRefId(passcode, userId);
            getBackupKey(refId, token, crypto, userId, passcode, preferences);
        }
    }

    @Override
    public void validatePasscodeTwo(String passcode, SharedPreferences preferences) {
        firstLogin = preferences.getBoolean("first_login", true);
        if (firstLogin) {
            AppUtils.showLog(TAG, "First Login");
            SharedPreferences.Editor editor = preferences.edit();
            if (passcode != null) {
                editor.putString(Constants.PASSCODE, passcode);
                editor.putBoolean("first_login", false);
                editor.apply();
                activity.validPassCodeTwo();
            }

        } else {
            activity.hideKeyboard();
            String passcodeFromPref = preferences.getString(Constants.PASSCODE, null);
            AppUtils.showLog(TAG, "savedPasscode:" + passcode + "passcodePref:" + passcodeFromPref);
            if (passcode.equals(passcodeFromPref)) {
                AppUtils.showLog(TAG, "Passcode Match");
                activity.validPassCodeTwo();
            } else {
                AppUtils.showLog(TAG, "Passcode not match");

                activity.invalidPassCodeTwo();
            }
        }
    }

    @Override
    public void checkIfFirstLogin(SharedPreferences preferences) {
        AppUtils.showLog(TAG, "first login check");
        if (preferences.getBoolean("first_login", true)) {
            activity.changeTitle();
        }
    }


    @Override
    public void backupKeys(final String passcode, final Crypto crypto, final String userId, String token,
                           SharedPreferences preferences) {
        activity.showLoading();
        AppUtils.showLog(TAG, "backupKeys()");
        final WalletProto.KeysBackup keysBackup = crypto.keyBackUp(userId, passcode);
        final String refId = crypto.generateRefId(passcode, userId);
        final String publicKey = crypto.getPubKey(userId);

        AppUtils.showLog(TAG, "Token: " + token);
        AppUtils.showLog(TAG, "KeysBackup" + keysBackup);
        AppUtils.showLog(TAG, "RefId: " + refId);
        AppUtils.showLog(TAG, "Public Key: " + publicKey);


        endpoints.backupKey(token, refId, publicKey, keysBackup).enqueue(new CallbackWrapper<>(activity,
                new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
                    @Override
                    public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                        activity.hideLoading();
                        ReqResProto.BaseResponse baseResponse = response.body();
                        if (baseResponse == null) {
                            activity.showMessage("key backup failed");
                            return;
                        }

                        AppUtils.showLog(TAG, " backup base response: " + baseResponse.toString());

                        if (baseResponse.getError()) {
                            if (baseResponse.getMsg().equals("Authorization failed.")) {
                                clearSharedPrefsAndRealm(preferences);
                                Intent intent = new Intent(activity, LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                activity.startActivity(intent);
                                Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                            } else activity.showMessage(baseResponse.getMsg());
                            return;
                        }

                        activity.keyBackupSuccess();

//                        boolean recoverKeys = crypto.recoverKeys(userId, passcode, keysBackup);
                        activity.validPassCodeOne(passcode);
                    }

                    @Override
                    public void onFailureResult() {
                        activity.hideLoading();
                        activity.keyBackupFail(null);
                    }
                }));

    }

    @Override
    public void createNewWallet(String token, String walletAddress, String userId, SharedPreferences preferences) {
        AppUtils.showLog(TAG, "createNewWallet()");

        String currency = preferences.getString(Constants.SELECTED_CURRENCY, null);
        String countryCode = preferences.getString(Constants.SELECTED_COUNTRY_CODE, null);

        AppUtils.showLog(TAG, "currency:" + currency);
        AppUtils.showLog(TAG, "countryCode:" + countryCode);

        final WalletProto.WalletAddress walletAddressPb = WalletProto.WalletAddress.newBuilder()
                .setWalletAddress(walletAddress)
                .setIsDefault(true)
                .setCurrency(currency)
                .setCountryCode(countryCode)
                .setWalletName("ZIP Point")
                .setUserId(userId)
                .setTimestamp(System.currentTimeMillis())
                .build();


        endpoints.createNewWallet(token, walletAddressPb).enqueue(new CallbackWrapper<>(activity,
                new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
                    @Override
                    public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                        ReqResProto.BaseResponse baseResponse = response.body();

                        if (baseResponse == null) {
                            activity.showMessage("Base response null");
                            return;
                        }


                        if (baseResponse.getError()) {
                            if (baseResponse.getMsg().equals("Authorization failed.")) {
                                clearSharedPrefsAndRealm(preferences);
                                Intent intent = new Intent(activity, LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                activity.startActivity(intent);
                                Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                            } else activity.showMessage(baseResponse.getMsg());
                            return;
                        }

                        AppUtils.showLog(TAG, "base response: " + baseResponse.toString());

                        activity.createWalletSuccess();


                    }

                    @Override
                    public void onFailureResult() {
                        activity.showMessage("Wallet creation failed");
                    }
                }));
    }

    @Override
    public void getBackupKey(String refId, String token, final Crypto crypto, final String userId,
                             final String passcode, SharedPreferences preferences) {
        activity.showLoading();
        AppUtils.showLog(TAG, "getBackupKey()");
        endpoints.getBackupKey(token, refId).enqueue(new CallbackWrapper<>(activity,
                new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
                    @Override
                    public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                        activity.hideLoading();
                        ReqResProto.BaseResponse baseResponse = response.body();
                        WalletProto.KeysBackup keysBackup = baseResponse.getKeysBackUp();
                        AppUtils.showLog(TAG, "base response: " + baseResponse.toString());

                        if (baseResponse == null) {
                            activity.showMessage("Base response null");
                            return;
                        }


                        if (baseResponse.getError()) {
                            if (baseResponse.getMsg().equals("Authorization failed.")) {
                                clearSharedPrefsAndRealm(preferences);
                                Intent intent = new Intent(activity, LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                activity.startActivity(intent);
                                Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                            } else activity.showMessage("Invalid passcode");
                            return;
                        }

                        //get backup keys(required if user logout and new user keys is not backed up) and recover keys
                        WalletProto.KeysBackup recoveryKey = baseResponse.getKeysBackUp();
                        crypto.recoverKeys(userId, passcode, recoveryKey);

                        activity.validPassCodeOne(passcode);
                    }

                    @Override
                    public void onFailureResult() {
                        activity.hideLoading();
                        activity.showMessage(null);
                    }
                }));
    }

    private void clearSharedPrefsAndRealm(SharedPreferences preferences) {
        //clear shared preferences and realm after logout
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();

        final Realm realm = RealmDatabase.getInstance().getRealm();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });
    }
}
