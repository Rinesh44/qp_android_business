package com.yanky.savxbusiness.activities.waitinglist;

import android.content.SharedPreferences;

public interface EnterCredentialPresenter {

    void verifyCredential(String token, String emailPhone, SharedPreferences preferences);

    void fetchWaitingUser(String token, String emailPhone, SharedPreferences preferences);
}
