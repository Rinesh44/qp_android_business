package com.yanky.savxbusiness.activities.account;

/**
 * Created by Leesa Shakya on 27/09/18.
 * leezshk@gmail.com
 */

public interface RegisterInteractor {
    interface OnRegisterFinishedListener{
        void emptyFullName();
        void validFullName();
        void emptyEmailPhone();
        void validEmailPhone();
        void invalidEmailPhone();
        void emptyPassword();
        void validPassword();
        void invalidPassword();
        void emptyCountry();
        void validCountry();
        void onValidationSuccess(String fullName, String emailPhone,
                                 String password, String country);
    }

    void onRegister(String fullName, String emailPhone, String password,
                    String country, OnRegisterFinishedListener listener);
    void validateFullName(String fullName, OnRegisterFinishedListener listener);
    void validateEmailPhone(String emailPhone, OnRegisterFinishedListener listener);
    void validatePassword(String password, OnRegisterFinishedListener listener);
    void validateCountry(String country, OnRegisterFinishedListener listener);
}
