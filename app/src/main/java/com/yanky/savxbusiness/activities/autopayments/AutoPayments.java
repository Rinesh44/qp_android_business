package com.yanky.savxbusiness.activities.autopayments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yanky.savxbusiness.QuiqpayApp;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.adapters.AutoPaymentAdapter;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.realm.models.Autopayment;
import com.yanky.savxbusiness.repo.AutoPaymentRepo;
import com.yanky.savxbusiness.repo.Repo;
import com.yanky.savxbusiness.utils.AppForegroundListener;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.RecyclerViewClickListener;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmList;

import static com.yanky.savxbusiness.QuiqpayApp.getMyApplication;

public class AutoPayments extends BaseActivity implements AutoPaymentView, AppForegroundListener, RecyclerViewClickListener {
    private static final String TAG = "AutoPayments";
    @Inject
    Endpoints endpoints;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.empty_autopayment_holder)
    LinearLayout mEmptyViewHolder;
    @BindView(R.id.rv_autopayments)
    RecyclerView mRecycler;

    private AutoPaymentPresenterImpl presenter;
    private SharedPreferences preferences;
    private AutoPaymentAdapter mAdapter;
    private List<Autopayment> autopaymentList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auto_payments);
        ButterKnife.bind(this);

        init();

        presenter.getAutoPayments(getToken(), preferences);

        updateRecycler();

    }

    public void updateRecycler() {
        autopaymentList = AutoPaymentRepo.getInstance().getAllAutoPayments();
        AppUtils.showLog(TAG, "walletForMeCount: " + autopaymentList.size());
        mAdapter = new AutoPaymentAdapter(autopaymentList);
        mAdapter.setClickListener(this);
        mRecycler.setLayoutManager(new LinearLayoutManager(this));
        mRecycler.setItemAnimator(new DefaultItemAnimator());
        mRecycler.setAdapter(mAdapter);

        if (autopaymentList.isEmpty()) {
            mRecycler.setVisibility(View.GONE);
            mEmptyViewHolder.setVisibility(View.VISIBLE);
        } else {
            mRecycler.setVisibility(View.VISIBLE);
            mEmptyViewHolder.setVisibility(View.GONE);
        }
    }

    private void init() {

        getMyApplication(this).getAppComponent().inject(this);
        presenter = new AutoPaymentPresenterImpl(endpoints, this);

        setUpToolbar(mToolbar);
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        mToolbarTitle.setText(AutoPayments.class.getSimpleName());
    }

    @Override
    protected void onResume() {
        super.onResume();
        QuiqpayApp.setAppForegroundListener(this);

        presenter.getAutoPayments(getToken(), preferences);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public void autoPaymentSuccess(RealmList<Autopayment> autopaymentRealmList) {
        AutoPaymentRepo.getInstance().saveAutoPayments(autopaymentRealmList, new Repo.Callback() {
            @Override
            public void success(Object o) {
                AppUtils.showLog(TAG, "successfully saved autopayments to db");
                updateRecycler();
            }

            @Override
            public void fail() {
                AppUtils.showLog(TAG, "failed to save autopayments to db");
            }
        });
    }

    @Override
    public void autoPaymentFail(String msg) {
        showMessage(msg);
    }

    @Override
    public void onAppForeground() {
        showPasscodeDialog(this);
    }

    @Override
    public void onClick(View view, int position) {
        AppUtils.showLog(TAG, "item clicked: " + position);
        Autopayment autopayment = autopaymentList.get(position);
        Intent intent = new Intent(this, AutoPaymentDetails.class);
        intent.putExtra("sender_name", autopayment.getSenderName());
        intent.putExtra("date", autopayment.getDate());
        intent.putExtra("amount", autopayment.getAmount());
        intent.putExtra("payment_id", autopayment.getPaymentId());
        intent.putExtra("currency", autopayment.getCurrency());
        intent.putExtra("status", autopayment.getStatus());
        intent.putExtra("for_user", autopayment.getToUsername());
        intent.putExtra("from_wallet_address", autopayment.getFromWalletAddress());
        startActivity(intent);

    }
}
