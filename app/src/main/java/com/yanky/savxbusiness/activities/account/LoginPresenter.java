package com.yanky.savxbusiness.activities.account;

import com.yanky.savxbusiness.entities.AccountProto;

/**
 * Created by Leesa Shakya on 27/09/18.
 * leezshk@gmail.com
 */

public interface LoginPresenter {
    void getCountryCode();

    void validateEmailPhone(String emailPhone);

    void validatePassword(String password);

    void onLogin(String emailPhone, String password);

    void checkUserStatus(AccountProto.User profile, boolean isKeyBackedUp);

    void onStatusPending();

    void onStatusVerified(AccountProto.User profile, boolean isKeyBackedUp);
}
