package com.yanky.savxbusiness.activities.account.password;

/**
 * Created by Leesa Shakya on 03/10/18.
 * leezshk@gmail.com
 */

public interface ResetPasswordView {
    void emptyPassword();
    void validPassword();
    void invalidPassword();
    void resetPasswordFailure(String message);
    void resetPasswordSuccess();
}
