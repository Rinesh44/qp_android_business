package com.yanky.savxbusiness.activities.profile;

import android.content.SharedPreferences;

import java.io.File;

public interface UpdatePersonalPresenter {
    void updateInfo(String token, String fullName, String dob, String gender, SharedPreferences preferences);

    void uploadProPic(String token, File file, String mimeType, SharedPreferences preferences);
}
