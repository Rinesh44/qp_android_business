package com.yanky.savxbusiness.activities.profile;

public interface UpdatePersonalInfoView {

    void updateSuccess();

    void updateFail(String msg);

    void propicUploadSuccess();

    void propicUploadFail(String msg);
}
