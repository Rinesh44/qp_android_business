package com.yanky.savxbusiness.activities.invoice;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.yanky.savxbusiness.QuiqpayApp;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.adapters.InvoiceItemAdapter;
import com.yanky.savxbusiness.realm.models.PaidInvoice;
import com.yanky.savxbusiness.utils.AppForegroundListener;
import com.yanky.savxbusiness.utils.AppUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaidInvoiceDetails extends BaseActivity implements AppForegroundListener {
    private static final String TAG = "PaidInvoiceDetails";
    @BindView(R.id.tv_invoice_heading)
    TextView mInvoiceHeading;
    @BindView(R.id.tv_invoice_no)
    TextView mInvoiceNo;
    @BindView(R.id.tv_invoice_date)
    TextView mInvoiceDate;
    @BindView(R.id.tv_paid_on)
    TextView mPaidOn;
    @BindView(R.id.tv_paid_from)
    TextView mPaidFrom;
    @BindView(R.id.tv_paid_as)
    TextView mPaidAs;
    @BindView(R.id.tv_sub_total)
    TextView mSubTotal;
    @BindView(R.id.tv_discount)
    TextView mDiscount;
    @BindView(R.id.tv_taxable_amount)
    TextView mTaxableAmount;
    @BindView(R.id.tv_vat)
    TextView mVat;
    @BindView(R.id.tv_total)
    TextView mTotal;
    @BindView(R.id.tv_total_big)
    TextView mTotalBig;
    @BindView(R.id.amount_in_words)
    TextView mAmountInWords;
    @BindView(R.id.iv_qr)
    ImageView mQrCode;
    @BindView(R.id.tv_receiver)
    TextView mReceiver;
    @BindView(R.id.tv_receiver_signature)
    TextView mReceiverSig;
    @BindView(R.id.tv_sender)
    TextView mSender;
    @BindView(R.id.tv_sender_signature)
    TextView mSenderSig;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.tv_discount_percent)
    TextView mDiscountPercent;
    @BindView(R.id.tv_vat_percent)
    TextView mVatPercent;
    @BindView(R.id.items_container)
    LinearLayout mItemsContainer;
    @BindView(R.id.items_recycler_view)
    RecyclerView mItemsRecyclerView;


    private InvoiceItemAdapter itemAdapter;
    private double finalAmount = 0;
    private PaidInvoice invoice;
    private String signature;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paid_invoice_details);

        ButterKnife.bind(this);
        init();
        getIntentDataAndMap();
    }

    private void init() {
        setUpToolbar(mToolbar);
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        mToolbarTitle.setText("Invoice Detail");
    }

    private void getIntentDataAndMap() {
        Intent i = getIntent();
        invoice = (PaidInvoice) i.getParcelableExtra("invoice_object");
        signature = (String) i.getStringExtra("signature");

        AppUtils.showLog(TAG, "invoiceItemList:" + invoice.getInvoiceItemList().get(0).getQuantity());

        mInvoiceHeading.setText(invoice.getInvoiceHeading());
        mInvoiceNo.setText(invoice.getInvoiceId());
        mInvoiceDate.setText(getDateSimple(Long.valueOf(invoice.getInvoiceDate())));
        mPaidOn.setText(getDateSimple(Long.valueOf(invoice.getDueDate())));


        if (!invoice.getInvoiceItemList().isEmpty()) {
            itemAdapter = new InvoiceItemAdapter(this, invoice.getInvoiceItemList());
            mItemsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            mItemsRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mItemsRecyclerView.setAdapter(itemAdapter);

        }

        int itemscount = itemAdapter.getItemCount();
        AppUtils.showLog(TAG, "recyclerItemCount" + itemscount);
        postAndNotifyAdapter(new Handler(), mItemsRecyclerView, itemscount);


        mAmountInWords.setText(invoice.getTotalAmountInWords());

        mReceiver.setText(invoice.getReceiverName());
        mSender.setText(invoice.getSenderName());


        String encoded = createJsonObjectForQRCode(invoice.getInvoiceHeading(), invoice.getInvoiceId(),
                invoice.getReceiverUserId(), invoice.getSenderUserId());

        createQRcode(encoded);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.paid_invoice_done, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    protected void postAndNotifyAdapter(final Handler handler, final RecyclerView recyclerView, final int count) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (recyclerView.findViewHolderForLayoutPosition(0) != null) {
                    for (int j = 1; j <= count; j++) {
                        AppUtils.showLog(TAG, "count:" + count);
                        TextView view = recyclerView.findViewHolderForLayoutPosition(0).itemView.findViewById(R.id.tv_amount);
                        double amount = Double.valueOf(view.getText().toString().trim());
                        finalAmount = amount + finalAmount;
                        AppUtils.showLog(TAG, "countAMount" + finalAmount);
                    }

                    mSubTotal.setText(String.valueOf(finalAmount));
                    StringBuilder discountBuilder = new StringBuilder();
                    discountBuilder.append("(");
                    discountBuilder.append(invoice.getDisPercent());
                    discountBuilder.append("%):");
                    mDiscountPercent.setText(discountBuilder);

                    Double discountedAmount = (Double.valueOf(invoice.getDisPercent()) / 100) * (finalAmount);
                    mDiscount.setText(String.valueOf(discountedAmount));
                    mTaxableAmount.setText(invoice.getTaxableAmount());
                    AppUtils.showLog(TAG, "taxable amount:" + invoice.getTaxableAmount());

                    StringBuilder vatBuilder = new StringBuilder();
                    vatBuilder.append("(");
                    vatBuilder.append(invoice.getVatPercent());
                    vatBuilder.append("%):");
                    mVatPercent.setText(vatBuilder);
                    Double vatAddedAmount = (Double.valueOf(invoice.getVatPercent()) / 100) * Double.valueOf(invoice.getTaxableAmount());
                    mVat.setText(String.valueOf(vatAddedAmount));

                    Double total = Double.valueOf(mSubTotal.getText().toString()) - discountedAmount + vatAddedAmount +
                            Double.valueOf(mTaxableAmount.getText().toString());

                    mTotal.setText(String.valueOf(total));
                    StringBuilder totalBigBuilder = new StringBuilder();
                    totalBigBuilder.append("USD ");
                    totalBigBuilder.append(total);
                    mTotalBig.setText(totalBigBuilder);

                    String senderSignature = invoice.getSignature();
                    senderSignature = senderSignature.replaceAll("..", "$0 ");
                    mSenderSig.setText(senderSignature);

                    signature = signature.replaceAll("..", "$0 ");
                    mReceiverSig.setText(signature);


                } else {
                    //
                    postAndNotifyAdapter(handler, recyclerView, count);
                }

            }
        });
    }

    private String createJsonObjectForQRCode(String invoiceHeading, String invoiceId, String receiverUserId,
                                             String senderUserId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", "INVOICE");
            jsonObject.put("invoiceTitle", invoiceHeading);
            jsonObject.put("invoiceId", invoiceId);
            jsonObject.put("toUserId", receiverUserId);
            jsonObject.put("fromUserId", senderUserId);

            AppUtils.showLog(TAG, "jsonObject" + jsonObject.toString());

            byte[] data = jsonObject.toString().getBytes(StandardCharsets.UTF_8);
            String base64 = Base64.encodeToString(data, Base64.DEFAULT);

            AppUtils.showLog(TAG, "encoded:" + base64);
            return base64;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void createQRcode(String input) {
        try {
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.encodeBitmap(input, BarcodeFormat.QR_CODE, 400, 400);
            mQrCode.setImageBitmap(bitmap);
        } catch (Exception e) {
            AppUtils.showLog(TAG, e.toString());
        }
    }

    @Override
    public void onAppForeground() {
        showPasscodeDialog(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        QuiqpayApp.setAppForegroundListener(this);
    }
}
