package com.yanky.savxbusiness.activities.dashboard;

import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.yanky.savxbusiness.activities.account.LoginActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.entities.TxProto;
import com.yanky.savxbusiness.entities.WalletProto;
import com.yanky.savxbusiness.realm.RealmDatabase;
import com.yanky.savxbusiness.realm.models.CountryDetails;
import com.yanky.savxbusiness.realm.models.InvoiceItem;
import com.yanky.savxbusiness.realm.models.PaidInvoice;
import com.yanky.savxbusiness.realm.models.UnpaidInvoice;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.CallbackWrapper;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import retrofit2.Response;

public class DashboardPresenterImpl implements DashboardPresenter {
    private String TAG = DashboardPresenterImpl.class.getSimpleName();
    private final DashboardActivity activity;
    private final Endpoints endpoints;

    private List<UnpaidInvoice> invoiceListUnpaid = new ArrayList<>();
    private List<WalletProto.InvoiceHolder> invoiceListUnpaidProto = new ArrayList<>();
    private List<PaidInvoice> invoiceListPaid = new ArrayList<>();
    private RealmList<InvoiceItem> invoiceItemListPaid = new RealmList<>();
    private List<WalletProto.InvoiceHolder> invoiceListPaidProto = new ArrayList<>();
    private RealmList<InvoiceItem> invoiceItemListUnpaid = new RealmList<>();
    private List<CountryDetails> countryDetailsList = new ArrayList<>();

    public DashboardPresenterImpl(DashboardActivity activity, Endpoints endpoints) {
        this.activity = activity;
        this.endpoints = endpoints;
    }

    @Override
    public void getAllWallets(String token, SharedPreferences preferences) {
        AppUtils.showLog(TAG, "getAllWallets()");
//        activity.showLoading();

        AppUtils.showLog(TAG, "token: " + token);

        endpoints.getWallets(token).enqueue(new CallbackWrapper<>(activity,
                new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {

                    @Override
                    public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
//                        activity.hideLoading();
                        ReqResProto.BaseResponse baseResponse = response.body();

                        AppUtils.showLog(TAG, "wallet count" + baseResponse.getWalletAddressesCount());
                        if (baseResponse == null) {
                            activity.showMessage("get wallet failed");
                            return;
                        }


                        if (baseResponse.getError()) {
                            if (baseResponse.getMsg().equals("Authorization failed.")) {
                                clearSharedPrefsAndRealm(preferences);
                                Intent intent = new Intent(activity, LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                activity.startActivity(intent);
                                Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                            } else activity.showMessage(baseResponse.getMsg());
                            return;
                        }


                        activity.getWalletSuccess(baseResponse);
                    }

                    @Override
                    public void onFailureResult() {
//                        activity.hideLoading();
                        activity.getWalletFail(null);
                    }
                }));
    }

    @Override
    public void getUnpaidInvoice(String token, SharedPreferences preferences) {
        AppUtils.showLog(TAG, "getUnpaidInvoices()");
//        activity.showLoading();

        AppUtils.showLog(TAG, "token: " + token);

        endpoints.getUnpaidInvoice(token).enqueue(new CallbackWrapper<>(activity,
                new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
                    @Override
                    public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
//                        activity.hideLoading();
                        ReqResProto.BaseResponse baseResponse = response.body();

                        AppUtils.showLog(TAG, " invoices: " + baseResponse.getInvoiceEncryption());
                        if (baseResponse == null) {
                            activity.getUnpaidInvoiceFail(null);
                            return;
                        }

                        if (baseResponse.getError()) {
                            if (baseResponse.getMsg().equals("Authorization failed.")) {
                                clearSharedPrefsAndRealm(preferences);
                                Intent intent = new Intent(activity, LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                activity.startActivity(intent);
                                Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                            } else activity.getUnpaidInvoiceFail(baseResponse.getMsg());
                            return;
                        }


                        getUnpaidInvoiceFromResponse(baseResponse);
                        activity.getUnpaidInvoiceSuccess(invoiceListUnpaid, invoiceListUnpaidProto);
                    }

                    @Override
                    public void onFailureResult() {
//                        activity.hideLoading();
                        activity.getUnpaidInvoiceFail(null);
                    }
                }));
    }

    @Override
    public void getPaidInvoice(String token, SharedPreferences preferences) {
        AppUtils.showLog(TAG, "getPaidInvoices()");
//        activity.showLoading();

        AppUtils.showLog(TAG, "token: " + token);

        endpoints.getPaidInvoice(token).enqueue(new CallbackWrapper<>(activity,
                new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
                    @Override
                    public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
//                        activity.hideLoading();
                        ReqResProto.BaseResponse baseResponse = response.body();

                        AppUtils.showLog(TAG, "base response: " + baseResponse.toString());
                        AppUtils.showLog(TAG, " paid invoices: " + baseResponse.getInvoiceEncryption());
                        if (baseResponse == null) {
                            activity.getPaidInvoiceFail(null);
                            return;
                        }

                        if (baseResponse.getError()) {

                            if (baseResponse.getMsg().equals("Authorization failed.")) {
                                clearSharedPrefsAndRealm(preferences);
                                Intent intent = new Intent(activity, LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                activity.startActivity(intent);
                                Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                            } else activity.getPaidInvoiceFail(baseResponse.getMsg());
                            return;
                        }

                        getPaidInvoiceFromResponse(baseResponse);
                        activity.getPaidInvoiceSuccess(invoiceListPaid, invoiceListPaidProto);
                    }


                    @Override
                    public void onFailureResult() {
//                        activity.hideLoading();
                        activity.getUnpaidInvoiceFail(null);
                    }
                }));
    }

    public void getUnpaidInvoiceFromResponse(ReqResProto.BaseResponse baseResponse) {
        int count = baseResponse.getInvoiceEncryptionsCount();
        AppUtils.showLog(TAG, "UnpaidinvoiceCount:" + count);
        try {
            for (int i = 0; i < count; i++) {
                ByteString cipher = baseResponse.getInvoiceEncryptions(i).getCipher();

                WalletProto.InvoiceHolder invoiceHolder = WalletProto.InvoiceHolder.parseFrom(cipher);
                WalletProto.Invoice invoice = WalletProto.Invoice.parseFrom(invoiceHolder.getInvoice());
                UnpaidInvoice invoice1 = new UnpaidInvoice();

                AppUtils.showLog(TAG, "invoicePubKey:" + invoiceHolder.getSignaturePubKey());
                invoice1.setAnchorRef(invoice.getAnchorRef());
                invoice1.setForUserId(invoice.getForUserId());
                invoice1.setInvoiceHeading(invoice.getInvoiceHeading());
                invoice1.setInvoiceId(invoice.getInvoiceId());
                invoice1.setNotes(invoice.getNotes());
                invoice1.setReceiverName(invoice.getReceiverName());
                invoice1.setReceiverUserId(invoice.getReceiverUserId());
                invoice1.setReceiverVatNo(invoice.getReceiverVatNo());
                invoice1.setDueDate(String.valueOf(invoice.getDueDate()));
                invoice1.setSenderName(invoice.getSenderName());
                invoice1.setSenderUserId(invoice.getSenderUserId());
                invoice1.setSenderVatNo(invoice.getSenderVatNo());
                invoice1.setTotalAmountInWords(invoice.getTotalAmountInWords());
                invoice1.setTotalAmountToPay(Double.valueOf(invoice.getTotalAmountToPay()));
                invoice1.setTaxableAmount(String.valueOf(invoice.getTotalTaxableAmount()));
                invoice1.setVatPercent(String.valueOf(invoice.getVatPercent()));
                invoice1.setDisPercent(String.valueOf(invoice.getDiscountPercent()));
                invoice1.setShippingCharge(String.valueOf(invoice.getShippingCharge()));
                invoice1.setInvoiceDate(String.valueOf(invoice.getInvoiceDate()));
                invoice1.setSignature(invoiceHolder.getSignature());
                invoice1.setSignaturePubKey(invoiceHolder.getSignaturePubKey());


                int invoiceItemCount = invoice.getItemsCount();
                invoiceItemListUnpaid.clear();
                for (int j = 0; j < invoiceItemCount; j++) {
                    WalletProto.InvoiceItem invoiceItem = invoice.getItems(j);
                    InvoiceItem realmInvoiceItem = new InvoiceItem();
                    realmInvoiceItem.setId(invoice.getInvoiceId());
                    realmInvoiceItem.setQuantity(String.valueOf(invoiceItem.getQty()));
                    realmInvoiceItem.setDescription(String.valueOf(invoiceItem.getItemDescription()));
                    realmInvoiceItem.setUnitPrice(String.valueOf(invoiceItem.getUnitPrice()));

                    invoiceItemListUnpaid.add(realmInvoiceItem);

                }

                invoice1.setInvoiceItemList(invoiceItemListUnpaid);

                invoiceListUnpaid.add(invoice1);
                invoiceListUnpaidProto.add(invoiceHolder);

            }
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
    }

    public void getPaidInvoiceFromResponse(ReqResProto.BaseResponse baseResponse) {
        int count = baseResponse.getInvoiceEncryptionsCount();
        AppUtils.showLog(TAG, "paidInvoiceCount:" + count);

        try {
            for (int i = 0; i < count; i++) {
                ByteString cipher = baseResponse.getInvoiceEncryptions(i).getCipher();

                WalletProto.InvoiceHolder invoiceHolder = WalletProto.InvoiceHolder.parseFrom(cipher);

                WalletProto.Invoice invoice = WalletProto.Invoice.parseFrom(invoiceHolder.getInvoice());

                PaidInvoice invoice1 = new PaidInvoice();
                invoice1.setAnchorRef(invoice.getAnchorRef());
                invoice1.setForUserId(invoice.getForUserId());
                invoice1.setInvoiceHeading(invoice.getInvoiceHeading());
                invoice1.setInvoiceId(invoice.getInvoiceId());
                invoice1.setNotes(invoice.getNotes());
                invoice1.setReceiverName(invoice.getReceiverName());
                invoice1.setReceiverUserId(invoice.getReceiverUserId());
                invoice1.setReceiverVatNo(invoice.getReceiverVatNo());
                invoice1.setDueDate(String.valueOf(invoice.getDueDate()));
                invoice1.setSenderName(invoice.getSenderName());
                invoice1.setSenderUserId(invoice.getSenderUserId());
                invoice1.setSenderVatNo(invoice.getSenderVatNo());
                invoice1.setTotalAmountInWords(invoice.getTotalAmountInWords());
                invoice1.setTotalAmountToPay(String.valueOf(invoice.getTotalAmountToPay()));
                invoice1.setTaxableAmount(String.valueOf(invoice.getTotalTaxableAmount()));
                invoice1.setVatPercent(String.valueOf(invoice.getVatPercent()));
                invoice1.setDisPercent(String.valueOf(invoice.getDiscountPercent()));
                invoice1.setInvoiceDate(String.valueOf(invoice.getInvoiceDate()));
                invoice1.setSignature(invoiceHolder.getSignature());

                int invoiceItemCount = invoice.getItemsCount();
                invoiceItemListPaid.clear();
                for (int j = 0; j < invoiceItemCount; j++) {
                    WalletProto.InvoiceItem invoiceItem = invoice.getItems(j);
                    InvoiceItem realmInvoiceItem = new InvoiceItem();
                    realmInvoiceItem.setId(invoice.getInvoiceId());
                    realmInvoiceItem.setQuantity(String.valueOf(invoiceItem.getQty()));
                    realmInvoiceItem.setDescription(String.valueOf(invoiceItem.getItemDescription()));
                    realmInvoiceItem.setUnitPrice(String.valueOf(invoiceItem.getUnitPrice()));

                    invoiceItemListPaid.add(realmInvoiceItem);

                }

                invoice1.setInvoiceItemList(invoiceItemListPaid);

                invoiceListPaid.add(invoice1);
                invoiceListPaidProto.add(invoiceHolder);
            }
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
            AppUtils.showLog(TAG, e.toString());
        }
    }


    @Override
    public void getCountryCode() {
        endpoints.getCountry().enqueue(new CallbackWrapper<>(activity, new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
            @Override
            public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                ReqResProto.BaseResponse baseResponse = response.body();
                if (baseResponse == null) {
                    AppUtils.showLog(TAG, "baseResponse is null");
                    activity.onCountryCodeFail(null);
                    return;
                }

                if (baseResponse.getError()) {
                    activity.onCountryCodeFail(baseResponse.getMsg());
                    return;
                }

                AppUtils.showLog(TAG, "COUNTRYLIST count:" + baseResponse.getCountryDetailsList().size());
                mapCountryDetails(baseResponse.getCountryDetailsList());
                activity.onCountryCodeSuccess(countryDetailsList);
            }

            @Override
            public void onFailureResult() {
                activity.onCountryCodeFail(null);
            }
        }));
    }

    private void mapCountryDetails(List<TxProto.CountryDetail> DetailsList) {
        for (TxProto.CountryDetail countryDetail : DetailsList
        ) {
            CountryDetails countryDetails = new CountryDetails();
            countryDetails.setCountryCode(countryDetail.getCountryCode());
            countryDetails.setDialCode(countryDetail.getDialCode());
            countryDetails.setName(countryDetail.getName());

            countryDetailsList.add(countryDetails);
        }

    }

    private void clearSharedPrefsAndRealm(SharedPreferences prefs) {
        //clear shared preferences and realm after logout
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();

        final Realm realm = RealmDatabase.getInstance().getRealm();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });
    }


}
