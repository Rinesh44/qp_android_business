package com.yanky.savxbusiness.activities.autopayments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.repo.WalletsRepo;
import com.yanky.savxbusiness.utils.AppUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.yanky.savxbusiness.QuiqpayApp.getMyApplication;

public class AutoPaymentDetails extends BaseActivity implements AutoPaymentDetailsView {
    private static final String TAG = "AutoPaymentDetails";
    @Inject
    Endpoints endpoints;
    @BindView(R.id.tv_sender_name)
    TextView mSenderName;
    @BindView(R.id.tv_date)
    TextView mDate;
    @BindView(R.id.tv_max_amount)
    TextView mAmount;
    @BindView(R.id.tv_payment_id)
    TextView mPaymentId;
    @BindView(R.id.tv_created_for)
    TextView mCreatedFor;
    @BindView(R.id.tv_paying_from)
    TextView mPayingFrom;
    @BindView(R.id.btn_delete)
    Button mDelete;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.tv_status)
    TextView mStatus;

    private String paymentId;
    private SharedPreferences preferences;
    private AutoPaymentDetailsPresenterImpl presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auto_payment_details);

        ButterKnife.bind(this);

        getMyApplication(this).getAppComponent().inject(this);
        init();
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        presenter = new AutoPaymentDetailsPresenterImpl(endpoints, this);
        getIntentData();


        mDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.deleteAutoPayment(getToken(), paymentId, preferences);
            }
        });
    }

    private void init() {
        setUpToolbar(mToolbar);
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        mToolbarTitle.setText("Auto payment details");
    }

    private void getIntentData() {
        Intent intent = getIntent();
        String senderName = intent.getStringExtra("sender_name");
        Long date = intent.getLongExtra("date", 0);
        String amount = intent.getStringExtra("amount");
        paymentId = intent.getStringExtra("payment_id");
        String forUser = intent.getStringExtra("for_user");
        String fromWalletAddress = intent.getStringExtra("from_wallet_address");
        String status = intent.getStringExtra("status");
        String currency = intent.getStringExtra("currency");

        mSenderName.setText(senderName.trim());
        mDate.setText(String.valueOf(getDate(date)));
        mAmount.setText(currency + " " + amount.trim());
        mPaymentId.setText(paymentId.substring(0, 5));
        mCreatedFor.setText(forUser.trim());

        String walletName = WalletsRepo.getInstance().getWalletNameForAddress(fromWalletAddress.trim());
        if (walletName != null) mPayingFrom.setText(walletName);

        if (status != null) {
            mStatus.setText(status);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void deleteAutopaymentSuccess() {
        AppUtils.showLog(TAG, "delete autopayment success");
        finish();
    }

    @Override
    public void deleteAutopaymentFail(String msg) {
        showMessage(msg);
    }
}
