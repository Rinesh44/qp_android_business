package com.yanky.savxbusiness.activities.waitinglist;

public interface RegisterWaitingUserView {
    void registerWaitingUserSuccess(int queuePosition, int totalWaitingUsers, String username);

    void registerWaitingUserFail(String msg);
}
