package com.yanky.savxbusiness.activities.invoice;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.adapters.CustomSpinnerAdapter;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.realm.models.UnpaidInvoice;
import com.yanky.savxbusiness.realm.models.Wallet;
import com.yanky.savxbusiness.repo.WalletsRepo;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.DatePicker;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.yanky.savxbusiness.QuiqpayApp.getMyApplication;

public class AutoPaymentDialog extends BaseActivity implements AutoPaymentDialogView {
    private static final String TAG = "AutoPaymentDialog";
    @Inject
    Endpoints endpoints;
    @BindView(R.id.btn_save)
    Button mSave;
    @BindView(R.id.et_till_date)
    EditText mTillDate;
    @BindView(R.id.et_max_amount)
    EditText mMaxAmount;
    @BindView(R.id.btn_cancel)
    Button mCancel;
    @BindView(R.id.sp_wallets)
    Spinner mWalletSpinner;

    private String autoPayWalletAddress, countryCode, currency;
    private List<Wallet> walletList = new ArrayList<>();
    private AutoPaymentDialogPresenterImpl presenter;
    private UnpaidInvoice invoice;
    private SharedPreferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auto_payment_dialog);

        ButterKnife.bind(this);

        getMyApplication(this).getAppComponent().inject(this);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        getIntentData();

        setUpWalletSpinner();

        presenter = new AutoPaymentDialogPresenterImpl(this, endpoints);

        final DatePicker datePicker = new DatePicker(this, R.id.et_till_date);

        mTillDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePicker.onClick(view);
            }
        });

        mSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mMaxAmount.getText().toString().isEmpty()) {
                    Toast.makeText(AutoPaymentDialog.this, "Please enter max amount", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (mTillDate.getText().toString().isEmpty()) {
                    Toast.makeText(AutoPaymentDialog.this, "Please select till date", Toast.LENGTH_SHORT).show();
                    return;
                }

                try {
                    String str_date = mTillDate.getText().toString();
                    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = (Date) formatter.parse(str_date);
                    AppUtils.showLog(TAG, "date Timestamp: " + date.getTime());

                    String maxAmount = mMaxAmount.getText().toString().trim();
                    presenter.autoPay(getToken(), invoice, maxAmount, getUserID(), autoPayWalletAddress,
                            preferences, currency, countryCode, date.getTime());

                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        });

        mCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("cancelled", "true");
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        });
    }

    private void getIntentData() {

        Intent intent = getIntent();
        invoice = intent.getParcelableExtra("invoice_object");
    }


    private void setUpWalletSpinner() {
        walletList = WalletsRepo.getInstance().getAllWallets();

        CustomSpinnerAdapter walletSpinnerAdapter = new CustomSpinnerAdapter(this, (ArrayList<Wallet>) walletList);
        mWalletSpinner.setAdapter(walletSpinnerAdapter);

        mWalletSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                TextView textView = (TextView) view.findViewById(R.id.third_item);
                autoPayWalletAddress = textView.getText().toString();
                Wallet wallet = (Wallet) adapterView.getItemAtPosition(i);
                countryCode = wallet.getCountryCode();
                currency = wallet.getCurrency();
                AppUtils.showLog(TAG, "country: " + countryCode);
                AppUtils.showLog(TAG, "currency: " + currency);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    /**
     * disable backpress for autopayment
     */
    @Override
    public void onBackPressed() {

    }

    @Override
    public void autoPaySuccess() {
        AppUtils.showLog(TAG, "auto payment success");
        Toast.makeText(this, "Auto pay success", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void autoPayFail(String msg) {
        showMessage(msg);
    }


}
