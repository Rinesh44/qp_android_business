package com.yanky.savxbusiness.activities.account.password;

/**
 * Created by Leesa Shakya on 03/10/18.
 * leezshk@gmail.com
 */

public interface ResetPasswordPresenter {
    void validatePassword(String password);
    void resetPassword(String password, String userId, String resetToken);
}
