package com.yanky.savxbusiness.activities.profile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.hbb20.CountryCodePicker;
import com.yanky.savxbusiness.QuiqpayApp;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.utils.AppForegroundListener;
import com.yanky.savxbusiness.utils.AppUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.yanky.savxbusiness.QuiqpayApp.getMyApplication;

public class AddAddress extends BaseActivity implements AddAddressView, View.OnClickListener, AppForegroundListener {
    private final String TAG = AddAddress.class.getSimpleName();
    @Inject
    Endpoints endpoints;
    @BindView(R.id.sp_address_type)
    Spinner mAddressType;
    @BindView(R.id.et_address_line1)
    EditText mAddressLine1;
    @BindView(R.id.et_address_line2)
    EditText mAddressLine2;
    @BindView(R.id.et_city)
    EditText mCity;
    @BindView(R.id.et_state)
    EditText mState;
    @BindView(R.id.et_zip_code)
    EditText mZipCode;
    @BindView(R.id.country_picker)
    CountryCodePicker mCountryPicker;
    @BindView(R.id.btn_save)
    Button mSave;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.switch_default)
    Switch mDefault;

    private AddAddressPresenterImpl presenter;
    private String addressId, addressType, address1, address2, city, state, zipCode, country;
    private Boolean edit, isDefault, makeDefault;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_profile_info);
        ButterKnife.bind(this);


        //to remove keyboard glich
        getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.background));


        getMyApplication(this).getAppComponent().inject(this);
        init();

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        initializeAddressTypeSpinner();

        getIntentValues();
        checkIfEditAddress();

        mDefault.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                if (checked) presenter.makeDefaultAddress(getToken(), addressId, preferences);
            }
        });

        mSave.setOnClickListener(this);

    }

    private void initializeAddressTypeSpinner() {
        String[] items = new String[]{"Select Type of Address", "Home", "Work", "Billing", "Shipping"};
        ArrayAdapter<String> addressAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,
                items);
        addressAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mAddressType.setAdapter(addressAdapter);
    }

    private void checkIfEditAddress() {
        if (edit) {
            if (mDefault.getVisibility() == View.GONE) mDefault.setVisibility(View.VISIBLE);
            int addressTypeSpinnerId;
            mToolbarTitle.setText("Edit Address");
            AppUtils.showLog(TAG, "addressType:" + addressType);
            switch (addressType) {
                case "HOME":
                    addressTypeSpinnerId = 1;
                    mAddressType.setSelection(addressTypeSpinnerId);
                    break;

                case "WORK":
                    addressTypeSpinnerId = 2;
                    mAddressType.setSelection(addressTypeSpinnerId);
                    break;

                case "BILLING":
                    addressTypeSpinnerId = 3;
                    mAddressType.setSelection(addressTypeSpinnerId);
                    break;
                case "SHIPPING":
                    addressTypeSpinnerId = 4;
                    mAddressType.setSelection(addressTypeSpinnerId);
                    break;
            }

            if (isDefault) {
                mDefault.setChecked(true);
            }
            mAddressLine1.setText(address1);
            mAddressLine2.setText(address2);
            mCity.setText(city);
            mState.setText(state);
            mZipCode.setText(zipCode);
            mCountryPicker.setCountryForNameCode(country);

        } else {
            mToolbarTitle.setText("Add Address");
        }
    }

    private void getIntentValues() {
        Intent getIntent = getIntent();
        makeDefault = getIntent.getBooleanExtra("make_default", false);
        isDefault = getIntent.getBooleanExtra("is_default", false);
        addressId = getIntent.getStringExtra("address_id");
        addressType = getIntent.getStringExtra("address_type");
        address1 = getIntent.getStringExtra("address1");
        address2 = getIntent.getStringExtra("address2");
        city = getIntent.getStringExtra("city");
        state = getIntent.getStringExtra("state");
        zipCode = getIntent.getStringExtra("zip_code");
        country = getIntent.getStringExtra("country");
        edit = getIntent.getBooleanExtra("edit", false);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void init() {
        presenter = new AddAddressPresenterImpl(this, endpoints);

        setUpToolbar(mToolbar);
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }


    @Override
    public void uploadAddressSuccess() {
        AppUtils.showLog(TAG, "upload address success");
        Toast.makeText(this, "Upload Address Success", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void uploadAddressFail(String msg) {
        showMessage(msg);
    }

    @Override
    public void makeDefaultAddressFail(String msg) {
        showMessage(msg);
    }

    @Override
    public void makeDefaultAddressSuccess() {
        Toast.makeText(this, "Set default address", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                onSaveClick();
                break;
        }
    }

    public void onSaveClick() {
        String addressType = mAddressType.getSelectedItem().toString().trim();
        String addressLine1 = mAddressLine1.getText().toString().trim();
        String addressLine2 = mAddressLine2.getText().toString().trim();
        String city = mCity.getText().toString().trim();
        String state = mState.getText().toString().trim();
        String zipCode = mZipCode.getText().toString().trim();
        String country = mCountryPicker.getSelectedCountryNameCode();
        Boolean defaultWallet;
        if (makeDefault) {
            defaultWallet = true;
        } else {
            defaultWallet = mDefault.isChecked();
        }


        if (addressType.equals(getResources().getString(R.string.type_of_address))) {
            showMessage("Please select type of address");
            return;
        }

        if (addressLine1.isEmpty()) {
            showMessage("Please enter address1");
            return;
        }

        if (addressLine2.isEmpty()) {
            showMessage("Please enter address2");
            return;
        }

        if (state.isEmpty()) {
            showMessage("Please enter state");
            return;
        }

        if (zipCode.isEmpty() || zipCode.length() != 5) {
            showMessage("Please enter valid zip code");
            return;
        }

        presenter.uploadAddress(edit, getToken(), getUserID(), addressType, addressLine1, addressLine2, city, state,
                zipCode, country, addressId, defaultWallet, preferences);
    }

    @Override
    public void onAppForeground() {
        showPasscodeDialog(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        QuiqpayApp.setAppForegroundListener(this);
    }
}
