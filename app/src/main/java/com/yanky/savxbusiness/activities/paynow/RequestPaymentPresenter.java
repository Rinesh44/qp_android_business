package com.yanky.savxbusiness.activities.paynow;

import android.content.SharedPreferences;

public interface RequestPaymentPresenter {
    void searchUsers(String token, String charSequence, SharedPreferences preferences);

    void requestPayment(String token, String toUserId, String fromWalletAddress, String amount, String desc,
                        String currency, String countryCode, SharedPreferences preferences);
}
