package com.yanky.savxbusiness.activities.wallet.fund;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.scrounger.countrycurrencypicker.library.Country;
import com.yanky.savxbusiness.QuiqpayApp;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.activities.dashboard.DashboardActivity;
import com.yanky.savxbusiness.activities.search.SearchWallet;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.realm.models.Wallet;
import com.yanky.savxbusiness.utils.AppForegroundListener;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.SendSelectedWallet;

import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.yanky.savxbusiness.QuiqpayApp.getMyApplication;

public class TransferFund extends BaseActivity implements SendSelectedWallet, TransferFundView, AppForegroundListener {
    private static final String TAG = "TransferFund";
    @Inject
    Endpoints endpoints;
    @BindView(R.id.walletname)
    TextView mWalletName;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.amount)
    TextView mWalletAmount;
    @BindView(R.id.et_select_wallet)
    EditText mSelectWallet;
    @BindView(R.id.btn_transfer_fund)
    Button mTransferFund;
    @BindView(R.id.et_desc)
    EditText mDesc;
    @BindView(R.id.et_amount)
    EditText mTransferAmount;

    private String walletName, walletAmount, walletAddress, walletCurrency, walletCountryCode;
//    private boolean fixedWallet;

    TransferFundPresenterImpl presenter;
    Wallet mReceivedWallet;
    private SharedPreferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //prevent keyboard glich when its introduced
        getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.background));

        setContentView(R.layout.activity_transfer_fund);
        ButterKnife.bind(this);

        getMyApplication(this).getAppComponent().inject(this);
        init();


        SearchWallet.setSendSelectedWalletListener(this);
        mSelectWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TransferFund.this, SearchWallet.class);
                intent.putExtra("wallet_address", walletAddress);
                AppUtils.showLog(TAG, "wallet_address: " + walletAddress);
                startActivity(intent);
            }
        });

        mTransferFund.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSelectWallet.getText().toString().isEmpty()) {
                    showMessage("Please select wallet");
                    return;
                }


                if (mTransferAmount.getText().toString().isEmpty()) {
                    showMessage("Please enter amount");
                    return;
                }

                if (mDesc.getText().toString().isEmpty()) {
                    showMessage("Please enter description");
                    return;
                }

                AppUtils.showLog(TAG, "walletAMount" + mWalletAmount.getText().toString().substring(4));

                if (mTransferAmount.getText().toString().equals("0") || Integer.valueOf(mTransferAmount.getText().toString()) >
                        Double.valueOf(mWalletAmount.getText().toString().substring(4))) {
                    showMessage("Insufficient wallet amount");
                    return;
                }

           /*     if (fixedWallet) {
                    presenter.transferBalance(getToken(), walletName, walletAmount,
                            mDesc.getText().toString(), mReceivedWallet, walletName, walletAddress, getUserID(), walletCurrency,
                            preferences);
                    return;
                }*/


                showConfimationDialog();
            }
        });


        getIntentValues();

       /* if (fixedWallet) {
            mSelectWallet.setVisibility(View.GONE);
            mTransferAmount.setVisibility(View.GONE);
        }*/


        mToolbarTitle.setText("Transfer to Wallet");
        mWalletName.setText(walletName);

        Country countryWithCurrency = Country.getCountryWithCurrency(walletCountryCode, this);
        assert countryWithCurrency != null;
        String currencySymbol = Objects.requireNonNull(countryWithCurrency.getCurrency()).getSymbol();
        if (currencySymbol.length() > 1) {
            currencySymbol = currencySymbol.substring(currencySymbol.length() - 1);
        }

        StringBuilder amountBuilder = new StringBuilder();
        amountBuilder.append(walletCurrency);
        amountBuilder.append(" ");
        amountBuilder.append(currencySymbol);
        amountBuilder.append(" ");
        amountBuilder.append(String.format("%.0f", Double.valueOf(walletAmount)));


        mWalletAmount.setText(String.valueOf(amountBuilder));

  /*      int flagId = Country.getCountry(walletCountryCode, this).getFlagId();
        if (flagId != 0) {
            Drawable drawable = getResources().getDrawable(flagId);
            mWalletAmount.setCompoundDrawablePadding(20);
            mWalletAmount.setCompoundDrawablesWithIntrinsicBounds(resize(drawable), null, null, null);
        }
*/

    }

    private void init() {
        setUpToolbar(mToolbar);
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        presenter = new TransferFundPresenterImpl(this, endpoints);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
    }


    public void getIntentValues() {
        Intent i = getIntent();
        walletName = i.getStringExtra("wallet_name");
        walletAmount = i.getStringExtra("wallet_amount");
        walletAddress = i.getStringExtra("wallet_address");
        walletCountryCode = i.getStringExtra("wallet_country_code");
        walletCurrency = i.getStringExtra("wallet_currency");
//        fixedWallet = i.getBooleanExtra("fixed_wallet", false);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public void getWallet(Wallet wallet) {
        mReceivedWallet = wallet;
        AppUtils.showLog(TAG, "walletName; " + wallet.getWalletName());
        mSelectWallet.setText(wallet.getWalletName());
    }

    @Override
    public void transferSuccess() {
        hideLoading();
        AppUtils.showLog(TAG, "transfer successful");
        Toast.makeText(this, "Fund transfered successfully", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(this, DashboardActivity.class));
        finish();
    }

    @Override
    public void transferFail(String msg) {
        showMessage(msg);
    }

    @Override
    public void onAppForeground() {
        showPasscodeDialog(this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        QuiqpayApp.setAppForegroundListener(this);
    }

    private void showConfimationDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);

        //inflate custom layout in dialog
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.confirm_transfer_fund_self, null);
        dialogBuilder.setView(dialogView);

        TextView fromWallet = dialogView.findViewById(R.id.tv_from_wallet);
        TextView toWallet = dialogView.findViewById(R.id.tv_to_wallet);
        TextView amount = dialogView.findViewById(R.id.tv_amount);
        TextView description = dialogView.findViewById(R.id.tv_desc);

        fromWallet.setText(walletName);
        toWallet.setText(mSelectWallet.getText().toString().trim());
        amount.setText(mTransferAmount.getText().toString().trim());
        description.setText(mDesc.getText().toString().trim());

        Button cancel = dialogView.findViewById(R.id.btn_cancel);
        Button confirm = dialogView.findViewById(R.id.btn_confirm);

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                presenter.transferBalance(getToken(), mSelectWallet.getText().toString(), mTransferAmount.getText().toString(),
                        mDesc.getText().toString(), mReceivedWallet, walletName, walletAddress, getUserID(), walletCurrency,
                        preferences);

                alertDialog.dismiss();

            }
        });


    }

}
