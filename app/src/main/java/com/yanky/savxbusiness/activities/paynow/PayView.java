package com.yanky.savxbusiness.activities.paynow;

import com.yanky.savxbusiness.entities.AccountProto;
import com.yanky.savxbusiness.realm.models.User;

import java.util.ArrayList;

public interface PayView {
    void searchUserSuccess(ArrayList<User> users);

    void searchUserFail(String msg);

    void searchUserByBarcodeSuccess(AccountProto.User user);

    void searchUserByBarcodeFail(String msg);

    void payFail(String msg);

    void paySuccess();
}
