package com.yanky.savxbusiness.activities.profile;

public interface AddAddressView {
    void uploadAddressSuccess();
    void uploadAddressFail(String msg);
    void makeDefaultAddressFail(String msg);
    void makeDefaultAddressSuccess();
}
