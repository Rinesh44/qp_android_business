package com.yanky.savxbusiness.activities.account;

/**
 * Created by Leesa Shakya on 27/09/18.
 * leezshk@gmail.com
 */

public interface LoginInteractor {
    interface OnLoginFinishedListener{
        void emptyEmailPhone();
        void validEmailPhone();
        void invalidEmailPhone();
        void emptyPassword();
        void validPassword();
        void onValidationSuccess(String emailPhone, String password);
    }

    void onLogin(String emailPhone, String password, OnLoginFinishedListener listener);
    void validateEmailPhone(String emailPhone, OnLoginFinishedListener listener);
    void validatePassword(String password, OnLoginFinishedListener listener);
}
