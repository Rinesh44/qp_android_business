package com.yanky.savxbusiness.activities.waitinglist;

public interface RegisterUserPresenter {
    void fetchUser(String token, String emailPhone);
}
