package com.yanky.savxbusiness.activities.paynow;

import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.yanky.savxbusiness.activities.account.LoginActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.entities.WalletProto;
import com.yanky.savxbusiness.realm.RealmDatabase;
import com.yanky.savxbusiness.realm.models.User;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.CallbackWrapper;

import java.util.ArrayList;

import io.realm.Realm;
import retrofit2.Response;

public class RequestPaymentPresenterImpl implements RequestPaymentPresenter {
    private static final String TAG = "RequestPaymentPresenter";
    private final RequestPayment activity;
    private final Endpoints endpoints;

    public RequestPaymentPresenterImpl(RequestPayment activity, Endpoints endpoints) {
        this.activity = activity;
        this.endpoints = endpoints;
    }

    @Override
    public void searchUsers(String token, String charSequence, SharedPreferences preferences) {
        endpoints.searchUsers(token, charSequence).enqueue(new CallbackWrapper<>(activity,
                new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
                    @Override
                    public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                        ReqResProto.BaseResponse baseResponse = response.body();
                        AppUtils.showLog(TAG, "base response: " + baseResponse.toString());

                        if (baseResponse == null) {
                            activity.searchUserFail(null);
                            return;
                        }


                        if (baseResponse.getError()) {
                            if (baseResponse.getMsg().equals("Authorization failed.")) {
                                clearSharedPrefsAndRealm(preferences);
                                Intent intent = new Intent(activity, LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                activity.startActivity(intent);
                                Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                            }
                            activity.searchUserFail(baseResponse.getMsg());
                            return;
                        }


                        int count = baseResponse.getUsersCount();
                        ArrayList<User> userList = new ArrayList<>();
                        for (int i = 0; i < count; i++) {
                            User user = new User();
                            user.setFullName(baseResponse.getUsers(i).getFullName());
                            user.setEmailPhone(baseResponse.getUsers(i).getEmailPhone());
                            user.setUserId(baseResponse.getUsers(i).getUserId());
                            userList.add(user);
                        }
                        activity.searchUserSuccess(userList);

                    }

                    @Override
                    public void onFailureResult() {
                        activity.searchUserFail(null);
                    }
                }));
    }

    @Override
    public void requestPayment(String token, String toUserId, String fromWalletAddress,
                               String amount, String desc, String currency, String countryCode,
                               SharedPreferences preferences) {
        activity.showLoading();
        AppUtils.showLog(TAG, "countryCode:" + countryCode);
        WalletProto.RequestFund requestFund = WalletProto.RequestFund.newBuilder()
                .setAmount(Long.valueOf(amount) * 100)
                .setDescription(desc)
                .setCurrency(currency)
                .setCountryCode(countryCode)
                .setToUserId(toUserId)
                .setFromWalletAddress(fromWalletAddress)
                .build();
        endpoints.requestFund(token, requestFund).enqueue(new CallbackWrapper<>(activity, new
                CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
                    @Override
                    public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                        activity.hideLoading();
                        ReqResProto.BaseResponse baseResponse = response.body();

                        if (baseResponse == null) {
                            activity.requestPaymentFail(null);
                            return;
                        }

                        if (baseResponse.getError()) {
                            if (baseResponse.getMsg().equals("Authorization failed.")) {
                                clearSharedPrefsAndRealm(preferences);
                                Intent intent = new Intent(activity, LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                activity.startActivity(intent);
                                Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                            } else activity.requestPaymentFail(baseResponse.getMsg());
                            return;
                        }

                        activity.requestPaymentSuccess();
                    }

                    @Override
                    public void onFailureResult() {
                        activity.hideLoading();
                        activity.requestPaymentFail(null);
                    }
                }));
    }

    private void clearSharedPrefsAndRealm(SharedPreferences prefs) {
        //clear shared preferences and realm after logout
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();

        final Realm realm = RealmDatabase.getInstance().getRealm();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });
    }
}
