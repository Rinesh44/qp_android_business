package com.yanky.savxbusiness.activities.paynow;

import com.yanky.savxbusiness.realm.models.User;

import java.util.ArrayList;

public interface RequestView {
    void searchUserSuccess(ArrayList<User> users);

    void searchUserFail(String msg);

    void requestPaymentSuccess();

    void requestPaymentFail(String msg);
}
