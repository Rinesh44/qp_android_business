package com.yanky.savxbusiness.activities.account.password;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Leesa Shakya on 03/10/18.
 * leezshk@gmail.com
 */

public class ResetTokenActivity extends BaseActivity {
    @BindView(R.id.pin_entry_edit_text)
    PinEntryEditText pinEntryEditText;
    private String userId, resetToken;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_token);
        ButterKnife.bind(this);

        getIntentValues();
        pinEntryEditText.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
            @Override
            public void onPinEntered(CharSequence charSequence) {
                if (charSequence.length() == 6) {
                    resetToken = charSequence.toString();
                    hideKeyboard();
                    onVerify(resetToken);
                    return;
                }

                pinEntryEditText.setError(true);
                showMessage(getString(R.string.invalid_code));
            }
        });
    }

    private void getIntentValues() {
        Intent intent = getIntent();
        if (intent != null)
            userId = intent.getStringExtra("USER_ID");
    }

    private void onVerify(String resetToken) {
        Intent intent = new Intent(this, ResetPasswordActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("USER_ID", userId);
        intent.putExtra("RESET_TOKEN", resetToken);
        startActivity(intent);

        finish();
    }
}
