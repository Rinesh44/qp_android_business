package com.yanky.savxbusiness.activities.account.password;

public interface ChangePasswordView {
    void invalidPassword(String msg);
    void onChangePasswordSuccess();
    void onChangePasswordFail(String msg);
}
