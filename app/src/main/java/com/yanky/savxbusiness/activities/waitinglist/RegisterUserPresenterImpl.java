package com.yanky.savxbusiness.activities.waitinglist;

import android.content.Intent;
import android.widget.Toast;

import com.yanky.savxbusiness.activities.account.LoginActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.CallbackWrapper;

import retrofit2.Response;

public class RegisterUserPresenterImpl implements RegisterUserPresenter{
    private static final String TAG = "RegisterUserPresenterIm";

    private RegisterSuccess activity;
    private Endpoints endpoints;

    public RegisterUserPresenterImpl(RegisterSuccess activity, Endpoints endpoints) {
        this.activity = activity;
        this.endpoints = endpoints;
    }

    @Override
    public void fetchUser(String token, String emailPhone) {
        endpoints.getWaitingUser(token, emailPhone).enqueue(new CallbackWrapper<>(activity, new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
            @Override
            public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                ReqResProto.BaseResponse baseResponse = response.body();
                AppUtils.showLog(TAG, "baseResponse: " + baseResponse);

                if (baseResponse == null) {
                    AppUtils.showLog(TAG, "baseResponse is null");
                    activity.fetchUserFail(null);
                    return;
                }

                if (baseResponse.getError()) {
                    if (baseResponse.getMsg().equals("Authorization failed.")) {
                        Intent intent = new Intent(activity, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        activity.startActivity(intent);
                        Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                    } else activity.fetchUserFail(baseResponse.getMsg());
                } else activity.fetchUserSuccess(baseResponse);
            }

            @Override
            public void onFailureResult() {
                activity.showMessage(null);
                AppUtils.showLog(TAG, "fetch waiting user failed");
            }
        }));
    }
}
