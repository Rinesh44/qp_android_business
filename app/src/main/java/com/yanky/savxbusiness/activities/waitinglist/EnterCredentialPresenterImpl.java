package com.yanky.savxbusiness.activities.waitinglist;

import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.yanky.savxbusiness.QuiqpayApp;
import com.yanky.savxbusiness.activities.account.LoginActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.entities.AccountProto;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.realm.RealmDatabase;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.CallbackWrapper;

import io.realm.Realm;
import retrofit2.Response;

public class EnterCredentialPresenterImpl implements EnterCredentialPresenter {
    private static final String TAG = "EnterCredentialPresente";
    private Endpoints endpoints;
    private EnterCredential activity;


    public EnterCredentialPresenterImpl(Endpoints endpoints, EnterCredential activity) {
        this.endpoints = endpoints;
        this.activity = activity;
    }

    @Override
    public void verifyCredential(String token, String emailPhone, SharedPreferences preferences) {
        activity.showLoading();
        AppUtils.showLog(TAG, "token: " + token);
        AppUtils.showLog(TAG, "emailPhone : " + emailPhone);
        String firebaseToken = QuiqpayApp.getFirebaseToken();
        AppUtils.showLog(TAG, "firebaseToken:  " + firebaseToken);
        endpoints.verifyCredential(token, emailPhone, firebaseToken, AccountProto.DeviceType.ANDROID.getNumber()).enqueue(new CallbackWrapper<>(activity, new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
            @Override
            public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                activity.hideLoading();
                ReqResProto.BaseResponse baseResponse = response.body();
                AppUtils.showLog(TAG, "baseResponse: " + baseResponse);

                if (baseResponse == null) {
                    AppUtils.showLog(TAG, "baseResponse is null");
                    activity.credentialFail(null);
                    return;
                }

                if (baseResponse.getError()) {
                    if (baseResponse.getMsg().equals("Authorization failed.")) {
                        clearSharedPrefsAndRealm(preferences);
                        Intent intent = new Intent(activity, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        activity.startActivity(intent);
                        Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                    } else activity.credentialFail(baseResponse);
                } else activity.credentialSuccess();

            }

            @Override
            public void onFailureResult() {
                activity.hideLoading();

                activity.credentialFail(null);
            }
        }));
    }

    @Override
    public void fetchWaitingUser(String token, String emailPhone, SharedPreferences preferences) {
        AppUtils.showLog(TAG, "details: " + emailPhone + token);
        endpoints.getWaitingUser(token, emailPhone).enqueue(new CallbackWrapper<>(activity, new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
            @Override
            public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                ReqResProto.BaseResponse baseResponse = response.body();
                AppUtils.showLog(TAG, "baseResponse: " + baseResponse);

                if (baseResponse == null) {
                    AppUtils.showLog(TAG, "baseResponse is null");
                    activity.fetchWaitingUserFail(null);
                    return;
                }

                if (baseResponse.getError()) {
                    if (baseResponse.getMsg().equals("Authorization failed.")) {
                        clearSharedPrefsAndRealm(preferences);
                        Intent intent = new Intent(activity, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        activity.startActivity(intent);
                        Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                    } else activity.fetchWaitingUserFail(baseResponse.getMsg());
                } else activity.fetchWaitingUserSuccess(baseResponse);
            }

            @Override
            public void onFailureResult() {
                activity.showMessage(null);
                AppUtils.showLog(TAG, "fetch waiting user failed");
            }
        }));
    }


    private void clearSharedPrefsAndRealm(SharedPreferences prefs) {
        //clear shared preferences and realm after logout
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();

        final Realm realm = RealmDatabase.getInstance().getRealm();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });
    }
}
