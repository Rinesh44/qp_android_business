package com.yanky.savxbusiness.activities.dashboard;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.yanky.savxbusiness.QuiqpayApp;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.activities.fingerprint.FingerPrintActivity;
import com.yanky.savxbusiness.activities.profile.UserProfileActivity;
import com.yanky.savxbusiness.activities.wallet.ForSelf;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.entities.WalletProto;
import com.yanky.savxbusiness.fragments.Invoices;
import com.yanky.savxbusiness.fragments.More;
import com.yanky.savxbusiness.fragments.PayNow;
import com.yanky.savxbusiness.fragments.UnpaidFragment;
import com.yanky.savxbusiness.mqtt.MqttClient;
import com.yanky.savxbusiness.realm.models.CountryDetails;
import com.yanky.savxbusiness.realm.models.PaidInvoice;
import com.yanky.savxbusiness.realm.models.UnpaidInvoice;
import com.yanky.savxbusiness.realm.models.Wallet;
import com.yanky.savxbusiness.repo.CountryDetailsRepo;
import com.yanky.savxbusiness.repo.InvoiceRepo;
import com.yanky.savxbusiness.repo.Repo;
import com.yanky.savxbusiness.repo.WalletsRepo;
import com.yanky.savxbusiness.utils.AppForegroundListener;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.BottomNavigationViewHelper;
import com.yanky.savxbusiness.utils.Constants;
import com.yanky.savxbusiness.utils.UpdatePaidInvoice;
import com.yanky.savxbusiness.utils.UpdateUnpaidInvoice;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.yanky.savxbusiness.QuiqpayApp.getMyApplication;

/**
 * Created by Leesa Shakya on 02/10/18.
 * leezshk@gmail.com
 */

public class DashboardActivity extends BaseActivity implements
        com.yanky.savxbusiness.fragments.Wallet.OnFragmentInteractionListener,
        More.OnFragmentInteractionListener, PayNow.OnFragmentInteractionListener,
        Invoices.OnFragmentInteractionListener, AppForegroundListener {

    private String TAG = DashboardActivity.class.getSimpleName();
    @Inject
    Endpoints endpoints;
    @BindView(R.id.user_profile)
    ImageButton mUserImage;
    @BindView(R.id.navigation_view)
    BottomNavigationView mNavigationView;
    /*  @BindView(R.id.bottom_sheet)
      LinearLayout layoutBottomSheet;*/
/*    @BindView(R.id.btn_wallet_for_self)
    Button mSelfWallet;
    @BindView(R.id.btn_wallet_for_others)
    Button mOthersWallet;
    @BindView(R.id.btn_cancel)
    Button mCancel;*/
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.fab_menu)
    FloatingActionMenu mFabMenu;
    @BindView(R.id.fab_wallet_self)
    FloatingActionButton mFabWalletSelf;
    @BindView(R.id.fab_wallet_others)
    FloatingActionButton mFabWalletOthers;
    @BindView(R.id.iv_add_wallet)
    ImageButton mFingerPrint;

    private BottomSheetBehavior sheetBehavior;
    public SharedPreferences preferences;
    private DashboardPresenterImpl presenter;
    private Fragment mWallet, mMore, mInvoices, mPaynow;
    public com.yanky.savxbusiness.fragments.Wallet walletObj;
    public PayNow payNowObj;
    public More moreObj;
    public Invoices invoicesObj;
    public UnpaidFragment invoicesUnpaidObj;
//    private UnPaidInvoiceDataListener mUnpaidDataReceiverListener;

    private List<com.yanky.savxbusiness.realm.models.Wallet> walletList;
    public static UpdatePaidInvoice updatePaidInvoice;
    public static UpdateUnpaidInvoice updateUnpaidInvoice;
    private boolean activityActive;

    public static void setUpdatePaidInvoiceListener(UpdatePaidInvoice listener) {
        updatePaidInvoice = listener;
    }

    public static void setUpdateUnpaidInvoiceListener(UpdateUnpaidInvoice listener) {
        updateUnpaidInvoice = listener;
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);

        MqttClient.start(this);
        getMyApplication(this).getAppComponent().inject(this);
        init();

        AppUtils.showLog(TAG, "package name: " + getApplicationContext().getPackageName());
//        AppUtils.showLog(TAG, "getToekn: " + QuiqpayApp.getFirebaseToken());

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

//        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);

        List<CountryDetails> countryDetails = CountryDetailsRepo.getInstance().getAllCountryDetails();
        if (countryDetails.size() == 0) {
            presenter.getCountryCode();
        }

        //send token and userid to specific fragments
        Bundle bundle = new Bundle();
        bundle.putString("token", getToken());
        bundle.putString("user_id", getUserID());
        mMore.setArguments(bundle);

        mPaynow.setArguments(bundle);

        mToolbarTitle.setText(getResources().getString(R.string.wallet));

        BottomNavigationViewHelper.disableShiftMode(mNavigationView);
        mNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        loadFragment(new com.yanky.savxbusiness.fragments.Wallet());

        assignNotificationBadges();

        mFingerPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DashboardActivity.this, FingerPrintActivity.class));
            }
        });


        mUserImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DashboardActivity.this, UserProfileActivity.class));
            }
        });


        AppUtils.showLog(TAG, "existingwalletAddress:" + WalletsRepo.getInstance().getWalletNameForAddress("97b8b056570ed0a4b0155d62f55f1253d6ded50363864ba71a5467d2b39f304b"));


        mFabWalletSelf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DashboardActivity.this, ForSelf.class));
                mFabMenu.close(true);
            }
        });


    }

    public void assignNotificationBadges() {
        BottomNavigationMenuView bottomNavigationMenuView =
                (BottomNavigationMenuView) mNavigationView.getChildAt(0);

        int walletBadge = preferences.getInt(Constants.WALLETS, 0);
        int invoiceBadge = preferences.getInt(Constants.INVOICE, 0);

        int walletRequestBadge = preferences.getInt(Constants.WALLETREQUESTCREATED, 0);
        int paymentRequestBadge = preferences.getInt(Constants.FUNDREQUESTCREATED, 0);
        int transactionBadge = preferences.getInt(Constants.TRANSACTION, 0);

        int moreBadge = walletRequestBadge + paymentRequestBadge + transactionBadge;

        for (int i = 0; i <= 3; i++) {
            View v = bottomNavigationMenuView.getChildAt(i);

            BottomNavigationItemView itemView = (BottomNavigationItemView) v;
            View badge = LayoutInflater.from(this)
                    .inflate(R.layout.notification_badge, itemView, true);

            TextView badgeNumber = badge.findViewById(R.id.notifications_badge);

            switch (i) {
                case 0:
                    if (walletBadge != 0)
                        badgeNumber.setText(String.valueOf(walletBadge));
                    else badgeNumber.setVisibility(View.GONE);
                    break;

                case 1:
                    badgeNumber.setVisibility(View.GONE);
                    break;

                case 2:
                    if (invoiceBadge != 0)
                        badgeNumber.setText(String.valueOf(invoiceBadge));
                    else badgeNumber.setVisibility(View.GONE);
                    break;

                case 3:
                    if (moreBadge != 0)
                        badgeNumber.setText(String.valueOf(moreBadge));
                    else badgeNumber.setVisibility(View.GONE);
                    break;
                default:
                    break;

            }
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        activityActive = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        activityActive = false;
    }

    public void removeBadge(BottomNavigationView navigationView, int index) {
        BottomNavigationMenuView bottomNavigationMenuView = (BottomNavigationMenuView) navigationView.getChildAt(0);
        View v = bottomNavigationMenuView.getChildAt(index);
        BottomNavigationItemView itemView = (BottomNavigationItemView) v;
        View badge = itemView.findViewById(R.id.notifications_badge);
        if (badge != null) ((ViewGroup) badge.getParent()).removeView(badge);
    }

    @Override
    protected void onResume() {
        super.onResume();
        QuiqpayApp.setAppForegroundListener(this);
        AppUtils.showLog(TAG, "onResume()");
        presenter.getAllWallets(getToken(), preferences);

    }

    private void init() {
        presenter = new DashboardPresenterImpl(this, endpoints);

        //fragment instances
        mWallet = new com.yanky.savxbusiness.fragments.Wallet();
        mMore = new More();
        mInvoices = new Invoices();
        mPaynow = new PayNow();

        //fragment objects
        moreObj = (More) getSupportFragmentManager().findFragmentById(mMore.getId());
        invoicesObj = (Invoices) getSupportFragmentManager().findFragmentById(mInvoices.getId());
        payNowObj = (PayNow) getSupportFragmentManager().findFragmentById(mPaynow.getId());


        //initialize walletList
        walletList = new ArrayList<>();
    }

    public void getWalletSuccess(ReqResProto.BaseResponse walletResponse) {

        AppUtils.showLog(TAG, "Get Wallet Success");
        int walletCount = walletResponse.getWalletAddressesCount();
        AppUtils.showLog(TAG, "walletCount: " + walletCount);

        if (walletCount == 0) {
            WalletsRepo.getInstance().deleteAllWallets(new Repo.Callback() {
                @Override
                public void success(Object o) {
                    AppUtils.showLog(TAG, "cleared all wallets from db");

                }

                @Override
                public void fail() {
                    AppUtils.showLog(TAG, "failed to clear wallets from db");
                }
            });

            return;
        }


        walletList.clear();
        for (WalletProto.WalletAddress walletAddress : walletResponse.getWalletAddressesList()) {

//            filterItems(walletAddress);

            com.yanky.savxbusiness.realm.models.Wallet wallet = new Wallet
                    (walletAddress.getWalletAddress(),
                            walletAddress.getUserId(),
                            walletAddress.getTimestamp(),
                            walletAddress.getWalletName(),
                            walletAddress.getAmount() / 100,
                            walletAddress.getIsDefault(),
                            walletAddress.getLastActive(),
                            walletAddress.getCurrency(),
                            walletAddress.getCountryCode(),
                            walletAddress.getIsCreatedByMe());

            AppUtils.showLog(TAG, "wallet currency: " + walletAddress.getCurrency());
            AppUtils.showLog(TAG, "wallet country code: " + walletAddress.getCountryCode());
            walletList.add(wallet);
        }


        WalletsRepo.getInstance().saveWalletList(walletList, new Repo.Callback() {
            @Override
            public void success(Object o) {
                AppUtils.showLog(TAG, "saved wallets to db");
//                Toast.makeText(DashboardActivity.this, "saved to db", Toast.LENGTH_SHORT).show();
                walletObj = (com.yanky.savxbusiness.fragments.Wallet) getSupportFragmentManager().findFragmentById(R.id.frame_container);
                walletObj.updateRecyclerView();

            }

            @Override
            public void fail() {
                AppUtils.showLog(TAG, "failed to save wallets to db");
//                walletObj.updateRecyclerView();
            }
        });


    }

    private void filterItems(WalletProto.WalletAddress walletAddress) {
        String walletId = walletAddress.getWalletAddress();
        List<Wallet> dbWallets = WalletsRepo.getInstance().getAllWallets();
        for (Wallet wallet : dbWallets
        ) {
            String walletIdFromDb = wallet.getWalletAddress();
            if (!walletId.equals(walletIdFromDb)) {
                WalletsRepo.getInstance().deleteWalletById(walletIdFromDb, new Repo.Callback() {
                    @Override
                    public void success(Object o) {
                        AppUtils.showLog(TAG, "filter success");
                    }

                    @Override
                    public void fail() {
                        AppUtils.showLog(TAG, "filter failed");
                    }
                });
            }
        }

    }

    public void getWalletFail(String msg) {
        showMessage(msg);
    }



 /*   public void toggleBottomSheet() {
        if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }*/

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {


        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_wallet:
                    removeBadge(mNavigationView, 0);
                    if (mFabMenu.getVisibility() == View.GONE)
                        mFabMenu.setVisibility(View.VISIBLE);
                    mToolbarTitle.setText(getResources().getString(R.string.wallet));
                    loadFragment(mWallet);
                    presenter.getAllWallets(getToken(), preferences);
                    return true;
                case R.id.navigation_pay_now:
                    removeBadge(mNavigationView, 1);
                    mToolbarTitle.setText(getResources().getString(R.string.pay_now));
                    mFabMenu.setVisibility(View.GONE);
                    loadFragment(mPaynow);
                    return true;
                case R.id.navigation_invoices:
                    removeBadge(mNavigationView, 2);
                    mToolbarTitle.setText(getResources().getString(R.string.invoices));
                    mFabMenu.setVisibility(View.GONE);
                    loadFragment(mInvoices);
                    presenter.getUnpaidInvoice(getToken(), preferences);
                    presenter.getPaidInvoice(getToken(), preferences);
                    return true;
                case R.id.navigation_more:
                    removeBadge(mNavigationView, 3);
                    mToolbarTitle.setText(getResources().getString(R.string.more));
                    mFabMenu.setVisibility(View.GONE);
                    loadFragment(mMore);
                    return true;

            }
            return false;
        }
    };

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.disallowAddToBackStack();
        transaction.commit();
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }


    public void getUnpaidInvoiceSuccess(List<UnpaidInvoice> invoiceList, final List<WalletProto.InvoiceHolder> unpaidInvoiceHolder) {
        InvoiceRepo.getInstance().saveUnPaidInvoiceList(invoiceList, new Repo.Callback() {
            @Override
            public void success(Object o) {
                AppUtils.showLog(TAG, "saved unpaid invoices");
                if (updateUnpaidInvoice != null) {
                    updateUnpaidInvoice.updateUnpaidInvoiceRecyclerview();
                    updateUnpaidInvoice.updateUnpaidInvoiceHolder(unpaidInvoiceHolder);
                }

            }

            @Override
            public void fail() {
                AppUtils.showLog(TAG, "failed to save unpaid invoices");
            }
        });


//        mUnpaidDataReceiverListener.onDataReceived(invoiceList);

    }


    public void getUnpaidInvoiceFail(String msg) {
        showMessage(msg);
    }


    public void getPaidInvoiceSuccess(List<PaidInvoice> invoiceList, final List<WalletProto.InvoiceHolder> paidInvoiceHolder) {
        InvoiceRepo.getInstance().savePaidInvoiceList(invoiceList, new Repo.Callback() {
            @Override
            public void success(Object o) {
                AppUtils.showLog(TAG, "saved paid invoices");
                if (updatePaidInvoice != null) {
                    updatePaidInvoice.updatePaidInvoiceRecyclerView();
                    updatePaidInvoice.updatePaidInvoiceHolder(paidInvoiceHolder);

                }
            }

            @Override
            public void fail() {
                AppUtils.showLog(TAG, "failed to save paid invoices");
            }
        });

//        mUnpaidDataReceiverListener.onDataReceived(invoiceList);

    }


    public void getPaidInvoiceFail(String msg) {
        showMessage(msg);
    }

    @Override
    public void onAppForeground() {
        AppUtils.showLog(TAG, "appForeground");
        if (activityActive) showPasscodeDialog(this);
    }

    public void onCountryCodeSuccess(List<CountryDetails> countryDetailsList) {
        AppUtils.showLog(TAG, "country details fetch success");
        CountryDetailsRepo.getInstance().saveCountryDetails(countryDetailsList, new Repo.Callback() {
            @Override
            public void success(Object o) {
                AppUtils.showLog(TAG, "country details saved to db");
            }

            @Override
            public void fail() {
                AppUtils.showLog(TAG, "failed to save country details to db");
            }
        });
    }

    public void onCountryCodeFail(String msg) {
        showMessage(msg);
    }



 /*   public interface UnPaidInvoiceDataListener {
        void onDataReceived(List<UnpaidInvoice> invoiceList);
    }*/

 /*   public void setUnpaidDataListener(UnPaidInvoiceDataListener listener) {
        this.mUnpaidDataReceiverListener = listener;
    }*/


}
