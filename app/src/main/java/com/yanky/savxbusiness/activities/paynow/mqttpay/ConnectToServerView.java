package com.yanky.savxbusiness.activities.paynow.mqttpay;

import com.yanky.savxbusiness.entities.WalletProto;

public interface ConnectToServerView {
    void getInvoiceSuccess(WalletProto.InvoiceHolder unpaidInvoice);

    void getInvoiceFail(String msg);
}
