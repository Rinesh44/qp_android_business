package com.yanky.savxbusiness.activities.paynow.mqttpay;

public interface ConnectToServerPresenter {
    void getInvoice(String token, String invoiceId);
}
