package com.yanky.savxbusiness.activities.waitinglist;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.utils.AppUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.yanky.savxbusiness.QuiqpayApp.getMyApplication;

public class RegisterWaitingUser extends BaseActivity implements RegisterWaitingUserView {
    private static final String TAG = "RegisterWaitingUser";
    @Inject
    Endpoints endpoints;
    @BindView(R.id.et_email_phone)
    EditText mEmailPhone;
    @BindView(R.id.et_full_name)
    TextInputEditText mFullname;
    @BindView(R.id.btn_join)
    Button mJoin;

    private String emailPhone;
    private RegisterWaitingUserPresenterImpl presenter;
    private String deviceId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_waiting_user);

        ButterKnife.bind(this);

        checkRequiredPermissions();

        //to remove keyboard glich
        getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.background));
        getMyApplication(this).getAppComponent().inject(this);


        presenter = new RegisterWaitingUserPresenterImpl(this, endpoints);


        emailPhone = getEmailPhone();
        if (emailPhone != null) {
            mEmailPhone.setText(emailPhone);
        }

        mJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mFullname.getText().toString().isEmpty()) {
                    hideKeyboard();
                    showMessage("Please enter your full name");
                    return;
                }

                presenter.registerWaitingUser(getToken(), deviceId, mFullname.getText().toString().trim(),
                        emailPhone);


            }
        });

    }

    private String getEmailPhone() {
        Intent i = getIntent();
        return i.getStringExtra("email_phone");

    }


    private void checkRequiredPermissions() {
        if (hasPermission(Manifest.permission.READ_PHONE_STATE)) {
            deviceId = getDeviceId();
            return;
        }

        requestPermissionsSafely(new String[]{Manifest.permission.READ_PHONE_STATE}, 224);
    }


    @SuppressLint({"MissingPermission", "HardwareIds"})
    private String getDeviceId() {
        TelephonyManager telephonyManager = (TelephonyManager)
                this.getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager != null) {
            return telephonyManager.getDeviceId() + ":" + Build.MODEL;
        }
        return null;
    }

    @Override
    public void registerWaitingUserSuccess(int queuePosition, int totalWaitingUsers, String username) {
        AppUtils.showLog(TAG, "register waiting user success");
        Intent i = new Intent(this, RegisterSuccess.class);
        i.putExtra("queue_position", queuePosition);
        i.putExtra("total_waiting_users", totalWaitingUsers);
        i.putExtra("user_name", username);
        startActivity(i);

    }

    @Override
    public void registerWaitingUserFail(String msg) {
        showMessage(msg);
    }
}
