package com.yanky.savxbusiness.activities.invoice;

import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.yanky.savxbusiness.activities.account.LoginActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.entities.WalletProto;
import com.yanky.savxbusiness.realm.RealmDatabase;
import com.yanky.savxbusiness.realm.models.UnpaidInvoice;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.CallbackWrapper;

import io.realm.Realm;
import retrofit2.Response;

public class AutoPaymentDialogPresenterImpl implements AutoPaymentDialogPresenter {
    private static final String TAG = "AutoPaymentDialogPresen";
    private AutoPaymentDialog activity;
    private Endpoints endpoints;

    public AutoPaymentDialogPresenterImpl(AutoPaymentDialog activity, Endpoints endpoints) {
        this.activity = activity;
        this.endpoints = endpoints;
    }


    @Override
    public void autoPay(String token, UnpaidInvoice invoice, String maxAmount, String userId,
                        String fromWalletAddress, SharedPreferences preferences, String currency,
                        String countryCode, long tillDate) {
        activity.showLoading();
        StringBuilder signatureBuilder = new StringBuilder();
        signatureBuilder.append(fromWalletAddress);
        signatureBuilder.append(":");
        signatureBuilder.append(invoice.getSignaturePubKey());

        AppUtils.showLog(TAG, "Signature: " + signatureBuilder.toString());

        WalletProto.AutoPayment autoPayment = WalletProto.AutoPayment.newBuilder()
                .setAmount(Long.parseLong(maxAmount))
                .setBusinessUserId(invoice.getSenderUserId())
                .setDescription(invoice.getInvoiceHeading())
                .setCurrency(currency)
                .setToWalletAddress(invoice.getSignaturePubKey())
                .setToUserId(invoice.getForUserId())
                .setFromUserId(userId)
                .setSignature(signatureBuilder.toString())
                .setCountryCode(countryCode)
                .setDate(tillDate)
                .setFromWalletAddress(fromWalletAddress)
                .setInvoiceId(invoice.getInvoiceId())
                .setTimestamp(System.currentTimeMillis())
                .build();

        endpoints.postAutoPayment(token, autoPayment).enqueue(new CallbackWrapper<>(activity, new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
            @Override
            public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                activity.hideLoading();
                ReqResProto.BaseResponse baseResponse = response.body();

                if (baseResponse == null) {
                    activity.autoPayFail(null);
                }


                if (baseResponse.getError()) {
                    if (baseResponse.getMsg().equals("Authorization failed.")) {
                        clearSharedPrefsAndRealm(preferences);
                        Intent intent = new Intent(activity, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        activity.startActivity(intent);
                        Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                    } else activity.autoPayFail(baseResponse.getMsg());
                }

                activity.autoPaySuccess();
            }

            @Override
            public void onFailureResult() {
                activity.hideLoading();
            }
        }));
    }


    private void clearSharedPrefsAndRealm(SharedPreferences prefs) {
        //clear shared preferences and realm after logout
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();

        final Realm realm = RealmDatabase.getInstance().getRealm();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });
    }
}
