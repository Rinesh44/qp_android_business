package com.yanky.savxbusiness.activities.profile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.widget.Toast;

import com.yanky.savxbusiness.activities.account.LoginActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.entities.AccountProto;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.realm.RealmDatabase;
import com.yanky.savxbusiness.realm.models.User;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.CallbackWrapper;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.nio.charset.StandardCharsets;

import io.realm.Realm;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

public class UpdatePersonalPresenterImpl implements UpdatePersonalPresenter {
    private String TAG = UpdatePersonalPresenterImpl.class.getSimpleName();
    private final UpdatePersonalInfo activity;
    private final Endpoints endpoints;
    private String encodedMime;
    private User userdb;


    public UpdatePersonalPresenterImpl(UpdatePersonalInfo activity, Endpoints endpoints) {
        this.activity = activity;
        this.endpoints = endpoints;
    }

    @Override
    public void updateInfo(String token, String fullName, String dob, String gender, SharedPreferences preferences) {
        activity.showLoading();
        AccountProto.Gender gender1 = null;
        switch (gender) {
            case "Male":
                gender1 = AccountProto.Gender.MALE;
                break;

            case "Female":
                gender1 = AccountProto.Gender.FEMALE;
                break;

            case "Unknown":
                gender1 = AccountProto.Gender.UNKNOWN_GENDER;
                break;

        }
        final AccountProto.User user = AccountProto.User.newBuilder()
                .setFullName(fullName)
                .setDob(dob)
                .setGender(gender1)
                .build();

        endpoints.updateAccount(token, user).enqueue(new CallbackWrapper<>(activity,
                new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
                    @Override
                    public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                        activity.hideLoading();
                        ReqResProto.BaseResponse baseResponse = response.body();
                        if (baseResponse == null) {
                            activity.updateFail(null);
                            return;
                        }


                        if (baseResponse.getError()) {
                            if (baseResponse.getMsg().equals("Authorization failed.")) {
                                clearSharedPrefsAndRealm(preferences);
                                Intent intent = new Intent(activity, LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                activity.startActivity(intent);
                                Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                            } else activity.updateFail(baseResponse.getMsg());
                            return;
                        }


                        activity.updateSuccess();

                    }

                    @Override
                    public void onFailureResult() {
                        activity.hideLoading();
                        activity.updateFail(null);
                    }
                }));

    }

    private void mapUser(AccountProto.User userPb) {
        userdb = new User();
        userdb.setUserId(userPb.getUserId());
        userdb.setDob(userPb.getDob());
        userdb.setFullName(userPb.getFullName());

        AppUtils.showLog(TAG, "userid: " + userdb.getUserId());
        AppUtils.showLog(TAG, "dob: " + userdb.getDob());
        AppUtils.showLog(TAG, "fullname: " + userdb.getFullName());

        switch (userPb.getGender()) {
            case UNKNOWN_GENDER:
                userdb.setGender("Unknown");
                break;

            case MALE:
                userdb.setGender("Male");
                break;

            case FEMALE:
                userdb.setGender("Female");
                break;

        }


    }

    @Override
    public void uploadProPic(String token, File file, String mimeType, SharedPreferences preferences) {
        activity.showLoading();

        AppUtils.showLog(TAG, "mimeType:" + mimeType);

        Bitmap bm = BitmapFactory.decodeFile(file.getPath());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] b = baos.toByteArray();
        String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        String utf = new String(b, StandardCharsets.UTF_8);


      /*  RequestBody fileBody = RequestBody.create(MediaType.parse(mimeType), file);
        RequestBody mimeBody = RequestBody.create(FORM, mimeType);*/

//        ByteString byteString = fileByteArray.toString();


  /*      RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse(mimeType),
                        b);


        AppUtils.showLog(TAG, "filename:" + file.getName());
        MultipartBody.Part fileBody =
                MultipartBody.Part.createFormData("file", file.getName(), requestFile);

        MultipartBody.Part mimeBody =
                MultipartBody.Part.createFormData("mimeType", mimeType);

        AppUtils.showLog(TAG, "multipartData: " + fileBody + "\n" + mimeBody);*/

/*
        RequestBody req = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("mimeType", encodedMime)
                .addFormDataPart("file", file.getName(), RequestBody.create(MediaType.parse(mimeType), file)).build();*/

        RequestBody fBody = RequestBody.create(MediaType.parse(mimeType), encodedImage);
        RequestBody mimeBody = RequestBody.create(MultipartBody.FORM, mimeType);

        MultipartBody.Part filePart = MultipartBody.Part.createFormData("file", file.getName(), fBody);


//        AppUtils.showLog(TAG, "request: " + req.toString());
        endpoints.uploadProPic(token, filePart, mimeBody).enqueue(new CallbackWrapper<>(activity, new
                CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
                    @Override
                    public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                        activity.hideLoading();
                        ReqResProto.BaseResponse baseResponse = response.body();
                        if (baseResponse == null) {
                            AppUtils.showLog(TAG, "baseResponse is null");
                            activity.propicUploadFail(null);
                            return;
                        }

                        if (baseResponse.getError()) {
                            if (baseResponse.getMsg().equals("Authorization failed.")) {
                                clearSharedPrefsAndRealm(preferences);
                                Intent intent = new Intent(activity, LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                activity.startActivity(intent);
                                Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                            } else {
                                AppUtils.showLog(TAG, "file upload fail" + baseResponse.getMsg());
                                activity.propicUploadFail(baseResponse.getMsg());
                            }
                            return;
                        }

                        activity.propicUploadSuccess();

                    }

                    @Override
                    public void onFailureResult() {
                        activity.hideLoading();
                        activity.propicUploadFail(null);
                    }
                }));

    }


    private void clearSharedPrefsAndRealm(SharedPreferences prefs) {
        //clear shared preferences and realm after logout
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();

        final Realm realm = RealmDatabase.getInstance().getRealm();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });
    }
}
