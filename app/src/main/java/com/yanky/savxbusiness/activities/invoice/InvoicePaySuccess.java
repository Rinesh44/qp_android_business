package com.yanky.savxbusiness.activities.invoice;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.yanky.savxbusiness.QuiqpayApp;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.activities.dashboard.DashboardActivity;
import com.yanky.savxbusiness.realm.models.UnpaidInvoice;
import com.yanky.savxbusiness.utils.AppForegroundListener;
import com.yanky.savxbusiness.utils.AppUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InvoicePaySuccess extends BaseActivity implements AppForegroundListener {
    private static final String TAG = "InvoicePaySuccess";
    private static final int REQUEST_CODE = 7654;
    @BindView(R.id.tv_invoice_title)
    TextView mInvoiceTitle;
    @BindView(R.id.tv_wallet_name)
    TextView mWalletName;
    @BindView(R.id.tv_amount_paid)
    TextView mAmountPaid;
    @BindView(R.id.sw_auto_pay)
    SwitchCompat mAutoPay;
    @BindView(R.id.tv_auto_pay_text)
    TextView mAutoPayText;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;


    private UnpaidInvoice invoice;
    private String amountToPay, fromWalletAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_pay_success);

        ButterKnife.bind(this);

        init();

        getIntentDataAndSetValues();

        mAutoPay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                if (checked) {
                    Intent intent = new Intent(InvoicePaySuccess.this, AutoPaymentDialog.class);
                    intent.putExtra("invoice_object", invoice);
                    startActivityForResult(intent, REQUEST_CODE);
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                }
            }
        });
    }


    private void init() {
        setUpToolbar(mToolbar);
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        mToolbarTitle.setText("Invoice Payment");

    }

    private void getIntentDataAndSetValues() {
        Intent i = getIntent();
        invoice = (UnpaidInvoice) i.getParcelableExtra("invoice_object");
        String walletName = i.getStringExtra("wallet_name");
        amountToPay = i.getStringExtra("amount_to_pay");
        fromWalletAddress = i.getStringExtra("from_wallet_address");
        mInvoiceTitle.setText(invoice.getInvoiceHeading());
        mWalletName.setText(walletName);
        mAmountPaid.setText(amountToPay);


        StringBuilder autoPayTextBuilder = new StringBuilder();
        autoPayTextBuilder.append("Pay automatically when you receive invoices from ");
        autoPayTextBuilder.append(invoice.getSenderName());
        autoPayTextBuilder.append(" company");

        mAutoPayText.setText(autoPayTextBuilder);

    }


    @Override
    public void onAppForeground() {
        showPasscodeDialog(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        QuiqpayApp.setAppForegroundListener(this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE) {
            AppUtils.showLog(TAG, "request code fine");
            if (resultCode == RESULT_OK) {
                AppUtils.showLog(TAG, "result code fine");
                String cancelled = data.getStringExtra("cancelled");

                if (cancelled.equals("true")) {
                    if (mAutoPay.isChecked()) mAutoPay.setChecked(false);
                }
            }

            if (resultCode == RESULT_CANCELED) {
                AppUtils.showLog(TAG, "Data transfer cancelled");
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_done:
                Intent intent = new Intent(InvoicePaySuccess.this, DashboardActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_done, menu);
        return super.onCreateOptionsMenu(menu);
    }
}
