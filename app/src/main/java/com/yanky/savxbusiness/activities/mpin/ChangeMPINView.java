package com.yanky.savxbusiness.activities.mpin;

public interface ChangeMPINView {
    void invalidPasscode(String msg);
    void changeMPINSuccess(String passcode);
    void changeMPINFail();
}
