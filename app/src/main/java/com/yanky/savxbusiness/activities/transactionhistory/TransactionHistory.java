package com.yanky.savxbusiness.activities.transactionhistory;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.yanky.savxbusiness.QuiqpayApp;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.adapters.CustomSpinnerAdapter;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.realm.models.TransactionHolder;
import com.yanky.savxbusiness.realm.models.Wallet;
import com.yanky.savxbusiness.repo.Repo;
import com.yanky.savxbusiness.repo.TransactionRepo;
import com.yanky.savxbusiness.repo.WalletsRepo;
import com.yanky.savxbusiness.utils.AppForegroundListener;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.HeaderRecyclerViewSection;
import com.yanky.savxbusiness.utils.RecyclerViewClickListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;

import static com.yanky.savxbusiness.QuiqpayApp.getMyApplication;

public class TransactionHistory extends BaseActivity implements TransactionHistoryView, RecyclerViewClickListener, AppForegroundListener {
    private static final String TAG = "TransactionHistory";
    @Inject
    Endpoints endpoints;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.sp_wallet)
    Spinner mWalletSpinner;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.transaction_recycler_view)
    RecyclerView mRecyclerView;
    //    @BindView(R.id.tv_date)
//    TextView mDate;
    /*    @BindView(R.id.data_container)
        CardView mCardview;*/
    @BindView(R.id.progress_bar)
    ProgressBar mProgressbar;
    @BindView(R.id.empty_transaction_holder)
    LinearLayout mEmptyViewHolder;


    private List<Wallet> walletList = new ArrayList<>();
    private List<TransactionHolder> transactionHolderList = new ArrayList<>();
    private String walletAddress;
    private TransactionHistoryPresenterImpl presenter;
    //    private TransactionAdapter transactionAdapter;
    private SharedPreferences preferences;
    private SectionedRecyclerViewAdapter sectionAdapter;

    private List<TransactionHolder> transactions = new ArrayList<>();
    private String dateTitle;
    private HeaderRecyclerViewSection headerRecyclerViewSection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_history);
        ButterKnife.bind(this);

        getMyApplication(this).getAppComponent().inject(this);
        init();

        setUpWalletSpinner();
        presenter.getTrasactionHistory(getToken(), preferences);


    }

    private void init() {

        presenter = new TransactionHistoryPresenterImpl(endpoints, this);
        setUpToolbar(mToolbar);
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        mToolbarTitle.setText("Transaction History");
    }

    private void setUpWalletSpinner() {
        walletList = WalletsRepo.getInstance().getAllWallets();
        /*Wallet dummyWallet = new Wallet(null, null, 0,
                "All wallets", 0, false, 0, null, null, false);
        walletList.add(0, dummyWallet);*/
        CustomSpinnerAdapter walletSpinnerAdapter = new CustomSpinnerAdapter(this, (ArrayList<Wallet>) walletList);
        mWalletSpinner.setAdapter(walletSpinnerAdapter);


        mWalletSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                TextView textView = (TextView) view.findViewById(R.id.third_item);
                //wallet address
                Wallet wallet = (Wallet) adapterView.getItemAtPosition(i);
                walletAddress = wallet.getWalletAddress();
                //sort transaction history by wallet address
                if (walletAddress != null) {
                    transactionHolderList = TransactionRepo.getInstance().getTransactionsByWalletAddress(walletAddress);
                    AppUtils.showLog(TAG, "listSize: " + transactionHolderList.size());
                    updateRecyclerView(transactionHolderList);
                } else {
                    // display all transaction history
                    transactionHolderList = TransactionRepo.getInstance().getAllTransactionHolders();
                    updateRecyclerView(transactionHolderList);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void getTransactionHistorySuccess(List<TransactionHolder> transactionHolders) {
        TransactionRepo.getInstance().saveTransactionList(transactionHolders, new Repo.Callback() {
            @Override
            public void success(Object o) {
                AppUtils.showLog(TAG, "transaction data saved to database");
                transactionHolderList = TransactionRepo.getInstance().getAllTransactionHolders();
                updateRecyclerView(transactionHolderList);
            }

            @Override
            public void fail() {
                AppUtils.showLog(TAG, "transaction data save fail");
            }
        });
    }

    @Override
    public void getTransactionHistoryFail(String msg) {
        showMessage(msg);
        transactionHolderList = TransactionRepo.getInstance().getAllTransactionHolders();
        updateRecyclerView(transactionHolderList);
    }

    @Override
    public void onClick(View view, int position) {
        Intent intent = new Intent(this, TransactionDetails.class);
        TransactionHolder transactionHolder = transactionHolderList.get(position);
        intent.putExtra("transaction_object", transactionHolder);
        startActivity(intent);
    }

    public void updateRecyclerView(List<TransactionHolder> transactionHolderList) {
        mProgressbar.setVisibility(View.GONE);
//        mCardview.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.VISIBLE);
        sectionAdapter = new SectionedRecyclerViewAdapter();

        //string for checking repetition of date
        String dateComparison = "";


        for (TransactionHolder holder : transactionHolderList) {
            AppUtils.showLog(TAG, "holder count: " + transactionHolderList.size());
            Date date = new Date(holder.getTimestamp());
            String day = (String) DateFormat.format("dd", date);

            dateTitle = getDateSimple(holder.getTimestamp());

            //condition to prevent repetition of dates
            if (!dateTitle.equals(dateComparison)) {
                transactions = getTransactionsWithDay(day);
                AppUtils.showLog(TAG, "filtered transactions count: " + transactions.size());


                if (transactions.size() > 0) {
                    headerRecyclerViewSection = new HeaderRecyclerViewSection(this, dateTitle, transactions);
                    headerRecyclerViewSection.setClickListener(this);
                    AppUtils.showLog(TAG, "header: " + headerRecyclerViewSection.getHeader());

//                    if (!sectionAdapter.getSectionsMap().containsKey(dateTitle))
                    sectionAdapter.addSection(headerRecyclerViewSection);

                }
            }

            dateComparison = dateTitle;

        }


        mRecyclerView.setLayoutManager(new LinearLayoutManager(TransactionHistory.this));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());


        mRecyclerView.setAdapter(sectionAdapter);
        AppUtils.showLog(TAG, "totalTransactions: " + transactions.size());

        if (transactionHolderList.size() == 0) {
//            mCardview.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.GONE);
//            mDate.setVisibility(View.GONE);
            mEmptyViewHolder.setVisibility(View.VISIBLE);
        } else {
//            mCardview.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.VISIBLE);
//            mDate.setVisibility(View.VISIBLE);
            mEmptyViewHolder.setVisibility(View.GONE);
        }
    }

    private List<TransactionHolder> getTransactionsWithDay(String day) {
        List<TransactionHolder> transactions = new ArrayList<>();
        transactions.clear();
        for (TransactionHolder holder : transactionHolderList) {

            Date date = new Date(holder.getTimestamp());
            String getDay = (String) DateFormat.format("dd", date);

            if (getDay.equals(day)) {
                transactions.add(holder);
            }
        }
        return transactions;
    }

    @Override
    public void onAppForeground() {
        showPasscodeDialog(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        QuiqpayApp.setAppForegroundListener(this);
    }
}
