package com.yanky.savxbusiness.activities.account;

import com.yanky.savxbusiness.activities.base.MvpView;
import com.yanky.savxbusiness.entities.AccountProto;

/**
 * Created by Leesa Shakya on 27/09/18.
 * leezshk@gmail.com
 */

public interface RegisterView extends MvpView {
    void emptyFullName();
    void validFullName();
    void emptyEmailPhone();
    void validEmailPhone();
    void invalidEmailPhone();
    void emptyPassword();
    void validPassword();
    void invalidPassword();
    void emptyCountry();
    void validCountry();
    void registerSuccess(AccountProto.User user);
    void registerFailure(String message);
}
