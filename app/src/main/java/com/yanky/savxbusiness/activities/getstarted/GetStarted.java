package com.yanky.savxbusiness.activities.getstarted;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;

import com.yanky.savxbusiness.MainActivity;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.SplashActivity;
import com.yanky.savxbusiness.utils.Constants;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GetStarted extends AppCompatActivity {

    @BindView(R.id.btn_get_started)
    Button mGetStarted;

    @BindView(R.id.wv_text)
    WebView mText;

    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_started);

        ButterKnife.bind(this);

        //for transparent status bar
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        SharedPreferences.Editor editor = preferences.edit();
        if (!preferences.getBoolean(Constants.FIRST_TIME_LAUNCH, true)) {
            Intent intent = new Intent(this, SplashActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            editor.putBoolean(Constants.FIRST_TIME_LAUNCH, false);
            editor.apply();
        }

        String justifyTag = "<html><style type=\"text/css\">@font-face {font-family: MyFont;src: url(\"file:///android_asset/fonts/poppins_regular.ttf\")}body {font-family: MyFont;font-size: medium;text-align: justify;}</style><body style='text-align:justify; color:white;'>%s</body></html>";

        String dataString = String.format(Locale.US, justifyTag, getResources().getString(R.string.welcome));

        mText.loadDataWithBaseURL("", dataString, "text/html", "UTF-8", "");

        mText.setBackgroundColor(getResources().getColor(R.color.pelorous));

        mGetStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(GetStarted.this, MainActivity.class));
            }
        });
    }

}
