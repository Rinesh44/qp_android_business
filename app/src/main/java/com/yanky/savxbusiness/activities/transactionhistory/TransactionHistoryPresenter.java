package com.yanky.savxbusiness.activities.transactionhistory;

import android.content.SharedPreferences;

public interface TransactionHistoryPresenter {
    void getTrasactionHistory(String token, SharedPreferences preferences);
}
