package com.yanky.savxbusiness.activities.synccontacts;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.widget.Toast;

import com.yanky.savxbusiness.QuiqpayApp;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.adapters.ContactsAdapter;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.model.Contacts;
import com.yanky.savxbusiness.model.Email;
import com.yanky.savxbusiness.model.PhoneNumber;
import com.yanky.savxbusiness.realm.models.SyncedContacts;
import com.yanky.savxbusiness.repo.Repo;
import com.yanky.savxbusiness.repo.SyncedContactsRepo;
import com.yanky.savxbusiness.utils.AppForegroundListener;
import com.yanky.savxbusiness.utils.AppUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.yanky.savxbusiness.QuiqpayApp.getMyApplication;

public class SyncContacts extends BaseActivity implements SyncContactsView, AppForegroundListener {
    private static final String TAG = "SyncContacts";
    @Inject
    Endpoints endpoints;
    @BindView(R.id.rv_contacts)
    RecyclerView mRecycleView;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    private ContactsAdapter mAdapter;
    private List<Contacts> contactsList = new ArrayList<>();

    private SyncContactsPresenterImpl presenter;
    private SharedPreferences preferences;
    private List<Email> emailList = new ArrayList<>();
    private List<PhoneNumber> numberList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_contacts);

        ButterKnife.bind(this);

        init();

        getAllPhoneNo();
        getEmailDetails();
        mergeAllContactDetails();

        setupRecyclerView();

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        presenter.syncContact(getToken(), contactsList, preferences);

    }


    private void init() {

        //register dagger
        getMyApplication(this).getAppComponent().inject(this);

        presenter = new SyncContactsPresenterImpl(this, endpoints);

        setUpToolbar(mToolbar);
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        mToolbarTitle.setText("Sync Contacts");

    }

    /**
     * set adapter for recycler view
     */
    private void setupRecyclerView() {
        mRecycleView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecycleView.setLayoutManager(linearLayoutManager);
        mAdapter = new ContactsAdapter(SyncContacts.this, contactsList);
        mRecycleView.setAdapter(mAdapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;

    }


    /**
     * get all Phone numbers from device
     */
    private void getAllPhoneNo() {

        AppUtils.showLog(TAG, "getAllPhoneNo");
        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        if (phones != null) {

            while (phones.moveToNext()) {
                String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                String allPhoneNo = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                String mobileNo = getExactMobileNo(allPhoneNo);
                if (!mobileNo.isEmpty()) {
                    numberList.add(new PhoneNumber(name, mobileNo));
                    contactsList.add(new Contacts(name, mobileNo, ""));
                }
            }
            phones.close();

        }
    }


    /**
     * Filter only mobile numbers
     */
    private String getExactMobileNo(String mobileNumber) {

        AppUtils.showLog(TAG, "getExactMobileNo");
        String mobileNo = mobileNumber.replace("+977 ", "").replace("-", "");
        if (mobileNo.length() == 10) {
            return mobileNo;
        } else {
            return "";
        }


    }


    /**
     * get all emails from device
     */
    public void getEmailDetails() {
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        assert cur != null;
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                Cursor cur1 = cr.query(
                        ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                        new String[]{id}, null);
                assert cur1 != null;
                while (cur1.moveToNext()) {
                    //to get the contact names
                    String name = cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    String email = cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));

                    emailList.add(new Email(name, email));

                }
                cur1.close();
            }
        }
        cur.close();
    }


    private void mergeAllContactDetails() {
        for (PhoneNumber number : numberList
        ) {
            for (Email email : emailList
            ) {
                if (number.getName().equals(email.getName())) {
                    int position = numberList.indexOf(number);
                    contactsList.set(position, new Contacts(number.getName(), number.getNumber(), email.getEmail()));
                }
            }
        }
    }

    public static int getContactIDFromNumber(String contactNumber, Context context) {
        contactNumber = Uri.encode(contactNumber);
        int phoneContactID = new Random().nextInt();
        Cursor contactLookupCursor = context.getContentResolver().query(Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, contactNumber), new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID}, null, null, null);
        while (contactLookupCursor.moveToNext()) {
            phoneContactID = contactLookupCursor.getInt(contactLookupCursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
        }
        contactLookupCursor.close();

        return phoneContactID;
    }


    @Override
    public void syncContactSuccess(List<SyncedContacts> syncedContactsList) {
        AppUtils.showLog(TAG, "sync contacts success");
        AppUtils.showLog(TAG, "sync contact count: " + syncedContactsList.size());
        Toast.makeText(this, "Contacts synced", Toast.LENGTH_SHORT).show();

        SyncedContactsRepo.getInstance().saveContacts(syncedContactsList, new Repo.Callback() {
            @Override
            public void success(Object o) {
                AppUtils.showLog(TAG, "synced contacts saved to db");
            }

            @Override
            public void fail() {
                AppUtils.showLog(TAG, "failed to save synced contacts to db");
            }
        });
    }

    @Override
    public void syncContactFail(String msg) {
        AppUtils.showLog(TAG, "Sync contacts fail");
        showMessage(msg);
    }

    @Override
    public void onAppForeground() {
        showPasscodeDialog(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        QuiqpayApp.setAppForegroundListener(this);
    }
}
