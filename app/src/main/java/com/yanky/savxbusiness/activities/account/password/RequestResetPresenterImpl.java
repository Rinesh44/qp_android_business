package com.yanky.savxbusiness.activities.account.password;

import android.text.TextUtils;

import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.utils.CallbackWrapper;
import com.yanky.savxbusiness.utils.ValidationUtils;

import retrofit2.Response;

/**
 * Created by Leesa Shakya on 02/10/18.
 * leezshk@gmail.com
 */

public class RequestResetPresenterImpl implements RequestResetPresenter {
    private final RequestResetActivity activity;
    private final Endpoints endpoints;

    RequestResetPresenterImpl(RequestResetActivity activity, Endpoints endpoints) {
        this.activity = activity;
        this.endpoints = endpoints;
    }

    @Override
    public void validateEmailPhone(String emailPhone) {
        if (TextUtils.isEmpty(emailPhone)){
            activity.emptyEmailPhone();
            return;
        }
        if (ValidationUtils.isValidEmailPhone(emailPhone)){
            activity.invalidEmailPhone();
            return;
        }
        activity.validEmailPhone();
    }

    @Override
    public void requestResetPassword(String emailPhone) {
        if (TextUtils.isEmpty(emailPhone)){
            activity.emptyEmailPhone();
            return;
        }
        if (ValidationUtils.isValidEmailPhone(emailPhone)){
            activity.invalidEmailPhone();
            return;
        }

        activity.showLoading();
        System.out.println("email phone: " + emailPhone);

        endpoints.requestResetPassword(emailPhone).enqueue(new CallbackWrapper<>(activity,
                new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
            @Override
            public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                activity.hideLoading();
                ReqResProto.BaseResponse baseResponse = response.body();
                if (baseResponse == null) {
                    System.out.println("null");
                    activity.onRequestFailure(null);
                    return;
                }
                System.out.println("base response: " + baseResponse.toString());
                if (baseResponse.getError()) {
                    System.out.println("error: " + baseResponse.getMsg());
                    activity.onRequestFailure(baseResponse.getMsg());
                    return;
                }
                System.out.println("success");
                activity.onRequestSuccess(baseResponse.getUser());
            }

            @Override
            public void onFailureResult() {
                activity.hideLoading();
                System.out.println("fail");
                activity.onRequestFailure(null);
            }
        }));
    }
}
