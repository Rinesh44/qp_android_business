package com.yanky.savxbusiness.activities.settings;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.yanky.savxbusiness.QuiqpayApp;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.account.password.ChangePassword;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.activities.mpin.ChangeMPIN;
import com.yanky.savxbusiness.activities.synccontacts.SyncContacts;
import com.yanky.savxbusiness.activities.touchid.ConfigureTouchId;
import com.yanky.savxbusiness.utils.AppForegroundListener;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Settings extends BaseActivity implements View.OnClickListener, AppForegroundListener {
    private final int CONTACTS_PERMISSION_CODE = 111;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.cv_change_mpin)
    CardView mChangeMpin;
    @BindView(R.id.cv_change_touch_id)
    CardView mChangeTouchId;
    @BindView(R.id.cv_change_password)
    CardView mChangePassword;
    @BindView(R.id.cv_sync_contacts)
    CardView mSyncContacts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);

        init();

        FingerprintManagerCompat fingerprintManager = FingerprintManagerCompat.from(this);

        if (!fingerprintManager.isHardwareDetected()) {
            mChangeTouchId.setVisibility(View.GONE);
        }

        mChangePassword.setOnClickListener(this);
        mChangeMpin.setOnClickListener(this);
        mChangeTouchId.setOnClickListener(this);
        mSyncContacts.setOnClickListener(this);
    }

    private void init() {
        setUpToolbar(mToolbar);
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        mToolbarTitle.setText(Settings.class.getSimpleName());
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CONTACTS_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "contacts permission granted", Toast.LENGTH_LONG).show();
                startActivity(new Intent(Settings.this, SyncContacts.class));
            } else {
                Toast.makeText(this, "contacts permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cv_change_mpin:
                startActivity(new Intent(Settings.this, ChangeMPIN.class));
                break;

            case R.id.cv_change_touch_id:
                startActivity(new Intent(this, ConfigureTouchId.class));
                break;

            case R.id.cv_change_password:
                startActivity(new Intent(Settings.this, ChangePassword.class));
                break;

            case R.id.cv_sync_contacts:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, CONTACTS_PERMISSION_CODE);
                    } else {
                        startActivity(new Intent(Settings.this, SyncContacts.class));
                    }
                } else {
                    startActivity(new Intent(Settings.this, SyncContacts.class));
                }
                break;

        }
    }

    @Override
    public void onAppForeground() {
        showPasscodeDialog(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        QuiqpayApp.setAppForegroundListener(this);
    }
}
