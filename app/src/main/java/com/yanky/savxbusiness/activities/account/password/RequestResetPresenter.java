package com.yanky.savxbusiness.activities.account.password;

/**
 * Created by Leesa Shakya on 02/10/18.
 * leezshk@gmail.com
 */

public interface RequestResetPresenter {
    void validateEmailPhone(String emailPhone);
    void requestResetPassword(String emailPhone);
}
