package com.yanky.savxbusiness.activities.wallet.fund;

import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.yanky.savxbusiness.activities.account.LoginActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.crypto.Crypto;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.entities.TxProto;
import com.yanky.savxbusiness.realm.RealmDatabase;
import com.yanky.savxbusiness.realm.models.Wallet;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.CallbackWrapper;

import org.libsodium.jni.encoders.Hex;

import io.realm.Realm;
import retrofit2.Response;


public class TransferFundPresenterImpl implements TransferFundPresenter {
    public String TAG = TransferFundPresenterImpl.class.getSimpleName();
    private Endpoints endpoints;
    private TransferFund activity;

    public TransferFundPresenterImpl(TransferFund activity, Endpoints endpoints) {
        this.endpoints = endpoints;
        this.activity = activity;
    }

    @Override
    public void transferBalance(String token, String walletName, String transferAmount,
                                String desc, Wallet wallet, String fromWalletName, String fromWalletAddress,
                                String fromUserId, String currency, SharedPreferences preferences) {

        activity.showLoading();
        AppUtils.showLog(TAG, "toUser id: " + wallet.getUserId());
        AppUtils.showLog(TAG, "wallet name: " + wallet.getWalletName());

        TxProto.Transaction transaction = TxProto.Transaction.newBuilder()
                .setAmount(Long.valueOf(transferAmount) * 100)
                .setFromPubKey(fromWalletAddress)
                .setFromUserId(fromUserId)
                .setCurrency(currency)
                .setToUserId(fromUserId)
                .setToPubKey(wallet.getWalletAddress())
                .setMemo(desc)
                .build();


        byte[] dataToSign = transaction.toByteArray();
        AppUtils.showLog(TAG, "dataToSign" + dataToSign.toString());
        AppUtils.showLog(TAG, "fromUserId" + fromUserId);
        AppUtils.showLog(TAG, "fromWalletAddress" + fromWalletAddress);

        byte[] signature = Crypto.build().sign(dataToSign, fromUserId, fromWalletAddress);

        if (signature == null) {
            activity.hideLoading();
            activity.showMessage("Signature cannot be generated");
            return;
        }
        String signatureString = Hex.HEX.encode(signature);
        AppUtils.showLog(TAG, "signature" + signatureString);

        TxProto.TransactionHolder transactionHolder = TxProto.TransactionHolder.newBuilder()
                .setTransaction(transaction.toByteString())
                .setSignature(signatureString)
                .setTransactionType(TxProto.TransactionType.FUNDTRANSFER)
                .setFromUserId(fromUserId)
                .setFromCurrency(currency)
                .setToCurrency(wallet.getCurrency())
                .setToUserId(wallet.getUserId())
                .setSignaturePubKey(fromWalletAddress)
                .setToWalletAddress(wallet.getWalletAddress())
                .build();

        endpoints.transferFund(token, transactionHolder).enqueue(new CallbackWrapper<>(activity, new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
            @Override
            public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                ReqResProto.BaseResponse baseResponse = response.body();

                if (baseResponse == null) {
                    activity.transferFail(null);
                    activity.hideLoading();
                    return;
                }

                if (baseResponse.getError()) {
                    activity.hideLoading();
                    if (baseResponse.getMsg().equals("Authorization failed.")) {
                        clearSharedPrefsAndRealm(preferences);
                        Intent intent = new Intent(activity, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        activity.startActivity(intent);
                        Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                    } else activity.transferFail(baseResponse.getMsg());
                    return;
                }

                activity.transferSuccess();
            }

            @Override
            public void onFailureResult() {
                activity.hideLoading();
                activity.transferFail(null);
            }
        }));
    }


    private void clearSharedPrefsAndRealm(SharedPreferences prefs) {
        //clear shared preferences and realm after logout
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();

        final Realm realm = RealmDatabase.getInstance().getRealm();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });
    }
}
