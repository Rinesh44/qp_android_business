package com.yanky.savxbusiness.activities.search;

import android.content.SharedPreferences;

public interface SearchUserPresenter {

    void search(String token, String charSeq, SharedPreferences preferences);
}
