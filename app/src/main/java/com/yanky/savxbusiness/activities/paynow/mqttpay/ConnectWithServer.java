package com.yanky.savxbusiness.activities.paynow.mqttpay;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.protobuf.InvalidProtocolBufferException;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.activities.invoice.UnpaidInvoiceBarcode;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.crypto.Crypto;
import com.yanky.savxbusiness.entities.WalletProto;
import com.yanky.savxbusiness.model.MqttPayload;
import com.yanky.savxbusiness.mqtt.MqttClient;
import com.yanky.savxbusiness.realm.models.InvoiceItem;
import com.yanky.savxbusiness.realm.models.UnpaidInvoice;
import com.yanky.savxbusiness.realm.models.User;
import com.yanky.savxbusiness.realm.models.Wallet;
import com.yanky.savxbusiness.repo.InvoiceRepo;
import com.yanky.savxbusiness.repo.Repo;
import com.yanky.savxbusiness.repo.UserRepo;
import com.yanky.savxbusiness.repo.WalletsRepo;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.MqttHelper;

import org.libsodium.jni.encoders.Hex;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmList;

import static com.yanky.savxbusiness.QuiqpayApp.getMyApplication;

public class ConnectWithServer extends BaseActivity implements ConnectToServerView {
    @Inject
    Endpoints endpoints;
    private static final String TAG = "ConnectWithServer";
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tv_user_name)
    TextView mUserName;
    @BindView(R.id.tv_amount)
    TextView mAmount;
    @BindView(R.id.btn_status)
    Button mStatus;


    private String toWalletAddress, sessionId, publishTopic, subscribeTopic, signatureString, currency;
    private User getUser;
    private ConnectToServerPresenterImpl presenter;
    private UnpaidInvoice unpaidInvoiceBarcode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect_with_server);

        ButterKnife.bind(this);

        init();

        getIntentData();

        mUserName.setText(getUser.getFullName());

        publishTopic = "savx/merchantpay/" + getUser.getUserId() + "/" + sessionId + "/merchant";
        subscribeTopic = "savx/merchantpay/" + getUser.getUserId() + "/" + sessionId + "/user";

        AppUtils.showLog(TAG, "subscribeTopic: " + subscribeTopic);
        AppUtils.showLog(TAG, "publishTopic: " + publishTopic);

        subscribeToMqtt();

        mStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mStatus.getText().toString().equals("Cancel")) {
                    MqttClient.getInstance().unsubscribe(subscribeTopic);
                } else {
                    MqttPayload payload = new MqttPayload();
                    payload.setData("request to connect");
                    payload.setType("CONNECTION_REQUEST");
                    if (MqttClient.getInstance().publish(publishTopic, MqttHelper.getBytes(payload))) {
                        AppUtils.showLog(TAG, "publish success");
                    } else {
                        AppUtils.showLog(TAG, "publish failed");
                    }
                }
            }
        });

    }

    private void init() {

        getMyApplication(this).getAppComponent().inject(this);
        presenter = new ConnectToServerPresenterImpl(endpoints, this);

        setUpToolbar(mToolbar);
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        mToolbarTitle.setText("Make Payment");
    }

    private void subscribeToMqtt() {
        AppUtils.showLog(TAG, "subscribeMqtt()");
        MqttClient.getInstance().subscribe(subscribeTopic, payload -> {
            MqttPayload mqttPayload = MqttHelper.parseFrom(payload.message.getPayload());
            AppUtils.showLog(TAG, "subscribePayload: " + mqttPayload.getAmount());
            AppUtils.showLog(TAG, "payload type: " + mqttPayload.getType());
            if (null == mqttPayload) {
                return;
            }
            switch (mqttPayload.getType()) {
                case "CONNECTION_CONFIRMED":
                    connectionConfirmed(mqttPayload);
                    break;
                case "INVOICE":
                    paymentRequestConfirmed(mqttPayload);
                    break;
                default:
                    Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void paymentRequestConfirmed(MqttPayload payload) {
        //send invoice id to api to get invoice detail
        AppUtils.showLog(TAG, "paymentRequestConfirmed()");
        presenter.getInvoice(getToken(), payload.getInvoiceId());

    }


    private void connectionConfirmed(MqttPayload payload) {
        mAmount.setVisibility(View.VISIBLE);
        mAmount.setText("Amount: " + String.format("%.2f", payload.getAmount()));
        currency = payload.getCurrency();
        publishProceedToPay();
    }

    private void publishProceedToPay() {
        MqttPayload mqttPayload = new MqttPayload();
        User currentUser = UserRepo.getInstance().getUserFromUserId(getUserID());
        mqttPayload.setUserId(currentUser.getUserId());
        mqttPayload.setFullname(currentUser.getFullName());

        Wallet defaultWallet = WalletsRepo.getInstance().getDefaultWallet();
        mqttPayload.setPubKey(defaultWallet.getWalletAddress());
        mqttPayload.setType("PAY_REQUEST");
        MqttClient.getInstance().publish(publishTopic, MqttHelper.getBytes(mqttPayload));
        mStatus.setText("Cancel");
        mStatus.setBackgroundColor(getResources().getColor(R.color.reds));
        showLoading();

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void getIntentData() {
        Intent i = getIntent();
        getUser = i.getParcelableExtra("user_parcelable");
        toWalletAddress = i.getStringExtra("to_wallet_address");
        sessionId = i.getStringExtra("session_id");
        AppUtils.showLog(TAG, "user" + getUser.toString());
        AppUtils.showLog(TAG, "walletAddress" + toWalletAddress);
    }


    @Override
    public void getInvoiceSuccess(WalletProto.InvoiceHolder unpaidInvoiceHolder) {
        hideLoading();
        if (unpaidInvoiceHolder != null) {

            UnpaidInvoice unpaidInvoiceFromBarcode = signAndMapInvoice(unpaidInvoiceHolder);

            if (unpaidInvoiceFromBarcode != null) {
                InvoiceRepo.getInstance().saveUnpaidInvoiceFromBarcode(unpaidInvoiceFromBarcode, new Repo.Callback() {
                    @Override
                    public void success(Object o) {
                        AppUtils.showLog(TAG, "successfully saved invoice to db");

                        Intent intent = new Intent(ConnectWithServer.this, UnpaidInvoiceBarcode.class);
                        intent.putExtra("invoice_object_barcode", unpaidInvoiceFromBarcode);
                        intent.putExtra("signature", signatureString);
                        startActivity(intent);
                    }

                    @Override
                    public void fail() {
                        AppUtils.showLog(TAG, "failed to save invoice to db");
                    }
                });
            } else {
                Toast.makeText(this, "unable to get invoice", Toast.LENGTH_SHORT).show();
            }


        } else {
            AppUtils.showLog(TAG, "null invoice");
        }
    }

    private UnpaidInvoice signAndMapInvoice(WalletProto.InvoiceHolder invoiceHolder) {
        try {

            //sign the invoice first for signature
            Wallet defaultWallet = WalletsRepo.getInstance().getDefaultWallet();
            String defaultWalletAddress = defaultWallet.getWalletAddress();

            WalletProto.Invoice invoice = WalletProto.Invoice.parseFrom(invoiceHolder.getInvoice());
            byte[] signature = Crypto.build().sign(invoice.toByteArray(), getUserID(), defaultWalletAddress);

            if (null != signature) {
                signatureString = Hex.HEX.encode(signature);
                AppUtils.showLog(TAG, "signature:" + signatureString);

            } else {
                AppUtils.showLog(TAG, "unable to generate signature");
            }

            UnpaidInvoice unpaidInvoice = new UnpaidInvoice();
            AppUtils.showLog(TAG, "invoicePubKey:" + invoiceHolder.getSignaturePubKey());
            unpaidInvoice.setAnchorRef(invoice.getAnchorRef());
            unpaidInvoice.setForUserId(invoice.getForUserId());
            unpaidInvoice.setInvoiceHeading(invoice.getInvoiceHeading());
            unpaidInvoice.setInvoiceId(invoice.getInvoiceId());
            unpaidInvoice.setNotes(invoice.getNotes());
            unpaidInvoice.setReceiverName(invoice.getReceiverName());
            unpaidInvoice.setReceiverUserId(invoice.getReceiverUserId());
            unpaidInvoice.setReceiverVatNo(invoice.getReceiverVatNo());
            unpaidInvoice.setDueDate(String.valueOf(invoice.getDueDate()));
            unpaidInvoice.setSenderName(invoice.getSenderName());
            unpaidInvoice.setSenderUserId(invoice.getSenderUserId());
            unpaidInvoice.setSenderVatNo(invoice.getSenderVatNo());
            unpaidInvoice.setTotalAmountInWords(invoice.getTotalAmountInWords());
            unpaidInvoice.setTotalAmountToPay(Double.valueOf(invoice.getTotalAmountToPay()));
            unpaidInvoice.setTaxableAmount(String.valueOf(invoice.getTotalTaxableAmount()));
            unpaidInvoice.setVatPercent(String.valueOf(invoice.getVatPercent()));
            unpaidInvoice.setDisPercent(String.valueOf(invoice.getDiscountPercent()));
            unpaidInvoice.setShippingCharge(String.valueOf(invoice.getShippingCharge()));
            unpaidInvoice.setInvoiceDate(String.valueOf(invoice.getInvoiceDate()));
            unpaidInvoice.setSignature(invoiceHolder.getSignature());
            unpaidInvoice.setSignaturePubKey(invoiceHolder.getSignaturePubKey());
            unpaidInvoice.setCurrency(currency);


            RealmList<InvoiceItem> invoiceItemRealmList = new RealmList<>();
            invoiceItemRealmList.clear();
            int invoiceItemCount = invoice.getItemsCount();
            for (int j = 0; j < invoiceItemCount; j++) {
                WalletProto.InvoiceItem invoiceItem = invoice.getItems(j);
                InvoiceItem realmInvoiceItem = new InvoiceItem();
                realmInvoiceItem.setId(invoice.getInvoiceId());
                realmInvoiceItem.setQuantity(String.valueOf(invoiceItem.getQty()));
                realmInvoiceItem.setDescription(String.valueOf(invoiceItem.getItemDescription()));
                realmInvoiceItem.setUnitPrice(String.valueOf(invoiceItem.getUnitPrice()));

                invoiceItemRealmList.add(realmInvoiceItem);

            }

            unpaidInvoice.setInvoiceItemList(invoiceItemRealmList);

            return unpaidInvoice;
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void getInvoiceFail(String msg) {
        showMessage(msg);
    }


}
