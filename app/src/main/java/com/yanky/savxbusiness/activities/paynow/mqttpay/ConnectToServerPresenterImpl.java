package com.yanky.savxbusiness.activities.paynow.mqttpay;

import android.content.Intent;
import android.widget.Toast;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.yanky.savxbusiness.activities.account.LoginActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.entities.WalletProto;
import com.yanky.savxbusiness.utils.CallbackWrapper;

import retrofit2.Response;

public class ConnectToServerPresenterImpl implements ConnectToServerPresenter {
    private static final String TAG = "ConnectToServerPresente";
    private Endpoints endpoints;
    private ConnectWithServer activity;


    public ConnectToServerPresenterImpl(Endpoints endpoints, ConnectWithServer activity) {
        this.endpoints = endpoints;
        this.activity = activity;
    }


    @Override
    public void getInvoice(String token, String invoiceId) {
        endpoints.getInvoiceFromId(token, invoiceId).enqueue(new CallbackWrapper<>(activity, new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
            @Override
            public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                ReqResProto.BaseResponse baseResponse = response.body();

                if (baseResponse == null) {
                    activity.getInvoiceFail(null);
                }

                if (baseResponse.getError()) {
                    if (baseResponse.getMsg().equals("Authorization failed.")) {
                        Intent intent = new Intent(activity, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        activity.startActivity(intent);
                        Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                    } else activity.getInvoiceFail(baseResponse.getMsg());
                }


                WalletProto.InvoiceHolder unpaidInvoiceHolder = extractInvoiceFromResponse(baseResponse);
                activity.getInvoiceSuccess(unpaidInvoiceHolder);
            }

            @Override
            public void onFailureResult() {
                activity.getInvoiceFail(null);
            }
        }));
    }

    private WalletProto.InvoiceHolder extractInvoiceFromResponse(ReqResProto.BaseResponse baseResponse) {
        ByteString cipher = baseResponse.getInvoiceEncryption().getCipher();

        try {
            WalletProto.InvoiceHolder invoiceHolder = WalletProto.InvoiceHolder.parseFrom(cipher);

            return invoiceHolder;

        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }

        return null;
    }
}
