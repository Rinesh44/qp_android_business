package com.yanky.savxbusiness.activities.verification;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.account.LoginActivity;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.activities.waitinglist.RegisterSuccess;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.repo.UserRepo;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.Constants;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.yanky.savxbusiness.QuiqpayApp.getMyApplication;

/**
 * Created by Leesa Shakya on 02/10/18.
 * leezshk@gmail.com
 */

public class VerificationActivity extends BaseActivity implements VerificationView {
    private static final String TAG = "VerificationActivity";
    @Inject
    Endpoints endpoints;
    @BindView(R.id.pin_entry_edit_text)
    PinEntryEditText pinEntryEditText;
    @BindView(R.id.text_view_resend_code)
    TextView resendCodeTextView;
    @BindView(R.id.text_view_timer)
    TextView timerTextView;

    private CountDownTimer countDownTimer;
    private String userId, code;
    static VerificationPresenterImpl presenter;
    private boolean isForWaitingUser;
    private String emailPhone;
    private String deviceId;
    private String fullname;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        ButterKnife.bind(this);

        getMyApplication(this).getAppComponent().inject(this);

        checkRequiredPermissions();

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        presenter = new VerificationPresenterImpl(this, endpoints);

        isForWaitingUser = checkIfWaitingUser();
        emailPhone = getEmailPhone();
        fullname = getFullName();

        AppUtils.showLog(TAG, "waitingUser: " + isForWaitingUser);
        userId = getUserID();
        pinEntryEditText.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
            @Override
            public void onPinEntered(CharSequence charSequence) {
                if (charSequence.length() == 6) {
                    code = charSequence.toString();
                    hideKeyboard();
                    if (isForWaitingUser) presenter.onVerifyWaitingUserCode(emailPhone, code);
                    else presenter.onVerify(userId, code);
                    return;
                }

                pinEntryEditText.setError(true);
                showMessage(getString(R.string.invalid_code));
            }
        });
    }

    private boolean checkIfWaitingUser() {
        Intent i = getIntent();
        return i.getBooleanExtra(Constants.IS_FOR_WAITING_USER, false);
    }

    private String getEmailPhone() {
        Intent i = getIntent();
        return i.getStringExtra("email_phone");
    }

    private String getFullName() {
        Intent i = getIntent();
        return i.getStringExtra("fullname");
    }


    public void onResendCode(View view) {
        pinEntryEditText.setText(null);
        presenter.onResendCode(userId);
    }

    private void setTimer() {
        final ColorStateList oldColors = resendCodeTextView.getTextColors();
        countDownTimer = new CountDownTimer(60000, 1000) {

            @Override
            public void onTick(long l) {
                resendCodeTextView.setClickable(false);
                resendCodeTextView.setTextColor(getResources().getColor(R.color.lightGray));
                timerTextView.setText(String.format(getString(R.string.resend_timer), l / 1000));
            }

            @Override
            public void onFinish() {
                timerTextView.setText("");
                resendCodeTextView.setClickable(true);
                resendCodeTextView.setTextColor(oldColors);
            }
        };
        countDownTimer.start();
    }

    @Override
    public void verifyWaitingUserCodeSuccess() {
        AppUtils.showLog(TAG, "verify waiting user code success");
        AppUtils.showLog(TAG, "verifySuccess");
           /* Intent i = new Intent(this, RegisterWaitingUser.class);
            i.putExtra("email_phone", emailPhone);
            startActivity(i);*/
        presenter.registerWaitingUser(getToken(), deviceId, fullname, emailPhone, preferences);

    }

    @Override
    public void verifyWaitingUserCodeFail(String msg) {
        AppUtils.showLog(TAG, "verify waiting user code fail");
    }

    @Override
    public void verifySuccess() {

        UserRepo.getInstance().setUserStatus();
        if (getToken() == null) {
            hideKeyboard();
            showMessage("Token is null");
            return;
        }

        Toast.makeText(this, "User verified", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();

    }

    @Override
    public void resendCodeSuccess() {
        setTimer();
        showToastMessage(getString(R.string.verification_code_resent_successfully));
    }

    @Override
    public void verifyFailure(String message) {
        showMessage(message);
    }

    @Override
    public void registerWaitingUserSuccess(int queuePosition, int totalWaitingUsers, String username) {
        AppUtils.showLog(TAG, "register waiting user success");
        Intent i = new Intent(this, RegisterSuccess.class);
        i.putExtra("queue_position", queuePosition);
        i.putExtra("total_waiting_users", totalWaitingUsers);
        i.putExtra("user_name", username);
        startActivity(i);
    }

    @Override
    public void registerWaitingUserFail(String msg) {
        showMessage(msg);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (countDownTimer != null) countDownTimer.cancel();
    }


    private void checkRequiredPermissions() {
        if (hasPermission(Manifest.permission.READ_PHONE_STATE)) {
            deviceId = getDeviceId();
            return;
        }

        requestPermissionsSafely(new String[]{Manifest.permission.READ_PHONE_STATE}, 224);
    }


    @SuppressLint({"MissingPermission", "HardwareIds"})
    private String getDeviceId() {
        TelephonyManager telephonyManager = (TelephonyManager)
                this.getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager != null) {
            return telephonyManager.getDeviceId() + ":" + Build.MODEL;
        }
        return null;
    }
}