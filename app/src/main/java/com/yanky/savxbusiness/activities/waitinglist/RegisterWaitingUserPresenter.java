package com.yanky.savxbusiness.activities.waitinglist;

public interface RegisterWaitingUserPresenter {
    void registerWaitingUser(String token, String deviceId, String fullName, String emailPhone);
}
