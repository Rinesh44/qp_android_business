package com.yanky.savxbusiness.activities.account.password;

import android.content.SharedPreferences;

public interface ChangePasswordPresenter {
    void changePassword(String token, String currentPassword, String newPassword,
                        String confirmPassword, SharedPreferences preferences);
}
