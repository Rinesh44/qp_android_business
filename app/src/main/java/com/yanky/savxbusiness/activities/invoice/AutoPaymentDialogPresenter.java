package com.yanky.savxbusiness.activities.invoice;

import android.content.SharedPreferences;

import com.yanky.savxbusiness.realm.models.UnpaidInvoice;

public interface AutoPaymentDialogPresenter {

    void autoPay(String token, UnpaidInvoice invoice, String amountToPay, String userId,
                 String fromWalletAddress, SharedPreferences preferences, String currency,
                 String countryCode, long tillDate);
}

