package com.yanky.savxbusiness.activities.autopayments;

import android.content.SharedPreferences;

public interface AutoPaymentPresenter {
    void getAutoPayments(String token, SharedPreferences preferences);
}
