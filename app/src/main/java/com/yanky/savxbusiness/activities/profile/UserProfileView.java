package com.yanky.savxbusiness.activities.profile;

import com.yanky.savxbusiness.entities.AccountProto;

public interface UserProfileView {
    void profileFetchFailure(String msg);

    void profileFetchSuccess(AccountProto.User user);

    void addressDeleteSuccess(int pos, String addressId);

    void addressDeleteFail(String msg);
}
