package com.yanky.savxbusiness.activities.waitinglist;

import com.yanky.savxbusiness.QuiqpayApp;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.entities.AccountProto;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.CallbackWrapper;

import retrofit2.Response;

public class RegisterWaitingUserPresenterImpl implements RegisterWaitingUserPresenter {
    private static final String TAG = "RegisterWaitingUserPres";

    private RegisterWaitingUser activity;
    private Endpoints endpoints;

    public RegisterWaitingUserPresenterImpl(RegisterWaitingUser activity, Endpoints endpoints) {
        this.activity = activity;
        this.endpoints = endpoints;
    }


    @Override
    public void registerWaitingUser(String token, String deviceId, String fullName, String emailPhone) {
        AccountProto.WaitingUser waitingUser = AccountProto.WaitingUser.newBuilder()
                .setEmailPhone(emailPhone)
                .setName(fullName)
                .setDeviceId(deviceId)
                .setPushToken(QuiqpayApp.getFirebaseToken())
                .setDeviceInfo("")
                .setDeviceType(AccountProto.DeviceType.ANDROID)
                .build();

        endpoints.registerWaitingUser(token, waitingUser).enqueue(new CallbackWrapper<>(activity,
                new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
                    @Override
                    public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                        ReqResProto.BaseResponse baseResponse = response.body();

                        if (baseResponse != null) {
                            int queuePosition = baseResponse.getWaitingUser().getQueuePosition();
                            int totalWaitingUsers = baseResponse.getWaitingUser().getTotal();
                            String username = baseResponse.getWaitingUser().getName();
                            AppUtils.showLog(TAG, "baseResponse: " + queuePosition + " " + totalWaitingUsers
                                    + " " + username);
                            if (baseResponse == null) {
                                AppUtils.showLog(TAG, "baseResponse is null");
                                activity.registerWaitingUserFail(null);
                                return;
                            }

                            if (baseResponse.getError()) {
                                activity.registerWaitingUserFail(baseResponse.getMsg());
                            } else
                                activity.registerWaitingUserSuccess(queuePosition, totalWaitingUsers,
                                        username);
                        }
                    }

                    @Override
                    public void onFailureResult() {
                        activity.registerWaitingUserFail(null);
                    }
                }));
    }
}
