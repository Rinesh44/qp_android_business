package com.yanky.savxbusiness.activities.wallet;

import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.yanky.savxbusiness.activities.account.LoginActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.crypto.Crypto;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.entities.TxProto;
import com.yanky.savxbusiness.entities.WalletProto;
import com.yanky.savxbusiness.realm.RealmDatabase;
import com.yanky.savxbusiness.realm.models.TransactionOnHoldHolder;
import com.yanky.savxbusiness.realm.models.User;
import com.yanky.savxbusiness.realm.models.Wallet;
import com.yanky.savxbusiness.repo.UserRepo;
import com.yanky.savxbusiness.repo.WalletsRepo;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.CallbackWrapper;

import org.libsodium.jni.encoders.Hex;

import io.realm.Realm;
import retrofit2.Response;

public class ForSelfPresenterImpl implements ForSelfPresenter {
    private String TAG = ForSelfPresenterImpl.class.getSimpleName();
    private final ForSelf activity;
    private final Endpoints endpoints;

    public ForSelfPresenterImpl(ForSelf activity, Endpoints endpoints) {
        this.activity = activity;
        this.endpoints = endpoints;
    }

    @Override
    public void transferBalance(String token, String userId, TransactionOnHoldHolder transactionOnHoldHolder, WalletProto.WalletAddress newWallet, SharedPreferences preferences) {

        activity.showLoading();
        AppUtils.showLog(TAG, "toUser id: " + newWallet.getUserId());
        AppUtils.showLog(TAG, "wallet name: " + newWallet.getWalletName());

        Wallet defaultWallet = WalletsRepo.getInstance().getDefaultWallet();
        User user = UserRepo.getInstance().getUserFromUserId(userId);

        TxProto.Transaction transaction = TxProto.Transaction.newBuilder()
                .setAmount(Long.valueOf(transactionOnHoldHolder.getAmount()))
                .setFromPubKey(defaultWallet.getWalletAddress())
                .setFromUserId(userId)
                .setToUserId(userId)
                .setCurrency(newWallet.getCurrency())
                .setToPubKey(newWallet.getWalletAddress())
                .setMemo("Converting")
                .build();


        byte[] dataToSign = transaction.toByteArray();
        AppUtils.showLog(TAG, "dataToSign" + dataToSign.toString());

        byte[] signature = Crypto.build().sign(dataToSign, userId, defaultWallet.getWalletAddress());

        if (signature == null) {
            activity.hideLoading();
            activity.showMessage("Signature cannot be generated");
            return;
        }
        String signatureString = Hex.HEX.encode(signature);
        AppUtils.showLog(TAG, "signature" + signatureString);

        TxProto.TransactionHolder transactionHolder = TxProto.TransactionHolder.newBuilder()
                .setTransaction(transaction.toByteString())
                .setSignature(signatureString)
                .setRefId(transactionOnHoldHolder.getRefId())
                .setTransactionType(TxProto.TransactionType.FUNDTRANSFER)
                .setFromUserId(userId)
                .setFromUserFullName(user.getFullName())
                .setToUserFullName(user.getFullName())
                .setFromCurrency(newWallet.getCurrency())
                .setToCurrency(newWallet.getCurrency())
                .setToUserId(userId)
                .setSignaturePubKey(defaultWallet.getWalletAddress())
                .setToWalletAddress(newWallet.getWalletAddress())
                .build();

        endpoints.releaseFund(token, transactionHolder).enqueue(new CallbackWrapper<>(activity, new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
            @Override
            public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                ReqResProto.BaseResponse baseResponse = response.body();

                if (baseResponse == null) {
                    activity.transferBalanceFail(null);
                    activity.hideLoading();
                    return;
                }

                if (baseResponse.getError()) {
                    activity.hideLoading();
                    if (baseResponse.getMsg().equals("Authorization failed.")) {
                        clearSharedPrefsAndRealm(preferences);
                        Intent intent = new Intent(activity, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        activity.startActivity(intent);
                        Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                    } else activity.transferBalanceFail(baseResponse.getMsg());
                    return;
                }

                activity.transferBalanceSuccess();
            }

            @Override
            public void onFailureResult() {
                activity.hideLoading();
                activity.transferBalanceFail(null);
            }
        }));
    }


    @Override
    public void createSelfWallet(String userId, String token, String walletName, String walletAddress, String passcode, String currency,
                                 String countryCode, Long walletAmount, SharedPreferences preferences) {
        AppUtils.showLog(TAG, "createSelfWallet()");

        final WalletProto.WalletAddress walletAddressPb = WalletProto.WalletAddress.newBuilder()
                .setWalletAddress(walletAddress)
                .setIsDefault(false)
                .setCurrency(currency)
                .setCountryCode(countryCode)
                .setWalletName(walletName)
                .setIsCreatedByMe(true)
                .setUserId(userId)
                .setTimestamp(System.currentTimeMillis())
                .build();


        endpoints.createNewWallet(token, walletAddressPb).enqueue(new CallbackWrapper<>(activity,
                new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
                    @Override
                    public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                        activity.hideLoading();
                        ReqResProto.BaseResponse baseResponse = response.body();
                        AppUtils.showLog(TAG, "base response: " + baseResponse.toString());

                        if (baseResponse == null) {
                            activity.showMessage("Base response null");
                            return;
                        }


                        if (baseResponse.getError()) {
                            if (baseResponse.getMsg().equals("Authorization failed.")) {
                                clearSharedPrefsAndRealm(preferences);
                                Intent intent = new Intent(activity, LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                activity.startActivity(intent);
                                Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                            } else activity.showMessage(baseResponse.getMsg());
                            return;
                        }

                        WalletProto.WalletAddress newWalletAddress = baseResponse.getWalletAddress();
                        activity.createSelfWalletSuccess(newWalletAddress);

                    }

                    @Override
                    public void onFailureResult() {
                        activity.hideLoading();
                        activity.createSelfWalletFail("Wallet creation failed");
                    }
                }));
    }

    @Override
    public void backUpKeys(String token, String referenceId, String pubKey, WalletProto.KeysBackup keysBackup, SharedPreferences
            preferences) {
        AppUtils.showLog(TAG, "");
        activity.showLoading();
        endpoints.backupKey(token, referenceId, pubKey, keysBackup).enqueue(new CallbackWrapper<>(activity,
                new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
                    @Override
                    public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                        ReqResProto.BaseResponse baseResponse = response.body();
                        if (baseResponse == null) {
                            activity.showMessage("key backup failed");
                            return;
                        }

                        AppUtils.showLog(TAG, " backup base response: " + baseResponse.toString());

                        if (baseResponse.getError()) {
                            if (baseResponse.getMsg().equals("Authorization failed.")) {
                                clearSharedPrefsAndRealm(preferences);
                                Intent intent = new Intent(activity, LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                activity.startActivity(intent);
                                Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                            } else activity.showMessage(baseResponse.getMsg());
                            return;
                        }

                        activity.keyBackupSuccess();
                    }

                    @Override
                    public void onFailureResult() {
                        activity.hideLoading();
                        activity.keyBackupFail(null);
                    }
                }));
    }

    private void clearSharedPrefsAndRealm(SharedPreferences prefs) {
        //clear shared preferences and realm after logout
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();

        final Realm realm = RealmDatabase.getInstance().getRealm();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });
    }


}
