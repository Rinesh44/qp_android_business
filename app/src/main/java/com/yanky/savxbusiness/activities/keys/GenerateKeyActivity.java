package com.yanky.savxbusiness.activities.keys;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.activities.dashboard.DashboardActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.crypto.Crypto;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.Constants;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.yanky.savxbusiness.QuiqpayApp.getMyApplication;

/**
 * Created by Leesa Shakya on 03/10/18.
 * leezshk@gmail.com
 */

public class GenerateKeyActivity extends BaseActivity implements GenerateKeyView {
    String TAG = GenerateKeyActivity.class.getSimpleName();
    @Inject
    Endpoints endpoints;
    @BindView(R.id.relative_layout_button)
    RelativeLayout mGenerateRelativeLayout;

    GenerateKeyPresenter presenter;
    Crypto crypto;
    TextInputLayout mPassCodeLayout;
    TextInputEditText mPassCodeEditText;
    String userId;
    boolean isKeyBackedUp;
    SharedPreferences preferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encryption);
        ButterKnife.bind(this);

        getMyApplication(this).getAppComponent().inject(this);
        presenter = new GenerateKeyPresenterImpl(this);
        crypto = Crypto.build();

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        isKeyBackedUp = preferences.getBoolean(Constants.IS_KEY_BACKUP, false);

        getIntentValues();
    }

    public void onGenerateKeys(View view) {
        AppUtils.showLog(TAG, "onGenerateKeys():");
        if (crypto.isKeyGenerated(userId)) {
            Toast.makeText(this, "Key already generated", Toast.LENGTH_SHORT).show();
            keyGenerationSuccess(getUserID());
        } else {
            String key = crypto.generateKey(userId);

            if (key == null) {
                AppUtils.showLog(TAG, "Key Is Null");
                keyGenerationFailed();
            } else {
                AppUtils.showLog(TAG, "Key not null");
                keyGenerationSuccess(userId);
            }
        }

    }

    public void onRecoverKeys(View view) {

    }

    private void getIntentValues() {
        Intent intent = getIntent();
        userId = intent.getStringExtra(Constants.USER_ID);
        if (userId == null) userId = getUserID();
    }

/*    public void onBackUp(View view) {
        EnterPasscodeDialog dialog = new EnterPasscodeDialog(this);
        final View dialogView = View.inflate(this, R.layout.dialog_backup_key, null);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView, ViewUtils.getDialogParams(this));

        mPassCodeLayout = dialogView.findViewById(R.id.input_layout_pass_code);
        mPassCodeEditText = dialogView.findViewById(R.id.edit_text_pass_code);
        Button mBackupButton = dialogView.findViewById(R.id.button_backup);

        mPassCodeEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                presenter.validatePassCode(mPassCodeEditText.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        mBackupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String passCode = mPassCodeEditText.getText().toString();
                presenter.onBackupKey(getUser().getUserId(), passCode, crypto);
            }
        });

        if (dialog.getWindow() != null)
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        dialog.show();
    }*/

    public void onSkip(View view) {
        startActivity(new Intent(GenerateKeyActivity.this, DashboardActivity.class));
        finish();
    }

    @Override
    public void emptyPassCode() {
        mPassCodeLayout.setErrorEnabled(true);
        mPassCodeLayout.setError(getString(R.string.this_field_is_required));
        mPassCodeEditText.requestFocus();
    }

    @Override
    public void validPassCode() {
        mPassCodeLayout.setErrorEnabled(false);
        mPassCodeLayout.setError(null);
    }

    @Override
    public void invalidPassCode() {
        mPassCodeLayout.setErrorEnabled(true);
        mPassCodeLayout.setError(getString(R.string.invalid_password));
        mPassCodeEditText.requestFocus();
    }

    @Override
    public void keyGenerationFailed() {
        showMessage(getString(R.string.key_generation_failed));
    }

    @Override
    public void keyGenerationSuccess(String userId) {
        Intent passcodeActivity = new Intent(GenerateKeyActivity.this, EnterPasscodeActivity.class);
        passcodeActivity.putExtra(Constants.USER_ID, userId);
        passcodeActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(passcodeActivity);
    }
}
