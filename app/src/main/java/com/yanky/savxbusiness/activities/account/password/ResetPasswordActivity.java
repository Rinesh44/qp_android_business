package com.yanky.savxbusiness.activities.account.password;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.account.LoginActivity;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.utils.AppUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;

import static com.yanky.savxbusiness.QuiqpayApp.getMyApplication;

/**
 * Created by Leesa Shakya on 03/10/18.
 * leezshk@gmail.com
 */

public class ResetPasswordActivity extends BaseActivity implements ResetPasswordView {
    private static final String TAG = "ResetPasswordActivity";
    @Inject
    Endpoints endpoints;
    @BindView(R.id.input_layout_password)
    TextInputLayout mPasswordLayout;
    @BindView(R.id.edit_text_password)
    TextInputEditText mPasswordEditText;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;

    private String userId, resetToken;
    ResetPasswordPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        ButterKnife.bind(this);

        init();
        getIntentValues();

        getMyApplication(this).getAppComponent().inject(this);
        presenter = new ResetPasswordPresenterImpl(this, endpoints);
    }

    private void getIntentValues() {
        Intent intent = getIntent();
        if (intent != null) {
            userId = intent.getStringExtra("USER_ID");
            AppUtils.showLog(TAG, "userId: " + userId);
            resetToken = intent.getStringExtra("RESET_TOKEN");
        }
    }


    private void init() {
        setUpToolbar(mToolbar);
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        mToolbarTitle.setText("Reset Password");
    }

    @OnTextChanged(R.id.edit_text_password)
    void validatePassword(Editable editable) {
        presenter.validatePassword(editable.toString());
    }

    public void onResetPassword(View view) {
        hideKeyboard();
        String password = mPasswordEditText.getText().toString();
        if (userId == null) {
            startActivity(new Intent(this, RequestResetActivity.class));
            return;
        }
        presenter.resetPassword(password, userId, resetToken);
    }

    @Override
    public void emptyPassword() {
        mPasswordLayout.setErrorEnabled(true);
        mPasswordLayout.setError(getString(R.string.this_field_is_required));
        mPasswordEditText.requestFocus();
    }

    @Override
    public void validPassword() {
        mPasswordLayout.setErrorEnabled(false);
        mPasswordLayout.setError(null);
    }

    @Override
    public void invalidPassword() {
        mPasswordLayout.setErrorEnabled(true);
        mPasswordLayout.setError(getString(R.string.invalid_password));
        mPasswordEditText.requestFocus();
    }

    @Override
    public void resetPasswordFailure(String message) {
        if (message.equals("Invalid password reset code.")) {
            Intent intent = new Intent(this, ResetTokenActivity.class);
            startActivity(intent);
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void resetPasswordSuccess() {
        showMessage("Password reset successful");
        startActivity(new Intent(ResetPasswordActivity.this, LoginActivity.class));
        finish();
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
