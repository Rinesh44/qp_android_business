package com.yanky.savxbusiness.activities.invoice;

import android.content.SharedPreferences;

import com.yanky.savxbusiness.realm.models.UnpaidInvoice;

public interface PayInvoicePresenter {
    void payInvoice(String token, UnpaidInvoice invoice, String totalAmount, String userId,
                    String note, String fromAddress, SharedPreferences preferences);
}
