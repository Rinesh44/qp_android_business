package com.yanky.savxbusiness.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;

import com.yanky.savxbusiness.activities.account.LoginActivity;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.activities.keys.EnterPasscodeActivity;
import com.yanky.savxbusiness.activities.verification.VerificationActivity;
import com.yanky.savxbusiness.activities.waitinglist.EnterCredential;
import com.yanky.savxbusiness.activities.waitinglist.RegisterSuccess;
import com.yanky.savxbusiness.crypto.Crypto;
import com.yanky.savxbusiness.entities.AccountProto;
import com.yanky.savxbusiness.realm.models.User;
import com.yanky.savxbusiness.utils.AppUtils;

/**
 * Created by Leesa Shakya on 26/09/18.
 * leezshk@gmail.com
 */

public class SplashActivity extends BaseActivity {
    private String TAG = SplashActivity.class.getSimpleName();

    private SharedPreferences preferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Crypto crypto = Crypto.build();
                AppUtils.showLog(TAG, "userID:" + getUserID());
                AppUtils.showLog(TAG, "keyGenerated:" + crypto.isKeyGenerated(getUserID()));
                AppUtils.showLog(TAG, "token " + getToken());
//                Class toClass = getToken() == null || !loggedIn() ? LoginActivity.class : crypto.isKeyGenerated(getUserID()) ? EnterPasscodeActivity.class : GenerateKeyActivity.class;
                User user = getUser();
//                AppUtils.showLog(TAG, "verificationCheck: " + user.getFullName());


                //if token is null direct user to login activity
                if (getToken() == null || user == null) {
                    AppUtils.showLog(TAG, "token and user null");
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    finish();
                    return;
                }


                //check for user status for verification else direct user to enter passcode
                if (user.getUserStatus() != null) {
                    if (!(user.getUserStatus().equals(AccountProto.UserStatus.VERIFIED.toString()))) {
                        Intent intent = new Intent(SplashActivity.this, VerificationActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Intent intent = new Intent(SplashActivity.this, EnterPasscodeActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }


            }
        }, 1000);
    }
}
