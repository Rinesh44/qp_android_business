package com.yanky.savxbusiness.activities.profile;

import android.content.SharedPreferences;

public interface UserProfilePresenter {
    void fetchProfile(String token, String userId, SharedPreferences preferences);

    void deleteAddress(String token, String addressId, int pos, SharedPreferences preferences);

}
