package com.yanky.savxbusiness.activities.wallet.fund;

import android.content.SharedPreferences;

public interface LoadFundPresenter {
    void loadFund(String token, String bankAccountName, String accountNumber, String routingNumber,
                  String nameOnAccount, String accontType, String amountToLoad, String userId, String toWalletAddress,
                  String currency, SharedPreferences preferences);
}
