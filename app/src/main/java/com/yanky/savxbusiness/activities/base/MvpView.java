package com.yanky.savxbusiness.activities.base;

/**
 * Created by Leesa Shakya on 27/09/18.
 * leezshk@gmail.com
 */

public interface MvpView {
    void showKeyboard();

    void hideKeyboard();

    void showLoading();

    void hideLoading();

    void showMessage(String message);

    void openActivityOnTokenExpire();

}
