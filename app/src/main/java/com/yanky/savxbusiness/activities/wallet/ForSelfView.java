package com.yanky.savxbusiness.activities.wallet;

import com.yanky.savxbusiness.entities.WalletProto;

public interface ForSelfView {
    void createSelfWalletSuccess(WalletProto.WalletAddress newWalletAddress);

    void createSelfWalletFail(String msg);

    void keyBackupSuccess();

    void keyBackupFail(String msg);

    void transferBalanceSuccess();

    void transferBalanceFail(String msg);
}
