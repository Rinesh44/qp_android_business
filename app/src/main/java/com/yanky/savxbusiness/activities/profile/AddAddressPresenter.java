package com.yanky.savxbusiness.activities.profile;

import android.content.SharedPreferences;

public interface AddAddressPresenter {
    void uploadAddress(Boolean edit, String token, String refid, String addressType, String address1, String address2, String city, String state,
                       String zipcode, String country, String addressId, boolean isDefault, SharedPreferences preferences);

    void makeDefaultAddress(String token, String addressId, SharedPreferences preferences);

}
