package com.yanky.savxbusiness.activities.search;

import com.yanky.savxbusiness.realm.models.User;

import java.util.List;

public interface SearchUserView {

    void onSearchSuccess(List<User> userList);

    void onSearchFail(String msg);
}
