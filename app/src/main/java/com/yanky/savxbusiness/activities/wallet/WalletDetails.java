package com.yanky.savxbusiness.activities.wallet;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.scrounger.countrycurrencypicker.library.Country;
import com.yanky.savxbusiness.QuiqpayApp;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.activities.dashboard.DashboardActivity;
import com.yanky.savxbusiness.activities.wallet.fund.LoadFund;
import com.yanky.savxbusiness.activities.wallet.fund.TransferFund;
import com.yanky.savxbusiness.activities.wallet.fund.TransferToBank;
import com.yanky.savxbusiness.activities.wallet.fund.TransferToUser;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.realm.models.TransactionOnHoldHolder;
import com.yanky.savxbusiness.repo.Repo;
import com.yanky.savxbusiness.repo.TransactionRepo;
import com.yanky.savxbusiness.utils.AppForegroundListener;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.SwipeButtonListener;
import com.yanky.savxbusiness.utils.TransactionDescriptionHelper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.yanky.savxbusiness.QuiqpayApp.getMyApplication;

public class WalletDetails extends BaseActivity implements View.OnClickListener, AppForegroundListener, WalletDetailsView, SwipeButtonListener {
    private String TAG = WalletDetails.class.getSimpleName();
    @Inject
    Endpoints endpoints;
    @BindView(R.id.amount)
    TextView mAmount;
    @BindView(R.id.last_active)
    TextView mLastActive;
    @BindView(R.id.btn_load_fund)
    Button mLoadFund;
    @BindView(R.id.btn_transfer_fund)
    Button mTransferFund;
    //    @BindView(R.id.tv_transfer_fund)
//    TextView mTransferFund;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.bottom_sheet_transfer)
    LinearLayout mBottomSheet;
    @BindView(R.id.tv_bank)
    TextView mBank;
    @BindView(R.id.tv_user)
    TextView mUser;
    @BindView(R.id.tv_self)
    TextView mSelf;
    @BindView(R.id.rv_holded_transactions)
    RecyclerView mRecycler;
    @BindView(R.id.pb_loading)
    ProgressBar mLoading;
    @BindView(R.id.rv_transactions_holder)
    RelativeLayout mTransactionHolder;
    @BindView(R.id.tv_currency_in_words)
    TextView mCurrencyInWords;

    private String walletName, walletAddress;
    private double amount;
    private long lastActive;
    private String sLastActive, currency, countryCode;
    private String holdAmountCurrency;


    private BottomSheetBehavior sheetBehavior;
    private WalletDetailsPresenterImpl presenter;
    private SharedPreferences preferences;
    private List<TransactionOnHoldHolder> transactionHolderList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_details);
        ButterKnife.bind(this);

        getMyApplication(this).getAppComponent().inject(this);

        getIntentValues();
        init();

        presenter = new WalletDetailsPresenterImpl(this, endpoints);
        if (walletName.equals("ZIP Point"))
            presenter.getHoldedTransactions(getToken(), preferences);
        else mTransactionHolder.setVisibility(View.GONE);

        Country countryWithCurrency = Country.getCountryWithCurrency(countryCode, this);
        assert countryWithCurrency != null;
        String currencySymbol = Objects.requireNonNull(countryWithCurrency.getCurrency()).getSymbol();
        if (currencySymbol.length() > 1) {
            currencySymbol = currencySymbol.substring(currencySymbol.length() - 1);
        }

        StringBuilder amountBuilder = new StringBuilder();
        amountBuilder.append(currencySymbol);
        amountBuilder.append(" ");
        amountBuilder.append(String.format("%.0f", amount));
        mAmount.setText(String.valueOf(amountBuilder));


        StringBuilder currencyInWordsBuilder = new StringBuilder();
        currencyInWordsBuilder.append(countryWithCurrency.getCurrency().getName());
        currencyInWordsBuilder.append(" ");
        currencyInWordsBuilder.append("-");
        currencyInWordsBuilder.append(" ");
        currencyInWordsBuilder.append(currency);
        mCurrencyInWords.setText(currencyInWordsBuilder);

      /*  int flagId = Country.getCountry(countryCode, this).getFlagId();
        if (flagId != 0) {
            Drawable drawable = getResources().getDrawable(flagId);
            mAmount.setCompoundDrawablePadding(15);
            mAmount.setCompoundDrawablesWithIntrinsicBounds(resize(drawable), null, null, null);
        }*/

        StringBuilder lastActiveBuilder = new StringBuilder();
        lastActiveBuilder.append("Last active : ");
        lastActiveBuilder.append(sLastActive);
        mLastActive.setText(String.valueOf(lastActiveBuilder));

        mTransferFund.setOnClickListener(this);
        mLoadFund.setOnClickListener(this);
        mBank.setOnClickListener(this);
        mSelf.setOnClickListener(this);
        mUser.setOnClickListener(this);


   /*     mWithdrawFund.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(WalletDetails.this, WithdrawFund.class);
                i.putExtra("wallet_name", walletName);
                i.putExtra("wallet_amount", String.valueOf(amount));
                i.putExtra("wallet_amount", String.format("%.0f", amount));
                i.putExtra("wallet_address", String.valueOf(walletAddress));
                i.putExtra("wallet_currency", currency);
                i.putExtra("wallet_country_code", countryCode);
                startActivity(i);
            }
        });*/


    }


    private void init() {
        setUpToolbar(mToolbar);
        mToolbar.setTitleTextColor(getResources().getColor(R.color.black));

        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

        }

        mToolbarTitle.setText(walletName);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        sheetBehavior = BottomSheetBehavior.from(mBottomSheet);
    }

    void getIntentValues() {
        Intent i = getIntent();
        walletName = i.getStringExtra("name");
        amount = i.getDoubleExtra("amount", 0);
        lastActive = i.getLongExtra("last_active", 0);
        walletAddress = i.getStringExtra("wallet_address");
        countryCode = i.getStringExtra("country_code");
        currency = i.getStringExtra("currency");
        currency = i.getStringExtra("currency");

        sLastActive = getDate(lastActive);

        AppUtils.showLog(TAG, "data" + walletName + amount + "lastActive" + lastActive);

        AppUtils.showLog(TAG, "getDate:" + getDate(lastActive));

    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onAppForeground() {
        showPasscodeDialog(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        QuiqpayApp.setAppForegroundListener(this);

        if (sheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_transfer_fund:
                toggleBottomSheet();
                break;

            case R.id.btn_load_fund:
                loadFund();
                if (sheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
                break;

            case R.id.tv_bank:
                transferFundToBank();
                break;

            case R.id.tv_user:
                transferFundToUser();
                break;

            case R.id.tv_self:
                transferFundToWallet();
                break;
        }
    }

    private void transferFundToBank() {
        Intent i = new Intent(WalletDetails.this, TransferToBank.class);
        i.putExtra("wallet_name", walletName);
        i.putExtra("wallet_amount", String.format("%.0f", amount));
        i.putExtra("wallet_address", String.valueOf(walletAddress));
        i.putExtra("wallet_currency", currency);
        i.putExtra("wallet_country_code", countryCode);
        startActivity(i);
    }

    private void loadFund() {
        Intent i = new Intent(WalletDetails.this, LoadFund.class);
        i.putExtra("wallet_name", walletName);
        i.putExtra("wallet_amount", String.format("%.0f", amount));
        i.putExtra("wallet_address", String.valueOf(walletAddress));
        i.putExtra("wallet_currency", currency);
        i.putExtra("wallet_country_code", countryCode);
        startActivity(i);
    }

    private void transferFundToWallet() {
        Intent i = new Intent(WalletDetails.this, TransferFund.class);
        i.putExtra("wallet_name", walletName);
        i.putExtra("wallet_amount", String.format("%.0f", amount));
        i.putExtra("wallet_address", String.valueOf(walletAddress));
        i.putExtra("wallet_currency", currency);
        i.putExtra("wallet_country_code", countryCode);
        startActivity(i);
    }

    private void transferFundToUser() {
        Intent i = new Intent(WalletDetails.this, TransferToUser.class);
        i.putExtra("wallet_name", walletName);
        i.putExtra("wallet_amount", String.format("%.0f", amount));
        i.putExtra("wallet_address", String.valueOf(walletAddress));
        i.putExtra("wallet_currency", currency);
        i.putExtra("wallet_country_code", countryCode);
        startActivity(i);
    }


    public void toggleBottomSheet() {
        if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }

    /**
     * required to dismiss bottomsheet when touched outside
     *
     * @param event
     * @return
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (sheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {

                Rect outRect = new Rect();
                mBottomSheet.getGlobalVisibleRect(outRect);

                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY()))
                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        }

        return super.dispatchTouchEvent(event);
    }

    @Override
    public void getHoldedTransactionSuccess(List<TransactionOnHoldHolder> transactionHolders) {

        AppUtils.showLog(TAG, "fetch holded transactions success");

        TransactionRepo.getInstance().saveTransactionsOnHold(transactionHolders, new Repo.Callback() {
            @Override
            public void success(Object o) {
                AppUtils.showLog(TAG, "transaction data saved to database");
            }

            @Override
            public void fail() {
                AppUtils.showLog(TAG, "transaction data save fail");
            }
        });


        transactionHolderList = TransactionRepo.getInstance().getAllTransactionHoldersOnHold();
        mLoading.setVisibility(View.GONE);

        AppUtils.showLog(TAG, "count:" + transactionHolderList.size());

        Iterator<TransactionOnHoldHolder> iterator = transactionHolderList.iterator();
        while (iterator.hasNext()) {
            TransactionOnHoldHolder next = iterator.next();
            String status = TransactionDescriptionHelper.getStatus(next, WalletDetails.this);
            AppUtils.showLog(TAG, "status: " + status);

            if (status.equals("OUTGOING")) {
                iterator.remove();
            }
        }

        updateRecyclerView(transactionHolderList);
    }

    private void updateRecyclerView(List<TransactionOnHoldHolder> transactionHolderList) {

        AppUtils.showLog(TAG, "transactionsCount:" + transactionHolderList.size());
        mRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        if (!transactionHolderList.isEmpty()) {
            mTransactionHolder.setVisibility(View.VISIBLE);
        } else {
            mTransactionHolder.setVisibility(View.GONE);
        }
    }

    @Override
    public void getHoldedTransactionFail(String msg) {
        AppUtils.showLog(TAG, "fetch holded transactions failed");
        showMessage(msg);
    }

    @Override
    public void convertAndRedeemFail(String msg) {
        showMessage(msg);
    }

    @Override
    public void convertAndRedeemSuccess() {
        AppUtils.showLog(TAG, "convert and redeem success");
        Toast.makeText(this, "Amount added to zip point", Toast.LENGTH_SHORT).show();
        launchDashBoard();

    }

    @Override
    public void transferBalance() {

    }

    @Override
    public void accept() {

    }

    @Override
    public void reject(String requestId) {

    }

    @Override
    public void reject() {

    }

    @Override
    public void cancel(String requestId) {

    }

    @Override
    public void edit(int pos) {

    }

    @Override
    public void delete(String id, int pos) {

    }

    @Override
    public void claim(TransactionOnHoldHolder transactionOnHoldHolder) {
        presenter.convertAndRedeem(getToken(), getUserID(), transactionOnHoldHolder, preferences);
    }

    @Override
    public void createNewWallet(TransactionOnHoldHolder transactionOnHoldHolder) {
        Intent intent = new Intent(WalletDetails.this, ForSelf.class);
        intent.putExtra("fixed_currency", true);
        intent.putExtra("transaction_onhold", transactionOnHoldHolder);
        startActivity(intent);
    }


    private void launchDashBoard() {
        Intent gotoDashboard = new Intent(this, DashboardActivity.class);
        startActivity(gotoDashboard);
        finish();
    }
}
