package com.yanky.savxbusiness.activities.wallet;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.hbb20.CountryCodePicker;
import com.scrounger.countrycurrencypicker.library.Currency;
import com.yanky.savxbusiness.QuiqpayApp;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.activities.dashboard.DashboardActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.crypto.Crypto;
import com.yanky.savxbusiness.entities.WalletProto;
import com.yanky.savxbusiness.realm.models.CountryDetails;
import com.yanky.savxbusiness.realm.models.TransactionOnHoldHolder;
import com.yanky.savxbusiness.repo.CountryDetailsRepo;
import com.yanky.savxbusiness.utils.AppForegroundListener;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.Constants;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.yanky.savxbusiness.QuiqpayApp.getMyApplication;

public class ForSelf extends BaseActivity implements ForSelfView, AppForegroundListener, View.OnClickListener {
    public String TAG = ForSelf.class.getSimpleName();

    @Inject
    Endpoints endpoints;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.et_wallet_name)
    TextInputEditText mWalletName;
    @BindView(R.id.btn_create_wallet)
    Button mCreateWallet;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.wallet_country_picker)
    CountryCodePicker mCountryPicker;

    private Crypto crypto;
    private ForSelfPresenterImpl presenter;
    private SharedPreferences preferences;
    private String walletName, walletAddress, passcodeOne;
    private String currency, countryCode;
    private boolean fixedCurrency;
    private TransactionOnHoldHolder transactionOnHoldHolder;
    private long walletAmount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_for_self);
        ButterKnife.bind(this);

        //to remove keyboard glich
        getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.background));

        getMyApplication(this).getAppComponent().inject(this);
        init();

        crypto = Crypto.build();

        getIntentValues();

        mCreateWallet.setOnClickListener(this);
        setupCountryPicker();

    }

    private void getIntentValues() {
        Intent i = getIntent();
        fixedCurrency = i.getBooleanExtra("fixed_currency", false);
        transactionOnHoldHolder = i.getParcelableExtra("transaction_onhold");

    }

    private void setupCountryPicker() {
        mCountryPicker.setFlagSize(32);
        StringBuilder customCountries = new StringBuilder();
        List<CountryDetails> countryDetails = CountryDetailsRepo.getInstance().getAllCountryDetails();
        for (CountryDetails details : countryDetails
        ) {
            AppUtils.showLog(TAG, "countryCode:" + details.getCountryCode());
            customCountries.append(details.getCountryCode());
            customCountries.append(",");
            AppUtils.showLog(TAG, "countryName:" + details.getName());
        }


        customCountries.setLength(customCountries.length() - 1);
        AppUtils.showLog(TAG, "customCountries:" + customCountries.toString());
        if (customCountries.toString().contains("EU")) {

        }
        mCountryPicker.setCustomMasterCountries(customCountries.toString());
        mCountryPicker.setCountryForNameCode("US");

        if (fixedCurrency) {
            mCountryPicker.setCountryForNameCode(transactionOnHoldHolder.getFromCountryCode());
            mCountryPicker.setCcpClickable(false);
            walletAmount = Long.parseLong(transactionOnHoldHolder.getAmount());
            AppUtils.showLog(TAG, "amount: " + transactionOnHoldHolder.getAmount());
        }

        mCountryPicker.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                countryCode = mCountryPicker.getSelectedCountryNameCode();
                currency = Currency.getCurrency(mCountryPicker.getSelectedCountryNameCode(), ForSelf.this).getCode();
                AppUtils.showLog(TAG, "country: " + countryCode + "currency: " + currency);
            }
        });
    }

    private void init() {
        presenter = new ForSelfPresenterImpl(this, endpoints);

        setUpToolbar(mToolbar);
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        mToolbarTitle.setText("Wallet For Me");
    }


    public void createWallet() {
        walletName = mWalletName.getText().toString();
        walletAddress = crypto.addNewWalletAddress(getUserID());

        preferences = PreferenceManager.getDefaultSharedPreferences(ForSelf.this);

        passcodeOne = preferences.getString(Constants.PASSCODE_LONG, "");
        AppUtils.showLog(TAG, "passcodeoNE:" + passcodeOne);

        if (walletName.isEmpty()) {
            showMessage("Wallet name cannot be empty");
            return;
        }

        if (walletAddress.isEmpty()) {
            showMessage("Error creating wallet. Try again");
            return;
        }

        if (currency == null || currency.isEmpty()) {
            countryCode = mCountryPicker.getSelectedCountryNameCode();
            currency = Currency.getCurrency(mCountryPicker.getSelectedCountryNameCode(), ForSelf.this).getCode();
            AppUtils.showLog(TAG, "country: " + countryCode + "currency: " + currency);
        }

        String refId = Crypto.build().generateRefId(passcodeOne, getUserID());
        AppUtils.showLog(TAG, "refId:" + refId);

        WalletProto.KeysBackup keyBackUp = Crypto.build().keyBackUp(getUserID(), passcodeOne);

        final String pubKey = crypto.getPubKey(getUserID());
        AppUtils.showLog(TAG, "publicKey: " + pubKey);
        presenter.backUpKeys(getToken(), refId, pubKey, keyBackUp, preferences);
//                presenter.createSelfWallet(getUserID(), getToken(), walletName, walletAddress, passcodeOne);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void createSelfWalletSuccess(WalletProto.WalletAddress newWalletAddress) {
        if (fixedCurrency) {
            presenter.transferBalance(getToken(), getUserID(), transactionOnHoldHolder, newWalletAddress, preferences);
        } else {
            Toast.makeText(this, "Wallet created", Toast.LENGTH_SHORT).show();
            launchDashBoard();
        }
    }

    private void launchDashBoard() {
        Intent gotoDashboard = new Intent(ForSelf.this, DashboardActivity.class);
        startActivity(gotoDashboard);
        finish();
    }

    @Override
    public void createSelfWalletFail(String msg) {
        showMessage(msg);
    }

    @Override
    public void keyBackupSuccess() {
        AppUtils.showLog(TAG, "key backup success");
        presenter.createSelfWallet(getUserID(), getToken(), walletName, walletAddress, passcodeOne, currency,
                countryCode, walletAmount, preferences);
    }

    @Override
    public void keyBackupFail(String msg) {
        AppUtils.showLog(TAG, "key backup failed");
    }

    @Override
    public void transferBalanceSuccess() {
        AppUtils.showLog(TAG, "transferBalanceSuccess");
        launchDashBoard();
    }

    @Override
    public void transferBalanceFail(String msg) {
        showMessage(msg);
    }

    @Override
    public void onAppForeground() {
        showPasscodeDialog(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        QuiqpayApp.setAppForegroundListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_create_wallet:
                createWallet();
                break;
        }
    }
}
