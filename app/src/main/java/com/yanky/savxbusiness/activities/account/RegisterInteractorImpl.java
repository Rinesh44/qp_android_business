package com.yanky.savxbusiness.activities.account;

import android.text.TextUtils;

import com.yanky.savxbusiness.utils.ValidationUtils;

/**
 * Created by Leesa Shakya on 27/09/18.
 * leezshk@gmail.com
 */

public class RegisterInteractorImpl implements RegisterInteractor {
    @Override
    public void onRegister(String fullName, String emailPhone, String password,
                           String country, OnRegisterFinishedListener listener) {
        if (TextUtils.isEmpty(fullName)){
            listener.emptyFullName();
            return;
        }
        if (TextUtils.isEmpty(emailPhone)){
            listener.emptyEmailPhone();
            return;
        }
        if (ValidationUtils.isValidEmailPhone(emailPhone)){
            listener.invalidEmailPhone();
            return;
        }

        if (TextUtils.isEmpty(password)){
            listener.emptyPassword();
            return;
        }
        if (password.length() < 6){
            listener.invalidPassword();
            return;
        }
        if (TextUtils.isEmpty(country)){
            listener.emptyCountry();
            return;
        }
        listener.onValidationSuccess(fullName, emailPhone, password, country);
    }

    @Override
    public void validateFullName(String fullName, OnRegisterFinishedListener listener) {
        if (TextUtils.isEmpty(fullName)){
            listener.emptyFullName();
            return;
        }
        listener.validFullName();
    }

    @Override
    public void validateEmailPhone(String emailPhone, OnRegisterFinishedListener listener) {
        if (TextUtils.isEmpty(emailPhone)){
            listener.emptyEmailPhone();
            return;
        }
        if (ValidationUtils.isValidEmailPhone(emailPhone)){
            listener.invalidEmailPhone();
            return;
        }
        listener.validEmailPhone();
    }

    @Override
    public void validatePassword(String password, OnRegisterFinishedListener listener) {
        if (TextUtils.isEmpty(password)){
            listener.emptyPassword();
            return;
        }
        if (password.length() < 6){
            listener.invalidPassword();
            return;
        }
        listener.validPassword();
    }

    @Override
    public void validateCountry(String country, OnRegisterFinishedListener listener) {
        if (TextUtils.isEmpty(country)){
            listener.emptyCountry();
            return;
        }
        listener.validCountry();
    }
}