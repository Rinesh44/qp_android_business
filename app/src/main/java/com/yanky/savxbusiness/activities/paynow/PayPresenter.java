package com.yanky.savxbusiness.activities.paynow;

import android.content.SharedPreferences;

public interface PayPresenter {
    void searchUsers(String token, String charSequence, SharedPreferences preferences);

    void searchUsersByBarcode(String token, String userId, SharedPreferences preferences);

    void pay(String token, String fromUserId, String fromWalletAddress, String toUserId, String toWalletAddress,
             String amount, String desc, String fromCurrency, SharedPreferences preferences);
}
