package com.yanky.savxbusiness.activities.waitinglist;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.account.LoginActivity;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.activities.verification.VerificationActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.entities.AccountProto;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.realm.models.WaitingUser;
import com.yanky.savxbusiness.repo.Repo;
import com.yanky.savxbusiness.repo.WaitingUserRepo;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.Constants;
import com.yanky.savxbusiness.utils.ValidationUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.yanky.savxbusiness.QuiqpayApp.getMyApplication;

public class EnterCredential extends BaseActivity implements EnterCredentialView {
    private static final String TAG = "EnterCredential";
    @Inject
    Endpoints endpoints;
    @BindView(R.id.et_email_phone)
    EditText mEmailPhone;
    @BindView(R.id.btn_secure_spot)
    Button mButtonGo;
    @BindView(R.id.et_full_name)
    EditText mFullName;

    private EnterCredentialPresenterImpl presenter;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_credential);

        ButterKnife.bind(this);

        //to remove keyboard glich
        getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.background));


        getMyApplication(this).getAppComponent().inject(this);

        presenter = new EnterCredentialPresenterImpl(endpoints, this);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        mButtonGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String emailPhone = mEmailPhone.getText().toString().trim();
                if (emailPhone.isEmpty()) {
                    hideKeyboard();
                    showMessage("Please enter email/phone");
                    return;
                }

                if (mFullName.getText().toString().trim().isEmpty()) {
                    hideKeyboard();
                    showMessage("Please enter fullname");
                    return;
                }

                if (ValidationUtils.isValidEmailPhone(emailPhone)) {
                    hideKeyboard();
                    showMessage("Invalid email/phone");
                    return;
                }


                presenter.verifyCredential(getToken(), emailPhone, preferences);

            }
        });


    }

    @Override
    public void credentialSuccess() {
        AppUtils.showLog(TAG, "credential success");

        Intent i = new Intent(this, VerificationActivity.class);
        i.putExtra(Constants.IS_FOR_WAITING_USER, true);
        i.putExtra("email_phone", mEmailPhone.getText().toString().trim());
        i.putExtra("fullname", mFullName.getText().toString().trim());

        startActivity(i);

    }

    @Override
    public void credentialFail(ReqResProto.BaseResponse baseResponse) {
        AppUtils.showLog(TAG, "verify credential failed");
        if (baseResponse != null) {
            AppUtils.showLog(TAG, "base Response");
            String msg = baseResponse.getMsg();
            if (msg.equals("User already in waiting list.")) {
                presenter.fetchWaitingUser(getToken(), mEmailPhone.getText().toString().trim(), preferences);
            } else if (msg.equals("User already Registered.")) {
                Toast.makeText(this, "Your account is already registered. Please login to continue", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else showMessage(msg);
        }

    }

    @Override
    public void fetchWaitingUserSuccess(ReqResProto.BaseResponse baseResponse) {
        AppUtils.showLog(TAG, "fetch waiting user success");

        saveWaitingUser(baseResponse.getWaitingUser());
        int queuePosition = baseResponse.getWaitingUser().getQueuePosition();
        int totalWaitingUsers = baseResponse.getWaitingUser().getTotal();
        String userName = baseResponse.getWaitingUser().getName();

        Intent i = new Intent(this, RegisterSuccess.class);
        i.putExtra("queue_position", queuePosition);
        i.putExtra("total_waiting_users", totalWaitingUsers);
        i.putExtra("user_name", userName);
        startActivity(i);
    }

    private void saveWaitingUser(AccountProto.WaitingUser waitingUser) {
        WaitingUser user = new WaitingUser();
        user.setPushToken(waitingUser.getPushToken());
        user.setName(waitingUser.getName());
        user.setEmailPhone(waitingUser.getEmailPhone());
        user.setDeviceInfo(waitingUser.getDeviceInfo());
        user.setDeviceId(waitingUser.getDeviceId());

        WaitingUserRepo.getInstance().saveWaitingUser(user, new Repo.Callback() {
            @Override
            public void success(Object o) {
                AppUtils.showLog(TAG, "waiting user saved");
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("waiting_user_registered", true);
                editor.apply();
            }

            @Override
            public void fail() {
                AppUtils.showLog(TAG, "waiting user save fail");
            }
        });
    }

    @Override
    public void fetchWaitingUserFail(String msg) {
        AppUtils.showLog(TAG, "fetch waiting user fail");
        showMessage(msg);
    }


}
