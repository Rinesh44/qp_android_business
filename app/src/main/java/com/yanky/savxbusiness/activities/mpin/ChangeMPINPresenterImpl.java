package com.yanky.savxbusiness.activities.mpin;

public class ChangeMPINPresenterImpl implements ChangeMPINPresenter {
    private static final String TAG = "ChangeMPINPresenterImpl";
    private ChangeMPIN activity;

    public ChangeMPINPresenterImpl(ChangeMPIN activity) {
        this.activity = activity;
    }

    @Override
    public void changeMPIN(String oldpin, String newpin, String confirmpin, int pinLength,
                           String passcode) {
        if (oldpin.isEmpty()) {
            activity.invalidPasscode("MPIN cannot be empty");
            return;
        }

        if (!oldpin.equals(passcode)) {
            activity.invalidPasscode("Invalid old MPIN");
            return;
        }

        if (newpin.isEmpty()) {
            activity.invalidPasscode("New MPIN cannot be empty");
            return;
        }

        if (newpin.length() != pinLength) {
            activity.invalidPasscode("Please enter " + pinLength + " digit new MPIN");
        }

        if (confirmpin.isEmpty()) {
            activity.invalidPasscode("Confirm MPIN cannot be empty");
            return;
        }

        if (confirmpin.length() != pinLength) {
            activity.invalidPasscode("Please enter " + pinLength + " digit confirm MPIN");
            return;
        }

        if (!newpin.equals(confirmpin)) {
            activity.invalidPasscode("MPINs did not match");
            return;
        }

        activity.changeMPINSuccess(confirmpin);


    }
}
