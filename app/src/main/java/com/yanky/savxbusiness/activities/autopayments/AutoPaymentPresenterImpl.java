package com.yanky.savxbusiness.activities.autopayments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.yanky.savxbusiness.activities.account.LoginActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.entities.WalletProto;
import com.yanky.savxbusiness.realm.RealmDatabase;
import com.yanky.savxbusiness.realm.models.Autopayment;
import com.yanky.savxbusiness.repo.AutoPaymentRepo;
import com.yanky.savxbusiness.repo.Repo;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.CallbackWrapper;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import retrofit2.Response;

public class AutoPaymentPresenterImpl implements AutoPaymentPresenter {
    private static final String TAG = "AutoPaymentPresenterImp";
    private Endpoints endpoints;
    private AutoPayments activity;
    private RealmList<Autopayment> autopaymentRealmList = new RealmList<>();


    public AutoPaymentPresenterImpl(Endpoints endpoints, AutoPayments activity) {
        this.endpoints = endpoints;
        this.activity = activity;
    }

    @Override
    public void getAutoPayments(String token, SharedPreferences preferences) {
        endpoints.autoPayment(token).enqueue(new CallbackWrapper<>(activity, new
                CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
                    @Override
                    public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                        ReqResProto.BaseResponse baseResponse = response.body();

                        if (baseResponse == null) {
                            activity.autoPaymentFail(null);
                        }

                        if (baseResponse.getError()) {
                            if (baseResponse.getMsg().equals("Authorization failed.")) {
                                clearSharedPrefsAndRealm(preferences);
                                Intent intent = new Intent(activity, LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                activity.startActivity(intent);
                                Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                            } else activity.autoPaymentFail(baseResponse.getMsg());
                            return;
                        }

                        int count = baseResponse.getAutoPaymentsCount();
                        AppUtils.showLog(TAG, "autoPaymentLIst" + count);


                        //clear existing data if no autopayments found from server
                        if (count == 0) {
                            AutoPaymentRepo.getInstance().deleteAllAutoPayments(new Repo.Callback() {
                                @Override
                                public void success(Object o) {
                                    AppUtils.showLog(TAG, "cleared all autopayments from db");
                                    activity.updateRecycler();
                                }

                                @Override
                                public void fail() {
                                    AppUtils.showLog(TAG, "failed to clear autopayments from db");
                                }
                            });

                            return;
                        }


                        mapDataForAutoPayments(baseResponse);
                        activity.autoPaymentSuccess(autopaymentRealmList);
                    }

                    @Override
                    public void onFailureResult() {
                        activity.autoPaymentFail(null);
                    }
                }));
    }

    private void mapDataForAutoPayments(ReqResProto.BaseResponse baseResponse) {
        for (WalletProto.AutoPayment autoPaymentProto : baseResponse.getAutoPaymentsList()
        ) {

            ByteString cipher = autoPaymentProto.getInvoiceEncryption().getCipher();
            try {
                WalletProto.InvoiceHolder invoiceHolder = WalletProto.InvoiceHolder.parseFrom(cipher);
                WalletProto.Invoice invoice = WalletProto.Invoice.parseFrom(invoiceHolder.getInvoice());

                filterItems(autoPaymentProto);

                Autopayment autopayment = new Autopayment();
                autopayment.setAmount(String.valueOf(autoPaymentProto.getAmount()));
                autopayment.setBusinessUserId(autoPaymentProto.getBusinessUserId());
                autopayment.setCountryCode(autoPaymentProto.getCountryCode());
                autopayment.setCurrency(autoPaymentProto.getCurrency());
                autopayment.setDate(autoPaymentProto.getDate());
                autopayment.setDesc(autoPaymentProto.getDescription());
                autopayment.setFromUserName(autoPaymentProto.getFromUser().getFullName());
                autopayment.setFromUserId(autoPaymentProto.getFromUserId());
                autopayment.setFromWalletAddress(autoPaymentProto.getFromWalletAddress());
                autopayment.setInvoiceId(autoPaymentProto.getInvoiceId());
                autopayment.setPaymentId(autoPaymentProto.getPaymentId());
                autopayment.setSignature(autoPaymentProto.getSignature());
                autopayment.setStatus(autoPaymentProto.getStatus().getNumber());
                autopayment.setTimeStamp(autoPaymentProto.getTimestamp());
                autopayment.setToUserId(autoPaymentProto.getToUserId());
                autopayment.setToUsername(autoPaymentProto.getToUser().getFullName());
                autopayment.setToWalletAddress(autoPaymentProto.getToWalletAddress());
                autopayment.setSenderName(invoice.getSenderName());


                autopaymentRealmList.add(autopayment);
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * tallying autopayment from db and server to filter out deleted items from db
     *
     * @param autoPaymentProto
     */
    private void filterItems(WalletProto.AutoPayment autoPaymentProto) {

        String paymentId = autoPaymentProto.getPaymentId();
        List<Autopayment> dbAutopayment = AutoPaymentRepo.getInstance().getAllAutoPayments();
        for (Autopayment autoPayObjects : dbAutopayment
        ) {
            String paymentIdfromDb = autoPayObjects.getPaymentId();
            if (!paymentId.equals(paymentIdfromDb)) {
                AutoPaymentRepo.getInstance().deleteAutoPaymentsById(paymentIdfromDb, new Repo.Callback() {
                    @Override
                    public void success(Object o) {
                        AppUtils.showLog(TAG, "filter success");
                    }

                    @Override
                    public void fail() {
                        AppUtils.showLog(TAG, "filter failed");
                    }
                });
            }
        }
    }

    private void clearSharedPrefsAndRealm(SharedPreferences prefs) {
        //clear shared preferences and realm after logout
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();

        final Realm realm = RealmDatabase.getInstance().getRealm();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });
    }
}
