package com.yanky.savxbusiness.activities.account;

import android.text.TextUtils;

import com.yanky.savxbusiness.utils.ValidationUtils;

/**
 * Created by Leesa Shakya on 27/09/18.
 * leezshk@gmail.com
 */

public class LoginInteractorImpl implements LoginInteractor{

    @Override
    public void onLogin(String emailPhone, String password, OnLoginFinishedListener listener) {
        if (TextUtils.isEmpty(emailPhone)){
            listener.emptyEmailPhone();
            return;
        }
        if (ValidationUtils.isValidEmailPhone(emailPhone)){
            listener.invalidEmailPhone();
            return;
        }
        if (TextUtils.isEmpty(password)){
            listener.emptyPassword();
            return;
        }
        listener.onValidationSuccess(emailPhone, password);
    }

    @Override
    public void validateEmailPhone(String emailPhone, OnLoginFinishedListener listener) {
        if (TextUtils.isEmpty(emailPhone)){
            listener.emptyEmailPhone();
            return;
        }
        if (ValidationUtils.isValidEmailPhone(emailPhone)){
            listener.invalidEmailPhone();
            return;
        }
        listener.validEmailPhone();
    }

    @Override
    public void validatePassword(String password, OnLoginFinishedListener listener) {
        if (TextUtils.isEmpty(password)){
            listener.emptyPassword();
            return;
        }
        listener.validPassword();
    }
}