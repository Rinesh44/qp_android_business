package com.yanky.savxbusiness.activities.waitinglist;

import com.yanky.savxbusiness.entities.ReqResProto;

public interface RegisterSuccessView {
    void fetchUserSuccess(ReqResProto.BaseResponse baseResponse);

    void fetchUserFail(String msg);
}
