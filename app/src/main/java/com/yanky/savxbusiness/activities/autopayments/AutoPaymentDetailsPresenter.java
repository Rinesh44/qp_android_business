package com.yanky.savxbusiness.activities.autopayments;

import android.content.SharedPreferences;

public interface AutoPaymentDetailsPresenter {
    void deleteAutoPayment(String token, String paymentId, SharedPreferences preferences);
}
