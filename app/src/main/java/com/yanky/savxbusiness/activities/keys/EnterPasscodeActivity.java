package com.yanky.savxbusiness.activities.keys;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.account.LoginActivity;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.activities.dashboard.DashboardActivity;
import com.yanky.savxbusiness.activities.fingerprint.FingerPrintActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.crypto.Crypto;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.realm.RealmDatabase;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.Constants;
import com.yanky.savxbusiness.utils.FingerprintHandler;
import com.yanky.savxbusiness.utils.TouchIdSuccessListener;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.yanky.savxbusiness.QuiqpayApp.getMyApplication;

public class EnterPasscodeActivity extends BaseActivity implements EnterPasscodeView, TouchIdSuccessListener {
    public String TAG = EnterPasscodeActivity.class.getSimpleName();
    @Inject
    Endpoints endpoints;
    @BindView(R.id.input_layout_passcode)
    TextInputLayout mPasscodeLayout;
    @BindView(R.id.edit_text_passcode)
    TextInputEditText mPasscodeEditText;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    EnterPasscodePresenterImpl presenter;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    TextView mPasscodeTitle;
    Crypto crypto;
    boolean isKeyBackedUp;
    Dialog dialog;
    int passcodeLength;
    private PinEntryEditText mPinEntryPasscode;
    private boolean isFromLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_passcode);
        ButterKnife.bind(this);

        //to remove keyboard glich
        getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.background));

        getMyApplication(this).getAppComponent().inject(this);
        init();

        isFromLogin = getIntentValues();

        //initialize fingerprint handler
        FingerprintHandler.setTouchIdSuccessListener(this);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        AppUtils.showLog(TAG, "token:" + getToken());
        AppUtils.showLog(TAG, "userId:" + getUserID());
        isKeyBackedUp = preferences.getBoolean(Constants.IS_KEY_BACKUP, false);
        AppUtils.showLog(TAG, "isKeybackedup: " + isKeyBackedUp);
        crypto = Crypto.build();
//        getIntentValues();

        //if validated is true all the processes(verification, key generate and backup) has been completed
        // so direct user to final passcode screen
        AppUtils.showLog(TAG, "validated:" + preferences.getBoolean(Constants.VALIDATED, false));
        if (isFromLogin) {
            onPasscodeEntered();
            return;
        }
        if (preferences.getBoolean(Constants.VALIDATED, false))
            validPassCodeOne(preferences.getString(Constants.PASSCODE_LONG, ""));
        else onPasscodeEntered();

    }

    private void init() {

        setUpToolbar(mToolbar);
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle("Enter Passcode");
        }

        presenter = new EnterPasscodePresenterImpl(this, endpoints);
    }

    @Override
    public void onPasscodeEntered() {
     /*   mPasscodeEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                if (getUserID() == null) getUserID() = getUserID();
                if (charSequence.length() == 8)
                    presenter.validatePasscodeOne(isKeyBackedUp, mPasscodeEditText.getText().toString(), crypto, getUserID(), getToken());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });*/

        mPasscodeEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (motionEvent.getRawX() >= (mPasscodeEditText.getRight() - mPasscodeEditText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // do your action here
                        if (mPasscodeEditText.length() <= 3) {
                            Toast.makeText(EnterPasscodeActivity.this, "Please enter passcode greater than 3", Toast.LENGTH_SHORT).show();
                            return true;
                        }
                        presenter.validatePasscodeOne(isKeyBackedUp, mPasscodeEditText.getText().toString(), crypto, getUserID(), getToken(), preferences);

                        return true;
                    }
                }
                return false;

            }
        });

        mPasscodeEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    presenter.validatePasscodeOne(isKeyBackedUp, mPasscodeEditText.getText().toString(), crypto, getUserID(), getToken(),
                            preferences);
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void emptyPassCode() {
        mPasscodeLayout.setErrorEnabled(true);
        mPasscodeLayout.setError(getString(R.string.this_field_is_required));
        mPasscodeEditText.requestFocus();
    }

    /**
     * passcode one has been validated
     *
     * @param passcodeOne validated passcode
     */
    @Override
    public void validPassCodeOne(String passcodeOne) {

        AppUtils.showLog(TAG, "valid passcode one");
        if (preferences.getString(Constants.PASSCODE_LONG, "").isEmpty()) {
            //save eight digited passcode to preferences
            editor = preferences.edit();
            editor.putString(Constants.PASSCODE_LONG, passcodeOne);
            editor.apply();
        }


        AppUtils.showLog(TAG, "Valid PassCodeOne");


        //if touch id enabled by user use touch id else use regular passcode verification
        if (preferences.getBoolean(Constants.TOUCHID_ENABLED, false)) {
            //make fingerprint activity only one on activity stack
            Intent intent = new Intent(this, FingerPrintActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);

        } else {
            //user can choose between 4 and 6 digit passcode length in settings
            mPasscodeLayout.setErrorEnabled(false);
            String passcode = preferences.getString(Constants.PASSCODE, null);
            if (passcode != null) {
                passcodeLength = passcode.length();
            } else {
                passcodeLength = 4;
            }

            //show dialog for passcode authentication
            dialog = new Dialog(this, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
            final View dialogView = View.inflate(this, R.layout.dialog_backup_key, null);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(dialogView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            dialog.setCancelable(false);


            mPinEntryPasscode = dialog.findViewById(R.id.pin_entry_passcode);
            mPasscodeTitle = dialog.findViewById(R.id.text_view_passcode);
            mPinEntryPasscode.setMaxLength(passcodeLength);

            //if first login change dialog title to "set passcode" else "enter passcode"
            presenter.checkIfFirstLogin(preferences);


            mPinEntryPasscode.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (charSequence.length() == passcodeLength)
                        presenter.validatePasscodeTwo(mPinEntryPasscode.getText().toString(), preferences);
                }

                @Override
                public void afterTextChanged(Editable editable) {
                }
            });

            if (dialog.getWindow() != null)
                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            dialog.show();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * second passcode verification completed, direct user to dashboard
     */
    @Override
    public void validPassCodeTwo() {
        AppUtils.showLog(TAG, "validPasscodeTwo");
        editor = preferences.edit();
        editor.putBoolean(Constants.VALIDATED, true);
        editor.apply();

        startDashboardActivity();
    }

    public void startDashboardActivity() {
        Intent intent = new Intent(EnterPasscodeActivity.this, DashboardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void invalidPassCode() {
        mPasscodeLayout.setErrorEnabled(true);
        mPasscodeLayout.setError(getString(R.string.invalid_password_alt));
        mPasscodeEditText.requestFocus();
    }

    @Override
    public void invalidPassCodeTwo() {
        AppUtils.showLog(TAG, "Invalid Passcode two");
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mPinEntryPasscode.getText().clear();
                showToastMessage(getString(R.string.invalid_password_alt));
            }
        }, 500);

    }

    @Override
    public void changeTitle() {
        mPasscodeTitle.setText(getString(R.string.set_passcode));
    }


    public boolean getIntentValues() {
        Intent intent = getIntent();
        return intent.getBooleanExtra(Constants.IS_FROM_LOGIN, false);
    }

    /**
     * key backed up, save to preference
     */
    @Override
    public void keyBackupSuccess() {
        AppUtils.showLog(TAG, "key backup success");

        String walletAddress = crypto.addNewWalletAddress(getUserID());
        AppUtils.showLog(TAG, "walletAddress" + walletAddress);

        presenter.createNewWallet(getToken(), walletAddress, getUserID(), preferences);


        editor = preferences.edit();
        editor.putBoolean(Constants.IS_KEY_BACKUP, true);
        editor.apply();
    }

    @Override
    public void keyBackupFail(String message) {
        showMessage(message);
    }

    /**
     * wallet creation successful, save to preferences
     */
    @Override
    public void createWalletSuccess() {
        showMessage(getResources().getString(R.string.wallet_creation_success));
        AppUtils.showLog(TAG, "default wallet created");
    }


    @Override
    public void onTouchIDSuccess() {
        validPassCodeTwo();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dialog != null) dialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
                builder1.setMessage("Are you sure you want to logout?");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                showLoading();
                                endpoints.logout(getToken()).enqueue(new Callback<ReqResProto.BaseResponse>() {
                                    @Override
                                    public void onResponse(Call<ReqResProto.BaseResponse> call, Response<ReqResProto.BaseResponse> response) {
                                        ReqResProto.BaseResponse baseResponse = response.body();
                                        if (baseResponse == null) {
                                            logoutFail(null);
                                            hideLoading();
                                            return;
                                        }

                                        if (baseResponse.getError()) {
                                            hideLoading();
                                            logoutFail(baseResponse.getMsg());
                                            return;
                                        }


                                        logoutSuccess();
                                    }

                                    @Override
                                    public void onFailure(Call<ReqResProto.BaseResponse> call, Throwable t) {
                                        logoutFail(t.toString());
                                        hideLoading();
                                    }
                                });
                            }
                        });

                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                final AlertDialog alert11 = builder1.create();
                alert11.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialogInterface) {
                        alert11.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(
                                getResources().getColor(android.R.color.holo_blue_light));

                        alert11.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(
                                getResources().getColor(android.R.color.holo_red_light));

                    }
                });
                alert11.show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.logout, menu);
        return super.onCreateOptionsMenu(menu);
    }


    /**
     * clear shared preferences and database on logout
     */
    public void logoutSuccess() {
        AppUtils.showLog(TAG, "Logout success");

        hideLoading();
        clearSharedPrefsAndRealm();

        Intent logoutIntent = new Intent(this, LoginActivity.class);
        logoutIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(logoutIntent);
        finish();
    }


    private void clearSharedPrefsAndRealm() {
        //clear shared preferences and realm after logout
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();

        final Realm realm = RealmDatabase.getInstance().getRealm();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });
    }

    public void logoutFail(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

}
