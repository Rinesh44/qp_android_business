package com.yanky.savxbusiness.activities.waitinglist;

import com.yanky.savxbusiness.entities.ReqResProto;

public interface EnterCredentialView {

    void credentialSuccess();

    void credentialFail(ReqResProto.BaseResponse baseResponse);

    void fetchWaitingUserSuccess(ReqResProto.BaseResponse baseResponse);

    void fetchWaitingUserFail(String msg);
}
