package com.yanky.savxbusiness.activities.wallet.fund;

import android.content.SharedPreferences;

import com.yanky.savxbusiness.realm.models.Wallet;

public interface TransferFundPresenter {
    void transferBalance(String token, String walletName, String walletAmount, String Desc, Wallet wallet,
                         String fromWalletName, String fromWalletAddress, String fromUserId, String
                                 currency, SharedPreferences preferences);
}
