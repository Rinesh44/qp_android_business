package com.yanky.savxbusiness.activities.profile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.yanky.savxbusiness.activities.account.LoginActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.entities.AccountProto;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.realm.RealmDatabase;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.CallbackWrapper;

import io.realm.Realm;
import retrofit2.Response;

public class AddAddressPresenterImpl implements AddAddressPresenter {
    private String TAG = AddAddressPresenterImpl.class.getSimpleName();
    private AddAddress activity;
    private Endpoints endpoints;

    public AddAddressPresenterImpl(AddAddress activity, Endpoints endpoints) {
        this.activity = activity;
        this.endpoints = endpoints;
    }

    @Override
    public void uploadAddress(Boolean edit, String token, String refId, String addressType, String address1, String address2, String city,
                              String state, String zipCode, String country, String addressId, boolean isDefault, SharedPreferences preferences) {
        activity.showLoading();
        AccountProto.AddressType type = null;
        switch (addressType) {
            case "Home":
                type = AccountProto.AddressType.HOME;
                break;
            case "Work":
                type = AccountProto.AddressType.WORK;
                break;
            case "Billing":
                type = AccountProto.AddressType.BILLING;
                break;
            case "Shipping":
                type = AccountProto.AddressType.SHIPPING;
                break;
        }

        if (edit) {
            final AccountProto.Address address = AccountProto.Address.newBuilder()
                    .setAddressId(addressId)
                    .setRefId(refId)
                    .setType(type)
                    .setIsDefault(isDefault)
                    .setLine1(address1)
                    .setLine2(address2)
                    .setCity(city)
                    .setState(state)
                    .setZipCode(zipCode)
                    .setCountry(country)
                    .build();


            endpoints.editAddress(token, address).enqueue(new CallbackWrapper<>(activity, new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
                @Override
                public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                    activity.hideLoading();
                    ReqResProto.BaseResponse baseResponse = response.body();
                    if (baseResponse == null) {
                        activity.uploadAddressFail(null);
                        return;
                    }


                    if (baseResponse.getError()) {
                        if (baseResponse.getMsg().equals("Authorization failed.")) {
                            clearSharedPrefsAndRealm(preferences);
                            Intent intent = new Intent(activity, LoginActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            activity.startActivity(intent);
                            Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                        } else activity.uploadAddressFail(baseResponse.getMsg());
                        return;
                    }

                    activity.uploadAddressSuccess();
                }

                @Override
                public void onFailureResult() {
                    activity.hideLoading();
                    activity.uploadAddressFail(null);
                }


            }));
        } else {
            final AccountProto.Address address = AccountProto.Address.newBuilder()
                    .setRefId(refId)
                    .setType(type)
                    .setLine1(address1)
                    .setLine2(address2)
                    .setIsDefault(isDefault)
                    .setCity(city)
                    .setState(state)
                    .setZipCode(zipCode)
                    .setCountry(country)
                    .build();

            endpoints.uploadAddress(token, address).enqueue(new CallbackWrapper<>(activity, new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
                @Override
                public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                    activity.hideLoading();
                    ReqResProto.BaseResponse baseResponse = response.body();
                    if (baseResponse == null) {
                        activity.uploadAddressFail(null);
                        return;
                    }

                    if (baseResponse.getError()) {
                        clearSharedPrefsAndRealm(preferences);
                        if (baseResponse.getMsg().equals("Authorization failed.")) {
                            clearSharedPrefsAndRealm(preferences);
                            Intent intent = new Intent(activity, LoginActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            activity.startActivity(intent);
                            Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                        } else activity.uploadAddressFail(baseResponse.getMsg());
                        return;
                    }

                    activity.uploadAddressSuccess();
                }

                @Override
                public void onFailureResult() {
                    activity.hideLoading();
                    activity.uploadAddressFail(null);
                }


            }));
        }

    }

    @Override
    public void makeDefaultAddress(String token, String addressId, SharedPreferences preferences) {
        AppUtils.showLog(TAG, "token:" + token + "addressId:" + addressId);
        endpoints.defaultAddress(token, addressId).enqueue(new CallbackWrapper<>(activity, new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
            @Override
            public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                ReqResProto.BaseResponse baseResponse = response.body();
                if (baseResponse == null) {
                    activity.makeDefaultAddressFail(null);
                    return;
                }

                if (baseResponse.getError()) {
                    if (baseResponse.getMsg().equals("Authorization failed.")) {
                        clearSharedPrefsAndRealm(preferences);
                        Intent intent = new Intent(activity, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        activity.startActivity(intent);
                        Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                    } else activity.makeDefaultAddressFail(baseResponse.getMsg());
                    return;
                }

                activity.makeDefaultAddressSuccess();
            }

            @Override
            public void onFailureResult() {
                activity.makeDefaultAddressFail(null);
            }


        }));
    }


    private void clearSharedPrefsAndRealm(SharedPreferences prefs) {
        //clear shared preferences and realm after logout
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();

        final Realm realm = RealmDatabase.getInstance().getRealm();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });
    }


}
