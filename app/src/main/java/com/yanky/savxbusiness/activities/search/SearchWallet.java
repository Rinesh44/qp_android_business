package com.yanky.savxbusiness.activities.search;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.yanky.savxbusiness.QuiqpayApp;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.adapters.WalletAdapter;
import com.yanky.savxbusiness.realm.models.Wallet;
import com.yanky.savxbusiness.repo.WalletsRepo;
import com.yanky.savxbusiness.utils.AppForegroundListener;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.RecyclerViewClickListener;
import com.yanky.savxbusiness.utils.SendSelectedWallet;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchWallet extends BaseActivity implements AppForegroundListener, RecyclerViewClickListener {
    private static final String TAG = "SearchWallet";
    /*    @BindView(R.id.tabs)
        TabLayout mTabLayout;
        @BindView(R.id.viewpager)
        ViewPager mViewPager;*/
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.et_search)
    EditText mSearch;
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    private WalletAdapter mAdapter;


    private List<Wallet> mWalletList = new ArrayList<>();

    public static SendSelectedWallet sendSelectedWallet;
    public static String walletAddress;

    public static void setSendSelectedWalletListener(SendSelectedWallet listener) {
        sendSelectedWallet = listener;

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_wallet);

        ButterKnife.bind(this);

        init();
//        addTabs(mViewPager);
//        mTabLayout.setupWithViewPager(mViewPager);

        walletAddress = getIntentValues();

        setUpRecyclerView();

        mSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(final CharSequence charSequence, int i, int i1, int i2) {
                mAdapter.getFilter().filter(charSequence.toString());

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    public String getIntentValues() {
        Intent intent = getIntent();
        return intent == null ? null : intent.getStringExtra("wallet_address");
    }

    private void init() {
        setUpToolbar(mToolbar);
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        mToolbarTitle.setText("Select Wallet");
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

   /* private void addTabs(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new MyWallets(), "My Wallets");
        adapter.addFrag(new Contacts(), "Contacts");
        viewPager.setAdapter(adapter);
    }*/


    @Override
    public void onAppForeground() {
        showPasscodeDialog(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        QuiqpayApp.setAppForegroundListener(this);
    }

    @Override
    public void onClick(View view, int position) {
        Wallet wallet = mWalletList.get(position);
        AppUtils.showLog(TAG, "walletName:" + wallet.getWalletName());
        if (sendSelectedWallet != null) sendSelectedWallet.getWallet(wallet);
        finish();
    }

  /*  class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }*/


    private void setUpRecyclerView() {
        mWalletList = WalletsRepo.getInstance().getWalletsByMe();
        Iterator<Wallet> iter = mWalletList.iterator();

        while (iter.hasNext()) {
            Wallet wallet = iter.next();

            if (SearchWallet.walletAddress != null) {
                if (walletAddress.equalsIgnoreCase(wallet.getWalletAddress())) {
                    iter.remove();
                }
            } else {

            }
        }

        if (!mWalletList.isEmpty()) {
            mAdapter = new WalletAdapter(this, mWalletList);
            mAdapter.setClickListener(this);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mRecyclerView.setAdapter(mAdapter);

        }
    }
}
