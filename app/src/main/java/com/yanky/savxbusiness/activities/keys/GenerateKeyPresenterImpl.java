package com.yanky.savxbusiness.activities.keys;

import android.text.TextUtils;

import com.yanky.savxbusiness.crypto.Crypto;

/**
 * Created by Leesa Shakya on 08/10/18.
 * leezshk@gmail.com
 */

public class GenerateKeyPresenterImpl implements GenerateKeyPresenter {
    private final GenerateKeyActivity activity;

    GenerateKeyPresenterImpl(GenerateKeyActivity activity) {
        this.activity = activity;
    }

    @Override
    public void validatePassCode(String passCode) {
        if (TextUtils.isEmpty(passCode)) {
            activity.emptyPassCode();
            return;
        }
        if (passCode.length() < 6){
            activity.invalidPassCode();
            return;
        }

        activity.validPassCode();
    }

    @Override
    public void onBackupKey(String userId, String passCode, Crypto crypto) {
        if (TextUtils.isEmpty(passCode)) {
            activity.emptyPassCode();
            return;
        }
        if (passCode.length() < 6){
            activity.invalidPassCode();
            return;
        }

//        File file = crypto.keyBackUp(userId, passCode);

//        QuiqPayPb.KeysBackup.newBuilder().setEncryptedKeys();
//        call api and save to realm
    }
}
