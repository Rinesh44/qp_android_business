package com.yanky.savxbusiness.activities.wallet.fund;

import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.yanky.savxbusiness.activities.account.LoginActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.entities.TxProto;
import com.yanky.savxbusiness.realm.RealmDatabase;
import com.yanky.savxbusiness.utils.CallbackWrapper;

import io.realm.Realm;
import retrofit2.Response;

public class LoadFundPresenterImpl implements LoadFundPresenter {
    private Endpoints endpoints;
    private LoadFund activity;


    public LoadFundPresenterImpl(Endpoints endpoints, LoadFund activity) {
        this.endpoints = endpoints;
        this.activity = activity;
    }


    @Override
    public void loadFund(String token, String bankAccountName, String accountNumber, String routingNumber,
                         String nameOnAccount, String accountType, String amountToLoad, String userId,
                         String toWalletAddress, String currency, SharedPreferences preferences) {
        activity.showLoading();
        TxProto.BankAcType bankAcType = null;
        switch (accountType) {
            case "Consumer Checking":
                bankAcType = TxProto.BankAcType.CONSUMER_CHECKING;
                break;

            case "Consumer Savings":
                bankAcType = TxProto.BankAcType.CONSUMER_SAVINGS;
                break;

            case "Business Checking":
                bankAcType = TxProto.BankAcType.BUSINESS_CHECKING;
                break;

            case "Business Savings":
                bankAcType = TxProto.BankAcType.BUSINESS_SAVINGS;
                break;
        }


        TxProto.Ach ach = TxProto.Ach.newBuilder()
                .setAccountNumber(accountNumber)
                .setAccountType(bankAcType)
                .setBankAcName(bankAccountName)
                .setRoutingNumber(routingNumber)
                .setNameOnAccount(nameOnAccount)
                .build();
        TxProto.AchLoad achLoad = TxProto.AchLoad.newBuilder()
                .setAch(ach)
                .setCurrency(currency)
                .setUserId(userId)
                .setAmount(Long.valueOf(amountToLoad) * 100)
                .setToWalletAddress(toWalletAddress)
                .build();
        endpoints.loadFund(token, achLoad).enqueue(new CallbackWrapper<>(activity, new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
            @Override
            public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                activity.hideLoading();
                ReqResProto.BaseResponse baseResponse = response.body();

                if (baseResponse == null) {
                    activity.loadFundFail(null);
                }

                if (baseResponse.getError()) {
                    if (baseResponse.getMsg().equals("Authorization failed.")) {
                        clearSharedPrefsAndRealm(preferences);
                        Intent intent = new Intent(activity, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        activity.startActivity(intent);
                        Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                    } else activity.loadFundFail(baseResponse.getMsg());
                    return;
                }

                activity.loadFundSuccess();
            }

            @Override
            public void onFailureResult() {
                activity.hideLoading();
                activity.loadFundFail(null);

            }
        }));
    }

    private void clearSharedPrefsAndRealm(SharedPreferences prefs) {
        //clear shared preferences and realm after logout
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();

        final Realm realm = RealmDatabase.getInstance().getRealm();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });
    }
}
