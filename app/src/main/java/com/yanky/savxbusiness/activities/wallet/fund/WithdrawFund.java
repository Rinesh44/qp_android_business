package com.yanky.savxbusiness.activities.wallet.fund;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.scrounger.countrycurrencypicker.library.Country;
import com.yanky.savxbusiness.QuiqpayApp;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.utils.AppForegroundListener;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WithdrawFund extends BaseActivity implements AppForegroundListener {
    @BindView(R.id.walletname)
    TextView mWalletName;
    @BindView(R.id.toolbar_title)
    TextView mTitle;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.amount)
    TextView mWalletAmount;
    @BindView(R.id.sp_account_type)
    Spinner mAccountType;

    private String walletName, walletAmount, walletCurrency, walletCountryCode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdraw_fund);
        ButterKnife.bind(this);

        setUpToolbar(mToolbar);
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        String[] items = new String[]{"Select A/C Type", "Consumer Checking", "Consumer Savings", "Business Checking", "Business Savings"};
        ArrayAdapter<String> accountTypeAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,
                items);
        accountTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mAccountType.setAdapter(accountTypeAdapter);

        getIntentValues();
        mTitle.setText("With Draw to Bank");
        mWalletName.setText(walletName);
        StringBuilder amountBuilder = new StringBuilder();
        amountBuilder.append(walletCurrency);
        amountBuilder.append(" ");
        amountBuilder.append(walletAmount);
        mWalletAmount.setText(amountBuilder);

        int flagId = Country.getCountry(walletCountryCode, this).getFlagId();
        if (flagId != 0) {
            Drawable drawable = getResources().getDrawable(flagId);
            mWalletAmount.setCompoundDrawablePadding(20);
            mWalletAmount.setCompoundDrawablesWithIntrinsicBounds(resize(drawable), null, null, null);
        }

    }

    public void getIntentValues() {
        Intent i = getIntent();
        walletName = i.getStringExtra("wallet_name");
        walletAmount = i.getStringExtra("wallet_amount");
        walletCountryCode = i.getStringExtra("wallet_country_code");
        walletCurrency = i.getStringExtra("wallet_currency");
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onAppForeground() {
        showPasscodeDialog(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        QuiqpayApp.setAppForegroundListener(this);
    }
}
