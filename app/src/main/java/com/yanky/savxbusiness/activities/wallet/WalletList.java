package com.yanky.savxbusiness.activities.wallet;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.yanky.savxbusiness.QuiqpayApp;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.activities.wallet.fund.TransferFund;
import com.yanky.savxbusiness.adapters.WalletAdapter;
import com.yanky.savxbusiness.realm.models.Wallet;
import com.yanky.savxbusiness.repo.WalletsRepo;
import com.yanky.savxbusiness.utils.AppForegroundListener;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.RecyclerViewClickListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WalletList extends BaseActivity implements RecyclerViewClickListener, AppForegroundListener {
    private static final String TAG = "WalletList";
    @BindView(R.id.rv_wallet_list)
    RecyclerView mRecyclerView;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;

    private WalletAdapter mWalletAdapter;
    List<Wallet> walletList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_list);

        ButterKnife.bind(this);

        init();

        walletList = WalletsRepo.getInstance().getAllWallets();

        AppUtils.showLog(TAG, "walletLIst:" + walletList.size());
        mWalletAdapter = new WalletAdapter(this, walletList);
        mWalletAdapter.setClickListener(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mWalletAdapter);

    }


    public void init() {
        setUpToolbar(mToolbar);
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        mToolbarTitle.setText("Select Wallet");
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onClick(View view, int position) {
        AppUtils.showLog(TAG, "onclick()");
        Wallet wallet = walletList.get(position);
        Intent intent = new Intent(this, TransferFund.class);
        intent.putExtra("wallet_name", wallet.getWalletName());
        intent.putExtra("wallet_address", wallet.getWalletAddress());
        intent.putExtra("wallet_amount", String.valueOf(wallet.getAmount()));
        startActivity(intent);
    }

    @Override
    public void onAppForeground() {
        showPasscodeDialog(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        QuiqpayApp.setAppForegroundListener(this);
    }
}
