package com.yanky.savxbusiness.activities.account;

import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.entities.AccountProto;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.CallbackWrapper;

import retrofit2.Response;

/**
 * Created by Leesa Shakya on 27/09/18.
 * leezshk@gmail.com
 */

public class RegisterPresenterImpl implements RegisterPresenter,
        RegisterInteractor.OnRegisterFinishedListener {
    private static final String TAG = "RegisterPresenterImpl";
    private final RegisterActivity activity;
    private final Endpoints endpoints;
    private final RegisterInteractorImpl interactor;

    RegisterPresenterImpl(RegisterActivity activity, Endpoints endpoints,
                          RegisterInteractorImpl interactor) {
        this.activity = activity;
        this.endpoints = endpoints;
        this.interactor = interactor;
    }

    @Override
    public void validateFullName(String fullName) {
        interactor.validateFullName(fullName, this);
    }

    @Override
    public void validateEmailPhone(String emailPhone) {
        interactor.validateEmailPhone(emailPhone, this);
    }

    @Override
    public void validatePassword(String password) {
        interactor.validatePassword(password, this);
    }

    @Override
    public void validateCountry(String country) {
        interactor.validateCountry(country, this);
    }

    @Override
    public void onRegister(String fullName, String emailPhone, String password,
                           String country) {
        interactor.onRegister(fullName, emailPhone, password, country, this);
    }

    @Override
    public void emptyFullName() {
        activity.emptyFullName();
    }

    @Override
    public void validFullName() {
        activity.validFullName();
    }

    @Override
    public void emptyEmailPhone() {
        activity.emptyEmailPhone();
    }

    @Override
    public void validEmailPhone() {
        activity.validEmailPhone();
    }

    @Override
    public void invalidEmailPhone() {
        activity.invalidEmailPhone();
    }

    @Override
    public void emptyPassword() {
        activity.emptyPassword();
    }

    @Override
    public void validPassword() {
        activity.validPassword();
    }

    @Override
    public void invalidPassword() {
        activity.invalidPassword();
    }

    @Override
    public void emptyCountry() {
        activity.emptyCountry();
    }

    @Override
    public void validCountry() {
        activity.validCountry();
    }

    @Override
    public void onValidationSuccess(String fullName, String emailPhone,
                                    String password, String country) {
        activity.showLoading();
        AccountProto.User user = AccountProto.User.newBuilder()
                .setFullName(fullName)
                .setEmailPhone(emailPhone)
                .setCountry(country)
                .build();

        AccountProto.UserRegister userRegister = AccountProto.UserRegister.newBuilder()
                .setUser(user)
                .setPassword(password)
                .build();

        endpoints.register(userRegister).enqueue(new CallbackWrapper<>(activity,
                new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
                    @Override
                    public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                        activity.hideLoading();
                        ReqResProto.BaseResponse baseResponse = response.body();
                        AppUtils.showLog(TAG, "baseResponse: " + baseResponse);
                        if (baseResponse == null) {
                            activity.registerFailure(null);
                            return;
                        }
                        if (baseResponse.getError()) {
                            activity.registerFailure(baseResponse.getMsg());
                            return;
                        }
                        activity.registerSuccess(baseResponse.getUser());
                    }

                    @Override
                    public void onFailureResult() {
                        activity.hideLoading();
                        activity.registerFailure(null);
                    }
                }));
    }
}
