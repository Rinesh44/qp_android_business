package com.yanky.savxbusiness.activities.wallet;

import android.content.SharedPreferences;

import com.yanky.savxbusiness.realm.models.TransactionOnHoldHolder;

public interface WalletDetailsPresenter {

    void getHoldedTransactions(String token, SharedPreferences preferences);

    void convertAndRedeem(String token, String userId, TransactionOnHoldHolder transactionOnHoldHolder, SharedPreferences preferences);
}
