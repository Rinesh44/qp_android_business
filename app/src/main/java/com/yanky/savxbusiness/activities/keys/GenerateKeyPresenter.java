package com.yanky.savxbusiness.activities.keys;

import com.yanky.savxbusiness.crypto.Crypto;

/**
 * Created by Leesa Shakya on 08/10/18.
 * leezshk@gmail.com
 */

public interface GenerateKeyPresenter {
    void validatePassCode(String passCode);
    void onBackupKey(String userId, String passCode, Crypto crypto);
}
