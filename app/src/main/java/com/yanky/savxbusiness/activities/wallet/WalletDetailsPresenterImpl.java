package com.yanky.savxbusiness.activities.wallet;

import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.google.protobuf.InvalidProtocolBufferException;
import com.yanky.savxbusiness.activities.account.LoginActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.crypto.Crypto;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.entities.TxProto;
import com.yanky.savxbusiness.realm.RealmDatabase;
import com.yanky.savxbusiness.realm.models.TransactionOnHoldHolder;
import com.yanky.savxbusiness.realm.models.User;
import com.yanky.savxbusiness.realm.models.Wallet;
import com.yanky.savxbusiness.repo.Repo;
import com.yanky.savxbusiness.repo.TransactionRepo;
import com.yanky.savxbusiness.repo.UserRepo;
import com.yanky.savxbusiness.repo.WalletsRepo;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.CallbackWrapper;

import org.libsodium.jni.encoders.Hex;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import retrofit2.Response;

public class WalletDetailsPresenterImpl implements WalletDetailsPresenter {
    private static final String TAG = "WalletDetailsPresenterI";
    private WalletDetails activity;
    private Endpoints endpoints;

    private List<TransactionOnHoldHolder> transactionHolderList = new ArrayList<>();


    public WalletDetailsPresenterImpl(WalletDetails activity, Endpoints endpoints) {
        this.activity = activity;
        this.endpoints = endpoints;
    }


    @Override
    public void getHoldedTransactions(String token, SharedPreferences preferences) {

        TxProto.TransactionGetRequest transactionGetRequest = TxProto.TransactionGetRequest.newBuilder()
                .setToTime(0)
                .setFromTime(0)
                .build();


        endpoints.getTransactionsOnHold(token, transactionGetRequest).enqueue(new CallbackWrapper<>(activity, new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
            @Override
            public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                ReqResProto.BaseResponse baseResponse = response.body();
                AppUtils.showLog(TAG, "baseResponse: " + baseResponse);

                if (baseResponse == null) {
                    AppUtils.showLog(TAG, "baseResponse is null");
                    activity.getHoldedTransactionFail(null);
                    return;
                }

                if (baseResponse.getError()) {
                    if (baseResponse.getMsg().equals("Authorization failed.")) {
                        clearSharedPrefsAndRealm(preferences);
                        Intent intent = new Intent(activity, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        activity.startActivity(intent);
                        Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                    } else activity.getHoldedTransactionFail(baseResponse.getMsg());
                } else {
                    AppUtils.showLog(TAG, "holded transactions count: " + baseResponse.getTransactionHoldersCount());
                    mapTransactions(baseResponse.getTransactionHoldersList());
                    activity.getHoldedTransactionSuccess(transactionHolderList);
                }
            }

            @Override
            public void onFailureResult() {
                activity.getHoldedTransactionFail(null);
            }
        }));
    }


    @Override
    public void convertAndRedeem(String token, String userId, TransactionOnHoldHolder transactionOnHoldHolder, SharedPreferences preferences) {

        activity.showLoading();

        Wallet defaultWallet = WalletsRepo.getInstance().getDefaultWallet();
        User user = UserRepo.getInstance().getUserFromUserId(userId);

        TxProto.Transaction transaction = TxProto.Transaction.newBuilder()
                .setAmount(Long.valueOf(transactionOnHoldHolder.getAmount()))
                .setFromPubKey(defaultWallet.getWalletAddress())
                .setFromUserId(userId)
                .setToUserId(userId)
                .setCurrency(transactionOnHoldHolder.getFromCurrency())
                .setToPubKey(defaultWallet.getWalletAddress())
                .setMemo("Converting")
                .build();


        byte[] dataToSign = transaction.toByteArray();
        AppUtils.showLog(TAG, "dataToSign" + dataToSign.toString());

        byte[] signature = Crypto.build().sign(dataToSign, userId, defaultWallet.getWalletAddress());

        if (signature == null) {
            activity.hideLoading();
            activity.showMessage("Signature cannot be generated");
            return;
        }

        String signatureString = Hex.HEX.encode(signature);
        AppUtils.showLog(TAG, "signature" + signatureString);

        TxProto.TransactionHolder transactionHolder = TxProto.TransactionHolder.newBuilder()
                .setTransaction(transaction.toByteString())
                .setSignature(signatureString)
                .setRefId(transactionOnHoldHolder.getRefId())
                .setTransactionType(TxProto.TransactionType.FUNDTRANSFER)
                .setFromUserId(userId)
                .setFromUserFullName(user.getFullName())
                .setToUserFullName(user.getFullName())
                .setFromCurrency(transactionOnHoldHolder.getFromCurrency())
                .setToCurrency(defaultWallet.getCurrency())
                .setToUserId(userId)
                .setSignaturePubKey(defaultWallet.getWalletAddress())
                .setToWalletAddress(defaultWallet.getWalletAddress())
                .build();

        endpoints.releaseFund(token, transactionHolder).enqueue(new CallbackWrapper<>(activity, new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
            @Override
            public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                ReqResProto.BaseResponse baseResponse = response.body();

                if (baseResponse == null) {
                    activity.convertAndRedeemFail(null);
                    activity.hideLoading();
                    return;
                }

                if (baseResponse.getError()) {
                    activity.hideLoading();
                    if (baseResponse.getMsg().equals("Authorization failed.")) {
                        clearSharedPrefsAndRealm(preferences);
                        Intent intent = new Intent(activity, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        activity.startActivity(intent);
                        Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                    } else activity.convertAndRedeemFail(baseResponse.getMsg());
                    return;
                }

                activity.convertAndRedeemSuccess();
            }

            @Override
            public void onFailureResult() {
                activity.hideLoading();
                activity.convertAndRedeemFail(null);
            }
        }));
    }

    private void mapTransactions(List<TxProto.TransactionHolder> transactionHoldersList) {
        for (TxProto.TransactionHolder transactionHolder : transactionHoldersList
        ) {
            String accountType = null;
            String transactionType = null;
            switch (transactionHolder.getAch().getAccountType().getNumber()) {
                case -1:
                    accountType = "Unrecognized";
                    break;

                case 0:
                    accountType = "Unknown Account Type";
                    break;

                case 1:
                    accountType = "Consumer Checking";
                    break;

                case 2:
                    accountType = "Consumer Savings";
                    break;

                case 3:
                    accountType = "Business Checking";
                    break;

                case 4:
                    accountType = "Business Business Savings";
                    break;

            }

            switch (transactionHolder.getTransactionType().getNumber()) {
                case -1:
                    transactionType = "Unrecognized";
                    break;

                case 0:
                    transactionType = "Unknown";

                case 1:
                    transactionType = "Invoice";
                    break;

                case 2:
                    transactionType = "Wallet Request";
                    break;

                case 3:
                    transactionType = "Request Fund";
                    break;

                case 4:
                    transactionType = "Load Fund";
                    break;

                case 5:
                    transactionType = "Withdraw Fund";
                    break;

                case 6:
                    transactionType = "Fund Transfer";
                    break;
            }


            try {

                filterItems(transactionHolder);

                TxProto.Transaction transaction = TxProto.Transaction.parseFrom(transactionHolder.getTransaction());
                TransactionOnHoldHolder transactionHolders = new TransactionOnHoldHolder(transactionHolder.getTransactionId()
                        , transactionHolder.getSignature(), transactionHolder.getRefId(), transactionHolder.getFromUserId(),
                        transactionHolder.getToUserId(), transactionHolder.getFromUserFullName(),
                        transactionHolder.getFromUserProfilePicUrl(), transactionHolder.getToUserFullName(),
                        transactionHolder.getToUserProfilePicUrl(), transactionHolder.getSignaturePubKey(),
                        transactionHolder.getToWalletAddress(), transactionHolder.getSourceId(), transactionHolder.getWalletName(),
                        transactionHolder.getMcc(), transactionHolder.getAch().getAccountNumber(), accountType, transactionHolder.getAch().getBankAcName(),
                        transactionHolder.getAch().getNameOnAccount(), transactionHolder.getAch().getRoutingNumber(),
                        transactionType, transactionHolder.getTimestamp(), String.valueOf(transaction.getAmount()),
                        transaction.getToPubKey(), transaction.getFromPubKey(),
                        transactionHolder.getToCurrency(), transactionHolder.getFromCurrency(),
                        transactionHolder.getFromCountryCode(), transactionHolder.getToCountryCode());


                transactionHolderList.add(transactionHolders);

                AppUtils.showLog(TAG, "TRANSACTIONS SIZE: " + transactionHolderList.size());

            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
            }
        }
    }


    private void clearSharedPrefsAndRealm(SharedPreferences prefs) {
        //clear shared preferences and realm after logout
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();

        final Realm realm = RealmDatabase.getInstance().getRealm();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });
    }


    private void filterItems(TxProto.TransactionHolder transactionHolder) {
        String transactionId = transactionHolder.getTransactionId();
        List<TransactionOnHoldHolder> dbTransaction = TransactionRepo.getInstance().getAllTransactionHoldersOnHold();
        for (TransactionOnHoldHolder transactionOnHoldHolder : dbTransaction
        ) {
            String transactionIdFromDb = transactionOnHoldHolder.getTransactionId();
            if (!transactionId.equals(transactionIdFromDb)) {
                TransactionRepo.getInstance().deleteTransactionOnHoldById(transactionIdFromDb, new Repo.Callback() {
                    @Override
                    public void success(Object o) {
                        AppUtils.showLog(TAG, "filter success");
                    }

                    @Override
                    public void fail() {
                        AppUtils.showLog(TAG, "filter failed");
                    }
                });
            }
        }

    }
}
