package com.yanky.savxbusiness.activities.invoice;

public interface AutoPaymentDialogView {
    void autoPaySuccess();

    void autoPayFail(String msg);
}
