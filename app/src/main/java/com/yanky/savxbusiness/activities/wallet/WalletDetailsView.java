package com.yanky.savxbusiness.activities.wallet;

import com.yanky.savxbusiness.realm.models.TransactionOnHoldHolder;

import java.util.List;

public interface WalletDetailsView {

    void getHoldedTransactionSuccess(List<TransactionOnHoldHolder> transactionHolderList);

    void getHoldedTransactionFail(String msg);

    void convertAndRedeemFail(String msg);

    void convertAndRedeemSuccess();

}
