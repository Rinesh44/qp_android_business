package com.yanky.savxbusiness.activities.account.password;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.view.View;

import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.entities.AccountProto;
import com.yanky.savxbusiness.utils.AppUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;

import static com.yanky.savxbusiness.QuiqpayApp.getMyApplication;

/**
 * Created by Leesa Shakya on 02/10/18.
 * leezshk@gmail.com
 */

public class RequestResetActivity extends BaseActivity implements RequestResetView {
    private static final String TAG = "RequestResetActivity";
    @Inject
    Endpoints endpoints;
    @BindView(R.id.edit_text_email_phone)
    TextInputEditText mEmailPhoneEditText;
    @BindView(R.id.input_layout_email_phone)
    TextInputLayout mEmailPhoneLayout;
    RequestResetPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_reset);
        ButterKnife.bind(this);

        getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.background));

        getMyApplication(this).getAppComponent().inject(this);
        presenter = new RequestResetPresenterImpl(this, endpoints);

    }

    @OnTextChanged(R.id.edit_text_email_phone)
    void validateEmailPhone(Editable editable) {
        presenter.validateEmailPhone(editable.toString());
    }

    public void onRequestResetPassword(View view) {
        String emailPhone = mEmailPhoneEditText.getText().toString();
        presenter.requestResetPassword(emailPhone);
    }

    @Override
    public void emptyEmailPhone() {
        mEmailPhoneLayout.setErrorEnabled(true);
        mEmailPhoneLayout.setError(getString(R.string.this_field_is_required));
        mEmailPhoneEditText.requestFocus();
    }

    @Override
    public void validEmailPhone() {
        mEmailPhoneLayout.setErrorEnabled(false);
        mEmailPhoneLayout.setError(null);
    }

    @Override
    public void invalidEmailPhone() {
        mEmailPhoneLayout.setErrorEnabled(true);
        mEmailPhoneLayout.setError(getString(R.string.invalid_email_phone));
        mEmailPhoneEditText.requestFocus();
    }

    @Override
    public void onRequestSuccess(AccountProto.User user) {
        startActivity(new Intent(RequestResetActivity.this, ResetTokenActivity.class)
                .putExtra("USER_ID", user.getUserId()));
        AppUtils.showLog(TAG, "userId: " + user.getUserId());
        finish();
    }

    @Override
    public void onRequestFailure(String message) {
        showMessage(message);
    }
}
