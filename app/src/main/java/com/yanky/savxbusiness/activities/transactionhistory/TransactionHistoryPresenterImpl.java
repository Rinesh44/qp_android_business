package com.yanky.savxbusiness.activities.transactionhistory;

import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.google.protobuf.InvalidProtocolBufferException;
import com.yanky.savxbusiness.activities.account.LoginActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.entities.TxProto;
import com.yanky.savxbusiness.realm.RealmDatabase;
import com.yanky.savxbusiness.realm.models.TransactionHolder;
import com.yanky.savxbusiness.utils.CallbackWrapper;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import retrofit2.Response;

public class TransactionHistoryPresenterImpl implements TransactionHistoryPresenter {
    private static final String TAG = "TransactionHistoryPrese";
    private Endpoints endpoints;
    private TransactionHistory activity;
    private List<TransactionHolder> transactionHolderList = new ArrayList<>();


    public TransactionHistoryPresenterImpl(Endpoints endpoints, TransactionHistory activity) {
        this.endpoints = endpoints;
        this.activity = activity;
    }

    @Override
    public void getTrasactionHistory(String token, SharedPreferences preferences) {

        final TxProto.TransactionGetRequest transactionGetRequest = TxProto.TransactionGetRequest.newBuilder()
                .setFromTime(0)
                .setToTime(0)
                .build();

        endpoints.getTransaction(token, transactionGetRequest).enqueue(new CallbackWrapper<>(activity,
                new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
                    @Override
                    public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                        ReqResProto.BaseResponse baseResponse = response.body();

                        if (baseResponse == null) {
                            activity.getTransactionHistoryFail(null);
                        }

                        if (baseResponse.getError()) {
                            if (baseResponse.getMsg().equals("Authorization failed.")) {
                                clearSharedPrefsAndRealm(preferences);
                                Intent intent = new Intent(activity, LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                activity.startActivity(intent);
                                Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                            } else activity.getTransactionHistoryFail(baseResponse.getMsg());
                            return;
                        }

                        int transactionHolderCount = baseResponse.getTransactionHoldersCount();
                        for (TxProto.TransactionHolder transactionHolder :
                                baseResponse.getTransactionHoldersList()) {
                            String accountType = null;
                            String transactionType = null;
                            switch (transactionHolder.getAch().getAccountType().getNumber()) {
                                case -1:
                                    accountType = "Unrecognized";
                                    break;

                                case 0:
                                    accountType = "Unknown Account Type";
                                    break;

                                case 1:
                                    accountType = "Consumer Checking";
                                    break;

                                case 2:
                                    accountType = "Consumer Savings";
                                    break;

                                case 3:
                                    accountType = "Business Checking";
                                    break;

                                case 4:
                                    accountType = "Business Business Savings";
                                    break;

                            }

                            switch (transactionHolder.getTransactionType().getNumber()) {
                                case -1:
                                    transactionType = "Unrecognized";
                                    break;

                                case 0:
                                    transactionType = "Unknown";

                                case 1:
                                    transactionType = "Invoice";
                                    break;

                                case 2:
                                    transactionType = "Wallet Request";
                                    break;

                                case 3:
                                    transactionType = "Request Fund";
                                    break;

                                case 4:
                                    transactionType = "Load Fund";
                                    break;

                                case 5:
                                    transactionType = "Withdraw Fund";
                                    break;

                                case 6:
                                    transactionType = "Fund Transfer";
                                    break;
                            }

                            try {
                                TxProto.Transaction transaction = TxProto.Transaction.parseFrom(transactionHolder.getTransaction());

                                TransactionHolder transactionHolder1 = new TransactionHolder(transactionHolder.getTransactionId()
                                        , transactionHolder.getSignature(), transactionHolder.getRefId(), transactionHolder.getFromUserId(),
                                        transactionHolder.getToUserId(), transactionHolder.getFromUserFullName(),
                                        transactionHolder.getFromUserProfilePicUrl(), transactionHolder.getToUserFullName(),
                                        transactionHolder.getToUserProfilePicUrl(), transactionHolder.getSignaturePubKey(),
                                        transactionHolder.getToWalletAddress(), transactionHolder.getSourceId(), transactionHolder.getWalletName(),
                                        transactionHolder.getMcc(), transactionHolder.getAch().getAccountNumber(), accountType, transactionHolder.getAch().getBankAcName(),
                                        transactionHolder.getAch().getNameOnAccount(), transactionHolder.getAch().getRoutingNumber(),
                                        transactionType, transactionHolder.getTimestamp(), String.valueOf(transaction.getAmount()),
                                        transaction.getToPubKey(), transaction.getFromPubKey(),
                                        transactionHolder.getToCurrency(), transactionHolder.getFromCurrency(),
                                        transactionHolder.getFromCountryCode(), transactionHolder.getToCountryCode());

                                transactionHolderList.add(transactionHolder1);

                            } catch (InvalidProtocolBufferException e) {
                                e.printStackTrace();
                            }
                        }
                        activity.getTransactionHistorySuccess(transactionHolderList);
                    }

                    @Override
                    public void onFailureResult() {
                        activity.getTransactionHistoryFail(null);
                    }
                }));
    }

    private void clearSharedPrefsAndRealm(SharedPreferences prefs) {
        //clear shared preferences and realm after logout
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();

        final Realm realm = RealmDatabase.getInstance().getRealm();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });
    }
}
