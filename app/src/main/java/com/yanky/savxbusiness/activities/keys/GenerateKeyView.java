package com.yanky.savxbusiness.activities.keys;

/**
 * Created by Leesa Shakya on 08/10/18.
 * leezshk@gmail.com
 */

public interface GenerateKeyView {
    void emptyPassCode();
    void validPassCode();
    void invalidPassCode();
    void keyGenerationFailed();
    void keyGenerationSuccess(String userId);
}
