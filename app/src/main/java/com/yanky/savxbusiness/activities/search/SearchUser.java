package com.yanky.savxbusiness.activities.search;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.yanky.savxbusiness.QuiqpayApp;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.activities.settings.Settings;
import com.yanky.savxbusiness.adapters.SyncContactAdapter;
import com.yanky.savxbusiness.adapters.UserAdapter;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.realm.models.SyncedContacts;
import com.yanky.savxbusiness.realm.models.User;
import com.yanky.savxbusiness.repo.SyncedContactsRepo;
import com.yanky.savxbusiness.utils.AppForegroundListener;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.RecyclerViewClickListener;
import com.yanky.savxbusiness.utils.SendSelectedUser;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.yanky.savxbusiness.QuiqpayApp.getMyApplication;

public class SearchUser extends BaseActivity implements SearchUserView, AppForegroundListener, RecyclerViewClickListener {
    private static final String TAG = "SearchUser";
    @Inject
    Endpoints endpoints;
    @BindView(R.id.rv_synced_contacts)
    RecyclerView mRecyclerView;
    @BindView(R.id.btn_sync_contact)
    Button mSyncContact;
    @BindView(R.id.ll_empty_contacts)
    LinearLayout mEmptyViewHolder;
    @BindView(R.id.et_search)
    EditText mSearch;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.rv_users)
    RecyclerView mUserRecycler;

    private SyncContactAdapter mAdapter;
    private UserAdapter mUserAdapter;
    private List<SyncedContacts> syncedContactsList = new ArrayList<>();
    private SharedPreferences preferences;

    private SearchUserPresenterImpl presenter;

    public static SendSelectedUser sendSelectedUser;

    private List<User> userListParsed = new ArrayList<>();

    public static void setSendSelectedUserListener(SendSelectedUser listener) {
        sendSelectedUser = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_user);

        ButterKnife.bind(this);

        getMyApplication(this).getAppComponent().inject(this);
        init();


        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        presenter = new SearchUserPresenterImpl(this, endpoints);

        setUpRecyclerView();

        mSyncContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SearchUser.this, Settings.class));
            }
        });

        mSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(final CharSequence charSequence, int i, int i1, int i2) {
//                mAdapter.getFilter().filter(charSequence.toString());
                presenter.search(getToken(), charSequence.toString(), preferences);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void setUpRecyclerView() {
        syncedContactsList = SyncedContactsRepo.getInstance().getSyncedContacts();
        AppUtils.showLog(TAG, "fetchedContactsCOunt: " + syncedContactsList.size());
        if (!syncedContactsList.isEmpty()) {
            mAdapter = new SyncContactAdapter(syncedContactsList);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mRecyclerView.setAdapter(mAdapter);
        } else {
            Toast.makeText(this, "No matching contacts found", Toast.LENGTH_SHORT).show();
            mRecyclerView.setVisibility(View.GONE);
//            mEmptyViewHolder.setVisibility(View.VISIBLE);
        }
    }


    private void init() {
        setUpToolbar(mToolbar);
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        mToolbarTitle.setText("Select User");
    }

    @Override
    public void onAppForeground() {
        showPasscodeDialog(this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        QuiqpayApp.setAppForegroundListener(this);
    }


    @Override
    public void onClick(View view, int position) {
        if (userListParsed.size() != 0) {
            User user = userListParsed.get(position);
            if (sendSelectedUser != null) sendSelectedUser.getUser(user);
            finish();
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onSearchSuccess(List<User> userList) {
        AppUtils.showLog(TAG, "search user success");
        userListParsed = userList;
        updateUserRecyclerView(userList);

    }

    private void updateUserRecyclerView(List<User> userList) {
        if (!userList.isEmpty()) {
            mUserAdapter = new UserAdapter(userList);
            mUserAdapter.setClickListener(this);
            mUserRecycler.setLayoutManager(new LinearLayoutManager(this));
            mUserRecycler.setItemAnimator(new DefaultItemAnimator());
            mUserRecycler.setAdapter(mUserAdapter);
        } else {
            Toast.makeText(this, "No users found", Toast.LENGTH_SHORT).show();
//            mUserRecycler.setVisibility(View.GONE);
        }
    }

    @Override
    public void onSearchFail(String msg) {
        showMessage(msg);
    }
}
