package com.yanky.savxbusiness.activities.mpin;

public interface ChangeMPINPresenter {
    void changeMPIN(String oldpin, String newpin, String confirmpin, int pinLength, String passcode);
}
