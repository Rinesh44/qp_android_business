package com.yanky.savxbusiness.activities.base;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.account.LoginActivity;
import com.yanky.savxbusiness.activities.fingerprint.FingerPrintActivity;
import com.yanky.savxbusiness.realm.RealmDatabase;
import com.yanky.savxbusiness.realm.models.User;
import com.yanky.savxbusiness.repo.UserRepo;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.Constants;
import com.yanky.savxbusiness.utils.KeyboardUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

import io.realm.Realm;

/**
 * Created by Leesa Shakya on 25/09/18.
 * leezshk@gmail.com
 */

public abstract class BaseActivity extends AppCompatActivity implements MvpView {
    private static String TAG = BaseActivity.class.getSimpleName();
    private ProgressDialog mProgressDialog;
    private static SharedPreferences preferences;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
    }

/*    public String getToken() {
        return UserRepo.getInstance().getToken() == null ? null : UserRepo.getInstance().getToken().getToken();
    } */

    public String getToken() {
        return preferences.getString(Constants.USER_TOKEN, null);
    }

    public Boolean loggedIn() {
        return preferences.getBoolean(Constants.LOGGED_IN, false);
    }


    public User getUser() {
        return UserRepo.getInstance().getToken() == null ? null : UserRepo.getInstance().getToken().getUser();
    }

/*    public String getUserID() {
        return UserRepo.getInstance().getToken() == null ? null : UserRepo.getInstance().getToken().getUser().getUserId();
    }
    */

    public String getUserID() {
        return preferences.getString(Constants.USER_ID, null);
    }

    protected void setUpToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
    }

    @Override
    public void showKeyboard() {
        KeyboardUtils.showKeyboard(this);
    }

    @Override
    public void hideKeyboard() {
        KeyboardUtils.hideKeyboard(this);
    }

    @Override
    public void showLoading() {
        hideLoading();
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.show();
        if (mProgressDialog.getWindow() != null) {
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        mProgressDialog.setContentView(R.layout.progress_dialog);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }


    @Override
    public void hideLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.cancel();
        }
    }

    @Override
    public void showMessage(String message) {
//        hideKeyboard();
        if (message != null) {
            showSnackBar(message);
        } else {
//            showSnackBar(getString(R.string.try_again));
        }
    }


    public String getDate(long time) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("E, d MMM yyyy, h:mm a");
            return sdf.format(new Date(time));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    public String getDateSimple(long time) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            return sdf.format(new Date(time));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    private void showSnackBar(String message) {
        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
                message, Snackbar.LENGTH_SHORT);
        View sbView = snackbar.getView();
        TextView textView = sbView
                .findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(this, android.R.color.white));
        snackbar.show();
    }

    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void openActivityOnTokenExpire() {
        Realm realm = RealmDatabase.getInstance().getRealm();
        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm1) {
                    realm1.deleteAll();
                    startActivity(new Intent(BaseActivity.this, LoginActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();
                }
            });
        } finally {
            realm.close();
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermissionsSafely(String[] permissions, int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean hasPermission(String permission) {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * dialog for authentication when app comes foreground
     */
    public static void showPasscodeDialog(final Context context) {
        if (preferences.getBoolean(Constants.TOUCHID_ENABLED, false)) {
            Intent intent = new Intent(context, FingerPrintActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(new Intent(context, FingerPrintActivity.class));
        } else {

            String passcode = preferences.getString(Constants.PASSCODE, null);
            if (passcode != null) {
                final int passcodeLength = passcode.length();


                final Dialog dialog = new Dialog(context, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
                final View dialogView = View.inflate(context, R.layout.dialog_backup_key, null);

                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(dialogView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
                dialog.setCancelable(false);


                final PinEntryEditText mPinEntryPasscode = dialog.findViewById(R.id.pin_entry_passcode);
                mPinEntryPasscode.setMaxLength(passcodeLength);

                mPinEntryPasscode.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        if (charSequence.length() == passcodeLength) {
                            String passcode = mPinEntryPasscode.getText().toString();
                            String passcodeFromPref = preferences.getString(Constants.PASSCODE, null);
                            AppUtils.showLog(TAG, "currentPasscode:" + passcode + "passcodePref:" + passcodeFromPref);
                            if (passcode.equals(passcodeFromPref)) {
                                AppUtils.showLog(TAG, "Passcode Match");
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        //Do something after 100ms
                                        dialog.dismiss();
                                    }
                                }, 1000);
                            } else {
                                AppUtils.showLog(TAG, "Passcode not match");
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        //Do something after 100ms
                                        mPinEntryPasscode.setText(null);
                                        Toast.makeText(context, R.string.invalid_password_alt, Toast.LENGTH_SHORT).show();
                                    }
                                }, 500);

                            }
                        }

                    }


                    @Override
                    public void afterTextChanged(Editable editable) {
                    }
                });

                if (dialog.getWindow() != null)
                    dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                dialog.show();

            }
        }

        //finally set wasInBackGround to false
//        AppLifecycleHandler.appInForeground = false;
    }


    /**
     * for resizing flag drawable
     *
     * @param image
     * @return
     */
    public Drawable resize(Drawable image) {
        Bitmap b = ((BitmapDrawable) image).getBitmap();
        Bitmap bitmapResized = Bitmap.createScaledBitmap(b, 45, 45, false);
        return new BitmapDrawable(getResources(), bitmapResized);
    }


}
