package com.yanky.savxbusiness.activities.verification;

import android.content.SharedPreferences;
import android.widget.Toast;

import com.yanky.savxbusiness.QuiqpayApp;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.entities.AccountProto;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.realm.models.WaitingUser;
import com.yanky.savxbusiness.repo.Repo;
import com.yanky.savxbusiness.repo.WaitingUserRepo;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.CallbackWrapper;

import retrofit2.Response;

/**
 * Created by Leesa Shakya on 02/10/18.
 * leezshk@gmail.com
 */

public class VerificationPresenterImpl implements VerificationPresenter {
    public String TAG = VerificationPresenterImpl.class.getSimpleName();
    private final VerificationActivity activity;
    private final Endpoints endpoints;

    VerificationPresenterImpl(VerificationActivity activity, Endpoints endpoints) {
        this.activity = activity;
        this.endpoints = endpoints;
    }

    @Override
    public void onVerifyWaitingUserCode(String emailPhone, String code) {
        activity.showLoading();
        AppUtils.showLog(TAG, "code: " + code);
        AppUtils.showLog(TAG, "emailPhone: " + emailPhone);
        if (emailPhone == null) {
            activity.showMessage("emailPhone is null");
            return;
        }

        endpoints.verifyWaitingListCode(emailPhone, code).enqueue(new CallbackWrapper<>(activity, new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
            @Override
            public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                activity.hideLoading();
                ReqResProto.BaseResponse baseResponse = response.body();
                AppUtils.showLog(TAG, "baseresponse: " + baseResponse);
                if (baseResponse == null) {
                    activity.verifyWaitingUserCodeFail(null);
                    return;
                }
                if (baseResponse.getError()) {
                    activity.verifyWaitingUserCodeFail(baseResponse.getMsg());
                    return;
                }

                AppUtils.showLog(TAG, "onVerify()");
                activity.verifyWaitingUserCodeSuccess();
            }

            @Override
            public void onFailureResult() {
                activity.hideLoading();
                activity.verifyWaitingUserCodeFail(null);
            }
        }));
    }

    @Override
    public void onVerify(String userId, String code) {
        activity.showLoading();
        AppUtils.showLog(TAG, "code: " + code);
        AppUtils.showLog(TAG, "userId: " + userId);
        if (userId == null) {
            activity.showMessage("User ID is null");
            return;
        }
        endpoints.verify(userId, code).enqueue(new CallbackWrapper<>(activity,
                new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
                    @Override
                    public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                        activity.hideLoading();
                        ReqResProto.BaseResponse baseResponse = response.body();

                        if (baseResponse == null) {
                            activity.verifyFailure(null);
                            return;
                        }
                        if (baseResponse.getError()) {
                            activity.verifyFailure(baseResponse.getMsg());
                            return;
                        }

                        AppUtils.showLog(TAG, "onVerify()");
                        activity.verifySuccess();
                    }

                    @Override
                    public void onFailureResult() {
                        activity.hideLoading();
                        activity.verifyFailure(null);
                    }
                }));
    }

    @Override
    public void onResendCode(String userId) {
        if (userId == null) {
            Toast.makeText(activity, "Empty user id", Toast.LENGTH_SHORT).show();
            return;
        }
        activity.showLoading();

        endpoints.resendVerificationCode(userId).enqueue(new CallbackWrapper<>(activity,
                new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
                    @Override
                    public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                        activity.hideLoading();
                        ReqResProto.BaseResponse baseResponse = response.body();
                        if (baseResponse == null) {
                            activity.verifyFailure(null);
                            return;
                        }
                        if (baseResponse.getError()) {
                            activity.verifyFailure(baseResponse.getMsg());
                            return;
                        }
                        activity.resendCodeSuccess();
                    }

                    @Override
                    public void onFailureResult() {
                        activity.hideLoading();
                        activity.verifyFailure(null);
                    }
                }));
    }


    @Override
    public void registerWaitingUser(String token, String deviceId, String fullName, String emailPhone,
                                    SharedPreferences preferences) {
        AccountProto.WaitingUser waitingUser = AccountProto.WaitingUser.newBuilder()
                .setEmailPhone(emailPhone)
                .setName(fullName)
                .setDeviceId(deviceId)
                .setPushToken(QuiqpayApp.getFirebaseToken())
                .setDeviceInfo("")
                .setDeviceType(AccountProto.DeviceType.ANDROID)
                .build();

        endpoints.registerWaitingUser(token, waitingUser).enqueue(new CallbackWrapper<>(activity,
                new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
                    @Override
                    public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                        ReqResProto.BaseResponse baseResponse = response.body();

                        if (baseResponse != null) {
                            int queuePosition = baseResponse.getWaitingUser().getQueuePosition();
                            int totalWaitingUsers = baseResponse.getWaitingUser().getTotal();
                            String username = baseResponse.getWaitingUser().getName();
                            AppUtils.showLog(TAG, "baseResponse: " + queuePosition + " " + totalWaitingUsers
                                    + " " + username);

                            if (baseResponse.getError()) {
                                activity.registerWaitingUserFail(baseResponse.getMsg());
                            } else {
                                saveWaitingUserToDb(baseResponse.getWaitingUser(), preferences);
                                activity.registerWaitingUserSuccess(queuePosition, totalWaitingUsers,
                                        username);
                            }
                        } else {
                            AppUtils.showLog(TAG, "baseResponse is null");
                            activity.registerWaitingUserFail(null);
                        }
                    }


                    @Override
                    public void onFailureResult() {
                        activity.registerWaitingUserFail(null);
                    }
                }));
    }

    private void saveWaitingUserToDb(AccountProto.WaitingUser waitingUser, SharedPreferences preferences) {
        WaitingUser user = new WaitingUser();
        user.setDeviceId(waitingUser.getDeviceId());
        user.setDeviceInfo(waitingUser.getDeviceInfo());
        user.setEmailPhone(waitingUser.getEmailPhone());
        user.setName(waitingUser.getName());
        user.setPushToken(waitingUser.getPushToken());

        WaitingUserRepo.getInstance().saveWaitingUser(user, new Repo.Callback() {
            @Override
            public void success(Object o) {
                AppUtils.showLog(TAG, "saved waiting user to db");
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("waiting_user_registered", true);
                editor.apply();
            }

            @Override
            public void fail() {
                AppUtils.showLog(TAG, "failed to save waiting user to db");
            }
        });
    }
}
