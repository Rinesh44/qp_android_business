package com.yanky.savxbusiness.activities.account;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.hbb20.CountryCodePicker;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegisterBusinessAccountFirst extends BaseActivity {
    @BindView(R.id.btn_next)
    Button mNext;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.et_contact_fullname)
    EditText mContactFullname;
    @BindView(R.id.et_email_phone)
    EditText mEmailPhone;
    @BindView(R.id.et_password)
    EditText mPassword;
    @BindView(R.id.country_picker)
    CountryCodePicker mCountryCodePicker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_business_account);

        ButterKnife.bind(this);

        init();

        mNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    private void init() {
        setUpToolbar(mToolbar);

        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

        }

        mToolbarTitle.setText("Create Account");
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
