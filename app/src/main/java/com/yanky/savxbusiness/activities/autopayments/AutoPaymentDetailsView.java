package com.yanky.savxbusiness.activities.autopayments;

public interface AutoPaymentDetailsView {
    void deleteAutopaymentSuccess();

    void deleteAutopaymentFail(String msg);
}
