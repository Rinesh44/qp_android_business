package com.yanky.savxbusiness.activities.wallet.fund;

import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.yanky.savxbusiness.activities.account.LoginActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.crypto.Crypto;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.entities.TxProto;
import com.yanky.savxbusiness.realm.RealmDatabase;
import com.yanky.savxbusiness.realm.models.User;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.CallbackWrapper;

import org.libsodium.jni.encoders.Hex;

import io.realm.Realm;
import retrofit2.Response;

public class TransferToUserPresenterImpl implements TransferToUserPresenter {
    private static final String TAG = "TransferToUserPresenter";

    private TransferToUser activity;
    private Endpoints endpoints;

    public TransferToUserPresenterImpl(TransferToUser activity, Endpoints endpoints) {
        this.activity = activity;
        this.endpoints = endpoints;
    }


    @Override
    public void transferToUser(String token, String transferAmount, String desc, User user, String fromWalletAddress, String fromUserId, String currency, SharedPreferences preferences) {
        activity.showLoading();

        TxProto.Transaction transaction = TxProto.Transaction.newBuilder()
                .setAmount(Long.valueOf(transferAmount) * 100)
                .setFromPubKey(fromWalletAddress)
                .setFromUserId(fromUserId)
                .setCurrency(currency)
                .setToPubKey(user.getAddress())
                .setToUserId(user.getUserId())
                .setMemo(desc)
                .build();

        byte[] dataToSign = transaction.toByteArray();

        AppUtils.showLog(TAG, "dataToSign" + dataToSign.toString());
        AppUtils.showLog(TAG, "fromUserId" + fromUserId);
        AppUtils.showLog(TAG, "fromWalletAddress" + fromWalletAddress);


        byte[] signature = Crypto.build().sign(dataToSign, fromUserId, fromWalletAddress);

        if (signature == null) {
            activity.hideKeyboard();
            activity.hideLoading();
            activity.showMessage("Signature not generated");
            return;
        }

        String signatureString = Hex.HEX.encode(signature);
        AppUtils.showLog(TAG, "signature" + signatureString);

        TxProto.TransactionHolder transactionHolder = TxProto.TransactionHolder.newBuilder()
                .setTransaction(transaction.toByteString())
                .setSignature(signatureString)
                .setTransactionType(TxProto.TransactionType.FUNDTRANSFER)
                .setFromUserId(fromUserId)
                .setFromCurrency(currency)
                .setToUserId(user.getUserId())
                .setSignaturePubKey(fromWalletAddress)
                .setToWalletAddress(user.getAddress())
                .build();

        endpoints.transferFund(token, transactionHolder).enqueue(new CallbackWrapper<>(activity, new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
            @Override
            public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                activity.hideLoading();
                ReqResProto.BaseResponse baseResponse = response.body();

                if (baseResponse == null) {
                    activity.transferToUserFail(null);
                    return;
                }

                if (baseResponse.getError()) {
                    if (baseResponse.getMsg().equals("Authorization failed.")) {
                        clearSharedPrefsAndRealm(preferences);
                        Intent intent = new Intent(activity, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        activity.startActivity(intent);
                        Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                    } else activity.transferToUserFail(baseResponse.getMsg());
                    return;
                }

                activity.transferToUserSuceess();
            }

            @Override
            public void onFailureResult() {
                activity.hideLoading();
                activity.transferToUserFail(null);
            }
        }));
    }

    private void clearSharedPrefsAndRealm(SharedPreferences prefs) {
        //clear shared preferences and realm after logout
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();

        final Realm realm = RealmDatabase.getInstance().getRealm();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });
    }
}
