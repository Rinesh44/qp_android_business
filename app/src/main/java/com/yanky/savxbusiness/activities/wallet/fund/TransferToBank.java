package com.yanky.savxbusiness.activities.wallet.fund;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.scrounger.countrycurrencypicker.library.Country;
import com.yanky.savxbusiness.QuiqpayApp;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.realm.models.Wallet;
import com.yanky.savxbusiness.utils.AppForegroundListener;
import com.yanky.savxbusiness.utils.AppUtils;

import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.yanky.savxbusiness.QuiqpayApp.getMyApplication;

public class TransferToBank extends BaseActivity implements AppForegroundListener {
    private static final String TAG = "TransferFund";
    @Inject
    Endpoints endpoints;
    @BindView(R.id.walletname)
    TextView mWalletName;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.amount)
    TextView mWalletAmount;
    @BindView(R.id.et_select_bank)
    EditText mSelectBank;
    @BindView(R.id.btn_transfer_fund)
    Button mTransferFund;
    @BindView(R.id.et_desc)
    EditText mDesc;
    @BindView(R.id.et_amount)
    EditText mTransferAmount;

    private String walletName, walletAmount, walletAddress, walletCurrency, walletCountryCode;

    TransferFundPresenterImpl presenter;
    Wallet mReceivedWallet;
    private SharedPreferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_to_bank);
        ButterKnife.bind(this);


        getMyApplication(this).getAppComponent().inject(this);

        //prevent keyboard glich when its introduced
        getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.background));

        init();

        mSelectBank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        mTransferFund.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSelectBank.getText().toString().isEmpty()) {
                    showMessage("Please select bank");
                    return;
                }


                if (mTransferAmount.getText().toString().isEmpty()) {
                    showMessage("Please enter amount");
                    return;
                }

                if (mDesc.getText().toString().isEmpty()) {
                    showMessage("Please enter description");
                    return;
                }

                AppUtils.showLog(TAG, "walletAMount" + mWalletAmount.getText().toString().substring(4));

                if (mTransferAmount.getText().toString().equals("0") || Integer.valueOf(mTransferAmount.getText().toString()) >
                        Double.valueOf(mWalletAmount.getText().toString().substring(4))) {
                    showMessage("Insufficient wallet amount");
                    return;
                }


            }
        });

        getIntentValues();

        mToolbarTitle.setText("Transfer to Bank");
        mWalletName.setText(walletName);

        Country countryWithCurrency = Country.getCountryWithCurrency(walletCountryCode, this);
        assert countryWithCurrency != null;
        String currencySymbol = Objects.requireNonNull(countryWithCurrency.getCurrency()).getSymbol();
        if (currencySymbol.length() > 1) {
            currencySymbol = currencySymbol.substring(currencySymbol.length() - 1);
        }

        StringBuilder amountBuilder = new StringBuilder();
        amountBuilder.append(walletCurrency);
        amountBuilder.append(" ");
        amountBuilder.append(currencySymbol);
        amountBuilder.append(" ");
        amountBuilder.append(String.format("%.0f", Double.valueOf(walletAmount)));


        mWalletAmount.setText(String.valueOf(amountBuilder));

     /*   int flagId = Country.getCountry(walletCountryCode, this).getFlagId();
        if (flagId != 0) {
            Drawable drawable = getResources().getDrawable(flagId);
            mWalletAmount.setCompoundDrawablePadding(20);
            mWalletAmount.setCompoundDrawablesWithIntrinsicBounds(resize(drawable), null, null, null);
        }*/


    }

    private void init() {
        setUpToolbar(mToolbar);
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
    }


    public void getIntentValues() {
        Intent i = getIntent();
        walletName = i.getStringExtra("wallet_name");
        walletAmount = i.getStringExtra("wallet_amount");
        walletAddress = i.getStringExtra("wallet_address");
        walletCountryCode = i.getStringExtra("wallet_country_code");
        walletCurrency = i.getStringExtra("wallet_currency");

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public void onAppForeground() {
        showPasscodeDialog(this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        QuiqpayApp.setAppForegroundListener(this);
    }

}
