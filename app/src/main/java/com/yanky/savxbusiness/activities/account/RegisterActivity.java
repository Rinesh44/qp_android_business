package com.yanky.savxbusiness.activities.account;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.view.View;
import android.widget.Toast;

import com.hbb20.CountryCodePicker;
import com.scrounger.countrycurrencypicker.library.Currency;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.entities.AccountProto;
import com.yanky.savxbusiness.realm.models.CountryDetails;
import com.yanky.savxbusiness.realm.models.WaitingUser;
import com.yanky.savxbusiness.repo.CountryDetailsRepo;
import com.yanky.savxbusiness.repo.WaitingUserRepo;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.Constants;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;

import static com.yanky.savxbusiness.QuiqpayApp.getMyApplication;

/**
 * Created by Leesa Shakya on 26/09/18.
 * leezshk@gmail.com
 */

public class RegisterActivity extends BaseActivity implements RegisterView {
    private static final String TAG = "RegisterActivity";
    @Inject
    Endpoints endpoints;
    @BindView(R.id.edit_text_full_name)
    TextInputEditText mFullNameEditText;
    @BindView(R.id.edit_text_email_phone)
    TextInputEditText mEmailPhoneEditText;
    @BindView(R.id.edit_text_password)
    TextInputEditText mPasswordEditText;

    @BindView(R.id.input_layout_full_name)
    TextInputLayout mFullNameLayout;
    @BindView(R.id.input_layout_email_phone)
    TextInputLayout mEmailPhoneLayout;
    @BindView(R.id.input_layout_password)
    TextInputLayout mPasswordLayout;
    @BindView(R.id.input_layout_country)
    TextInputLayout mCountryLayout;
    @BindView(R.id.country_picker)
    CountryCodePicker mCountryPicker;

    RegisterPresenter presenter;
    private String country;
    private SharedPreferences preferences;
    private boolean fromWaitingUser;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        getMyApplication(this).getAppComponent().inject(this);
        presenter = new RegisterPresenterImpl(this, endpoints, new RegisterInteractorImpl());

        getIntentData();
        if (fromWaitingUser) {
            WaitingUser user = WaitingUserRepo.getInstance().getWaitingUser();
            mFullNameEditText.setText(user.getName());
            mEmailPhoneEditText.setText(user.getEmailPhone());
            mFullNameEditText.setTextColor(Color.BLACK);
            mEmailPhoneEditText.setTextColor(Color.BLACK);
            mFullNameEditText.setEnabled(false);
            mEmailPhoneEditText.setEnabled(false);
        }

        mCountryPicker.setFlagSize(32);
        StringBuilder customCountries = new StringBuilder();
        List<CountryDetails> countryDetails = CountryDetailsRepo.getInstance().getAllCountryDetails();
        for (CountryDetails details : countryDetails
        ) {
            AppUtils.showLog(TAG, "countryCode:" + details.getCountryCode());
            customCountries.append(details.getCountryCode());
            customCountries.append(",");
            AppUtils.showLog(TAG, "countryName:" + details.getName());
        }


        if (customCountries.length() > 0) customCountries.setLength(customCountries.length() - 1);
        AppUtils.showLog(TAG, "customCountries:" + customCountries.toString());
        mCountryPicker.setCustomMasterCountries(customCountries.toString());

        mCountryPicker.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                country = mCountryPicker.getSelectedCountryNameCode();
                presenter.validateCountry(country);

                AppUtils.showLog(TAG, "currency: " + Currency.getCurrency(mCountryPicker.getSelectedCountryNameCode(), RegisterActivity.this).getCode());
            }
        });

    }

    private void getIntentData() {
        Intent i = getIntent();
        fromWaitingUser = i.getBooleanExtra("waiting_user_register_success", false);
    }

    @OnTextChanged(R.id.edit_text_full_name)
    void validateFullName(Editable editable) {
        presenter.validateFullName(editable.toString());
    }

    @OnTextChanged(R.id.edit_text_email_phone)
    void validateEmailPhone(Editable editable) {
        presenter.validateEmailPhone(editable.toString());
    }

    @OnTextChanged(R.id.edit_text_password)
    void validatePassword(Editable editable) {
        presenter.validatePassword(editable.toString());
    }

    public void onRegister(View view) {
//        hideKeyboard();
        String fullName = mFullNameEditText.getText().toString();
        String emailPhone = mEmailPhoneEditText.getText().toString();
        String password = mPasswordEditText.getText().toString();
        country = mCountryPicker.getSelectedCountryNameCode();
        presenter.onRegister(fullName, emailPhone, password, country);
    }

    @Override
    public void emptyFullName() {
        mFullNameLayout.setErrorEnabled(true);
        mFullNameLayout.setError(getString(R.string.this_field_is_required));
        mFullNameEditText.requestFocus();
    }

    @Override
    public void validFullName() {
        mFullNameLayout.setErrorEnabled(false);
        mFullNameLayout.setError(null);
    }

    @Override
    public void emptyEmailPhone() {
        mEmailPhoneLayout.setErrorEnabled(true);
        mEmailPhoneLayout.setError(getString(R.string.this_field_is_required));
        mEmailPhoneEditText.requestFocus();
    }

    @Override
    public void validEmailPhone() {
        mEmailPhoneLayout.setErrorEnabled(false);
        mEmailPhoneLayout.setError(null);
    }

    @Override
    public void invalidEmailPhone() {
        mEmailPhoneLayout.setErrorEnabled(true);
        mEmailPhoneLayout.setError(getString(R.string.invalid_email_phone));
        mEmailPhoneEditText.requestFocus();
    }


    @Override
    public void emptyPassword() {
        mPasswordLayout.setErrorEnabled(true);
        mPasswordLayout.setError(getString(R.string.this_field_is_required));
        mPasswordEditText.requestFocus();
    }

    @Override
    public void validPassword() {
        mPasswordLayout.setErrorEnabled(false);
        mPasswordLayout.setError(null);
    }

    @Override
    public void invalidPassword() {
        mPasswordLayout.setErrorEnabled(true);
        mPasswordLayout.setError(getString(R.string.invalid_password));
        mPasswordEditText.requestFocus();
    }

    @Override
    public void emptyCountry() {
        mCountryLayout.setErrorEnabled(true);
        mCountryLayout.setError(getString(R.string.this_field_is_required));
    }

    @Override
    public void validCountry() {
        mCountryLayout.setErrorEnabled(false);
        mCountryLayout.setError(null);
    }

    @Override
    public void registerSuccess(AccountProto.User user) {
        String userId = user.getUserId();

        String selectedCurrency = Currency.getCurrency(country, RegisterActivity.this).getCode();
        if (selectedCurrency != null) {
            AppUtils.showLog(TAG, "countrycode: " + country);
            AppUtils.showLog(TAG, "countryCurrency: " + selectedCurrency);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(Constants.USER_ID, userId);
            editor.putString(Constants.SELECTED_COUNTRY_CODE, country);
            editor.putString(Constants.SELECTED_CURRENCY, selectedCurrency);
            editor.apply();

        }

        Toast.makeText(this, "User registered successfully", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
        finish();
    }

    @Override
    public void registerFailure(String message) {
        showMessage(message);
    }
}
