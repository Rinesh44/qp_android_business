package com.yanky.savxbusiness.activities.wallet;

import android.content.SharedPreferences;

import com.yanky.savxbusiness.entities.WalletProto;
import com.yanky.savxbusiness.realm.models.TransactionOnHoldHolder;

public interface ForSelfPresenter {
    void createSelfWallet(String userId, String token, String walletName, String walletAddress, String passcode,
                          String currency, String countryCode, Long walletAmount, SharedPreferences preferences);

    void transferBalance(String token, String userId, TransactionOnHoldHolder transactionOnHoldHolder, WalletProto.WalletAddress newWallet, SharedPreferences preferences);

    void backUpKeys(String token, String referenceId, String pubKey, WalletProto.KeysBackup keysBackup, SharedPreferences preferences);
}
