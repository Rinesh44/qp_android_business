package com.yanky.savxbusiness.activities.invoice;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.yanky.savxbusiness.QuiqpayApp;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.adapters.CustomSpinnerAdapter;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.realm.models.UnpaidInvoice;
import com.yanky.savxbusiness.realm.models.Wallet;
import com.yanky.savxbusiness.repo.WalletsRepo;
import com.yanky.savxbusiness.utils.AppForegroundListener;
import com.yanky.savxbusiness.utils.AppUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.yanky.savxbusiness.QuiqpayApp.getMyApplication;

public class PayInvoice extends BaseActivity implements PayInvoiceView, AppForegroundListener {
    private static final String TAG = "PayInvoice";
    @Inject
    Endpoints endpoints;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.tv_company_name)
    TextView mCompanyName;
    @BindView(R.id.amount)
    TextView mAmount;
    @BindView(R.id.tv_invoice_id)
    TextView mInvoiceId;
    @BindView(R.id.tv_due_date)
    TextView mDueDate;
    @BindView(R.id.sp_wallet)
    Spinner mWalletSpinner;
    @BindView(R.id.et_note)
    EditText mNote;
    @BindView(R.id.amount_to_pay)
    TextView mAmountToPay;
    @BindView(R.id.amount_in_words)
    TextView mAmountInWords;

    private PayInvoicePresenterImpl presenter;

    private UnpaidInvoice invoice;
    private List<Wallet> walletList = new ArrayList<>();
    private String fromAddress, totalAmount, signature, selectedWalletName, currency;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_invoice);

        ButterKnife.bind(this);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        init();

        getIntentDataAndSetValues();

        setUpSpinner();

    }

    private void setUpSpinner() {
        walletList = WalletsRepo.getInstance().getAllWallets();
        if (walletList.size() != 0) {
            Iterator<Wallet> iter = walletList.iterator();

            while (iter.hasNext()) {
                Wallet wallet = iter.next();

                if (wallet.getAmount() < Double.valueOf(totalAmount)) {
                    iter.remove();
                }
            }
        }

        CustomSpinnerAdapter walletSpinnerAdapter = new CustomSpinnerAdapter(this, (ArrayList<Wallet>) walletList);
        mWalletSpinner.setAdapter(walletSpinnerAdapter);

        mWalletSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                TextView textView = (TextView) view.findViewById(R.id.third_item);
                TextView walletTextView = (TextView) view.findViewById(R.id.first_item);
                fromAddress = textView.getText().toString();
                selectedWalletName = walletTextView.getText().toString();
                AppUtils.showLog(TAG, "selectedWalletName:" + selectedWalletName);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void init() {
        getMyApplication(this).getAppComponent().inject(this);
        presenter = new PayInvoicePresenterImpl(this, endpoints);

        setUpToolbar(mToolbar);
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        mToolbarTitle.setText("Pay Amount");


    }


    private void getIntentDataAndSetValues() {
        Intent i = getIntent();
        invoice = i.getParcelableExtra("invoice_object");
        totalAmount = i.getStringExtra("total_amount");
        signature = i.getStringExtra("signature");
        currency = i.getStringExtra("currency");
        if (currency == null) {
            currency = "";
        }

        AppUtils.showLog(TAG, "invoiceData:" + invoice.getInvoiceId());
        AppUtils.showLog(TAG, "invoiceData:" + invoice.getSignaturePubKey());

        mCompanyName.setText(invoice.getReceiverName());
        mInvoiceId.setText(invoice.getInvoiceId());
        mDueDate.setText(getDateSimple(Long.valueOf(invoice.getDueDate())));
        StringBuilder amountBuilder = new StringBuilder();
        amountBuilder.append(currency);
        amountBuilder.append(" ");
        amountBuilder.append(totalAmount);
        mAmountToPay.setText(String.valueOf(amountBuilder));
        mAmount.setText(String.valueOf(amountBuilder));
        mAmountInWords.setText(String.valueOf(invoice.getTotalAmountInWords()));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.confirm_pay, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_confirm:
                //confirm pay
                String note = mNote.getText().toString().trim();
                AppUtils.showLog(TAG, "signature:" + signature);
                presenter.payInvoice(getToken(), invoice, totalAmount, getUserID(), note, fromAddress, preferences);
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void invoicePaySuccess() {
        AppUtils.showLog(TAG, "invoice pay successful");
        Toast.makeText(this, "Invoice payment successful", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(PayInvoice.this, InvoicePaySuccess.class);
        intent.putExtra("invoice_object", invoice);
        intent.putExtra("amount_to_pay", totalAmount);
        intent.putExtra("wallet_name", selectedWalletName);
        intent.putExtra("from_wallet_address", fromAddress);
        startActivity(intent);
    }

    @Override
    public void invvoicePayFail(String msg) {
        showMessage(msg);
    }

    @Override
    public void onAppForeground() {
        showPasscodeDialog(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        QuiqpayApp.setAppForegroundListener(this);
    }

}

