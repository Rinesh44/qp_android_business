package com.yanky.savxbusiness.activities.mpin;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.yanky.savxbusiness.QuiqpayApp;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.utils.AppForegroundListener;
import com.yanky.savxbusiness.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangeMPIN extends BaseActivity implements ChangeMPINView, View.OnClickListener, AppForegroundListener {
    @BindView(R.id.et_old_mpin)
    EditText mOldMPIN;
    @BindView(R.id.et_new_mpin)
    EditText mNewMPIN;
    @BindView(R.id.et_confirm_mpin)
    EditText mConfirmMPIN;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.btn_confirm)
    Button mConfirm;
    @BindView(R.id.sw_digit)
    SwitchCompat mDigits;

    private ChangeMPINPresenterImpl presenter;
    private SharedPreferences preferences;
    private int pinLength = 4;
    private String passcode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_mpin);

        ButterKnife.bind(this);

        getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.background));

        init();
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        passcode = preferences.getString(Constants.PASSCODE, null);
        int passcodeLength = passcode.length();

        mOldMPIN.setFilters(new InputFilter[]{new InputFilter.LengthFilter(passcodeLength)});
        mNewMPIN.setFilters(new InputFilter[]{new InputFilter.LengthFilter(4)});
        mConfirmMPIN.setFilters(new InputFilter[]{new InputFilter.LengthFilter(4)});

        mConfirm.setOnClickListener(this);

        mDigits.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                if (checked) {
                    mNewMPIN.getText().clear();
                    mConfirmMPIN.getText().clear();
                    mNewMPIN.setFilters(new InputFilter[]{new InputFilter.LengthFilter(4)});
                    mConfirmMPIN.setFilters(new InputFilter[]{new InputFilter.LengthFilter(4)});

                    pinLength = 4;
                } else {
                    mNewMPIN.getText().clear();
                    mConfirmMPIN.getText().clear();
                    mNewMPIN.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
                    mConfirmMPIN.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
                    pinLength = 6;
                }
            }
        });

    }

    private void init() {
        presenter = new ChangeMPINPresenterImpl(this);

        setUpToolbar(mToolbar);
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        mToolbarTitle.setText("Change MPIN");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btn_confirm:
                String oldMPIN = mOldMPIN.getText().toString().trim();
                String newMPIN = mNewMPIN.getText().toString().trim();
                String confrimMPIN = mConfirmMPIN.getText().toString().trim();
                presenter.changeMPIN(oldMPIN, newMPIN, confrimMPIN, pinLength, passcode);
                break;
        }
    }

    @Override
    public void invalidPasscode(String msg) {
        showMessage(msg);
    }

    @Override
    public void changeMPINSuccess(String passcode) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.PASSCODE, passcode);
        editor.apply();

        Toast.makeText(this, "MPIN changed", Toast.LENGTH_SHORT).show();

        finish();
    }

    @Override
    public void changeMPINFail() {

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onAppForeground() {
        showPasscodeDialog(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        QuiqpayApp.setAppForegroundListener(this);
    }
}
