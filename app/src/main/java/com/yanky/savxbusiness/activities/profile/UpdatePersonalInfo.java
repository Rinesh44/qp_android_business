package com.yanky.savxbusiness.activities.profile;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.yanky.savxbusiness.QuiqpayApp;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.utils.AppForegroundListener;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.Constants;
import com.yanky.savxbusiness.utils.DatePicker;

import java.io.ByteArrayOutputStream;
import java.io.File;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.webkit.MimeTypeMap.getFileExtensionFromUrl;
import static android.webkit.MimeTypeMap.getSingleton;
import static com.yanky.savxbusiness.QuiqpayApp.getMyApplication;

public class UpdatePersonalInfo extends BaseActivity implements UpdatePersonalInfoView, AppForegroundListener {
    private String TAG = UpdatePersonalInfo.class.getSimpleName();
    private final int MY_CAMERA_PERMISSION_CODE = 999;
    private final int CAMERA_REQUEST = 100;
    @Inject
    Endpoints endpoints;
    @BindView(R.id.et_full_name)
    EditText mFullname;
    @BindView(R.id.et_dob)
    EditText mDob;
    @BindView(R.id.sp_gender)
    Spinner mGender;
    @BindView(R.id.btn_update)
    Button mUpdate;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.tv_change_pic)
    TextView mChangePic;
    @BindView(R.id.iv_pro_pic)
    ImageView mProPic;

    private UpdatePersonalPresenterImpl presenter;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_personal_info);
        ButterKnife.bind(this);


        getMyApplication(this).getAppComponent().inject(this);
        init();

        String[] items = new String[]{"Select Gender", "Male", "Female", "Unknown"};
        ArrayAdapter<String> genderAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,
                items);
        genderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mGender.setAdapter(genderAdapter);

        getEditedValues();

        mFullname.setText(preferences.getString(Constants.FULL_NAME, ""));
        final DatePicker datePicker = new DatePicker(UpdatePersonalInfo.this, R.id.et_dob);


        mChangePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                    } else {
                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    }
                } else {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, CAMERA_REQUEST);
                }

            }
        });

        mDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePicker.onClick(view);
            }
        });

        mUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fullName = mFullname.getText().toString().trim();
                String dob = mDob.getText().toString().trim();
                String gender = mGender.getSelectedItem().toString().trim();
                if (fullName.isEmpty()) {
                    showMessage(getResources().getString(R.string.empty_name));
                    return;
                }

                if (gender.equals("Select Gender")) {
                    showMessage(getResources().getString(R.string.select_gender));
                    return;
                }

                presenter.updateInfo(getToken(), fullName, dob, gender, preferences);
            }
        });
    }

    private void getEditedValues() {
        String fullname = preferences.getString(Constants.FULL_NAME, "");
        String dob = preferences.getString(Constants.DOB, "");
        String gender = preferences.getString(Constants.GENDER, "");
        AppUtils.showLog(TAG, "gender: " + gender);

        if (!fullname.isEmpty()) {
            mFullname.setText(fullname);
        }

        if (!dob.isEmpty()) {
            mDob.setText(dob);
        }

        if (!gender.isEmpty()) {
            switch (gender) {
                case "Male":
                    AppUtils.showLog(TAG, "case male");
                    mGender.setSelection(1);
                    break;

                case "Female":
                    mGender.setSelection(2);
                    break;

                case "Unknown":
                    mGender.setSelection(3);
                    break;
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            } else {
                Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void init() {
        presenter = new UpdatePersonalPresenterImpl(this, endpoints);
        setUpToolbar(mToolbar);
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        mToolbarTitle.setText("Edit Profile");

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public void updateSuccess() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.FULL_NAME, mFullname.getText().toString().trim());
        editor.putString(Constants.GENDER, mGender.getSelectedItem().toString().trim());
        editor.putString(Constants.DOB, mDob.getText().toString().trim());
        editor.apply();
        finish();
    }

    @Override
    public void updateFail(String msg) {
        showMessage(msg);
    }

    @Override
    public void propicUploadSuccess() {
        AppUtils.showLog(TAG, "Profile pic uploaded");
        Toast.makeText(this, "Profile pic uploaded", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void propicUploadFail(String msg) {
        showMessage(msg);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            mProPic.setImageBitmap(photo);

            Uri tempUri = getImageUri(getApplicationContext(), photo);

            File imageFile = new File(getRealPathFromURI(tempUri));

            String mimeType = getSingleton()
                    .getMimeTypeFromExtension(getFileExtensionFromUrl(imageFile.getName()));
            AppUtils.showLog(TAG, "mime:" + mimeType);


            if (mimeType != null) {
                presenter.uploadProPic(getToken(), imageFile, mimeType, preferences);
            }
        }

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Avatar", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (getContentResolver() != null) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }

    @Override
    public void onAppForeground() {
        showPasscodeDialog(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        QuiqpayApp.setAppForegroundListener(this);
    }
}

