package com.yanky.savxbusiness.activities.profile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.yanky.savxbusiness.QuiqpayApp;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.adapters.AddressAdapter;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.entities.AccountProto;
import com.yanky.savxbusiness.realm.models.Address;
import com.yanky.savxbusiness.repo.AddressRepo;
import com.yanky.savxbusiness.repo.Repo;
import com.yanky.savxbusiness.utils.AppForegroundListener;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.Constants;
import com.yanky.savxbusiness.utils.SwipeController;
import com.yanky.savxbusiness.utils.SwipeControllerActions;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.yanky.savxbusiness.QuiqpayApp.getMyApplication;

public class UserProfileActivity extends BaseActivity implements UserProfileView, AppForegroundListener {
    private String TAG = UserProfileActivity.class.getSimpleName();
    @Inject
    Endpoints endpoints;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.user_image)
    ImageView mUserImage;
    @BindView(R.id.user_name)
    TextView mUserName;
    @BindView(R.id.user_phone)
    TextView mUserPhoneOrEmail;
    @BindView(R.id.user_dob)
    TextView mUserDob;
    @BindView(R.id.user_address_null)
    LinearLayout mNullAddressHolder;
    @BindView(R.id.address_holder)
    RelativeLayout mAddressHolder;
    @BindView(R.id.fab_add)
    FloatingActionButton mAddAddress;
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.pb_address)
    ProgressBar mProgressAddress;


    private UserProfilePresenterImpl presenter;
    private AddressAdapter addressAdapter;
    private List<Address> addressList = new ArrayList<>();
    private SwipeController swipeController = null;
    private SharedPreferences preferences;

    private boolean active;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        ButterKnife.bind(this);
        overridePendingTransition(R.anim.right_in, R.anim.right_out);

        //to remove keyboard glich
        getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.background));

        getMyApplication(this).getAppComponent().inject(this);
        init();

//        presenter.fetchProfile(getToken(), getUserID(), preferences);

        mAddAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Boolean makeDefaultAddress = false;
                if (addressList.isEmpty()) {
                    makeDefaultAddress = true;
                }
                Intent addInfoActivity = new Intent(UserProfileActivity.this, AddAddress.class);
                addInfoActivity.putExtra("make_default", makeDefaultAddress);
                startActivity(addInfoActivity);

            }
        });


    }


    @Override
    protected void onStart() {
        super.onStart();
        active = true;

    }

    @Override
    protected void onStop() {
        super.onStop();
        active = false;
    }

    private void init() {
        preferences = PreferenceManager.getDefaultSharedPreferences(UserProfileActivity.this);

        presenter = new UserProfilePresenterImpl(this, endpoints);
        setUpToolbar(mToolbar);
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        mUserName.setText(preferences.getString(Constants.FULL_NAME, ""));
        mUserPhoneOrEmail.setText(preferences.getString(Constants.PHONE_OR_EMAIL, ""));
        mUserDob.setText(preferences.getString(Constants.DOB, ""));

        setupRecyclerView();
        attachSwipeController();
    }

    private void setupRecyclerView() {
        addressList = AddressRepo.getInstance().getAllAddresses();
        AppUtils.showLog(TAG, "addressCount:" + addressList.size());
        addressAdapter = new AddressAdapter(addressList);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mRecyclerView.setAdapter(addressAdapter);

        if (!addressList.isEmpty()) {
            mProgressAddress.setVisibility(View.GONE);
            mNullAddressHolder.setVisibility(View.GONE);
            mAddressHolder.setVisibility(View.VISIBLE);
        } else {
            mNullAddressHolder.setVisibility(View.VISIBLE);
            mAddressHolder.setVisibility(View.GONE);
            mProgressAddress.setVisibility(View.GONE);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        QuiqpayApp.setAppForegroundListener(this);
        presenter.fetchProfile(getToken(), getUserID(), preferences);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in, R.anim.left_out);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                editPersonalInfo();
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.profile_menu, menu);
        return true;
    }

    public void editPersonalInfo() {
        Intent editActivity = new Intent(UserProfileActivity.this, UpdatePersonalInfo.class);
        startActivity(editActivity);
    }

    @Override
    public void profileFetchFailure(String msg) {
        showMessage(msg);
    }


    @Override
    public void profileFetchSuccess(AccountProto.User user) {
        if (user != null) {
            AppUtils.showLog(TAG, "totalAddress:" + user.getAddressesCount());

            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(Constants.FULL_NAME, user.getFullName());
            editor.putString(Constants.PHONE_OR_EMAIL, user.getEmailPhone());
            editor.apply();

            mUserName.setText(preferences.getString(Constants.FULL_NAME, ""));
            mUserPhoneOrEmail.setText(preferences.getString(Constants.PHONE_OR_EMAIL, ""));
            AppUtils.showLog(TAG, "DOB:" + user.getDob());

            if (user.getDob() != null) {
                editor.putString(Constants.DOB, user.getDob());
                editor.apply();
                mUserDob.setText(preferences.getString(Constants.DOB, ""));
            }

            setAddressDataAdapter(user);


            AddressRepo.getInstance().saveAddressList(addressList, new Repo.Callback() {
                @Override
                public void success(Object o) {
                    AppUtils.showLog(TAG, "addresses saved");
                    setupRecyclerView();
                    addressAdapter.notifyDataSetChanged();
                }

                @Override
                public void fail() {
                    AppUtils.showLog(TAG, "failed to save addresses");
                }
            });
        }




  /*      if(user.getAddress() != null){
            mNullAddressHolder.setVisibility(View.GONE);
        }*/
    }

    @Override
    public void addressDeleteSuccess(int pos, String addressId) {
        AppUtils.showLog(TAG, "addressId:" + addressId);

        Toast.makeText(this, "Address deleted", Toast.LENGTH_SHORT).show();
        AddressRepo.getInstance().deleteAddress(addressId, new Repo.Callback() {
            @Override
            public void success(Object o) {
                AppUtils.showLog(TAG, "address deleted");
                addressAdapter.removeItem(pos);
                addressAdapter.notifyItemRemoved(pos);
                addressAdapter.notifyItemRangeChanged(pos, addressAdapter.getItemCount());
            }

            @Override
            public void fail() {
                AppUtils.showLog(TAG, "address delete failed");
            }
        });

/*
        if (addressAdapter.getItemCount() == 1) {
            AppUtils.showLog(TAG, "itemCountOne");
            if (!addressList.get(0).getDefault()) {
                AppUtils.showLog(TAG, "getDefaultFalse");
                addressList.get(0).setDefault(true);
                AppUtils.showLog(TAG, "final" + addressList.get(0).getDefault());
            }
        }*/


        if (addressAdapter.getItemCount() > 0) {
            mNullAddressHolder.setVisibility(View.GONE);
            mAddressHolder.setVisibility(View.VISIBLE);
        } else {
            mNullAddressHolder.setVisibility(View.VISIBLE);
            mAddressHolder.setVisibility(View.GONE);
        }


    }

    @Override
    public void addressDeleteFail(String msg) {
        showMessage(msg);
    }

    private void attachSwipeController() {
        swipeController = new SwipeController(new SwipeControllerActions() {
            @Override
            public void onLeftClicked(int position) {
                Address address = addressAdapter.getItem(position);

                String addressId = address.getAddressId();
                String addressType = address.getAddressType();
                String address1 = address.getAddress1();
                String address2 = address.getAddress2();
                String city = address.getCity();
                String state = address.getState();
                String zipCode = address.getZipCode();
                String country = address.getCountry();
                Boolean isDefault = address.getDefault();
                Boolean edit = true;

                Intent editAddress = new Intent(UserProfileActivity.this, AddAddress.class);
                editAddress.putExtra("is_default", isDefault);
                editAddress.putExtra("address_id", addressId);
                editAddress.putExtra("address_type", addressType);
                editAddress.putExtra("address1", address1);
                editAddress.putExtra("address2", address2);
                editAddress.putExtra("city", city);
                editAddress.putExtra("state", state);
                editAddress.putExtra("zip_code", zipCode);
                editAddress.putExtra("country", country);
                editAddress.putExtra("edit", edit);

                startActivity(editAddress);

            }

            @Override
            public void onRightClicked(int position) {
                Address address = addressAdapter.getItem(position);
                presenter.deleteAddress(getToken(), address.getAddressId(), position, preferences);

            }
        });
        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
        itemTouchhelper.attachToRecyclerView(mRecyclerView);

        mRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                swipeController.onDraw(c);
            }
        });
    }

    private void setAddressDataAdapter(AccountProto.User user) {
        addressList.clear();
        int addressCount = user.getAddressesCount();

        //clear addresses if no address found from server
        if (addressCount == 0) {
            AddressRepo.getInstance().deleteAllAddresses(new Repo.Callback() {
                @Override
                public void success(Object o) {
                    AppUtils.showLog(TAG, "deleted all addresses");
                }

                @Override
                public void fail() {
                    AppUtils.showLog(TAG, "failed to deleted all addresses");
                }
            });

            return;
        }


        for (int i = 0; i < addressCount; i++) {

            filterItems(user.getAddresses(i));

            Address address = new Address();
            address.setAddressId(user.getAddresses(i).getAddressId());
            address.setAddressType(user.getAddresses(i).getType().toString());
            address.setAddress1(user.getAddresses(i).getLine1());
            address.setAddress2(user.getAddresses(i).getLine2());
            address.setCity(user.getAddresses(i).getCity());
            address.setCountry(user.getAddresses(i).getCountry());
            address.setState(user.getAddresses(i).getState());
            address.setZipCode(user.getAddresses(i).getZipCode());
            address.setDefault(user.getAddresses(i).getIsDefault());

            addressList.add(address);

        }
    }

    private void filterItems(AccountProto.Address address) {
        String addressId = address.getAddressId();
        List<Address> dbAddress = AddressRepo.getInstance().getAllAddresses();
        AppUtils.showLog(TAG, "addressCountDb: " + dbAddress.size());
        for (Address addressObj : dbAddress
        ) {
            String addressIdFromDb = addressObj.getAddressId();
            if (!addressId.equals(addressIdFromDb)) {
                AddressRepo.getInstance().deleteAddress(addressIdFromDb, new Repo.Callback() {
                    @Override
                    public void success(Object o) {
                        AppUtils.showLog(TAG, "filter success");
                    }

                    @Override
                    public void fail() {
                        AppUtils.showLog(TAG, "filter failed");
                    }
                });
            }
        }
    }

    @Override
    public void onAppForeground() {
        if (active) showPasscodeDialog(this);
    }
}
