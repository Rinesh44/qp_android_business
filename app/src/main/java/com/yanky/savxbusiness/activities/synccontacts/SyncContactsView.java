package com.yanky.savxbusiness.activities.synccontacts;

import com.yanky.savxbusiness.realm.models.SyncedContacts;

import java.util.List;

public interface SyncContactsView {
    void syncContactSuccess(List<SyncedContacts> syncedContactsList);

    void syncContactFail(String msg);
}
