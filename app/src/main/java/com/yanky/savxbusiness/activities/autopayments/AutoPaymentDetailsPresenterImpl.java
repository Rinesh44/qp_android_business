package com.yanky.savxbusiness.activities.autopayments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.yanky.savxbusiness.activities.account.LoginActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.realm.RealmDatabase;
import com.yanky.savxbusiness.utils.CallbackWrapper;

import io.realm.Realm;
import retrofit2.Response;

public class AutoPaymentDetailsPresenterImpl implements AutoPaymentDetailsPresenter {
    private static final String TAG = "AutoPaymentDetailsPrese";
    private Endpoints endpoints;
    private AutoPaymentDetails activity;


    public AutoPaymentDetailsPresenterImpl(Endpoints endpoints, AutoPaymentDetails activity) {
        this.endpoints = endpoints;
        this.activity = activity;
    }

    @Override
    public void deleteAutoPayment(String token, String paymentId, SharedPreferences preferences) {
        endpoints.deleteAutoPayment(token, paymentId).enqueue(new CallbackWrapper<>(activity, new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
            @Override
            public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                ReqResProto.BaseResponse baseResponse = response.body();

                if (baseResponse == null) {
                    activity.deleteAutopaymentFail(null);
                    return;
                }


                if (baseResponse.getError()) {
                    if (baseResponse.getMsg().equals("Authorization failed.")) {
                        clearSharedPrefsAndRealm(preferences);
                        Intent intent = new Intent(activity, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        activity.startActivity(intent);
                        Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                    } else activity.showMessage(baseResponse.getMsg());
                    return;
                }

                activity.deleteAutopaymentSuccess();
            }

            @Override
            public void onFailureResult() {
                activity.deleteAutopaymentFail(null);
            }
        }));
    }

    private void clearSharedPrefsAndRealm(SharedPreferences preferences) {
        //clear shared preferences and realm after logout
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();

        final Realm realm = RealmDatabase.getInstance().getRealm();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });
    }
}
