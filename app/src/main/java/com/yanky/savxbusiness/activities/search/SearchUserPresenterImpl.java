package com.yanky.savxbusiness.activities.search;

import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.yanky.savxbusiness.activities.account.LoginActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.entities.AccountProto;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.realm.RealmDatabase;
import com.yanky.savxbusiness.realm.models.User;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.CallbackWrapper;

import java.util.ArrayList;

import io.realm.Realm;
import retrofit2.Response;

public class SearchUserPresenterImpl implements SearchUserPresenter {
    private static final String TAG = "SearchUserPresenterImpl";
    private SearchUser activity;
    private Endpoints endpoints;


    public SearchUserPresenterImpl(SearchUser activity, Endpoints endpoints) {
        this.activity = activity;
        this.endpoints = endpoints;
    }

    @Override
    public void search(String token, String charSequence, SharedPreferences preferences) {
        endpoints.searchUsers(token, charSequence).enqueue(new CallbackWrapper<>(activity,
                new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
                    @Override
                    public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {

                        ReqResProto.BaseResponse baseResponse = response.body();

                        if (baseResponse == null) {
                            activity.onSearchFail(null);
                            return;
                        }

                        if (baseResponse.getError()) {
                            if (baseResponse.getMsg().equals("Authorization failed.")) {
                                clearSharedPrefsAndRealm(preferences);
                                Intent intent = new Intent(activity, LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                activity.startActivity(intent);
                                Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                            } else activity.onSearchFail(baseResponse.getMsg());
                            return;
                        }

                        int count = baseResponse.getUsersCount();
                        AppUtils.showLog(TAG, "usercount: " + count);

                        ArrayList<User> userList = new ArrayList<>();
                        for (AccountProto.User userPb : baseResponse.getUsersList()
                        ) {
                            User user = new User();
                            user.setFullName(userPb.getFullName());
                            user.setEmailPhone(userPb.getEmailPhone());
                            user.setUserId(userPb.getUserId());
                            user.setAddress(userPb.getWalletAddress());
                            userList.add(user);
                        }

                        activity.onSearchSuccess(userList);


                    }

                    @Override
                    public void onFailureResult() {
                        activity.onSearchFail(null);
                    }
                }));


    }


    private void clearSharedPrefsAndRealm(SharedPreferences prefs) {
        //clear shared preferences and realm after logout
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();

        final Realm realm = RealmDatabase.getInstance().getRealm();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });
    }

}
