package com.yanky.savxbusiness.activities.synccontacts;

import android.content.SharedPreferences;

import com.yanky.savxbusiness.model.Contacts;

import java.util.List;

public interface SyncContactPresenter {
    void syncContact(String token, List<Contacts> contactsList, SharedPreferences preferences);
}
