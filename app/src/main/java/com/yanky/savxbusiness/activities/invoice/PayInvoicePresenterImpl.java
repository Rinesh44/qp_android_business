package com.yanky.savxbusiness.activities.invoice;

import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.yanky.savxbusiness.activities.account.LoginActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.crypto.Crypto;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.entities.TxProto;
import com.yanky.savxbusiness.realm.RealmDatabase;
import com.yanky.savxbusiness.realm.models.UnpaidInvoice;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.CallbackWrapper;

import org.libsodium.jni.encoders.Hex;

import io.realm.Realm;
import retrofit2.Response;

public class PayInvoicePresenterImpl implements PayInvoicePresenter {
    private static final String TAG = "PayInvoicePresenterImpl";
    private PayInvoice activity;
    private Endpoints endpoints;
    private String signatureEncoded;

    public PayInvoicePresenterImpl(PayInvoice activity, Endpoints endpoints) {
        this.activity = activity;
        this.endpoints = endpoints;
    }

    @Override
    public void payInvoice(String token, UnpaidInvoice invoice, String totalAmount, String userId, String note,
                           String fromAddress, SharedPreferences preferences) {
        AppUtils.showLog(TAG, "fromPubKey:" + fromAddress);
        activity.showLoading();
        Double amount = Double.valueOf(totalAmount) * 100;
        String amountString = String.valueOf(amount);
        amountString = amountString.replace(".0", "");
        TxProto.Transaction transaction = TxProto.Transaction.newBuilder()
                .setMemo(note)
                .setFromUserId(userId)
                .setToPubKey(invoice.getSignaturePubKey())
                .setFromPubKey(fromAddress)
                .setToUserId(invoice.getSenderUserId())
                .setAmount(Long.valueOf(amountString))
                .build();

        AppUtils.showLog(TAG, "signaturePugKey:" + fromAddress);
        AppUtils.showLog(TAG, "toWalletAddress:" + invoice.getSignaturePubKey());

        byte[] signature = Crypto.build().sign(transaction.toByteArray(), userId, fromAddress);


        if (null != signature) {
            signatureEncoded = Hex.HEX.encode(signature);
            AppUtils.showLog(TAG, "signature: " + signatureEncoded);


            TxProto.TransactionHolder transactionHolder = TxProto.TransactionHolder.newBuilder()
                    .setTransaction(transaction.toByteString())
                    .setSignature(signatureEncoded)
                    .setTransactionType(TxProto.TransactionType.INVOICE)
                    .setFromUserId(userId)
                    .setToUserId(invoice.getSenderUserId())
                    .setSignaturePubKey(fromAddress)
                    .setToWalletAddress(invoice.getSignaturePubKey())
                    .setToUserId(invoice.getSenderUserId())
                    .build();

            endpoints.transferFund(token, transactionHolder).enqueue(new CallbackWrapper<>(activity, new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
                @Override
                public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                    activity.hideLoading();
                    ReqResProto.BaseResponse baseResponse = response.body();

                    if (baseResponse == null) {
                        activity.invvoicePayFail(null);
                        return;
                    }

                    if (baseResponse.getError()) {
                        if (baseResponse.getMsg().equals("Authorization failed.")) {
                            clearSharedPrefsAndRealm(preferences);
                            Intent intent = new Intent(activity, LoginActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            activity.startActivity(intent);
                            Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                        } else activity.invvoicePayFail(baseResponse.getMsg());
                        return;
                    }

                    activity.invoicePaySuccess();
                }

                @Override
                public void onFailureResult() {
                    activity.hideLoading();
                    activity.invvoicePayFail(null);
                }
            }));

        }
    }

    private void clearSharedPrefsAndRealm(SharedPreferences prefs) {
        //clear shared preferences and realm after logout
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();

        final Realm realm = RealmDatabase.getInstance().getRealm();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });
    }
}
