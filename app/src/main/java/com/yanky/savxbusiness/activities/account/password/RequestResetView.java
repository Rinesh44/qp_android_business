package com.yanky.savxbusiness.activities.account.password;

import com.yanky.savxbusiness.entities.AccountProto;

/**
 * Created by Leesa Shakya on 02/10/18.
 * leezshk@gmail.com
 */

public interface RequestResetView {
    void emptyEmailPhone();
    void validEmailPhone();
    void invalidEmailPhone();
    void onRequestSuccess(AccountProto.User user);
    void onRequestFailure(String message);
}
