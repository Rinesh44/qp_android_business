package com.yanky.savxbusiness.activities.dashboard;

import android.content.SharedPreferences;

public interface DashboardPresenter {
    void getAllWallets(String token, SharedPreferences preferences);

    void getUnpaidInvoice(String token, SharedPreferences preferences);

    void getPaidInvoice(String token, SharedPreferences preferences);

    void getCountryCode();
}
