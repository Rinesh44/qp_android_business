package com.yanky.savxbusiness.activities.account;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scrounger.countrycurrencypicker.library.Country;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.account.password.RequestResetActivity;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.activities.keys.EnterPasscodeActivity;
import com.yanky.savxbusiness.activities.keys.GenerateKeyActivity;
import com.yanky.savxbusiness.activities.verification.VerificationActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.entities.AccountProto;
import com.yanky.savxbusiness.realm.models.CountryDetails;
import com.yanky.savxbusiness.repo.CountryDetailsRepo;
import com.yanky.savxbusiness.repo.Repo;
import com.yanky.savxbusiness.repo.UserRepo;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.Constants;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;

import static com.yanky.savxbusiness.QuiqpayApp.getMyApplication;

/**
 * Created by Leesa Shakya on 25/09/18.
 * leezshk@gmail.com
 */

public class LoginActivity extends BaseActivity implements LoginView {
    public String TAG = LoginActivity.class.getSimpleName();
    @Inject
    Endpoints endpoints;
    @BindView(R.id.edit_text_email_phone)
    TextInputEditText mEmailPhoneEditText;
    @BindView(R.id.edit_text_password)
    TextInputEditText mPasswordEditText;
    @BindView(R.id.input_layout_email_phone)
    TextInputLayout mEmailPhoneLayout;
    @BindView(R.id.input_layout_password)
    TextInputLayout mPasswordLayout;
    @BindView(R.id.text_view_forget_password)
    TextView mForgotPassword;
    @BindView(R.id.button_login)
    Button mLoginButton;
    @BindView(R.id.bottomsheet_create_user)
    LinearLayout mBottomSheet;
    @BindView(R.id.tv_user)
    TextView mCreateUser;
    @BindView(R.id.tv_business)
    TextView mCreateBusinessUser;

    LoginPresenter presenter;
    String deviceId, deviceInfo;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    private BottomSheetBehavior sheetBehavior;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        getWindow().setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.background));
        getMyApplication(this).getAppComponent().inject(this);
        setUpDeviceId();

        init();
        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        presenter.getCountryCode();

        AppUtils.showLog(TAG, "countrycode:" + preferences.getString(Constants.SELECTED_COUNTRY_CODE, null));
        AppUtils.showLog(TAG, "currency:" + preferences.getString(Constants.SELECTED_CURRENCY, null));


        mCreateUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });

        mCreateBusinessUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, RegisterBusinessAccountFirst.class));
            }
        });
    }

    private void init() {
        System.out.println("device id:" + deviceId);
        presenter = new LoginPresenterImpl(this, endpoints, new LoginInteractorImpl(),
                deviceId, deviceInfo);

        sheetBehavior = BottomSheetBehavior.from(mBottomSheet);
    }

    @OnTextChanged(R.id.edit_text_email_phone)
    void validateEmailPhone(Editable editable) {
        presenter.validateEmailPhone(editable.toString());
    }

    @OnTextChanged(R.id.edit_text_password)
    void validatePassword(Editable editable) {
        presenter.validatePassword(editable.toString());
    }

    public void onLogin(View view) {
//        hideKeyboard();
        String emailPhone = mEmailPhoneEditText.getText().toString();
        String password = mPasswordEditText.getText().toString();
        presenter.onLogin(emailPhone, password);
    }

    public void onSignUp(View view) {
        toggleBottomSheet();
//        startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
    }

    public void onForgetPassword(View view) {
        startActivity(new Intent(LoginActivity.this, RequestResetActivity.class));
    }

    public void toggleBottomSheet() {
        if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }


    @Override
    public void emptyEmailPhone() {
        mEmailPhoneLayout.setErrorEnabled(true);
        mEmailPhoneLayout.setError(getString(R.string.this_field_is_required));
        mEmailPhoneEditText.requestFocus();
    }

    @Override
    public void validEmailPhone() {
        mEmailPhoneLayout.setErrorEnabled(false);
        mEmailPhoneLayout.setError(null);
    }

    @Override
    public void invalidEmailPhone() {
        mEmailPhoneLayout.setErrorEnabled(true);
        mEmailPhoneLayout.setError(getString(R.string.invalid_email_phone));
        mEmailPhoneEditText.requestFocus();
    }

    @Override
    public void emptyPassword() {
        mPasswordLayout.setErrorEnabled(true);
        mPasswordLayout.setError(getString(R.string.this_field_is_required));
        mPasswordEditText.requestFocus();
    }

    @Override
    public void validPassword() {
        mPasswordLayout.setErrorEnabled(false);
        mPasswordLayout.setError(null);
    }

    @Override
    public void loginSuccess(final AccountProto.LoginResponse loginResponse) {

        UserRepo.getInstance().saveUser(loginResponse, new Repo.Callback() {

            @Override
            public void success(Object o) {

                AppUtils.showLog(TAG, "loginSuccess()");
                AccountProto.User profile = loginResponse.getProfile();
                boolean isKeyBackedUp = loginResponse.getIsKeyBackup();
                String token = loginResponse.getToken();
                String userId = loginResponse.getProfile().getUserId();
                String country = loginResponse.getProfile().getCountry();

                Country userCountry = Country.getCountryWithCurrency(country, LoginActivity.this);
                String currency = userCountry.getCurrency().getName();

                AppUtils.showLog(TAG, "TOKen: " + token);
                AppUtils.showLog(TAG, "country: " + country);
                AppUtils.showLog(TAG, "currency: " + currency);

                //store token and iskeyBackup to shared prefs
                editor = preferences.edit();
                editor.putString(Constants.USER_TOKEN, token);
                editor.putString(Constants.SELECTED_COUNTRY_CODE, country);
                editor.putString(Constants.SELECTED_CURRENCY, currency);
                editor.putString(Constants.USER_ID, userId);
                editor.putBoolean(Constants.LOGGED_IN, true);
                editor.putBoolean(Constants.IS_KEY_BACKUP, isKeyBackedUp);
                editor.apply();

                presenter.checkUserStatus(profile, isKeyBackedUp);

            }

            @Override
            public void fail() {
                AppUtils.showLog(TAG, "loginSaveFail()");
                showMessage(null);
            }
        });
    }

    @Override
    public void loginFailure(String message) {
        showMessage(message);
    }

    @Override
    public void onVerificationSuccess(String userId) {
        Intent intent = new Intent(LoginActivity.this, GenerateKeyActivity.class);
        intent.putExtra(Constants.USER_ID, userId);
        startActivity(intent);
        finish();
    }

    @Override
    public void onVerificationFailure() {
        Intent intent = new Intent(this, VerificationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void enterPasscode(boolean isKeyBackedUp, String userId) {
        Intent intent = new Intent(LoginActivity.this, EnterPasscodeActivity.class);
//        intent.putExtra("key_backed_up", isKeyBackedUp);
        intent.putExtra(Constants.IS_FROM_LOGIN, true);
//        intent.putExtra(Constants.USER_ID, userId);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void onCountryCodeSuccess(List<CountryDetails> countryDetailsList) {
        AppUtils.showLog(TAG, "getCountryCodeSuccess");
        CountryDetailsRepo.getInstance().saveCountryDetails(countryDetailsList, new Repo.Callback() {
            @Override
            public void success(Object o) {
                AppUtils.showLog(TAG, "all country details saved");
            }

            @Override
            public void fail() {
                AppUtils.showLog(TAG, "failed to save country details");
            }
        });
    }

    @Override
    public void onCountryCodeFail(String msg) {
        showMessage(msg);
    }

    private void setUpDeviceId() {
        deviceInfo = Build.MANUFACTURER + " " + Build.MODEL;
        checkRequiredPermissions();
    }

    private void checkRequiredPermissions() {
        if (hasPermission(Manifest.permission.READ_PHONE_STATE) && hasPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                && hasPermission((Manifest.permission.WRITE_EXTERNAL_STORAGE))) {
            deviceId = getDeviceId();
            init();
            return;
        }
        requestPermissionsSafely(new String[]{Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE}, 22);
    }


    @SuppressLint({"MissingPermission", "HardwareIds"})
    private String getDeviceId() {
        TelephonyManager telephonyManager = (TelephonyManager)
                this.getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager != null) {
            return telephonyManager.getDeviceId() + ":" + Build.MODEL;
        }
        return null;
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (sheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 22) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                deviceId = getDeviceId();
                init();
            }
        }
    }

    /**
     * required to dismiss bottomsheet when touched outside
     *
     * @param event
     * @return
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (sheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {

                Rect outRect = new Rect();
                mBottomSheet.getGlobalVisibleRect(outRect);

                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY()))
                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        }

        return super.dispatchTouchEvent(event);
    }

}