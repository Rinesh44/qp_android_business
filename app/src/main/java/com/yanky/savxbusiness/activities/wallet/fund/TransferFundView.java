package com.yanky.savxbusiness.activities.wallet.fund;

public interface TransferFundView {
    void transferSuccess();

    void transferFail(String msg);
}
