package com.yanky.savxbusiness.activities.wallet.fund;

import android.content.SharedPreferences;

import com.yanky.savxbusiness.realm.models.User;

public interface TransferToUserPresenter {

    void transferToUser(String token, String transferAmount, String desc, User user,
                        String fromWalletAddress, String fromUserId, String currency,
                        SharedPreferences preferences);
}
