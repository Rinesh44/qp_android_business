package com.yanky.savxbusiness.activities.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.yanky.savxbusiness.utils.AppLifecycleHandler;
import com.yanky.savxbusiness.utils.AppUtils;

public class ScreenOffReceiver extends BroadcastReceiver {
    private String TAG = ScreenOffReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        AppUtils.showLog(TAG, "screenOffReceive");
        AppLifecycleHandler.appInForeground = false;
    }
}
