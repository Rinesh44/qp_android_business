package com.yanky.savxbusiness.activities.account.password;

import android.text.TextUtils;

import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.entities.AccountProto;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.utils.CallbackWrapper;

import retrofit2.Response;

/**
 * Created by Leesa Shakya on 03/10/18.
 * leezshk@gmail.com
 */

public class ResetPasswordPresenterImpl implements ResetPasswordPresenter {
    private final ResetPasswordActivity activity;
    private final Endpoints endpoints;

    ResetPasswordPresenterImpl(ResetPasswordActivity activity, Endpoints endpoints) {
        this.activity = activity;
        this.endpoints = endpoints;
    }

    @Override
    public void validatePassword(String password) {
        if (TextUtils.isEmpty(password)) {
            activity.emptyPassword();
            return;
        }
        if (password.length() < 6) {
            activity.invalidPassword();
            return;
        }
        activity.validPassword();
    }

    @Override
    public void resetPassword(String password, String userId, String resetToken) {
        if (TextUtils.isEmpty(password)) {
            activity.emptyPassword();
            return;
        }
        if (password.length() < 6) {
            activity.invalidPassword();
            return;
        }

        AccountProto.PasswordReset passwordReset = AccountProto.PasswordReset.newBuilder()
                .setNewPassword(password)
                .setResetToken(resetToken)
                .setUserId(userId)
                .build();

        activity.showLoading();
        endpoints.resetPassword(passwordReset).enqueue(new CallbackWrapper<>(activity,
                new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
                    @Override
                    public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                        activity.hideLoading();
                        ReqResProto.BaseResponse baseResponse = response.body();
                        if (baseResponse == null) {
                            activity.resetPasswordFailure(null);
                            return;
                        }
                        if (baseResponse.getError()) {
                            activity.resetPasswordFailure(baseResponse.getMsg());
                            return;
                        }
                        activity.resetPasswordSuccess();
                    }

                    @Override
                    public void onFailureResult() {
                        activity.hideLoading();
                        activity.resetPasswordFailure(null);
                    }
                }));
    }
}
