package com.yanky.savxbusiness.activities.wallet.fund;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.scrounger.countrycurrencypicker.library.Country;
import com.yanky.savxbusiness.QuiqpayApp;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.activities.dashboard.DashboardActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.utils.AppForegroundListener;
import com.yanky.savxbusiness.utils.AppUtils;

import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.yanky.savxbusiness.QuiqpayApp.getMyApplication;

public class LoadFund extends BaseActivity implements LoadFundView, AppForegroundListener {
    private static final String TAG = "LoadFund";
    @Inject
    Endpoints endpoints;
    @BindView(R.id.walletname)
    TextView mWalletName;
    @BindView(R.id.toolbar_title)
    TextView mTitle;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.amount)
    TextView mWalletAmount;
    @BindView(R.id.sp_account_type)
    Spinner mAccountType;
    @BindView(R.id.btn_load_fund)
    Button mLoadFund;
    @BindView(R.id.et_name_on_account)
    EditText mNameOnAccount;
    @BindView(R.id.et_account_no)
    EditText mAccountNo;
    @BindView(R.id.et_account_routing_no)
    EditText mRoutingNumber;
    @BindView(R.id.et_account_name)
    EditText mAccountName;
    @BindView(R.id.et_amount_to_load)
    EditText mAmountToLoad;
    @BindView(R.id.et_desc)
    EditText mDesc;


    private String walletName, walletAmount, walletAddress, walletCurrency, walletCountryCode;
    private LoadFundPresenterImpl presenter;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_fund);
        ButterKnife.bind(this);

        getMyApplication(this).getAppComponent().inject(this);


        //prevent keyboard glich when its introduced
        getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.background));

        init();

        setUpSpinner();

        getIntentValues();
        mTitle.setText(getResources().getString(R.string.load_fund_from_bank));
        mWalletName.setText(walletName);

        Country countryWithCurrency = Country.getCountryWithCurrency(walletCountryCode, this);
        assert countryWithCurrency != null;
        String currencySymbol = Objects.requireNonNull(countryWithCurrency.getCurrency()).getSymbol();
        if (currencySymbol.length() > 1) {
            currencySymbol = currencySymbol.substring(currencySymbol.length() - 1);
        }

        StringBuilder amountBuilder = new StringBuilder();
        amountBuilder.append(walletCurrency);
        amountBuilder.append(" ");
        amountBuilder.append(currencySymbol);
        amountBuilder.append(" ");
        amountBuilder.append(String.format("%.0f", Double.valueOf(walletAmount)));

        mWalletAmount.setText(String.valueOf(amountBuilder));

     /*   int flagId = Country.getCountry(walletCountryCode, this).getFlagId();
        if (flagId != 0) {
            Drawable drawable = this.getResources().getDrawable(flagId);
            mWalletAmount.setCompoundDrawablePadding(20);
            mWalletAmount.setCompoundDrawablesWithIntrinsicBounds(resize(drawable), null, null, null);
        }*/

        mLoadFund.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String bankAcountName = mNameOnAccount.getText().toString().trim();
                String accountNumber = mAccountNo.getText().toString().trim();
                String routingNumber = mRoutingNumber.getText().toString().trim();
                String nameOnAccount = mAccountName.getText().toString().trim();
                String accountType = mAccountType.getSelectedItem().toString();
                String amountToLoad = mAmountToLoad.getText().toString().trim();
                String description = mDesc.getText().toString().trim();

                //check for empty fields
                if (bankAcountName.isEmpty()) {
                    Toast.makeText(LoadFund.this, "Please enter bank account name", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (accountNumber.isEmpty()) {
                    Toast.makeText(LoadFund.this, "Please enter account number", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (routingNumber.isEmpty()) {
                    Toast.makeText(LoadFund.this, "Please enter routing number", Toast.LENGTH_SHORT).show();
                    return;
                }


                if (nameOnAccount.isEmpty()) {
                    Toast.makeText(LoadFund.this, "Please enter account name", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (accountType.equals("Select A/C Type")) {
                    Toast.makeText(LoadFund.this, "Please enter account type", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (amountToLoad.isEmpty()) {
                    Toast.makeText(LoadFund.this, "Please enter amount to load", Toast.LENGTH_SHORT).show();
                    return;
                }


                showConfimationDialog(nameOnAccount,
                        amountToLoad, description, bankAcountName, accountNumber, routingNumber, accountType);

            }
        });

    }

    private void showConfimationDialog(String nameOnAccount, String amountToload, String description, String
            bankAcountName, String accountNumber, String routingNumber,
                                       String accountType) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);

        //inflate custom layout in dialog
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.confirm_load_fund, null);
        dialogBuilder.setView(dialogView);

        TextView bankName = dialogView.findViewById(R.id.tv_bank_name);
        TextView amount = dialogView.findViewById(R.id.tv_amount);
        TextView desc = dialogView.findViewById(R.id.tv_desc);

        bankName.setText(nameOnAccount);
        amount.setText(amountToload);
        desc.setText(description);

        Button cancel = dialogView.findViewById(R.id.btn_cancel);
        Button confirm = dialogView.findViewById(R.id.btn_confirm);

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                presenter.loadFund(getToken(), bankAcountName, accountNumber, routingNumber,
                        nameOnAccount, accountType, amountToload, getUserID(), walletAddress,
                        walletCurrency, preferences);
            }
        });


    }

    private void setUpSpinner() {
        String[] items = new String[]{"Select A/C Type", "Consumer Checking", "Consumer Savings", "Business Checking", "Business Savings"};
        ArrayAdapter<String> accountTypeAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,
                items);
        accountTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mAccountType.setAdapter(accountTypeAdapter);
    }

    private void init() {

        presenter = new LoadFundPresenterImpl(endpoints, this);
        setUpToolbar(mToolbar);
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
    }

    public void getIntentValues() {
        Intent i = getIntent();
        walletName = i.getStringExtra("wallet_name");
        walletAmount = i.getStringExtra("wallet_amount");
        walletAddress = i.getStringExtra("wallet_address");
        walletCountryCode = i.getStringExtra("wallet_country_code");
        walletCurrency = i.getStringExtra("wallet_currency");

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void loadFundSuccess() {
        AppUtils.showLog(TAG, "loadFUndSUccess");
        Toast.makeText(this, "Fund loaded successfully", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(this, DashboardActivity.class));
        finish();
    }

    @Override
    public void loadFundFail(String msg) {

    }

    @Override
    public void onAppForeground() {
        showPasscodeDialog(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        QuiqpayApp.setAppForegroundListener(this);
    }
}
