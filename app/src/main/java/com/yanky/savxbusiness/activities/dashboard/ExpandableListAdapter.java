package com.yanky.savxbusiness.activities.dashboard;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.realm.models.User;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Leesa Shakya on 03/10/18.
 * leezshk@gmail.com
 */

public class ExpandableListAdapter extends BaseExpandableListAdapter {
    private final Context context;
    private final List<String> menuData;
    private final HashMap<String, List<String>> optionData;
    private HashMap<String, Drawable> iconData;
    private User user;

    ExpandableListAdapter(Context context, List<String> menuData, HashMap<String,
            List<String>> optionData, HashMap<String, Drawable> iconData, User user) {
        this.context = context;
        this.menuData = menuData;
        this.optionData = optionData;
        this.iconData = iconData;
        this.user = user;
    }

    @Override
    public int getGroupCount() {
        return menuData.size();
    }

    @Override
    public int getChildrenCount(int i) {
        if (optionData.get(menuData.get(i)) == null) {
            return 0;
        }
        return optionData.get(menuData.get(i)).size();
    }

    @Override
    public Object getGroup(int i) {
        return menuData.get(i);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return optionData.get(menuData.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupId) {
        return groupId;
    }

    @Override
    public long getChildId(int groupId, int childId) {
        return childId;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (groupPosition == 0) {
            view = inflater.inflate(R.layout.layout_nav_header, null);
            SimpleDraweeView mProfilePicture = view.findViewById(R.id.image_profile);
            TextView mNameTextView = view.findViewById(R.id.text_view_name);
            TextView mEmailPhoneTextView = view.findViewById(R.id.text_view_email_phone);
            TextView mBalanceTextView = view.findViewById(R.id.text_view_balance);

            mNameTextView.setText(user.getFullName());
            mEmailPhoneTextView.setText(user.getEmailPhone());
        }

        else{
            view = inflater.inflate(R.layout.layout_nav_menu_title, null);

            TextView mTitleTextView = view.findViewById(R.id.text_view_menu_title);
            ImageView arrowIcon = view.findViewById(R.id.image_view_arrow);
            ImageView icon = view.findViewById(R.id.image_view_icon);
            View divider = view.findViewById(R.id.divider);

            mTitleTextView.setText(menuData.get(groupPosition));
            arrowIcon.setVisibility(optionData.get(menuData.get(groupPosition)) == null ?
                    View.GONE : View.VISIBLE);
            arrowIcon.setImageDrawable(context.getDrawable(isExpanded ? R.drawable.ic_arrow_up :
                    R.drawable.ic_arrow_down));
            icon.setImageDrawable(iconData.get(menuData.get(groupPosition)));

            divider.setVisibility(groupPosition == menuData.size() - 5 ? View.VISIBLE :View.INVISIBLE);
        }
        return view;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean b, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.layout_nav_options, null);
        }
        TextView txtListChild = view.findViewById(R.id.text_view_options);
        txtListChild.setText(optionData.get(menuData.get(groupPosition)).get(childPosition));
        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }

    public void keyBackupSuccess(){};
}
