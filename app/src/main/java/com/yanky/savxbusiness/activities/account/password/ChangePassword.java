package com.yanky.savxbusiness.activities.account.password;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.api.Endpoints;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.yanky.savxbusiness.QuiqpayApp.getMyApplication;

public class ChangePassword extends BaseActivity implements ChangePasswordView, View.OnClickListener {
    @Inject
    Endpoints endpoints;
    @BindView(R.id.et_current_password)
    EditText mCurrentPassword;
    @BindView(R.id.et_new_password)
    EditText mNewPassword;
    @BindView(R.id.et_confirm_password)
    EditText mConfirmPassword;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.btn_confirm)
    Button mConfirm;

    private ChangePasswordPresenterImpl presenter;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        ButterKnife.bind(this);

        getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.background));

        getMyApplication(this).getAppComponent().inject(this);
        init();

        mConfirm.setOnClickListener(this);

    }

    private void init() {
        presenter = new ChangePasswordPresenterImpl(this, endpoints);

        setUpToolbar(mToolbar);
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        mToolbarTitle.setText("Change Password");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btn_confirm:
                String currentPassword = mCurrentPassword.getText().toString().trim();
                String newPassword = mNewPassword.getText().toString().trim();
                String confirmPassword = mConfirmPassword.getText().toString().trim();
                presenter.changePassword(getToken(), currentPassword, newPassword, confirmPassword, preferences);
                break;
        }
    }

    @Override
    public void invalidPassword(String msg) {
        showMessage(msg);
    }

    @Override
    public void onChangePasswordSuccess() {
        Toast.makeText(this, "Password changed", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onChangePasswordFail(String msg) {
        showMessage(msg);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
