package com.yanky.savxbusiness.activities.waitinglist;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.account.RegisterActivity;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.realm.models.WaitingUser;
import com.yanky.savxbusiness.repo.Repo;
import com.yanky.savxbusiness.repo.WaitingUserRepo;
import com.yanky.savxbusiness.utils.AppUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.yanky.savxbusiness.QuiqpayApp.getMyApplication;

public class RegisterSuccess extends BaseActivity implements RegisterSuccessView {
    private static final String TAG = "RegisterSuccess";
    @Inject
    Endpoints endpoints;
    @BindView(R.id.tv_success_msg)
    TextView mSuccessMsg;
    @BindView(R.id.tv_position)
    TextView mPosition;
    @BindView(R.id.tv_total)
    TextView mTotal;
    @BindView(R.id.btn_bump)
    Button mBump;
    @BindView(R.id.btn_logout)
    Button mLogout;
    public static Button mRegisterNow;

    private int queuePosition, totalWaitingUsers;
    private String username;
    private boolean fetchUser;

    private RegisterUserPresenterImpl presenter;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_success);

        ButterKnife.bind(this);

        getMyApplication(this).getAppComponent().inject(this);
        presenter = new RegisterUserPresenterImpl(this, endpoints);

        mRegisterNow = findViewById(R.id.btn_register_now);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        getIntentValues();

        setCongratulationsText(username);

        if (fetchUser) {
            WaitingUser waitingUser = WaitingUserRepo.getInstance().getWaitingUser();
            presenter.fetchUser(getToken(), waitingUser.getEmailPhone());
        } else {
            mPosition.setText(String.valueOf(queuePosition));
            mTotal.setText(String.valueOf(totalWaitingUsers));
        }

        mLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                WaitingUserRepo.getInstance().deleteWaitingUser(new Repo.Callback() {
                    @Override
                    public void success(Object o) {
                        AppUtils.showLog(TAG, "waiting user deleted");
                    }

                    @Override
                    public void fail() {
                        AppUtils.showLog(TAG, "failed to delete waiting user");
                    }
                });


                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("waiting_user_registered", false);
                editor.apply();


                Intent i = new Intent(RegisterSuccess.this, EnterCredential.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                finish();
            }
        });


        mRegisterNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RegisterSuccess.this, RegisterActivity.class);
                intent.putExtra("waiting_user_register_success", true);
                startActivity(intent);
            }
        });

    }

    private void setCongratulationsText(String username) {
        StringBuilder successMsgBuilder = new StringBuilder();
        successMsgBuilder.append("Congratulations ");
        successMsgBuilder.append(username);
        successMsgBuilder.append(" you are in our waiting list. You will be notified when our app is ready");

        mSuccessMsg.setText(successMsgBuilder);
    }

    private void getIntentValues() {

        Intent i = getIntent();

        fetchUser = i.getBooleanExtra("fetch_user", false);
        queuePosition = i.getIntExtra("queue_position", 0);
        totalWaitingUsers = i.getIntExtra("total_waiting_users", 0);
        username = i.getStringExtra("user_name");

    }

    @Override
    public void fetchUserSuccess(ReqResProto.BaseResponse baseResponse) {
        AppUtils.showLog(TAG, "fetch user success");

        int queuePosition = baseResponse.getWaitingUser().getQueuePosition();
        int totalWaitingUsers = baseResponse.getWaitingUser().getTotal();
        username = baseResponse.getWaitingUser().getName();

        mPosition.setText(String.valueOf(queuePosition));
        mTotal.setText(String.valueOf(totalWaitingUsers));

        setCongratulationsText(username);

    }

    @Override
    public void fetchUserFail(String msg) {
        showMessage(msg);
    }
}
