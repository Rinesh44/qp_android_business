package com.yanky.savxbusiness.activities.wallet.fund;

public interface LoadFundView {
    void loadFundSuccess();

    void loadFundFail(String msg);
}
