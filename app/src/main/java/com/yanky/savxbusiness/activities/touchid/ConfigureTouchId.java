package com.yanky.savxbusiness.activities.touchid;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ConfigureTouchId extends BaseActivity {
    @BindView(R.id.sw_touch_id)
    SwitchCompat mTouchId;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configure_touch_id);

        ButterKnife.bind(this);

        init();


        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor editor = preferences.edit();


        if (preferences.getBoolean(Constants.TOUCHID_ENABLED, false)) {
            mTouchId.setChecked(true);
        } else {
            mTouchId.setChecked(false);
        }

        mTouchId.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                if (checked) {
                    editor.putBoolean(Constants.TOUCHID_ENABLED, true);
                    editor.apply();
                } else {
                    editor.putBoolean(Constants.TOUCHID_ENABLED, false);
                    editor.apply();
                }
            }
        });
    }

    private void init() {

        setUpToolbar(mToolbar);
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        mToolbarTitle.setText("Configure touch ID");
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
