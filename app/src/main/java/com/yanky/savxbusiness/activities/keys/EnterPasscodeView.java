package com.yanky.savxbusiness.activities.keys;

public interface EnterPasscodeView {

    void onPasscodeEntered();

    void emptyPassCode();

    void validPassCodeOne(String passcode);

    void validPassCodeTwo();

    void invalidPassCode();

    void invalidPassCodeTwo();

    void changeTitle();

//    void getIntentValues();

    void keyBackupSuccess();

    void keyBackupFail(String message);

    void createWalletSuccess();

}
