package com.yanky.savxbusiness.activities.transactionhistory;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scrounger.countrycurrencypicker.library.Country;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.realm.models.TransactionHolder;
import com.yanky.savxbusiness.utils.TransactionDescriptionHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TransactionDetails extends BaseActivity {
    @BindView(R.id.transaction_title)
    TextView mTransactionTitle;
    @BindView(R.id.date)
    TextView mDate;
    @BindView(R.id.status)
    TextView mStatus;
    @BindView(R.id.amount)
    TextView amount;
    @BindView(R.id.transaction_id)
    TextView mTransactionId;
    @BindView(R.id.fund_title)
    TextView mFundTitle;
    @BindView(R.id.wallet_name)
    TextView mWalletName;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.ll_items_container)
    LinearLayout mItemsContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_details);

        ButterKnife.bind(this);

        init();

        getIntentValuesAndSet();
    }

    private void init() {
        setUpToolbar(mToolbar);
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        mToolbarTitle.setText("Transaction Details");
    }

    private void getIntentValuesAndSet() {
        Intent i = getIntent();
        TransactionHolder transactionHolder = i.getParcelableExtra("transaction_object");
        String transactionTitle = TransactionDescriptionHelper.getDescriptionFor(transactionHolder, this);
        mTransactionTitle.setText(transactionTitle);

        mDate.setText(getDate(transactionHolder.getTimestamp()));
        mStatus.setText(TransactionDescriptionHelper.getStatus(transactionHolder, this));
        if (mStatus.getText().equals("INCOMING")) {
            mStatus.setTextColor(getResources().getColor(R.color.green));
            mFundTitle.setText("Fund loaded on wallet");
        } else {
            mStatus.setTextColor(getResources().getColor(R.color.reds));
            mFundTitle.setText("Fund transfered from wallet");
        }

        String transactionId = transactionHolder.getTransactionId().substring(0, 5);
        mTransactionId.setText(transactionId);
        mWalletName.setText(transactionHolder.getWalletName());


        StringBuilder amountBuilder = new StringBuilder();
        int flagId;
        if (transactionHolder.getTransactiontype().equals("Load Fund")) {
            amountBuilder.append(transactionHolder.getToCurrency());
            flagId = Country.getCountry(transactionHolder.getToCountryCode(), this).getFlagId();
        } else {
            amountBuilder.append(transactionHolder.getFromCurrency());
            flagId = Country.getCountry(transactionHolder.getFromCountryCode(), this).getFlagId();
        }

        amountBuilder.append(" ");
        amountBuilder.append(String.format("%.0f", Double.valueOf(transactionHolder.getAmount()) / 100));
        amount.setText(amountBuilder);


        if (flagId != 0) {
            Drawable drawable = getResources().getDrawable(flagId);
            Drawable resized = resize(drawable);
            amount.setCompoundDrawablePadding(10);
            amount.setCompoundDrawablesWithIntrinsicBounds(resized, null, null, null);
        }


        inflateRespectiveLayout(transactionHolder);
    }

    private void inflateRespectiveLayout(TransactionHolder transactionHolder) {
        switch (transactionHolder.getTransactiontype()) {
            case "Load Fund":
                inflateLoadFundLayout(transactionHolder);
                break;

            default:
                inflateDefaultLayout(transactionHolder);
                break;
        }
    }

    private void inflateDefaultLayout(TransactionHolder transactionHolder) {
        LinearLayout layout = (LinearLayout) getLayoutInflater().inflate(
                R.layout.additional_info_signature, mItemsContainer, false);
        mItemsContainer.addView(layout);

        //check if transaction is incoming or outgoing
        String status = TransactionDescriptionHelper.getStatus(transactionHolder, this);
        TextView userName = layout.findViewById(R.id.tv_user_name);
        TextView signatureTitle = layout.findViewById(R.id.tv_signature_title);
        TextView userNameTitle = layout.findViewById(R.id.tv_user_name_title);
        if (status.equals("INCOMING")) {
            userName.setText(transactionHolder.getFromUserFullName());
        } else {
            userNameTitle.setText("RECEIVER NAME");
            signatureTitle.setText("RECEIVER SIGNATURE");
            userName.setText(transactionHolder.getToUserFullName());
        }


        TextView transactionDesc = layout.findViewById(R.id.tv_transaction_desc);
        TextView signature = layout.findViewById(R.id.tv_signature);

        transactionDesc.setText(transactionHolder.getTransactionId().substring(0, 5));

        signature.setText(transactionHolder.getSignature().replaceAll("..", "$0 "));

    }

    private void inflateLoadFundLayout(TransactionHolder transactionHolder) {
        LinearLayout layout = (LinearLayout) getLayoutInflater().inflate(
                R.layout.additional_info_load_fund, mItemsContainer, false);
        mItemsContainer.addView(layout);

        TextView bankName = layout.findViewById(R.id.tv_bank_name);
        TextView accountName = layout.findViewById(R.id.tv_account_name);
        TextView routingNumber = layout.findViewById(R.id.tv_routing_number);
        TextView nameOnAccount = layout.findViewById(R.id.tv_name_on_account);
        TextView accountType = layout.findViewById(R.id.tv_account_type);
        TextView transactionDesc = layout.findViewById(R.id.tv_transaction_desc);

        bankName.setText(transactionHolder.getBankAccountName());
        accountName.setText(transactionHolder.getAccountNumber());
        routingNumber.setText(transactionHolder.getRoutingNumber());
        nameOnAccount.setText(transactionHolder.getNameOnAccount());
        accountType.setText(transactionHolder.getAccountType());
        transactionDesc.setText(transactionHolder.getBankAccountName());

    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
