package com.yanky.savxbusiness.activities.account.password;

import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.yanky.savxbusiness.activities.account.LoginActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.entities.AccountProto;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.realm.RealmDatabase;
import com.yanky.savxbusiness.utils.CallbackWrapper;

import io.realm.Realm;
import retrofit2.Response;

public class ChangePasswordPresenterImpl implements ChangePasswordPresenter {
    private static final String TAG = "ChangePasswordPresenter";
    private ChangePassword activity;
    private Endpoints endpoints;

    public ChangePasswordPresenterImpl(ChangePassword activity, Endpoints endpoints) {
        this.activity = activity;
        this.endpoints = endpoints;
    }

    @Override
    public void changePassword(String token, String currentPassword, String newPassword,
                               String confirmPassword, SharedPreferences preferences) {

        if (currentPassword.isEmpty()) {
            activity.invalidPassword("Old password cannot be empty");
            return;
        }

        if (currentPassword.length() < 6) {
            activity.invalidPassword("Old password cannot be less than 6");
            return;
        }

        if (newPassword.isEmpty()) {
            activity.invalidPassword("New password cannot be empty");
            return;
        }

        if (newPassword.length() < 6) {
            activity.invalidPassword("New password cannot be less than 6");
            return;
        }

        if (confirmPassword.isEmpty()) {
            activity.invalidPassword("Confirm password cannot be empty");
            return;
        }

        if (confirmPassword.length() < 6) {
            activity.invalidPassword("Confirm password cannot be less than 6");
            return;
        }

        if (!newPassword.equals(confirmPassword)) {
            activity.invalidPassword("Passwords did not match");
            return;
        }

        final AccountProto.PasswordChangeRequest passwordChangeRequest = AccountProto.PasswordChangeRequest.newBuilder()
                .setOldPassword(currentPassword)
                .setNewPassword(confirmPassword)
                .build();

        endpoints.updatePassword(token, passwordChangeRequest).enqueue(new CallbackWrapper<>(activity,
                new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
                    @Override
                    public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                        ReqResProto.BaseResponse baseResponse = response.body();

                        if (baseResponse == null) {
                            activity.onChangePasswordFail(null);
                            return;
                        }

                        if (baseResponse.getError()) {
                            if (baseResponse.getMsg().equals("Authorization failed.")) {
                                clearSharedPrefsAndRealm(preferences);
                                Intent intent = new Intent(activity, LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                activity.startActivity(intent);
                                Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                            } else activity.onChangePasswordFail(baseResponse.getMsg());
                            return;
                        }
                        activity.onChangePasswordSuccess();
                    }

                    @Override
                    public void onFailureResult() {
                        activity.hideLoading();
                        activity.onChangePasswordFail(null);
                    }
                }));
    }


    private void clearSharedPrefsAndRealm(SharedPreferences prefs) {
        //clear shared preferences and realm after logout
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();

        final Realm realm = RealmDatabase.getInstance().getRealm();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });
    }
}
