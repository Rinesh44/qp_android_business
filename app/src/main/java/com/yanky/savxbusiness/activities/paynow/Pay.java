package com.yanky.savxbusiness.activities.paynow;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.yanky.savxbusiness.QuiqpayApp;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.activities.paynow.mqttpay.ConnectWithServer;
import com.yanky.savxbusiness.activities.search.SearchUser;
import com.yanky.savxbusiness.adapters.CustomAutoCompleteAdapter;
import com.yanky.savxbusiness.adapters.CustomSpinnerAdapter;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.entities.AccountProto;
import com.yanky.savxbusiness.realm.models.User;
import com.yanky.savxbusiness.realm.models.Wallet;
import com.yanky.savxbusiness.repo.WalletsRepo;
import com.yanky.savxbusiness.utils.AppForegroundListener;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.SendSelectedUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.yanky.savxbusiness.QuiqpayApp.getMyApplication;

public class Pay extends BaseActivity implements PayView, AppForegroundListener, SendSelectedUser {
    private static final String TAG = "Pay";
    @Inject
    Endpoints endpoints;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.sp_wallets)
    Spinner mWalletSpinner;
    @BindView(R.id.at_search)
    AutoCompleteTextView mHolderName;
    @BindView(R.id.et_amount_to_pay)
    EditText mAmountToPay;
    @BindView(R.id.et_desc)
    EditText mDescription;
    @BindView(R.id.btn_pay_now)
    Button mPaynow;
    @BindView(R.id.btn_scan)
    Button mScan;

    private String userNameFromBarcode;


    private List<Wallet> walletList = new ArrayList<>();
    private List<String> spinnerItems = new ArrayList<>();
    private PayPresenterImpl presenter;
    private String fromAddress, toUserId;
    private TextWatcher textWatcher;

    private String toWalletAddress, currency, countryCode;
    private String barcodeToWalletAddress, barcodeToUserId, barcodeUserFullName;
    private String publishtopic, subscribeTopic, sessionId;
    private User mappedUser;
    private SharedPreferences preferences;
    private User getUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay);
        ButterKnife.bind(this);

        //to remove keyboard glich
        getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.background));

        getMyApplication(this).getAppComponent().inject(this);
        init();
        setUpWalletSpinner();


        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SearchUser.setSendSelectedUserListener(this);

    /*    textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                AppUtils.showLog(TAG, "onTextChanged()");
                //set this var empty to make payment to searched user
                barcodeUserFullName = "";
                presenter.searchUsers(getToken(), charSequence.toString(), preferences);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };*/

//        mHolderName.addTextChangedListener(textWatcher);
        mHolderName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Pay.this, SearchUser.class);
                startActivity(intent);
            }
        });
        mScan.setOnClickListener(new View.OnClickListener(

        ) {
            @Override
            public void onClick(View view) {
                new IntentIntegrator(Pay.this)
                        .setOrientationLocked(false)
                        .setBeepEnabled(false)
                        .setPrompt("")
                        .initiateScan();
            }
        });


        mPaynow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                String amount = mAmountToPay.getText().toString().trim();
                String description = mDescription.getText().toString().trim();

                if (mHolderName.getText().toString().isEmpty()) {
                    Toast.makeText(Pay.this, "Receiver name cannot be empty", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (mAmountToPay.getText().toString().isEmpty()) {
                    Toast.makeText(Pay.this, "Amount cannot be empty", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (mDescription.getText().toString().isEmpty()) {
                    Toast.makeText(Pay.this, "Description cannot be empty", Toast.LENGTH_SHORT).show();
                    return;
                }


                AppUtils.showLog(TAG, "toUserId: " + toUserId);
                if (toUserId == null) {
                    Toast.makeText(Pay.this, "Invalid user", Toast.LENGTH_SHORT).show();
                    return;
                }
                //make payment from default search
                presenter.pay(getToken(), getUserID(), fromAddress, toUserId, toWalletAddress, amount,
                        description, currency, preferences);

            }
        });


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        AppUtils.showLog(TAG, "onActivityResult()");
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            } else {
                byte[] jsonData = Base64.decode(result.getContents(), Base64.DEFAULT);
                String text = new String(jsonData, StandardCharsets.UTF_8);

                try {
                    JSONObject jsonObject = new JSONObject(text);
                    AppUtils.showLog(TAG, "scan details: " + jsonObject.toString());
                    if (jsonObject.has("sessionId")) {
                        sessionId = jsonObject.getString("sessionId");
                        AppUtils.showLog(TAG, "sessionId: " + sessionId);
                    }
                    String userId = jsonObject.getString("userId");
                    AppUtils.showLog(TAG, "userId" + userId);

                    presenter.searchUsersByBarcode(getToken(), userId, preferences);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    private void init() {
        presenter = new PayPresenterImpl(this, endpoints);

        setUpToolbar(mToolbar);
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

        }
        mToolbarTitle.setText("Make Payment");

    }

    private void setUpWalletSpinner() {
        walletList = WalletsRepo.getInstance().getAllWallets();
        for (Wallet wallet : walletList) {
            spinnerItems.add(wallet.getWalletName());
        }

        CustomSpinnerAdapter walletSpinnerAdapter = new CustomSpinnerAdapter(this, (ArrayList<Wallet>) walletList);
        mWalletSpinner.setAdapter(walletSpinnerAdapter);

        mWalletSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (view != null) {
                    TextView textView = (TextView) view.findViewById(R.id.third_item);
                    fromAddress = textView.getText().toString();
                    Wallet wallet = (Wallet) adapterView.getItemAtPosition(i);
                    currency = wallet.getCurrency();
                    countryCode = wallet.getCountryCode();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void searchUserSuccess(final ArrayList<User> users) {
        AppUtils.showLog(TAG, "search success");

        if (users.size() != 0) AppUtils.showLog(TAG, "userId" + users.get(0).getUserId());
        CustomAutoCompleteAdapter autocompleteAdapter = new CustomAutoCompleteAdapter(this, R.id.et_holder_name, users);
        mHolderName.setThreshold(1);
        mHolderName.setAdapter(autocompleteAdapter);

        if (users.isEmpty()) mHolderName.dismissDropDown();


        mHolderName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView userId = (TextView) view.findViewById(R.id.third_item);
                toUserId = userId.getText().toString();

                TextView walletAddress = (TextView) view.findViewById(R.id.fourth_item);
                toWalletAddress = walletAddress.getText().toString();

                AppUtils.showLog(TAG, "toWalletAddress" + toWalletAddress);
                AppUtils.showLog(TAG, "toUserId" + toUserId);
                hideKeyboard();
            }
        });

    }


    @Override
    public void searchUserFail(String msg) {
        showMessage(msg);
    }

    @Override
    public void searchUserByBarcodeSuccess(final AccountProto.User user) {
        AppUtils.showLog(TAG, "searchSuccessBarcode");
        AppUtils.showLog(TAG, "fullname:" + user.getFullName());

        //check if you are making payment to yourself
        if (getUserID().equals(user.getUserId())) {
            Toast.makeText(this, "Cannot make payment to own wallet", Toast.LENGTH_SHORT).show();
            return;
        }

        toUserId = user.getUserId();
        toWalletAddress = user.getWalletAddress();
        if (sessionId == null) mHolderName.setText(user.getFullName());

        AppUtils.showLog(TAG, "sessionId: " + sessionId);

        if (sessionId != null) {

            mapUser(user);

            Intent intent = new Intent(this, ConnectWithServer.class);
            intent.putExtra("user_parcelable", mappedUser);
            intent.putExtra("to_wallet_address", user.getWalletAddress());
            intent.putExtra("session_id", sessionId);
            startActivity(intent);
            barcodeToWalletAddress = user.getWalletAddress();
        }
    }

    private void mapUser(AccountProto.User user) {
        mappedUser = new User(user.getUserId(), user.getFullName(), user.getUsername(), user.getUserType().name(),
                user.getUserStatus().name(), user.getGender().name(), user.getProfilePicUrl(), user.getVerifiedProfile(),
                true, user.getJoinedTimestamp(), user.getDob(), user.getEmailPhone(), user.getAddress(),
                user.getCountry());

    }

    @Override
    public void searchUserByBarcodeFail(String msg) {
        showMessage(msg);
    }

    @Override
    public void payFail(String msg) {
        showMessage(msg);
    }

    @Override
    public void paySuccess() {
        Toast.makeText(this, "Payment successful", Toast.LENGTH_SHORT).show();
        AppUtils.showLog(TAG, "Pay success");
        finish();
    }

    @Override
    public void onAppForeground() {
        showPasscodeDialog(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        QuiqpayApp.setAppForegroundListener(this);
    }

    @Override
    public void getUser(User user) {
        getUser = user;
        mHolderName.setText(user.getFullName());
        toUserId = getUser.getUserId();
        toWalletAddress = getUser.getAddress();
    }
}
