package com.yanky.savxbusiness.activities.autopayments;

import com.yanky.savxbusiness.realm.models.Autopayment;

import io.realm.RealmList;

public interface AutoPaymentView {
    void autoPaymentSuccess(RealmList<Autopayment> autopaymentRealmList);

    void autoPaymentFail(String msg);
}
