package com.yanky.savxbusiness.activities.synccontacts;

import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.yanky.savxbusiness.activities.account.LoginActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.entities.AccountProto;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.model.Contacts;
import com.yanky.savxbusiness.realm.RealmDatabase;
import com.yanky.savxbusiness.realm.models.SyncedContacts;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.CallbackWrapper;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import retrofit2.Response;

public class SyncContactsPresenterImpl implements SyncContactPresenter {
    private static final String TAG = "SyncContactsPresenterIm";
    private SyncContacts activity;
    private Endpoints endpoints;

    List<AccountProto.ContactSync> contactsSyncList = new ArrayList<>();

    public SyncContactsPresenterImpl(SyncContacts activity, Endpoints endpoints) {
        this.activity = activity;
        this.endpoints = endpoints;
    }

    @Override
    public void syncContact(String token, List<Contacts> contactsList, SharedPreferences preferences) {
        AppUtils.showLog(TAG, "contactsListCount;" + contactsList.size());

        for (Contacts contact : contactsList) {
            AppUtils.showLog(TAG, "contact:" + contact.getName() + contact.getNumber() + contact.getEmail());
            AccountProto.ContactSync contactSync = AccountProto.ContactSync.newBuilder()
                    .setName(contact.getName())
                    .addPhone(contact.getNumber())
                    .addEmail(contact.getEmail())
                    .build();

            contactsSyncList.add(contactSync);
        }

        AccountProto.ContactSyncRequest contactSyncRequest = AccountProto.ContactSyncRequest.newBuilder().addAllContactSync(contactsSyncList).build();

        endpoints.syncContacts(token, contactSyncRequest).enqueue(new CallbackWrapper<>(activity, new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
            @Override
            public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                ReqResProto.BaseResponse baseResponse = response.body();

                if (baseResponse == null) {
                    activity.syncContactFail(null);
                    return;
                }

                if (baseResponse.getError()) {
                    if (baseResponse.getMsg().equals("Authorization failed.")) {
                        clearSharedPrefsAndRealm(preferences);
                        Intent intent = new Intent(activity, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        activity.startActivity(intent);
                        Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                    } else activity.syncContactFail(baseResponse.getMsg());
                    return;
                }

                List<AccountProto.User> userList = baseResponse.getUsersList();
                List<SyncedContacts> syncedContactsList = new ArrayList<>();
                for (AccountProto.User user : userList) {
                    SyncedContacts syncedContacts = new SyncedContacts(user.getFullName(), user.getEmailPhone());
                    syncedContactsList.add(syncedContacts);
                }

                activity.syncContactSuccess(syncedContactsList);
            }

            @Override
            public void onFailureResult() {
                activity.syncContactFail(null);
            }
        }));
    }

    private void clearSharedPrefsAndRealm(SharedPreferences prefs) {
        //clear shared preferences and realm after logout
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();

        final Realm realm = RealmDatabase.getInstance().getRealm();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });
    }
}
