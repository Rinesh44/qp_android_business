package com.yanky.savxbusiness.activities.transactionhistory;

import com.yanky.savxbusiness.realm.models.TransactionHolder;

import java.util.List;

public interface TransactionHistoryView {
    void getTransactionHistorySuccess(List<TransactionHolder> transactionHolderList);

    void getTransactionHistoryFail(String msg);
}
