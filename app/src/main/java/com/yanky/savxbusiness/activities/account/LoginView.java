package com.yanky.savxbusiness.activities.account;

import com.yanky.savxbusiness.entities.AccountProto;
import com.yanky.savxbusiness.realm.models.CountryDetails;

import java.util.List;

/**
 * Created by Leesa Shakya on 27/09/18.
 * leezshk@gmail.com
 */

public interface LoginView {
    void emptyEmailPhone();

    void validEmailPhone();

    void invalidEmailPhone();

    void emptyPassword();

    void validPassword();

    void loginSuccess(AccountProto.LoginResponse loginResponse);

    void loginFailure(String message);

    void onVerificationSuccess(String userId);

    void onVerificationFailure();

    void enterPasscode(boolean isKeyBackedUp, String userId);

    void onCountryCodeSuccess(List<CountryDetails> countryDetailsList);

    void onCountryCodeFail(String msg);
}
