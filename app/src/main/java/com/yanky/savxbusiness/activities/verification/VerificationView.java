package com.yanky.savxbusiness.activities.verification;

/**
 * Created by Leesa Shakya on 02/10/18.
 * leezshk@gmail.com
 */

public interface VerificationView {

    void verifyWaitingUserCodeSuccess();

    void verifyWaitingUserCodeFail(String msg);

    void verifySuccess();

    void resendCodeSuccess();

    void verifyFailure(String message);

    void registerWaitingUserSuccess(int queuePosition, int totalWaitingUsers, String username);

    void registerWaitingUserFail(String msg);
}
