package com.yanky.savxbusiness.activities.paynow;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.yanky.savxbusiness.QuiqpayApp;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.base.BaseActivity;
import com.yanky.savxbusiness.activities.search.SearchUser;
import com.yanky.savxbusiness.adapters.CustomAutoCompleteAdapter;
import com.yanky.savxbusiness.adapters.CustomSpinnerAdapter;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.realm.models.User;
import com.yanky.savxbusiness.realm.models.Wallet;
import com.yanky.savxbusiness.repo.WalletsRepo;
import com.yanky.savxbusiness.utils.AppForegroundListener;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.SendSelectedUser;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.yanky.savxbusiness.QuiqpayApp.getMyApplication;

public class RequestPayment extends BaseActivity implements RequestView, AppForegroundListener, SendSelectedUser {
    private static final String TAG = "RequestPayment";
    @Inject
    Endpoints endpoints;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.sp_wallets)
    Spinner mWalletSpinner;
    @BindView(R.id.et_holder_name)
    AutoCompleteTextView mHolderName;
    @BindView(R.id.et_amount_to_pay)
    EditText mAmountToPay;
    @BindView(R.id.et_desc)
    EditText mDescription;
    @BindView(R.id.btn_send)
    Button mSend;

    private User getUser;
    private List<Wallet> walletList = new ArrayList<>();
    private List<String> spinnerItems = new ArrayList<>();
    private RequestPaymentPresenterImpl presenter;
    private String fromAddress, toUserId, currency, countryCode;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_payment);
        ButterKnife.bind(this);

        //to remove keyboard glich
        getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.background));

        getMyApplication(this).getAppComponent().inject(this);
        init();
        SearchUser.setSendSelectedUserListener(this);

        setUpWalletSpinner();

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        mSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String amount = mAmountToPay.getText().toString().trim();
                String description = mDescription.getText().toString().trim();
                if (amount.isEmpty() || amount.equals(0)) {
                    Toast.makeText(RequestPayment.this, "Please enter amount", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (description.isEmpty()) {
                    Toast.makeText(RequestPayment.this, "Please enter description", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (toUserId == null || toUserId.isEmpty()) {
                    Toast.makeText(RequestPayment.this, "Please select user", Toast.LENGTH_SHORT).show();
                    return;
                }
                presenter.requestPayment(getToken(), toUserId, fromAddress, amount, description, currency, countryCode,
                        preferences);
            }
        });

        mHolderName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RequestPayment.this, SearchUser.class));
            }
        });

 /*       mHolderName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                presenter.searchUsers(getToken(), charSequence.toString(), preferences);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });*/
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;

    }

    private void init() {
        presenter = new RequestPaymentPresenterImpl(this, endpoints);

        setUpToolbar(mToolbar);
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

        }
        mToolbarTitle.setText("Request for Payment");

    }

    private void setUpWalletSpinner() {
        walletList = WalletsRepo.getInstance().getAllWallets();
        for (Wallet wallet : walletList) {
            spinnerItems.add(wallet.getWalletName());
        }

        CustomSpinnerAdapter walletSpinnerAdapter = new CustomSpinnerAdapter(this, (ArrayList<Wallet>) walletList);
        mWalletSpinner.setAdapter(walletSpinnerAdapter);

        mWalletSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                TextView textView = (TextView) view.findViewById(R.id.third_item);
                fromAddress = textView.getText().toString();
                Wallet wallet = (Wallet) adapterView.getItemAtPosition(i);
                currency = wallet.getCurrency();
                countryCode = wallet.getCountryCode();
                AppUtils.showLog(TAG, "selected wallet currency: " + currency);
                AppUtils.showLog(TAG, "selected wallet countryCode: " + countryCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void searchUserSuccess(ArrayList<User> users) {
        AppUtils.showLog(TAG, "search success");
//        AppUtils.showLog(TAG, "userId" + users.get(0).getUserId());
        CustomAutoCompleteAdapter autocompleteAdapter = new CustomAutoCompleteAdapter(this, R.id.et_holder_name, users);
        mHolderName.setThreshold(1);
        mHolderName.setAdapter(autocompleteAdapter);

        if (users.isEmpty()) mHolderName.dismissDropDown();

        mHolderName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                TextView text = (TextView) view.findViewById(R.id.third_item);
                toUserId = text.getText().toString();
                AppUtils.showLog(TAG, "toUserId" + toUserId);
            }
        });
    }

    @Override
    public void searchUserFail(String msg) {
        showMessage(msg);
    }

    @Override
    public void requestPaymentSuccess() {
        Toast.makeText(this, "Payment request success", Toast.LENGTH_SHORT).show();
        AppUtils.showLog(TAG, "request payment success");
        finish();
    }

    @Override
    public void requestPaymentFail(String msg) {
        showMessage(msg);
    }

    @Override
    public void onAppForeground() {
        showPasscodeDialog(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        QuiqpayApp.setAppForegroundListener(this);
    }

    @Override
    public void getUser(User user) {
        getUser = user;
        toUserId = user.getUserId();
        mHolderName.setText(user.getFullName());
    }
}
