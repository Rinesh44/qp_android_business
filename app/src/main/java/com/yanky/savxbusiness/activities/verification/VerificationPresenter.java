package com.yanky.savxbusiness.activities.verification;

import android.content.SharedPreferences;

/**
 * Created by Leesa Shakya on 02/10/18.
 * leezshk@gmail.com
 */

public interface VerificationPresenter {

    void onVerifyWaitingUserCode(String emailPhone, String code);

    void onVerify(String userId, String code);

    void onResendCode(String userId);

    void registerWaitingUser(String token, String deviceId, String fullName, String emailPhone,
                             SharedPreferences preferences);
}

