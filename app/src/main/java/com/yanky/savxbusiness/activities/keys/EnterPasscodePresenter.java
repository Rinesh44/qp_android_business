package com.yanky.savxbusiness.activities.keys;

import android.content.SharedPreferences;

import com.yanky.savxbusiness.crypto.Crypto;

public interface EnterPasscodePresenter {

    void validatePasscodeOne(boolean isKeyBackedUp, String passcode, Crypto crypto, String userId, String token,
                             SharedPreferences preferences);

    void validatePasscodeTwo(String passcode, SharedPreferences preferences);

    void checkIfFirstLogin(SharedPreferences preferences);

    void backupKeys(String passcode, Crypto crypto, String userId, String token, SharedPreferences preferences);

    void createNewWallet(String token, String walletAddress, String userId, SharedPreferences preferences);

    void getBackupKey(String refId, String token, Crypto crypto, String userId, String passcode, SharedPreferences preferences);
}
