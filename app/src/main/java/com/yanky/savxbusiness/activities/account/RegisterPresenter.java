package com.yanky.savxbusiness.activities.account;

/**
 * Created by Leesa Shakya on 27/09/18.
 * leezshk@gmail.com
 */

public interface RegisterPresenter  {
    void validateFullName(String fullName);
    void validateEmailPhone(String emailPhone);
    void validatePassword(String password);
    void validateCountry(String country);
    void onRegister(String fullName, String emailPhone, String password, String country);
}
