package com.yanky.savxbusiness.activities.paynow;

import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.yanky.savxbusiness.activities.account.LoginActivity;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.crypto.Crypto;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.entities.TxProto;
import com.yanky.savxbusiness.realm.RealmDatabase;
import com.yanky.savxbusiness.realm.models.User;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.CallbackWrapper;

import org.libsodium.jni.encoders.Hex;

import java.util.ArrayList;

import io.realm.Realm;
import retrofit2.Response;

public class PayPresenterImpl implements PayPresenter {
    private static final String TAG = "PayPresenterImpl";
    private final Pay activity;
    private final Endpoints endpoints;

    public PayPresenterImpl(Pay activity, Endpoints endpoints) {
        this.activity = activity;
        this.endpoints = endpoints;
    }


    @Override
    public void searchUsers(String token, String charSequence, SharedPreferences preferences) {
        AppUtils.showLog(TAG, "searchUsers()");
        endpoints.searchUsers(token, charSequence).enqueue(new CallbackWrapper<>(activity,
                new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
                    @Override
                    public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                        ReqResProto.BaseResponse baseResponse = response.body();

                        if (baseResponse == null) {
                            activity.searchUserFail(null);
                            return;
                        }


                        if (baseResponse.getError()) {
                            if (baseResponse.getMsg().equals("Authorization failed.")) {
                                clearSharedPrefsAndRealm(preferences);
                                Intent intent = new Intent(activity, LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                activity.startActivity(intent);
                                Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                            } else activity.searchUserFail(baseResponse.getMsg());
                            return;
                        }


                        int count = baseResponse.getUsersCount();
                        AppUtils.showLog(TAG, "userCount:" + count);
                        ArrayList<User> userList = new ArrayList<>();
                        for (int i = 0; i < count; i++) {
                            User user = new User();
                            user.setFullName(baseResponse.getUsers(i).getFullName());
                            user.setEmailPhone(baseResponse.getUsers(i).getEmailPhone());
                            user.setUserId(baseResponse.getUsers(i).getUserId());
                            user.setAddress(baseResponse.getUsers(i).getWalletAddress());
                            userList.add(user);
                        }

                        activity.searchUserSuccess(userList);

                    }

                    @Override
                    public void onFailureResult() {
                        activity.searchUserFail(null);
                    }
                }));
    }

    @Override
    public void searchUsersByBarcode(String token, String userId, SharedPreferences preferences) {
        AppUtils.showLog(TAG, "searchByBarcodeCalled()");
        endpoints.searchUsersBarcode(token, userId).enqueue(new CallbackWrapper<ReqResProto.BaseResponse>(activity,
                new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
                    @Override
                    public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                        ReqResProto.BaseResponse baseResponse = response.body();
                        if (baseResponse == null) {
                            activity.searchUserByBarcodeFail(null);
                            return;
                        }

                        if (baseResponse.getError()) {
                            if (baseResponse.getMsg().equals("Authorization failed.")) {
                                clearSharedPrefsAndRealm(preferences);
                                Intent intent = new Intent(activity, LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                activity.startActivity(intent);
                                Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                            } else activity.searchUserByBarcodeFail(baseResponse.getMsg());
                            return;
                        }

                        activity.searchUserByBarcodeSuccess(baseResponse.getUser());

                    }

                    @Override
                    public void onFailureResult() {
                        activity.searchUserByBarcodeFail(null);
                    }
                }));
    }

    @Override
    public void pay(String token, String fromUserId, String fromWalletAddress, String toUserId, String toWalletAddress, String amount, String desc,
                    String currency, SharedPreferences preferences) {
        activity.showLoading();
        TxProto.Transaction transaction = TxProto.Transaction.newBuilder()
                .setFromUserId(fromUserId)
                .setFromPubKey(fromWalletAddress)
                .setToUserId(toUserId)
                .setCurrency(currency)
                .setToPubKey(toWalletAddress)
                .setAmount(Long.valueOf(amount) * 100)
                .setMemo(desc)
                .build();

        AppUtils.showLog(TAG, "toWalletAddress" + toWalletAddress);

        byte[] dataToSign = transaction.toByteArray();
        byte[] signature = Crypto.build().sign(dataToSign, fromUserId, fromWalletAddress);

        if (signature == null) {
            activity.showMessage("Signature cannot be generated");
            return;
        }

        String signatureString = Hex.HEX.encode(signature);
        AppUtils.showLog(TAG, "signature" + signatureString);

        TxProto.TransactionHolder transactionHolder = TxProto.TransactionHolder.newBuilder()
                .setTransaction(transaction.toByteString())
                .setSignature(signatureString)
                .setTransactionType(TxProto.TransactionType.FUNDTRANSFER)
                .setFromUserId(fromUserId)
                .setFromCurrency(currency)
                .setToUserId(toUserId)
                .setSignaturePubKey(fromWalletAddress)
                .setToWalletAddress(toWalletAddress)
                .build();

        endpoints.transferFund(token, transactionHolder).enqueue(new CallbackWrapper<>(activity, new CallbackWrapper.Wrapper<ReqResProto.BaseResponse>() {
            @Override
            public void onSuccessResult(Response<ReqResProto.BaseResponse> response) {
                activity.hideLoading();
                ReqResProto.BaseResponse baseResponse = response.body();

                if (baseResponse == null) {
                    activity.payFail(null);
                    return;
                }

                if (baseResponse.getError()) {
                    if (baseResponse.getMsg().equals("Authorization failed.")) {
                        clearSharedPrefsAndRealm(preferences);
                        Intent intent = new Intent(activity, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        activity.startActivity(intent);
                        Toast.makeText(activity, "Invalid token", Toast.LENGTH_SHORT).show();
                    } else activity.payFail(baseResponse.getMsg());
                    return;
                }

                activity.paySuccess();
            }

            @Override
            public void onFailureResult() {
                activity.hideLoading();
                activity.payFail(null);
            }
        }));

    }


    private void clearSharedPrefsAndRealm(SharedPreferences prefs) {
        //clear shared preferences and realm after logout
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();

        final Realm realm = RealmDatabase.getInstance().getRealm();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });
    }


}
