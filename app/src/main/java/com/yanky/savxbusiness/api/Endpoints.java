package com.yanky.savxbusiness.api;

import com.yanky.savxbusiness.entities.AccountProto;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.entities.TxProto;
import com.yanky.savxbusiness.entities.WalletProto;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Leesa Shakya on 27/09/18.
 * leezshk@gmail.com
 */

public interface Endpoints {
    String API_BASE_URL = "https://api.savx.com";

    String ACCEPT = "Accept: application/protobuf";
    String CONTENT_TYPE = "Content-Type: application/protobuf";

    String REGISTER = "/account/register";
    String LOGIN = "/account/login";
    String VERIFY = "/account/verify/{userId}/{code}";
    String RESEND_VERIFICATION_CODE = "/account/verification/resend/{userId}";

    String REQUEST_RESET_PASSWORD = "/account/password/reset/request";
    String RESET_PASSWORD = "/account/password/reset";

    String PASSWORD_UPDATE = "/account/password/update";

    String BACKUP_KEY = "/account/key/backup";
    String GET_BACKUP_KEY = "/account/key/backup";

    String CREATE_NEW_WALLET = "/wallet";
    String WALLET_FOR_OTHERS = "/wallet/request";
    String GET_WALLET = "/wallet";

    String FETCH_PROFILE = "/account/user/{userId}";
    String ACCOUNT_UPDATE = "/account/update";
    String UPLOAD_PROFILE_PIC = "/files/user/picture";
    String UPLOAD_ADDRESS = "/account/address";
    String DELETE_ADDRESS = "/account/address/{addressId}";
    String EDIT_ADDRESS = "/account/address";
    String DEFAULT_ADDRESS = "/account/address/{addressId}";

    String GET_UNPAID_INVOICE = "/invoice/unpaid";
    String GET_PAID_INVOICE = "/invoice/paid";

    String SEARCH_USER = "/account/user/search";
    String SEARCH_USER_BARCODE = "/account/user/search/{userId}";

    String LOGOUT = "/account/logout";

    String GET_TRANSACTION = "/transaction/list/history";

    String LOAD_FUND = "/transaction/fund/load";

    String TRANSACTIONS_ON_HOLD = "/transaction/list/history/hold";

    String WALLET_REQUEST_FOR_ME = "/wallet/request/forme";
    String WALLET_REQUEST_BY_ME = "/wallet/request/byme";
    String AUTO_PAYMENT = "/invoice/payment/auto";
    String DELETE_AUTO_PAYMENT = "/invoice/payment/auto/{paymentId}";
    String PAYMENT_REQUEST_INCOMING = "/wallet/fund/request/forme";
    String PAYMENT_REQUEST_OUTGOING = "/wallet/fund/request/byme";
    String REQUEST_FUND = "/wallet/fund/request";

    String ACCEPT_WALLET_REQUEST = "/wallet/request/approve";
    String CANCEL_WALLET_REQUEST = "/wallet/request/cancel/{walletRequestId}";
    String REJECT_WALLET_REQUEST = "/wallet/request/reject/{walletRequestId}";

    String REJECT_PAYMENT_REQUEST = "/wallet/fund/request/reject/{requestId}";
    String CANCEL_PAYMENT_REQUEST = "/wallet/fund/request/cancel/{requestId}";

    String MAKE_TRANSACTION = "/transaction/invoke";
    String RELEASE_TRANSACTION = "/transaction/release";
    String SYNC_CONTACTS = "/account/contact/sync";
    String AUTHORIZATION = "Authorization";

    String KYC = "/account/kyc";
    String GET_COUNTRY = "/account/country";

    String GET_MERCHANTS = "/business/mcc";
    String GET_INVOICE_FROM_ID = "/invoice/{invoiceId}";

    String GET_CURRENCY_RATES = "/fx/rate";
    String POST_TRADE = "/fx/trade";
    String GET_TRADE = "/fx/trade/list";
    String DELETE_TRADE = "/fx/trade/{tradeId}";
    String EDIT_TRADE = "/fx/trade";
    String ACCEPT_TRADE = "/fx/trade/zippoint";

    String WAITING_LIST_VERIFY_REQUEST = "/account/emailphone/verify";
    String WAITING_LIST_VERIFY_CODE = "/account/emailphone/verify/{emailPhone}/{code}";

    String WAITING_LIST_REGISTER = "/account/user/waiting";
    String WAITING_LIST_FETCH_USER = "/account/user/waiting/{emailPhone}";


    @POST(API_BASE_URL + REGISTER)
    Call<ReqResProto.BaseResponse> register(@Body AccountProto.UserRegister userRegister);

    @Headers({CONTENT_TYPE})
    @POST(API_BASE_URL + LOGIN)
    Call<ReqResProto.BaseResponse> login(@Body AccountProto.LoginRequest loginRequest);

    @PATCH(API_BASE_URL + VERIFY)
    Call<ReqResProto.BaseResponse> verify(@Path(value = "userId") String userId,
                                          @Path(value = "code") String code);

    @GET(API_BASE_URL + WAITING_LIST_VERIFY_CODE)
    Call<ReqResProto.BaseResponse> verifyWaitingListCode(@Path(value = "emailPhone") String emailPhone,
                                                         @Path(value = "code") String code);

    @GET(API_BASE_URL + RESEND_VERIFICATION_CODE)
    Call<ReqResProto.BaseResponse> resendVerificationCode(@Path(value = "userId") String userId);

    @GET(API_BASE_URL + REQUEST_RESET_PASSWORD)
    Call<ReqResProto.BaseResponse> requestResetPassword(@Query(value = "emailPhone", encoded = true) String emailPhone);

    @PATCH(API_BASE_URL + RESET_PASSWORD)
    Call<ReqResProto.BaseResponse> resetPassword(@Body AccountProto.PasswordReset passwordReset);

    @PATCH(API_BASE_URL + PASSWORD_UPDATE)
    Call<ReqResProto.BaseResponse> updatePassword(@Header(AUTHORIZATION) String auth,
                                                  @Body AccountProto.PasswordChangeRequest passwordChangeRequest);

    @POST(API_BASE_URL + BACKUP_KEY)
    Call<ReqResProto.BaseResponse> backupKey(@Header(AUTHORIZATION) String authorization,
                                             @Header("refId") String refId,
                                             @Query(value = "pubKey", encoded = true) String pubKey,
                                             @Body WalletProto.KeysBackup keysBackup);

    @GET(API_BASE_URL + GET_BACKUP_KEY)
    Call<ReqResProto.BaseResponse> getBackupKey(@Header(AUTHORIZATION) String authorization,
                                                @Header("refId") String refId);

    @POST(API_BASE_URL + CREATE_NEW_WALLET)
    Call<ReqResProto.BaseResponse> createNewWallet(@Header(AUTHORIZATION) String authorization,
                                                   @Body WalletProto.WalletAddress object);

    @POST(API_BASE_URL + WALLET_FOR_OTHERS)
    Call<ReqResProto.BaseResponse> createWalletForOthers(@Header(AUTHORIZATION) String authorization,
                                                         @Body WalletProto.WalletRequest object);

    @GET(API_BASE_URL + GET_WALLET)
    Call<ReqResProto.BaseResponse> getWallets(@Header(AUTHORIZATION) String authorization);

    @GET(API_BASE_URL + FETCH_PROFILE)
    Call<ReqResProto.BaseResponse> fetchProfile(@Header(AUTHORIZATION) String authorization,
                                                @Path(value = "userId") String userId);

    @PATCH(API_BASE_URL + ACCOUNT_UPDATE)
    Call<ReqResProto.BaseResponse> updateAccount(@Header(AUTHORIZATION) String authorization,
                                                 @Body AccountProto.User user);

    @POST(API_BASE_URL + UPLOAD_ADDRESS)
    Call<ReqResProto.BaseResponse> uploadAddress(@Header(AUTHORIZATION) String authorization,
                                                 @Body AccountProto.Address address);

    @DELETE(API_BASE_URL + DELETE_ADDRESS)
    Call<ReqResProto.BaseResponse> deleteAddress(@Header(AUTHORIZATION) String auth,
                                                 @Path(value = "addressId") String addressId);

    @PATCH(API_BASE_URL + EDIT_ADDRESS)
    Call<ReqResProto.BaseResponse> editAddress(@Header(AUTHORIZATION) String auth,
                                               @Body AccountProto.Address address);

    @PUT(API_BASE_URL + DEFAULT_ADDRESS)
    Call<ReqResProto.BaseResponse> defaultAddress(@Header(AUTHORIZATION) String auth,
                                                  @Path(value = "addressId") String addressId);

    @Multipart
    @POST(API_BASE_URL + UPLOAD_PROFILE_PIC)
    Call<ReqResProto.BaseResponse> uploadProPic(@Header(AUTHORIZATION) String authorization,
                                                @Part MultipartBody.Part file,
                                                @Part("mimeType") RequestBody mimeType);

    @GET(API_BASE_URL + GET_UNPAID_INVOICE)
    Call<ReqResProto.BaseResponse> getUnpaidInvoice(@Header(AUTHORIZATION) String authorization);

    @GET(API_BASE_URL + GET_PAID_INVOICE)
    Call<ReqResProto.BaseResponse> getPaidInvoice(@Header(AUTHORIZATION) String authorization);

    @GET(API_BASE_URL + SEARCH_USER)
    Call<ReqResProto.BaseResponse> searchUsers(@Header(AUTHORIZATION) String authorization,
                                               @Query(value = "searchTerm", encoded = true) String searchTerm);

    @GET(API_BASE_URL + SEARCH_USER_BARCODE)
    Call<ReqResProto.BaseResponse> searchUsersBarcode(@Header(AUTHORIZATION) String authorization,
                                                      @Path(value = "userId") String userId);

    @DELETE(API_BASE_URL + LOGOUT)
    Call<ReqResProto.BaseResponse> logout(@Header(AUTHORIZATION) String authorization);

    @POST(API_BASE_URL + GET_TRANSACTION)
    Call<ReqResProto.BaseResponse> getTransaction(@Header(AUTHORIZATION) String auth,
                                                  @Body TxProto.TransactionGetRequest transactionGetRequest);

    @POST(API_BASE_URL + TRANSACTIONS_ON_HOLD)
    Call<ReqResProto.BaseResponse> getTransactionsOnHold(@Header(AUTHORIZATION) String auth,
                                                         @Body TxProto.TransactionGetRequest transactionGetRequest);

    @POST(API_BASE_URL + LOAD_FUND)
    Call<ReqResProto.BaseResponse> loadFund(@Header(AUTHORIZATION) String auth,
                                            @Body TxProto.AchLoad achload);

    @GET(API_BASE_URL + WALLET_REQUEST_FOR_ME)
    Call<ReqResProto.BaseResponse> walletRequestForMe(@Header(AUTHORIZATION) String auth);

    @GET(API_BASE_URL + WALLET_REQUEST_BY_ME)
    Call<ReqResProto.BaseResponse> walletRequestByMe(@Header(AUTHORIZATION) String auth);

    @GET(API_BASE_URL + AUTO_PAYMENT)
    Call<ReqResProto.BaseResponse> autoPayment(@Header(AUTHORIZATION) String auth);

    @DELETE(API_BASE_URL + DELETE_AUTO_PAYMENT)
    Call<ReqResProto.BaseResponse> deleteAutoPayment(@Header(AUTHORIZATION) String auth,
                                                     @Path(value = "paymentId") String paymentId);

    @GET(API_BASE_URL + PAYMENT_REQUEST_INCOMING)
    Call<ReqResProto.BaseResponse> paymentRequestIncoming(@Header(AUTHORIZATION) String auth);

    @GET(API_BASE_URL + PAYMENT_REQUEST_OUTGOING)
    Call<ReqResProto.BaseResponse> paymentRequestOutgoing(@Header(AUTHORIZATION) String auth);

    @POST(API_BASE_URL + REQUEST_FUND)
    Call<ReqResProto.BaseResponse> requestFund(@Header(AUTHORIZATION) String auth,
                                               @Body WalletProto.RequestFund requestFund);

    @POST(API_BASE_URL + ACCEPT_WALLET_REQUEST)
    Call<ReqResProto.BaseResponse> acceptWalletRequest(@Header(AUTHORIZATION) String auth,
                                                       @Body WalletProto.WalletRequest walletRequest);

    @PUT(API_BASE_URL + CANCEL_WALLET_REQUEST)
    Call<ReqResProto.BaseResponse> cancelWalletRequest(@Header(AUTHORIZATION) String auth,
                                                       @Path(value = "walletRequestId") String walletRequestId);

    @PUT(API_BASE_URL + REJECT_WALLET_REQUEST)
    Call<ReqResProto.BaseResponse> rejectWalletRequest(@Header(AUTHORIZATION) String auth,
                                                       @Path(value = "walletRequestId") String walletRequestId);

    @POST(API_BASE_URL + MAKE_TRANSACTION)
    Call<ReqResProto.BaseResponse> transferFund(@Header(AUTHORIZATION) String auth,
                                                @Body TxProto.TransactionHolder transactionHolder);

    @POST(API_BASE_URL + RELEASE_TRANSACTION)
    Call<ReqResProto.BaseResponse> releaseFund(@Header(AUTHORIZATION) String auth,
                                               @Body TxProto.TransactionHolder transactionHolder);

    @PUT(API_BASE_URL + REJECT_PAYMENT_REQUEST)
    Call<ReqResProto.BaseResponse> rejectPaymentRequest(@Header(AUTHORIZATION) String auth,
                                                        @Path(value = "requestId") String requestId);

    @PUT(API_BASE_URL + CANCEL_PAYMENT_REQUEST)
    Call<ReqResProto.BaseResponse> cancelPaymentRequest(@Header(AUTHORIZATION) String auth,
                                                        @Path(value = "requestId") String requestId);

    @POST(API_BASE_URL + AUTO_PAYMENT)
    Call<ReqResProto.BaseResponse> postAutoPayment(@Header(AUTHORIZATION) String auth,
                                                   @Body WalletProto.AutoPayment autoPayment);

    @POST(API_BASE_URL + SYNC_CONTACTS)
    Call<ReqResProto.BaseResponse> syncContacts(@Header(AUTHORIZATION) String auth,
                                                @Body AccountProto.ContactSyncRequest contactSyncRequest);

    @POST(API_BASE_URL + KYC)
    Call<ReqResProto.BaseResponse> saveKyc(@Header(AUTHORIZATION) String auth,
                                           @Body AccountProto.KnowYourCustomer knowYourCustomer);

    @PUT(API_BASE_URL + KYC)
    Call<ReqResProto.BaseResponse> updateKyc(@Header(AUTHORIZATION) String auth,
                                             @Body AccountProto.KnowYourCustomer knowYourCustomer);

    @GET(API_BASE_URL + GET_COUNTRY)
    Call<ReqResProto.BaseResponse> getCountry();

    @GET(API_BASE_URL + GET_MERCHANTS)
    Call<ReqResProto.BaseResponse> getMerchants(@Header(AUTHORIZATION) String auth);

    @GET(API_BASE_URL + GET_INVOICE_FROM_ID)
    Call<ReqResProto.BaseResponse> getInvoiceFromId(@Header(AUTHORIZATION) String auth,
                                                    @Path(value = "invoiceId") String invoiceId);

    @GET(API_BASE_URL + GET_CURRENCY_RATES)
    Call<ReqResProto.BaseResponse> getCurrencyRates(@Header(AUTHORIZATION) String auth);

    @POST(API_BASE_URL + POST_TRADE)
    Call<ReqResProto.BaseResponse> postTrade(@Header(AUTHORIZATION) String auth,
                                             @Body TxProto.FxTrade fxTrade);

    @GET(API_BASE_URL + GET_TRADE)
    Call<ReqResProto.BaseResponse> getTrades(@Header(AUTHORIZATION) String auth);

    @DELETE(API_BASE_URL + DELETE_TRADE)
    Call<ReqResProto.BaseResponse> deleteTrade(@Header(AUTHORIZATION) String auth,
                                               @Path(value = "tradeId") String tradeId);

    @PUT(API_BASE_URL + EDIT_TRADE)
    Call<ReqResProto.BaseResponse> editTrade(@Header(AUTHORIZATION) String auth,
                                             @Body TxProto.FxTrade fxTrade);

    @POST(API_BASE_URL + ACCEPT_TRADE)
    Call<ReqResProto.BaseResponse> acceptTrade(@Header(AUTHORIZATION) String auth,
                                               @Body TxProto.TransactionHolder holder);

    @GET(API_BASE_URL + WAITING_LIST_VERIFY_REQUEST)
    Call<ReqResProto.BaseResponse> verifyCredential(@Header(AUTHORIZATION) String auth,
                                                    @Query(value = "emailPhone", encoded = true) String emailPhone,
                                                    @Query(value = "pushToken", encoded = true) String pushToken,
                                                    @Query(value = "deviceType", encoded = true) int deviceType);

    @POST(API_BASE_URL + WAITING_LIST_REGISTER)
    Call<ReqResProto.BaseResponse> registerWaitingUser(@Header(AUTHORIZATION) String auth,
                                                       @Body AccountProto.WaitingUser waitingUser);

    @GET(API_BASE_URL + WAITING_LIST_FETCH_USER)
    Call<ReqResProto.BaseResponse> getWaitingUser(@Header(AUTHORIZATION) String auth,
                                                  @Path(value = "emailPhone") String emailPhone);


}
