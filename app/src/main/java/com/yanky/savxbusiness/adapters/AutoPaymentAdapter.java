package com.yanky.savxbusiness.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.realm.models.Autopayment;
import com.yanky.savxbusiness.utils.RecyclerViewClickListener;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class AutoPaymentAdapter extends RecyclerView.Adapter<AutoPaymentAdapter.AutoPaymentViewHolder> {
    private List<Autopayment> autopaymentList;
    private RecyclerViewClickListener clickListener;


    public void setClickListener(RecyclerViewClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }


    public class AutoPaymentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView name, date, amount;
        private CardView mCardview;

        public AutoPaymentViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.tv_name);
            date = (TextView) view.findViewById(R.id.tv_date);
            amount = (TextView) view.findViewById(R.id.tv_amount);
            mCardview = (CardView) view.findViewById(R.id.cv_autopayments);
            mCardview.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null)
                clickListener.onClick(mCardview, getAdapterPosition());
        }
    }

    public AutoPaymentAdapter(List<Autopayment> autopaymentList) {
        this.autopaymentList = autopaymentList;
    }

    @Override
    public AutoPaymentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.autopayment_row, parent, false);

        return new AutoPaymentViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AutoPaymentAdapter.AutoPaymentViewHolder holder, int position) {
        Autopayment autopayment = autopaymentList.get(position);
        holder.name.setText(autopayment.getToUsername());
        holder.amount.setText(String.valueOf(new BigDecimal(autopayment.getAmount()).toBigInteger()));
        holder.date.setText(getDate(autopayment.getDate()));

    }


    @Override
    public int getItemCount() {
        return autopaymentList.size();
    }


    public void removeItem(int position) {
        autopaymentList.remove(position);
    }

    public String getDate(long time) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("E, d MMM yyyy, h:mm a");
            return sdf.format(new Date(time));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

}