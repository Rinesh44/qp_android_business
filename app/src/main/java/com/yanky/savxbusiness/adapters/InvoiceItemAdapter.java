package com.yanky.savxbusiness.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.realm.models.InvoiceItem;

import java.util.List;


public class InvoiceItemAdapter extends RecyclerView.Adapter<InvoiceItemAdapter.MyViewHolder> {

    private Context mContext;
    private List<InvoiceItem> invoicelist;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView quantity, amount, unitPrice, description;

        public MyViewHolder(View view) {
            super(view);
            quantity = (TextView) view.findViewById(R.id.tv_quantity);
            amount = (TextView) view.findViewById(R.id.tv_amount);
            unitPrice = (TextView) view.findViewById(R.id.tv_unit_price);
            description = (TextView) view.findViewById(R.id.tv_desc);

        }

    }


    public InvoiceItemAdapter(Context mContext, List<InvoiceItem> invoicelist) {
        this.mContext = mContext;
        this.invoicelist = invoicelist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.items_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        InvoiceItem item = invoicelist.get(position);
        holder.quantity.setText(item.getQuantity());
        holder.unitPrice.setText(String.valueOf(item.getUnitPrice()));
        holder.description.setText(String.valueOf(item.getDescription()));
        holder.amount.setText(String.valueOf(Double.valueOf(item.getQuantity()) *
                Double.valueOf(item.getUnitPrice())));

    }


    @Override
    public int getItemCount() {
        return invoicelist.size();
    }


}