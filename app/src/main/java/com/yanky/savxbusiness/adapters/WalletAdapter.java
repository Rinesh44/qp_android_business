package com.yanky.savxbusiness.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.scrounger.countrycurrencypicker.library.Country;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.realm.models.Wallet;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.RecyclerViewClickListener;

import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class WalletAdapter extends RecyclerView.Adapter<WalletAdapter.MyViewHolder> implements Filterable {
    private static final String TAG = "WalletAdapter";
    private Context mContext;
    private List<com.yanky.savxbusiness.realm.models.Wallet> walletList;
    private List<com.yanky.savxbusiness.realm.models.Wallet> walletListFiltered = new ArrayList<>();
    private RecyclerViewClickListener clickListener;

    public void setClickListener(RecyclerViewClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                final String charString = charSequence.toString();
                AppUtils.showLog(TAG, "char " + charString);
                if (charString.isEmpty()) {
                    AppUtils.showLog(TAG, "char empty");
                    walletListFiltered = walletList;
                } else {
                    AppUtils.showLog(TAG, "else");
                    List<Wallet> filteredList = new ArrayList<>();
                    for (Wallet row : walletList) {
                        AppUtils.showLog(TAG, "for");
                        AppUtils.showLog(TAG, "rows: " + row.getWalletAddress());
                        if (row.getWalletName().toLowerCase().contains(charString.toLowerCase())) {
                            AppUtils.showLog(TAG, "if");
                            filteredList.add(row);
                        }

                    }

                    AppUtils.showLog(TAG, "counts: " + filteredList.size());

                    walletListFiltered = filteredList;
                    AppUtils.showLog(TAG, "counts:" + walletListFiltered.size());
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = walletListFiltered;
                return filterResults;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                walletListFiltered = (ArrayList<Wallet>) filterResults.values;
                notifyDataSetChanged();

            }
        };
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView walletName, amount;

        public MyViewHolder(View view) {
            super(view);
            walletName = (TextView) view.findViewById(R.id.wallet_name);
            amount = (TextView) view.findViewById(R.id.wallet_amount);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }


    public WalletAdapter(Context mContext, List<com.yanky.savxbusiness.realm.models.Wallet> walletList) {
        this.mContext = mContext;
        this.walletList = walletList;
        this.walletListFiltered = walletList;
    }


    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.wallet_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NotNull final MyViewHolder holder, int position) {
        Wallet wallet = walletList.get(position);
        holder.walletName.setText(wallet.getWalletName());

        Country countryWithCurrency = Country.getCountryWithCurrency(wallet.getCountryCode(), mContext);
        assert countryWithCurrency != null;
        String currencySymbol = Objects.requireNonNull(countryWithCurrency.getCurrency()).getSymbol();
        if (currencySymbol.length() > 1) {
            currencySymbol = currencySymbol.substring(currencySymbol.length() - 1);
        }

        StringBuilder amountBuilder = new StringBuilder();
//        amountBuilder.append(wallet.getCurrency());
        amountBuilder.append(wallet.getCurrency());
        amountBuilder.append(" ");
        amountBuilder.append(currencySymbol);
        amountBuilder.append(" ");
        amountBuilder.append(new BigDecimal(Double.parseDouble(String.valueOf(wallet.getAmount()))).toBigInteger());
        holder.amount.setText(amountBuilder);


    /*    int flagId = Country.getCountry(wallet.getCountryCode(), mContext).getFlagId();
        if (flagId != 0) {
            Drawable drawable = mContext.getResources().getDrawable(flagId);
            Drawable resized = resize(drawable);
            holder.amount.setCompoundDrawablePadding(20);
            holder.amount.setCompoundDrawablesWithIntrinsicBounds(resized, null, null, null);
        }*/

    }


    @Override
    public int getItemCount() {
        return walletList.size();
    }

    /**
     * resise flag drawable for better look
     *
     * @param image
     * @return
     */
    private Drawable resize(Drawable image) {
        Bitmap b = ((BitmapDrawable) image).getBitmap();
        Bitmap bitmapResized = Bitmap.createScaledBitmap(b, 50, 50, false);
        return new BitmapDrawable(mContext.getResources(), bitmapResized);
    }

}