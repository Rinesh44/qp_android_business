package com.yanky.savxbusiness.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.scrounger.countrycurrencypicker.library.Country;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.realm.models.TransactionHolder;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.RecyclerViewClickListener;
import com.yanky.savxbusiness.utils.TransactionDescriptionHelper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.MyViewHolder> {
    private static final String TAG = "TransactionAdapter";
    private Context mContext;
    private List<TransactionHolder> transactionHolderList;
    private RecyclerViewClickListener clickListener;

    public void setClickListener(RecyclerViewClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView heading, amount, date;

        public MyViewHolder(View view) {
            super(view);
            heading = (TextView) view.findViewById(R.id.tv_heading);
            amount = (TextView) view.findViewById(R.id.tv_amount);
            date = (TextView) view.findViewById(R.id.tv__date);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }


    public TransactionAdapter(Context mContext, List<TransactionHolder> transactionHolderList) {
        this.mContext = mContext;
        this.transactionHolderList = transactionHolderList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.transaction_row, parent, false);

        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        TransactionHolder transactionHolder = transactionHolderList.get(position);
        String desc = TransactionDescriptionHelper.getDescriptionFor(transactionHolder, mContext);
        String status = TransactionDescriptionHelper.getStatus(transactionHolder, mContext);
        AppUtils.showLog(TAG, "status" + status);

        holder.heading.setText(desc);
        StringBuilder amountBuilder = new StringBuilder();
        int flagId;


        try {
            if (transactionHolder.getTransactiontype().equals("Load Fund")) {
                Country countryWithCurrency = Country.getCountryWithCurrency(transactionHolder.getToCountryCode(), mContext);

                assert countryWithCurrency != null;
                amountBuilder.append(Objects.requireNonNull(countryWithCurrency.getCurrency()).getSymbol());
                amountBuilder.append(" ");
                amountBuilder.append(transactionHolder.getToCurrency());
//                flagId = Country.getCountry(transactionHolder.getToCountryCode(), mContext).getFlagId();
            } else {
                Country countryWithCurrency = Country.getCountryWithCurrency(transactionHolder.getFromCountryCode(), mContext);

                assert countryWithCurrency != null;
                amountBuilder.append(Objects.requireNonNull(countryWithCurrency.getCurrency()).getSymbol());
                amountBuilder.append(" ");
                amountBuilder.append(transactionHolder.getFromCurrency());
//                flagId = Country.getCountry(transactionHolder.getFromCountryCode(), mContext).getFlagId();
            }

            amountBuilder.append(" ");
            amountBuilder.append(String.format("%.0f", Double.valueOf(transactionHolder.getAmount()) / 100));
            holder.amount.setText(amountBuilder);


         /*   if (flagId != 0) {
                Drawable drawable = mContext.getResources().getDrawable(flagId);
                Drawable resized = resize(drawable);
                holder.amount.setCompoundDrawablePadding(10);
                holder.amount.setCompoundDrawablesWithIntrinsicBounds(resized, null, null, null);
            }*/

            holder.date.setText(getDate(transactionHolder.getTimestamp()));

            if (status.equals("INCOMING")) {
                holder.amount.setTextColor(mContext.getResources().getColor(R.color.green));
            } else {
                holder.amount.setTextColor(mContext.getResources().getColor(R.color.reds));
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }


    }


    @Override
    public int getItemCount() {
        return transactionHolderList.size();
    }

    private Drawable resize(Drawable image) {
        Bitmap b = ((BitmapDrawable) image).getBitmap();
        Bitmap bitmapResized = Bitmap.createScaledBitmap(b, 30, 30, false);
        return new BitmapDrawable(mContext.getResources(), bitmapResized);
    }

    private String getDate(long time) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("E, d MMM yyyy, h:mm a");
            return sdf.format(new Date(time));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
