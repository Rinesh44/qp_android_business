package com.yanky.savxbusiness.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.realm.models.UnpaidInvoice;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.RecyclerViewClickListener;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class UnpaidInvoiceAdapter extends RecyclerView.Adapter<UnpaidInvoiceAdapter.MyViewHolder> {
    private static final String TAG = "UnpaidInvoiceAdapter";
    private Context mContext;
    private List<UnpaidInvoice> invoiceList;
    private RecyclerViewClickListener clickListener;

    public void setClickListener(RecyclerViewClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView heading, amount, dueDate, deadline;

        public MyViewHolder(View view) {
            super(view);
            heading = (TextView) view.findViewById(R.id.heading);
            amount = (TextView) view.findViewById(R.id.amount);
            dueDate = (TextView) view.findViewById(R.id.dueDate);
            deadline = (TextView) view.findViewById(R.id.deadline);
            view.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }


    public UnpaidInvoiceAdapter(Context mContext, List<UnpaidInvoice> invoiceList) {
        this.mContext = mContext;
        this.invoiceList = invoiceList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.unpaid_invoice_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        UnpaidInvoice invoice = invoiceList.get(position);
        holder.heading.setText(invoice.getInvoiceHeading());
        StringBuilder amountBuilder = new StringBuilder();
        amountBuilder.append("USD ");
        amountBuilder.append(invoice.getTotalAmountToPay() / 100);
        holder.amount.setText(amountBuilder);

        StringBuilder dueDateBuilder = new StringBuilder();
        dueDateBuilder.append("Due date: ");
        dueDateBuilder.append(getDate(Long.valueOf(invoice.getDueDate())));
        holder.dueDate.setText(dueDateBuilder);
        AppUtils.showLog(TAG, "due Date:" + invoice.getDueDate());
        String day = getDayFromDate(Long.valueOf(invoice.getDueDate()));
        AppUtils.showLog(TAG, "day:" + day);
        String month = getMonthFromDate(Long.valueOf(invoice.getDueDate()));
        AppUtils.showLog(TAG, "month:" + month);


        String currentDay = (String) DateFormat.format("dd", new Date(System.currentTimeMillis()));
        AppUtils.showLog(TAG, "currentDay:" + currentDay);

        String currentMonth = (String) DateFormat.format("MM", new Date(System.currentTimeMillis()));
        AppUtils.showLog(TAG, "currentMonth:" + currentMonth);

        Date currentDate = new Date(System.currentTimeMillis());
        AppUtils.showLog(TAG, "currentDate:" + currentDate);
        Date dueDate = new Date(Long.valueOf(invoice.getDueDate()));
        AppUtils.showLog(TAG, "dueDate:" + dueDate);

        long diff = dueDate.getTime() - currentDate.getTime();
        long diffInDays = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        System.out.println("Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));

        long formattedDiffInDays = Long.valueOf(String.valueOf(diffInDays).replace("-", "").trim());
        if (formattedDiffInDays < 30) {
            StringBuilder deadlineBuilder = new StringBuilder();
            if (String.valueOf(diffInDays).contains("-")) deadlineBuilder.append(diffInDays);
            else deadlineBuilder.append(diffInDays + 1);
            deadlineBuilder.append(System.getProperty("line.separator"));
            deadlineBuilder.append("days");
            holder.deadline.setText(deadlineBuilder);
            if (String.valueOf(diffInDays).contains("-")) {
                holder.deadline.setTextColor(mContext.getResources().getColor(R.color.reds));
            }
        } else {
            int monthDifference = Integer.valueOf(month) - Integer.valueOf(currentMonth);
            StringBuilder monthBuilder = new StringBuilder();
            monthBuilder.append(monthDifference);
            monthBuilder.append(System.getProperty("line.separator"));
            monthBuilder.append("months");
            holder.deadline.setText(monthBuilder);
            if ((Integer.valueOf(month) - Integer.valueOf(currentMonth) < 0)) {
                holder.deadline.setTextColor(mContext.getResources().getColor(R.color.reds));
            }
        }

   /*     if (currentMonth.equals(month)) {
            int dayDifference = Integer.valueOf(day) - Integer.valueOf(currentDay);
            StringBuilder deadlineBuilder = new StringBuilder();
            deadlineBuilder.append(dayDifference);
            deadlineBuilder.append(System.getProperty("line.separator"));
            deadlineBuilder.append("days");
            holder.deadline.setText(deadlineBuilder);
            if ((Integer.valueOf(day) - Integer.valueOf(currentDay) < 0)) {
                holder.deadline.setTextColor(mContext.getResources().getColor(R.color.reds));
            }
        } else {
            int monthDifference = Integer.valueOf(month) - Integer.valueOf(currentMonth);
            StringBuilder monthBuilder = new StringBuilder();
            monthBuilder.append(monthDifference);
            monthBuilder.append(System.getProperty("line.separator"));
            monthBuilder.append("months");
            holder.deadline.setText(monthBuilder);
            if ((Integer.valueOf(month) - Integer.valueOf(currentMonth) < 0)) {
                holder.deadline.setTextColor(mContext.getResources().getColor(R.color.reds));
            }
        }*/


    }


    private String getDate(long time) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("E, d MMM yyyy, h:mm a");
            return sdf.format(new Date(time));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getMonthFromDate(long time) {
        try {
            return (String) DateFormat.format("MM", new Date(time));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    public String getDayFromDate(long time) {
        try {
            return (String) DateFormat.format("dd", new Date(time));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    @Override
    public int getItemCount() {
        return invoiceList.size();
    }
}