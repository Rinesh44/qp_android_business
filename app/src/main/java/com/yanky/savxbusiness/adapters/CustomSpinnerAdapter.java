package com.yanky.savxbusiness.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scrounger.countrycurrencypicker.library.Country;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.realm.models.Wallet;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Objects;


public class CustomSpinnerAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Wallet> wallets;

    public CustomSpinnerAdapter(Context context, ArrayList<Wallet> wallets) {
        this.context = context;
        this.wallets = wallets;
    }

    @Override
    public int getCount() {
        return wallets.size();
    }

    @Override
    public Object getItem(int position) {
        return wallets.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LinearLayout linearLayout;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            linearLayout = (LinearLayout) inflater.inflate(
                    R.layout.custom_spinner_row, null);
        } else {
            linearLayout = (LinearLayout) convertView;
        }

        TextView text1 = (TextView) linearLayout.findViewById(R.id.first_item);
        TextView text2 = (TextView) linearLayout.findViewById(R.id.second_item);
        TextView text3 = (TextView) linearLayout.findViewById(R.id.third_item);


        text1.setText(wallets.get(position).getWalletName());
        if (wallets.get(position).getWalletName().equals("All wallets") || wallets.get(position).getWalletName().equals("Tap to select wallet")) {
            text2.setVisibility(View.GONE);
            text3.setVisibility(View.GONE);
        } else {

            Country country = Country.getCountryWithCurrency(wallets.get(position).getCountryCode(), context);
            assert country != null;
            String currencySymbol = Objects.requireNonNull(country.getCurrency()).getSymbol();
            if(currencySymbol.length() > 1){
                currencySymbol = currencySymbol.substring(currencySymbol.length() -1);

            }
            StringBuilder amountBuilder = new StringBuilder();
            amountBuilder.append(wallets.get(position).getCurrency());
            amountBuilder.append(" ");
            amountBuilder.append(currencySymbol);
            amountBuilder.append(" ");
            amountBuilder.append(new BigDecimal(wallets.get(position).getAmount()).toBigInteger());
            text2.setText(amountBuilder);
        }

      /*  if (wallets.get(position).getCountryCode() != null) {
            int flagId = Country.getCountry(wallets.get(position).getCountryCode(), context).getFlagId();
            if (flagId != 0) {
                Drawable drawable = context.getResources().getDrawable(flagId);
                Drawable resized = resize(drawable);
                text2.setCompoundDrawablePadding(20);
                text2.setCompoundDrawablesWithIntrinsicBounds(resized, null, null, null);
            }
        } else {
            text2.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        }*/

        text3.setText(wallets.get(position).getWalletAddress());


        return linearLayout;
    }

    private Drawable resize(Drawable image) {
        Bitmap b = ((BitmapDrawable) image).getBitmap();
        Bitmap bitmapResized = Bitmap.createScaledBitmap(b, 50, 50, false);
        return new BitmapDrawable(context.getResources(), bitmapResized);
    }
}
