package com.yanky.savxbusiness.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.realm.models.PaidInvoice;
import com.yanky.savxbusiness.utils.RecyclerViewClickListener;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class PaidInvoiceAdapter extends RecyclerView.Adapter<PaidInvoiceAdapter.MyViewHolder> {
    private Context mContext;
    private List<PaidInvoice> invoiceList;
    private RecyclerViewClickListener clickListener;

    public void setClickListener(RecyclerViewClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView heading, amount, dueDate, deadline;

        public MyViewHolder(View view) {
            super(view);
            heading = (TextView) view.findViewById(R.id.heading);
            amount = (TextView) view.findViewById(R.id.amount);
            dueDate = (TextView) view.findViewById(R.id.dueDate);
            deadline = (TextView) view.findViewById(R.id.deadline);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }


    public PaidInvoiceAdapter(Context mContext, List<PaidInvoice> invoiceList) {
        this.mContext = mContext;
        this.invoiceList = invoiceList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.paid_invoice_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        PaidInvoice invoice = invoiceList.get(position);
        holder.heading.setText(invoice.getInvoiceHeading());
        StringBuilder amountBuilder = new StringBuilder();
        amountBuilder.append("USD ");
        amountBuilder.append(String.format("%.0f", Double.valueOf(invoice.getTotalAmountToPay()) / 100));
        holder.amount.setText(amountBuilder);
        String formattedDate = getDate(Long.valueOf(invoice.getDueDate()));
        StringBuilder dateBuilder = new StringBuilder();
        dateBuilder.append("Paid Date: ");
        dateBuilder.append(formattedDate);
        holder.dueDate.setText(dateBuilder);

    }

    private String getDate(long time) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("E, d MMM yyyy, h:mm a");
            return sdf.format(new Date(time));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public int getItemCount() {
        return invoiceList.size();
    }
}