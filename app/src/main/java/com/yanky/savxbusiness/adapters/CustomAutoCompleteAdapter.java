package com.yanky.savxbusiness.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.realm.models.User;

import java.util.ArrayList;
import java.util.List;

public class CustomAutoCompleteAdapter extends ArrayAdapter<User> {

    private ArrayList<User> mUsers;
    private LayoutInflater layoutInflater;


    private Filter mFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            return ((User) resultValue).getFullName();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null) {
                ArrayList<User> suggestions = new ArrayList<User>();
                for (User user : mUsers) {
                    // Note: change the "contains" to "startsWith" if you only want starting matches
                    if (user.getFullName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(user);
                    }
                }

                results.values = suggestions;
                results.count = suggestions.size();
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            if (results != null && results.count > 0) {
                // we have filtered results
                addAll((ArrayList<User>) results.values);
            } else {
                // no filter, add entire original list back in
                addAll(mUsers);
            }
            notifyDataSetChanged();
        }
    };

    public CustomAutoCompleteAdapter(Context context, int textViewResourceId, List<User> users) {
        super(context, textViewResourceId, users);
        // copy all the customers into a master list
        mUsers = new ArrayList<User>(users.size());
        mUsers.addAll(users);
        layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            view = layoutInflater.inflate(R.layout.custom_spinner_row, null);
        }

        User user = getItem(position);

        TextView name = (TextView) view.findViewById(R.id.first_item);
        name.setText(user.getFullName());
        TextView amount = (TextView) view.findViewById(R.id.second_item);
        amount.setText(user.getEmailPhone());
        TextView toUserId = (TextView) view.findViewById(R.id.third_item);
        toUserId.setText(user.getUserId());
        TextView toAddress = (TextView) view.findViewById(R.id.fourth_item);
        toAddress.setText(user.getAddress());

        return view;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    public User getUser(int position) {
        if (position >= 0) return mUsers.get(position);
        return null;
    }


}
