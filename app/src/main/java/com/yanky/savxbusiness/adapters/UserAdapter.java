package com.yanky.savxbusiness.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.realm.models.User;
import com.yanky.savxbusiness.utils.RecyclerViewClickListener;

import java.util.List;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserHolder> {
    private List<User> userList;
    private RecyclerViewClickListener clickListener;

    public void setClickListener(RecyclerViewClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    public class UserHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView name, email;

        public UserHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.tv_name);
            email = (TextView) view.findViewById(R.id.tv_email);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }

    public UserAdapter(List<User> userList) {
        this.userList = userList;
    }

    @Override
    public UserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_row, parent, false);

        return new UserHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull UserAdapter.UserHolder holder, int position) {
        User user = userList.get(position);
        holder.name.setText(user.getFullName());
        holder.email.setText(user.getEmailPhone());
    }


    @Override
    public int getItemCount() {
        return userList.size();
    }


}
