package com.yanky.savxbusiness.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.model.Contacts;

import java.util.List;

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ViewHolder> {
    private static final String TAG = "ContactsAdapter";

    private Context ctx;
    private List<Contacts> contactsList;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public final TextView mName;
        public final TextView mNumber;

        public ViewHolder(View v) {
            super(v);
            mName = (TextView) v.findViewById(R.id.tv_name);
            mNumber = (TextView) v.findViewById(R.id.tv_number);
        }
    }

    public ContactsAdapter(Context mContext, List<Contacts> contactsList) {
        this.contactsList = contactsList;
        ctx = mContext;
    }

    @Override
    public int getItemCount() {
        return contactsList.size();
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Contacts contacts = contactsList.get(position);
        holder.mName.setText(contacts.getName());
        holder.mNumber.setText(contacts.getNumber());
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int arg1) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_row, parent,
                false));

    }

}