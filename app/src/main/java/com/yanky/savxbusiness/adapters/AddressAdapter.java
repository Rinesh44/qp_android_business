package com.yanky.savxbusiness.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.realm.models.Address;

import java.util.List;

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.AddressViewHolder> {
    private List<Address> addresses;

    public class AddressViewHolder extends RecyclerView.ViewHolder {
        private TextView addressId, addressType, address1, address2, state, city, zipCode, country,
                isDefault;

        public AddressViewHolder(View view) {
            super(view);
            addressId = (TextView) view.findViewById(R.id.tv_address_id);
            addressType = (TextView) view.findViewById(R.id.tv_address_type);
            address1 = (TextView) view.findViewById(R.id.tv_address1);
            address2 = (TextView) view.findViewById(R.id.tv_address2);
            state = (TextView) view.findViewById(R.id.tv_state);
            city = (TextView) view.findViewById(R.id.tv_city);
            zipCode = (TextView) view.findViewById(R.id.tv_zip_code);
            country = (TextView) view.findViewById(R.id.tv_country);
            isDefault = (TextView) view.findViewById(R.id.tv_default);
        }
    }

    public AddressAdapter(List<Address> addresses) {
        this.addresses = addresses;
    }

    @Override
    public AddressViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.address_row, parent, false);

        return new AddressViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AddressAdapter.AddressViewHolder holder, int position) {
        Address address = addresses.get(position);
        holder.addressId.setText(address.getAddressId());
        holder.addressType.setText(address.getAddressType());
        holder.address1.setText(address.getAddress1());
        holder.address2.setText(address.getAddress2());
        holder.state.setText(address.getState());
        holder.city.setText(address.getCity());
        holder.zipCode.setText(address.getZipCode());
        holder.country.setText(address.getCountry());
        if (address.getDefault()) {
            holder.isDefault.setText("DEFAULT");
        } else {
            holder.isDefault.setVisibility(View.INVISIBLE);
        }
    }


    @Override
    public int getItemCount() {
        return addresses.size();
    }

    public Address getItem(int position) {
        if (position >= 0) return addresses.get(position);
        return null;
    }

    public void removeItem(int position) {
        addresses.remove(position);
    }

}