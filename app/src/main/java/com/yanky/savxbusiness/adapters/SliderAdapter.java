package com.yanky.savxbusiness.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yanky.savxbusiness.R;

public class SliderAdapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;

    public SliderAdapter(Context context) {
        this.context = context;
    }

    public int[] slide_images = {
            R.drawable.slide_img1,
            R.drawable.slide_img2,
            R.drawable.slide_img3,
            R.drawable.slide_img4
    };

    public int[] slide_headings = {
            R.string.slide_1,
            R.string.slide_2,
            R.string.slide_3,
            R.string.slide_4
    };

    @Override
    public int getCount() {
        return slide_headings.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == (RelativeLayout) object;
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);

        View view = layoutInflater.inflate(R.layout.slide_layout, container, false);

        ImageView imageView = view.findViewById(R.id.iv_icon);
        TextView textView = view.findViewById(R.id.tv_desc);
        Typeface type = Typeface.createFromAsset(context.getAssets(), "fonts/poppins_medium.ttf");
        textView.setTypeface(type);
        textView.setPadding(15, 0, 0, 0);
        textView.setTextSize(20);

        imageView.setImageResource(slide_images[position]);
        textView.setText(slide_headings[position]);

        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((RelativeLayout) object);
    }
}
