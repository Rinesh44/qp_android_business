package com.yanky.savxbusiness.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.realm.models.SyncedContacts;

import java.util.List;

public class SyncContactAdapter extends RecyclerView.Adapter<SyncContactAdapter.ContactHolder> {
    private List<SyncedContacts> syncedContactsList;

    public class ContactHolder extends RecyclerView.ViewHolder {
        private TextView name, phone;

        public ContactHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.tv_name);
            phone = (TextView) view.findViewById(R.id.tv_number);
        }
    }

    public SyncContactAdapter(List<SyncedContacts> syncedContactsList) {
        this.syncedContactsList = syncedContactsList;
    }

    @Override
    public ContactHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contact_row, parent, false);

        return new ContactHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SyncContactAdapter.ContactHolder holder, int position) {
        SyncedContacts syncedContacts = syncedContactsList.get(position);
        holder.name.setText(syncedContacts.getName());
        holder.phone.setText(syncedContacts.getPhone());
    }


    @Override
    public int getItemCount() {
        return syncedContactsList.size();
    }


}