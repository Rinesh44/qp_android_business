package com.yanky.savxbusiness;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.Context;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.yanky.savxbusiness.activities.receivers.ScreenOffReceiver;
import com.yanky.savxbusiness.crypto.SodiumEncryption;
import com.yanky.savxbusiness.dagger.component.AppComponent;
import com.yanky.savxbusiness.dagger.component.DaggerAppComponent;
import com.yanky.savxbusiness.dagger.component.modules.AppModule;
import com.yanky.savxbusiness.dagger.component.modules.NetModule;
import com.yanky.savxbusiness.mqtt.MqttClient;
import com.yanky.savxbusiness.realm.RealmDatabase;
import com.yanky.savxbusiness.utils.AppForegroundListener;
import com.yanky.savxbusiness.utils.AppLifecycleHandler;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.BackgroundTask;
import com.yanky.savxbusiness.utils.LifecycleDelegate;

/**
 * Created by Leesa Shakya on 27/09/18.
 * leezshk@gmail.com
 */


@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class QuiqpayApp extends Application implements LifecycleDelegate {
    private static final String TAG = "QuiqpayApp";
    private AppComponent appComponent;
    private static BackgroundTask task;
    public static AppForegroundListener appForegroundListener;
    public static String refreshedToken;

    SharedPreferences preferences;
    private static Context context;

//    public static boolean firstTimeLaunch = true;

    public static void setAppForegroundListener(AppForegroundListener listener) {
        appForegroundListener = listener;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.init();

        QuiqpayApp.context = getApplicationContext();

        setUpRealm();
        Fresco.initialize(this);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        ScreenOffReceiver screenOffReceiver = new ScreenOffReceiver();

        registerReceiver(screenOffReceiver, new IntentFilter(
                "android.intent.action.SCREEN_OFF"));

        AppLifecycleHandler lifecycleHandler = new AppLifecycleHandler(this);
        registerLifecycleHandler(lifecycleHandler);

//        setFirebaseToken();

    }


    public static QuiqpayApp getMyApplication(Context context) {
        return (QuiqpayApp) context.getApplicationContext();
    }

    private void init() {
        AppUtils.showLog(TAG, "init()");

        task = new BackgroundTask();
        task.start();

  /*      //Start mqtt asynchronously.
        if (task.getHandler() != null) {
            AppUtils.showLog(TAG, "task handler not null");
            task.getHandler().post(() -> MqttClient.start(this));
        } else {
            AppUtils.showLog(TAG, "task handler null");
        }*/

        MqttClient.start(this);

        SodiumEncryption.getInstance().setKeyFilePath(getFilesDir());
    }

    public AppComponent getAppComponent() {
        if (appComponent == null) {
            appComponent = DaggerAppComponent.builder()
                    .appModule(new AppModule(this))
                    .netModule(new NetModule())
                    .build();
        }
        return appComponent;
    }

    private void setUpRealm() {
        RealmDatabase.init(this);
    }


    @Override
    public void onAppBackgrounded() {
        AppUtils.showLog(TAG, "app in background");
    }

    @Override
    public void onAppForegrounded() {
        AppUtils.showLog(TAG, "app in foreground");

        if (appForegroundListener != null) appForegroundListener.onAppForeground();

    }

    private void registerLifecycleHandler(AppLifecycleHandler lifecycleHandler) {
        registerActivityLifecycleCallbacks(lifecycleHandler);
        registerComponentCallbacks(lifecycleHandler);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        MqttClient.stop();
    }


/*    public void setFirebaseToken() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                refreshedToken = instanceIdResult.getToken();
                AppUtils.showLog(TAG, "refreshed token : " + refreshedToken);
            }
        });
    }*/

    public static String getFirebaseToken() {
        return refreshedToken;
    }

    public static Context getAppContext() {
        return QuiqpayApp.context;
    }
}
