package com.yanky.savxbusiness.crypto;

import android.util.Log;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.yanky.savxbusiness.entities.WalletProto;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.FileUtils;

import org.libsodium.jni.NaCl;
import org.libsodium.jni.Sodium;
import org.libsodium.jni.crypto.Hash;
import org.libsodium.jni.crypto.Random;
import org.libsodium.jni.crypto.SecretBox;
import org.libsodium.jni.encoders.Hex;

import java.io.File;
import java.nio.charset.Charset;
import java.util.Arrays;

/**
 * Author Dipak Malla
 * Created on 10/3/18.
 * Email: dpakmalla@gmail.com
 */
public final class SodiumEncryption {
    String TAG = SodiumEncryption.class.getSimpleName();
    private static final SodiumEncryption instance;
    private final Hash hash = new Hash();
    private File basePath;

    private SodiumEncryption() {
    }

    static {
        NaCl.sodium();
        instance = new SodiumEncryption();
        d("Loaded sodium encryption library.");
    }

    public static SodiumEncryption getInstance() {
        return instance;
    }

    public void setKeyFilePath(final File basePath) {
        this.basePath = basePath;
    }

    private static void d(String msg) {
        Log.d("SodiumEncryption", msg);
    }

    boolean recoverKeys(String userId, String passcode, WalletProto.KeysBackup keyBackup) {
        try {
          /*  byte[] fileData = FileUtils.toBytes(stream);
            WalletProto.KeysBackup keyBackup = WalletProto.KeysBackup.parseFrom(fileData);*/
            byte[] secret = getSecretFromPasscode(passcode, userId);
            if (secret == null) {
                d("Could not generate secret from given pass code for user : " + userId);
                return false;
            }
            SecretBox secretBox = new SecretBox(secret);
            byte[] msg = secretBox.decrypt(keyBackup.getNonce().toByteArray(),
                    keyBackup.getEncryptedKeys().toByteArray());
            File savePath = getPath(userId, basePath);
            if (savePath.exists() && savePath.isFile()) {
                d("Key already generated for user : " + userId);
                AppUtils.showLog(TAG, "Key already generated for user : " + userId);
                return false;
            }
            AppUtils.showLog(TAG, "key not imported, importing...");
            return FileUtils.write(msg, savePath);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    boolean isKeyGenerated(final String userId) {
        File savePath = getPath(userId, basePath);
        return savePath.exists() && savePath.isFile();
    }

    String hash256(String data) {
        return Hex.HEX.encode(hash.sha256(data.getBytes(Charset.forName("UTF-8"))));
    }

    WalletProto.KeysBackup keyBackup(final String userId, final String passcode) {
        byte[] keyBytes = loadKeyBytes(userId);
        if (null == keyBytes) {
            d("No key file found for user : " + userId);
            return null;
        }
        byte[] secret = getSecretFromPasscode(passcode, userId);
        if (null == secret) {
            d("Could not generate secret from given pass code for user : " + userId);
            return null;
        }
        byte[] nonce = nonce();
        SecretBox secretBox = new SecretBox(secret);
        byte[] cipher = secretBox.encrypt(nonce, keyBytes);
        return WalletProto.KeysBackup.newBuilder()
                .setNonce(ByteString.copyFrom(nonce))
                .setEncryptedKeys(ByteString.copyFrom(cipher))
                .build();
    }

    String addNewWallet(final String userId) {
        try {
            File savePath = getPath(userId, basePath);
            if (!savePath.exists()) {
                d("Key key found for user : " + userId);
                AppUtils.showLog(TAG, "key not found for user");
                return null;
            }
            WalletProto.Keys keys = loadKeys(userId);
            if (null == keys) {
                return null;
            }

            WalletProto.WalletAddressKey walletAddressKey = createNewWalletAddress();
            if (null == walletAddressKey) {
                return null;
            }
            keys = keys.toBuilder()
                    .addAllWalletAddressKeys(Arrays.asList(walletAddressKey))
                    .build();
            if (FileUtils.write(keys.toByteArray(), savePath)) {
                d("Added new wallet address for user : " + userId);
                AppUtils.showLog(TAG, "added new wallet address for user");
                return Hex.HEX.encode(walletAddressKey.getSigPublicKey().toByteArray());
            } else {
                AppUtils.showLog(TAG, "Could not add wallet address for user");
                d("Could not add wallet address for user : " + userId);
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public WalletProto.WalletAddressKey getWallet(String userId, String walletAddress) {
        WalletProto.Keys keys = this.loadKeys(userId);
        if (null == keys) {
            AppUtils.showLog(TAG, "keys are null");
            return null;
        } else {
            AppUtils.showLog(TAG, "keys not null");
            AppUtils.showLog(TAG, "one key:" + keys.getWalletAddressKeysList().get(0));

            AppUtils.showLog(TAG, "walletAddress:" + walletAddress);
            AppUtils.showLog(TAG, "count:" + keys.getWalletAddressKeysCount());
            for (WalletProto.WalletAddressKey walletAddressKey : keys.getWalletAddressKeysList()) {
                AppUtils.showLog(TAG, "Keys" + Hex.HEX.encode(walletAddressKey.getSigPublicKey().toByteArray()));
                if (Hex.HEX.encode(walletAddressKey.getSigPublicKey().toByteArray()).equalsIgnoreCase(walletAddress)) {
                    return walletAddressKey;
                }
            }
        }
        return null;
    }

    public String getExistingWalletAddress(String userId) {
        WalletProto.Keys keys = this.loadKeys(userId);
        if (null == keys) {
            return null;
        } else {
            return Hex.HEX.encode(keys.getWalletAddressKeysList().get(0).getSigPublicKey().toByteArray());
        }
    }

    byte[] sign(byte[] data, String userId, String walletAddress) {
        WalletProto.WalletAddressKey key = getWallet(userId, walletAddress);
        if (null == key) {
            AppUtils.showLog(TAG, "key null");
            return null;
        } else {
            return sign(data, key.getSigPrivateKey().toByteArray());
        }
    }

    private byte[] sign(byte[] data, byte[] privateKey) {
        int[] sigLen = new int[1];
        byte[] signature = new byte[Sodium.crypto_sign_ed25519_bytes()];
        int result = Sodium.crypto_sign_ed25519_detached(signature, sigLen, data,
                data.length, privateKey);
        if (result != 0) {
            AppUtils.showLog(TAG, "Error while signing.");
            d("Error while signing.");
            return null;
        } else {
            return signature;
        }
    }

    private WalletProto.WalletAddressKey createNewWalletAddress() {
        int signPubKeyLen = Sodium.crypto_sign_ed25519_publickeybytes();
        int signPrivateKeyLen = Sodium.crypto_sign_ed25519_secretkeybytes();
        //These are the default wallet's keys
        byte[] signPublicKey = new byte[signPubKeyLen];
        byte[] signPrivateKey = new byte[signPrivateKeyLen];
        int result = Sodium.crypto_sign_ed25519_keypair(signPublicKey, signPrivateKey);
        if (result != 0) {
            AppUtils.showLog(TAG, "Error while generating sign keys for wallet");
            d("Error while generating sign keys for wallet.");
            return null;
        }
        return WalletProto.WalletAddressKey
                .newBuilder()
                .setSigPrivateKey(ByteString.copyFrom(signPrivateKey))
                .setSigPublicKey(ByteString.copyFrom(signPublicKey))
                .build();
    }


    //Generate Wallet address public key.
    String generateKeys(final String userId) {
        try {
            File savePath = getPath(userId, basePath);
            if (savePath.exists() && savePath.isFile()) {
                AppUtils.showLog(TAG, "key already genrated for user");
                d("Key already generated for user : " + userId);
                return null;
            }
            int pubKeyLen = Sodium.crypto_box_publickeybytes();
            int privateKeyLen = Sodium.crypto_box_secretkeybytes();
            byte[] publicKey = new byte[pubKeyLen];
            byte[] privateKey = new byte[privateKeyLen];
            int result = Sodium.crypto_box_keypair(publicKey, privateKey);
            if (result != 0) {
                AppUtils.showLog(TAG, "error white generating keys");
                d("Error while generating keys.");
                return null;
            }
            byte[] key = new Random().randomBytes(32);
            byte[] refId = new Random().randomBytes(32);
            WalletProto.Keys.Builder keyBuilder = WalletProto.Keys.newBuilder();
            keyBuilder.setPrivateKey(ByteString.copyFrom(privateKey));
            keyBuilder.setPublicKey(ByteString.copyFrom(publicKey));
            keyBuilder.setEncryptionKey(ByteString.copyFrom(key));
            keyBuilder.setRefId(ByteString.copyFrom(refId));
            WalletProto.WalletAddressKey walletAddressKey = createNewWalletAddress();
            if (null == walletAddressKey) {
                return null;
            }
            keyBuilder.addAllWalletAddressKeys(Arrays.asList(walletAddressKey));
            WalletProto.Keys keys = keyBuilder.build();
            byte[] keyData = keys.toByteArray();
            if (FileUtils.write(keyData, savePath)) {
                AppUtils.showLog(TAG, "all keys generated for user");
                d("All keys generated for user : " + userId);
                return Hex.HEX.encode(walletAddressKey.getSigPublicKey().toByteArray());
            } else {
                AppUtils.showLog(TAG, "could not generate keys for user");
                d("Could not generate keys for user : " + userId);
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private byte[] nonce() {
        return new Random().randomBytes(24);
    }

    private byte[] getSecretFromPasscode(final String passcode, String userId) {
        byte[] key = new byte[Sodium.crypto_box_seedbytes()];
        byte[] password = passcode.getBytes();
        int result = Sodium.crypto_pwhash(key, key.length, password, password.length,
                Hex.HEX.decode(userId),
                2,
                67108864,
                Sodium.crypto_pwhash_alg_default());
        if (result == 0) {
            return key;
        } else {
            return null;
        }
    }

    private WalletProto.Keys loadKeys(final String userId) {
        byte[] keyBytes = loadKeyBytes(userId);
        if (null == keyBytes) {
            AppUtils.showLog(TAG, "load keys failed for user");
            d("No key file found for user : " + userId);
            return null;
        }
        try {
            return WalletProto.Keys.parseFrom(keyBytes);
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        return null;
    }

    private byte[] loadKeyBytes(final String userId) {
        File keyPath = getPath(userId, basePath);
        return FileUtils.open(keyPath);
    }

    private File getPath(String prefix, File path) {
        AppUtils.showLog(TAG, "path: " + path.getPath());
        return new File(String.format("%s%s%s.qp", path.getPath(),
                File.separator, prefix));
    }


    public String getPublicKey(String userId) {
        WalletProto.Keys key = loadKeys(userId);
        if (null != key)
            return Hex.HEX.encode(key.getPublicKey().toByteArray());
        else
            return null;
    }

}
