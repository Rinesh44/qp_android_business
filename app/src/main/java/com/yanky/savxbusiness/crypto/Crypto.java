package com.yanky.savxbusiness.crypto;

import com.yanky.savxbusiness.entities.WalletProto;

/**
 * Author Dipak Malla
 * Created on 10/3/18.
 * Email: dpakmalla@gmail.com
 */

public final class Crypto {
    private final SodiumEncryption encryption;

    public static Crypto build() {
        return new Crypto(SodiumEncryption.getInstance());
    }

    private Crypto(SodiumEncryption encryption) {
        this.encryption = encryption;
    }

    public String generateKey(String userId) {
        return this.encryption.generateKeys(userId);
    }

    public String addNewWalletAddress(String userId) {
        return this.encryption.addNewWallet(userId);
    }

    public WalletProto.KeysBackup keyBackUp(final String userId, final String passcode) {
        return this.encryption.keyBackup(userId, passcode);
    }

    public boolean recoverKeys(String userId, String passcode, WalletProto.KeysBackup keysBackup) {
        return this.encryption.recoverKeys(userId, passcode, keysBackup);
    }

    public String generateRefId(String passcode, String userId) {
        return this.encryption.hash256(this.encryption.hash256(passcode + userId));
    }

    public boolean isKeyGenerated(String userId) {
        return this.encryption.isKeyGenerated(userId);
    }

    public byte[] sign(byte[] data, String userId, String walletAddress) {
        return this.encryption.sign(data, userId, walletAddress);
    }

    public String getExistingWalletAddress(String userId) {
        return this.encryption.getExistingWalletAddress(userId);
    }

    public String getPubKey(String userId) {
        return this.encryption.getPublicKey(userId);
    }


}
