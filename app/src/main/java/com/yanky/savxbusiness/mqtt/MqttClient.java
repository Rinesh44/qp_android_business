package com.yanky.savxbusiness.mqtt;

import android.content.Context;

import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.NetworkUtils;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;


final public class MqttClient implements MqttCallback {
    private static final String TAG = "MqttClient";
    private static final String BROKER = "tcp://192.168.0.188:1883";
    //Deliver at least once with ack
    private static final int DEFAULT_QOS = 1;
    private static MqttClient instance;
    private MqttAndroidClient mqttClient;
    private Map<String, Processor> processors = new ConcurrentHashMap<>();

    private MqttClient() {
    }

    public static void start(final Context context) {
        if (null != instance && instance.isConnected(context)) {
            return;
        }
        try {
            instance = new MqttClient();
            instance.processors.clear();
            instance.connect(context);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    public static void reconnect(final Context context) {
        if (null == instance || instance.isConnected(context)) {
            return;
        }
        instance.connect(context);
    }

    public static void stop() {
        if (null != instance) {
            instance.disconnect();
        }
    }

    public static MqttClient getInstance() {
        return instance;
    }

    public boolean isConnected(Context context) {
        if (NetworkUtils.isNetworkConnected(context)) return mqttClient.isConnected();
        else return false;
    }

    public boolean subscribe(String topic, Processor processor) {
        try {
            mqttClient.subscribe(topic, DEFAULT_QOS);
            processors.put(topic, processor);
            return true;
        } catch (MqttException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean unsubscribe(String topic) {
        try {
            mqttClient.unsubscribe(topic);
            processors.remove(topic);
            return true;
        } catch (MqttException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean publish(String topic, String payload) {
        try {
            return publish(topic, payload.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean publish(String topic, byte[] payload) {
        try {
            if (!mqttClient.isConnected()) {
                AppUtils.showLog(TAG, "Mqtt is not connected.");
                return false;
            }
            mqttClient.publish(topic, payload, DEFAULT_QOS, false);
            return true;
        } catch (MqttException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void disconnect() {
        try {
            AppUtils.showLog(TAG, "Disconnected gracefully from : " + BROKER);
            mqttClient.unregisterResources();
            if (mqttClient.isConnected()) {
                mqttClient.disconnect();
            }
            mqttClient.close();
            processors.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void connect(final Context context) {
        AppUtils.showLog(TAG, "MQTTconnect()");
        if (!NetworkUtils.isNetworkConnected(context)) return;

        mqttClient = new MqttAndroidClient(context, BROKER, UUID.randomUUID().toString());
        mqttClient.setCallback(this);
        try {
            mqttClient.connect();
            AppUtils.showLog(TAG, "Connected to mqtt broker : " + BROKER);
        } catch (MqttException e) {
            AppUtils.showLog(TAG, "Mqtt exception: " + e.toString());
            e.printStackTrace();
        }
    }

    @Override
    public void connectionLost(Throwable cause) {
        //TODO need to reconnect if connection is lost.
        AppUtils.showLog(TAG, "Connection lost on mqtt with : " + BROKER);
        cause.printStackTrace();
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        Processor processor = processors.get(topic);
        if (null == processor) {
            AppUtils.showLog(TAG, "No processor was register for topic : " + topic);
        } else {
            processor.process(new Payload(topic, message));
        }
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {

    }

    public final static class Payload {
        final public String topic;
        final public MqttMessage message;

        public Payload(final String topic, final MqttMessage message) {
            this.topic = topic;
            this.message = message;
        }
    }

    public interface Processor {
        void process(Payload payload);
    }
}
