package com.yanky.savxbusiness.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.paynow.Pay;
import com.yanky.savxbusiness.activities.paynow.RequestPayment;
import com.yanky.savxbusiness.utils.AppUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PayNow.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PayNow#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PayNow extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String TAG = "PayNow";
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private ImageView ivQRcode;
    private String token, userId;
    private Button mPay;
    private TextView mRequestNow;

    private OnFragmentInteractionListener mListener;

    public PayNow() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PayNow.
     */
    // TODO: Rename and change types and number of parameters
    public static PayNow newInstance(String param1, String param2) {
        PayNow fragment = new PayNow();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        //to remove keyboard glich
        getActivity().getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.background));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pay_now, container, false);
        token = getArguments().getString("token");
        userId = getArguments().getString("user_id");
        ivQRcode = view.findViewById(R.id.qr_code);
        mPay = view.findViewById(R.id.btn_pay);
        mRequestNow = view.findViewById(R.id.tv_request_now);
        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        String encodedString = createJsonObjectForQRCode();

        createQRcode(encodedString);

        mPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), Pay.class);
                startActivity(intent);
            }
        });

        mRequestNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), RequestPayment.class);
                startActivity(intent);
            }
        });


    }

    private String createJsonObjectForQRCode() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", "USERID");
            jsonObject.put("userId", userId);

            AppUtils.showLog(TAG, "jsonObject" + jsonObject.toString());

            byte[] data = jsonObject.toString().trim().getBytes(StandardCharsets.UTF_8);
            String base64 = Base64.encodeToString(data, Base64.DEFAULT);

            AppUtils.showLog(TAG, "encoded:" + base64);
            base64 = base64.replace(System.getProperty("line.separator"), "");
            return base64;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void createQRcode(String input) {
        try {
            //to remove white border of barcode
            Map<EncodeHintType, Object> hintMap = new HashMap<EncodeHintType, Object>();
            hintMap.put(EncodeHintType.MARGIN, new Integer(0));

            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.encodeBitmap(input, BarcodeFormat.QR_CODE, 600, 600, hintMap);
            ivQRcode.setImageBitmap(bitmap);
        } catch (Exception e) {
            AppUtils.showLog(TAG, e.toString());
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
