package com.yanky.savxbusiness.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.wallet.WalletDetails;
import com.yanky.savxbusiness.adapters.WalletAdapter;
import com.yanky.savxbusiness.repo.WalletsRepo;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.RecyclerViewClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Wallet.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Wallet#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Wallet extends Fragment implements RecyclerViewClickListener {
    private String TAG = Wallet.class.getSimpleName();
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private RecyclerView mRecyclerView;
    private ProgressBar mProgressbar;
    private WalletAdapter walletAdapter;
    private List<com.yanky.savxbusiness.realm.models.Wallet> mWalletList;

    public Wallet() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Wallet.
     */
    // TODO: Rename and change types and number of parameters
    public static Wallet newInstance(String param1, String param2) {
        Wallet fragment = new Wallet();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_wallet, container, false);
        mRecyclerView = view.findViewById(R.id.recycler_view);
        mProgressbar = view.findViewById(R.id.pb_wallets);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mWalletList = new ArrayList<>();
        updateRecyclerView();

    }

    public void updateRecyclerView() {
        AppUtils.showLog(TAG, "updateRecyclerView()");

        mWalletList = WalletsRepo.getInstance().getAllWallets();
//        mWalletList = walletList;
        if (!mWalletList.isEmpty()) {
            mRecyclerView.setVisibility(View.VISIBLE);
            mProgressbar.setVisibility(View.GONE);
            AppUtils.showLog(TAG, "walletArray:" + mWalletList.get(0).getWalletName());
            walletAdapter = new WalletAdapter(getActivity(), mWalletList);
            walletAdapter.setClickListener(this);

            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mRecyclerView.setAdapter(walletAdapter);

            AppUtils.showLog(TAG, "walletArray:" + walletAdapter.getItemCount());
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view, int position) {
        final com.yanky.savxbusiness.realm.models.Wallet wallet = mWalletList.get(position);
        Intent i = new Intent(getActivity(), WalletDetails.class);
        i.putExtra("name", wallet.getWalletName());
        i.putExtra("amount", wallet.getAmount());
        i.putExtra("currency", wallet.getCurrency());
        i.putExtra("country_code", wallet.getCountryCode());
        AppUtils.showLog(TAG, "amount:" + wallet.getAmount());
        i.putExtra("last_active", wallet.getLastActive());
        i.putExtra("wallet_address", wallet.getWalletAddress());
        startActivity(i);

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
