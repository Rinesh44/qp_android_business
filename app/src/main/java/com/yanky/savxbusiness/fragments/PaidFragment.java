package com.yanky.savxbusiness.fragments;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.protobuf.InvalidProtocolBufferException;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.dashboard.DashboardActivity;
import com.yanky.savxbusiness.activities.invoice.PaidInvoiceDetails;
import com.yanky.savxbusiness.adapters.PaidInvoiceAdapter;
import com.yanky.savxbusiness.crypto.Crypto;
import com.yanky.savxbusiness.entities.WalletProto;
import com.yanky.savxbusiness.realm.models.PaidInvoice;
import com.yanky.savxbusiness.realm.models.Wallet;
import com.yanky.savxbusiness.repo.InvoiceRepo;
import com.yanky.savxbusiness.repo.WalletsRepo;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.Constants;
import com.yanky.savxbusiness.utils.RecyclerViewClickListener;
import com.yanky.savxbusiness.utils.UpdatePaidInvoice;

import org.libsodium.jni.encoders.Hex;

import java.util.ArrayList;
import java.util.List;

public class PaidFragment extends Fragment implements RecyclerViewClickListener, UpdatePaidInvoice {
    private static final String TAG = "PaidFragment";
    private RecyclerView mRecyclerView;
    private List<PaidInvoice> mInvoiceList;
    private PaidInvoiceAdapter invoiceAdapter;
    private List<WalletProto.InvoiceHolder> invoiceHoldersList;
    private SharedPreferences preferences;

    public PaidFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_paide, container, false);

        mRecyclerView = view.findViewById(R.id.recycler_view_paid);
        DashboardActivity.setUpdatePaidInvoiceListener(this);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mInvoiceList = new ArrayList<>();
        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        updateRecyclerView(InvoiceRepo.getInstance().getAllPaidInvoices());

    }


    public void updateRecyclerView(List<PaidInvoice> invoiceList) {
        AppUtils.showLog(TAG, "updateRecyclerView()");

        mInvoiceList = invoiceList;

//        AppUtils.showLog(TAG, "walletArray:" + mInvoiceList.get(0).getInvoiceHeading());
        invoiceAdapter = new PaidInvoiceAdapter(getActivity(), mInvoiceList);
        invoiceAdapter.setClickListener(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(invoiceAdapter);

        AppUtils.showLog(TAG, "paidinvoiceArray:" + invoiceAdapter.getItemCount());
    }


    @Override
    public void onClick(View view, int position) {
        String userId = preferences.getString(Constants.USER_ID, "");

        WalletProto.InvoiceHolder invoiceHolder = invoiceHoldersList.get(position);
        AppUtils.showLog(TAG, "userId" + userId);

        AppUtils.showLog(TAG, "invoiceHolder" + invoiceHolder.getSignaturePubKey());
        try {
            WalletProto.Invoice invoiceProto = WalletProto.Invoice.parseFrom(invoiceHolder.getInvoice());

            AppUtils.showLog(TAG, "invoiceProtoByte" + invoiceProto.toByteArray());

            Wallet defaultWallet = WalletsRepo.getInstance().getDefaultWallet();
            String defaultWalletAddress = defaultWallet.getWalletAddress();

            AppUtils.showLog(TAG, "walletAddressDefault:" + preferences.getString(Constants.DEFAULT_WALLET_ADDRESS, ""));
            byte[] signature = Crypto.build().sign(invoiceProto.toByteArray(), userId, defaultWalletAddress);

            if (null != signature) {
                String signatureString = Hex.HEX.encode(signature);
                AppUtils.showLog(TAG, "signature:" + signatureString);

                Intent showDetails = new Intent(getActivity(), PaidInvoiceDetails.class);
                PaidInvoice invoice = mInvoiceList.get(position);
                showDetails.putExtra("invoice_object", invoice);
                showDetails.putExtra("signature", signatureString);
                startActivity(showDetails);
            } else {
                Toast.makeText(getContext(), "Unable to get signature", Toast.LENGTH_SHORT).show();
            }

        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void updatePaidInvoiceRecyclerView() {
        AppUtils.showLog(TAG, "updatePaidInvoiceRecyclerView()");
        updateRecyclerView(InvoiceRepo.getInstance().getAllPaidInvoices());
        invoiceAdapter.notifyDataSetChanged();
    }

    @Override
    public void updatePaidInvoiceHolder(List<WalletProto.InvoiceHolder> invoiceHolders) {
        invoiceHoldersList = invoiceHolders;
    }
}

