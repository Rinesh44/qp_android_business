package com.yanky.savxbusiness.fragments;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.protobuf.InvalidProtocolBufferException;
import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.dashboard.DashboardActivity;
import com.yanky.savxbusiness.activities.invoice.UnpaidInvoiceDetails;
import com.yanky.savxbusiness.adapters.UnpaidInvoiceAdapter;
import com.yanky.savxbusiness.crypto.Crypto;
import com.yanky.savxbusiness.entities.WalletProto;
import com.yanky.savxbusiness.realm.models.UnpaidInvoice;
import com.yanky.savxbusiness.realm.models.Wallet;
import com.yanky.savxbusiness.repo.InvoiceRepo;
import com.yanky.savxbusiness.repo.WalletsRepo;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.Constants;
import com.yanky.savxbusiness.utils.RecyclerViewClickListener;
import com.yanky.savxbusiness.utils.UpdateUnpaidInvoice;

import org.libsodium.jni.encoders.Hex;

import java.util.ArrayList;
import java.util.List;

public class UnpaidFragment extends Fragment implements RecyclerViewClickListener, UpdateUnpaidInvoice {
    private String TAG = UnpaidFragment.class.getSimpleName();
    private RecyclerView mRecyclerView;
    private List<UnpaidInvoice> mInvoiceList;
    private UnpaidInvoiceAdapter invoiceAdapter;
    //    private DashboardActivity mActivity;
    public List<WalletProto.InvoiceHolder> invoiceHolders;
    private Wallet defaultWallet;
    private SharedPreferences preferences;

    public UnpaidFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        mActivity = (DashboardActivity) getActivity();
//        mActivity.setUnpaidDataListener(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_unpaid, container, false);

        mRecyclerView = view.findViewById(R.id.recycler_view_unpaid);
        mInvoiceList = new ArrayList<>();

        DashboardActivity.setUpdateUnpaidInvoiceListener(this);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        updateRecyclerView(InvoiceRepo.getInstance().getAllUnpaidInvoices());

        defaultWallet = WalletsRepo.getInstance().getDefaultWallet();
        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());

        if (defaultWallet != null && defaultWallet.getWalletAddress() != null) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(Constants.DEFAULT_WALLET_ADDRESS, defaultWallet.getWalletAddress());
            editor.apply();
        }


    }

    public void updateRecyclerView(List<UnpaidInvoice> invoiceList) {
        AppUtils.showLog(TAG, "updateRecyclerView()");

        mInvoiceList = invoiceList;
//        AppUtils.showLog(TAG, "walletArray:" + mInvoiceList.get(0).getInvoiceHeading());
        invoiceAdapter = new UnpaidInvoiceAdapter(getActivity(), mInvoiceList);
        invoiceAdapter.setClickListener(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(invoiceAdapter);

        AppUtils.showLog(TAG, "unpaidInvoiceArray:" + invoiceAdapter.getItemCount());
    }

    @Override
    public void onClick(View view, int position) {

        String userId = preferences.getString(Constants.USER_ID, "");
        if (invoiceHolders == null) {
            return;
        }
        WalletProto.InvoiceHolder invoiceHolder = invoiceHolders.get(position);
        AppUtils.showLog(TAG, "userId" + userId);

        AppUtils.showLog(TAG, "invoiceHolder" + invoiceHolder.getSignaturePubKey());
        try {
            WalletProto.Invoice invoiceProto = WalletProto.Invoice.parseFrom(invoiceHolder.getInvoice());

            AppUtils.showLog(TAG, "invoiceProtoByte" + invoiceProto.toByteArray());

//            String walletAddress = Crypto.build().getExistingWalletAddress(userId);
            Wallet defaultWallet = WalletsRepo.getInstance().getDefaultWallet();
            String defaultWalletAddress = defaultWallet.getWalletAddress();
            AppUtils.showLog(TAG, "defaultWallet: " + defaultWallet.getWalletName());
            AppUtils.showLog(TAG, "defaultWalletAddress: " + defaultWallet.getWalletAddress());
            byte[] signature = Crypto.build().sign(invoiceProto.toByteArray(), userId, defaultWalletAddress);

            if (null != signature) {
                String signatureString = Hex.HEX.encode(signature);
                AppUtils.showLog(TAG, "signature:" + signatureString);

                Intent showDetails = new Intent(getActivity(), UnpaidInvoiceDetails.class);
                UnpaidInvoice invoice = mInvoiceList.get(position);
                showDetails.putExtra("invoice_object", invoice);
                showDetails.putExtra("signature", signatureString);
                startActivity(showDetails);
            } else {
                Toast.makeText(getContext(), "Unable to get signature", Toast.LENGTH_SHORT).show();
            }

        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void updateUnpaidInvoiceRecyclerview() {
        AppUtils.showLog(TAG, "updateUnpaidInvoiceRecyclerview()");
        updateRecyclerView(InvoiceRepo.getInstance().getAllUnpaidInvoices());
        invoiceAdapter.notifyDataSetChanged();
    }

    @Override
    public void updateUnpaidInvoiceHolder(List<WalletProto.InvoiceHolder> unpaidInvoiceHolder) {
        AppUtils.showLog(TAG, "unpaidInvoiceHolder:" + unpaidInvoiceHolder.get(0).getSignaturePubKey());
        invoiceHolders = unpaidInvoiceHolder;


    }


}
