package com.yanky.savxbusiness.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.yanky.savxbusiness.R;
import com.yanky.savxbusiness.activities.account.LoginActivity;
import com.yanky.savxbusiness.activities.autopayments.AutoPayments;
import com.yanky.savxbusiness.activities.settings.Settings;
import com.yanky.savxbusiness.activities.transactionhistory.TransactionHistory;
import com.yanky.savxbusiness.api.Endpoints;
import com.yanky.savxbusiness.entities.ReqResProto;
import com.yanky.savxbusiness.realm.RealmDatabase;
import com.yanky.savxbusiness.utils.AppUtils;
import com.yanky.savxbusiness.utils.Constants;
import com.yanky.savxbusiness.utils.NetworkUtils;

import javax.inject.Inject;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.yanky.savxbusiness.QuiqpayApp.getMyApplication;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link More.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link More#newInstance} factory method to
 * create an instance of this fragment.
 */
public class More extends Fragment implements View.OnClickListener {
    private String TAG = More.class.getSimpleName();
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private Button mLogout, mTransactionHistory, mPaymentRequest, mAutopayments, mWalletRequest, mSettings,
            mKyc, mFxTrade;
    private SharedPreferences mPrefs;

    private OnFragmentInteractionListener mListener;
    @Inject
    Endpoints endpoints;
    String token;
    private TextView mWalletRequestBadge, mPaymentRequestBadge;

    public More() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment More.
     */
    // TODO: Rename and change types and number of parameters
    public static More newInstance(String param1, String param2) {
        More fragment = new More();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        getMyApplication(getActivity()).getAppComponent().inject(this);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_more, container, false);

        mPrefs = PreferenceManager.getDefaultSharedPreferences(getContext());

        mLogout = view.findViewById(R.id.btn_logout);
        mTransactionHistory = view.findViewById(R.id.btn_transaction_history);
        mPaymentRequest = view.findViewById(R.id.btn_payment_requests);
        mAutopayments = view.findViewById(R.id.btn_auto_payments);
        mWalletRequest = view.findViewById(R.id.btn_wallet_requests);
        mSettings = view.findViewById(R.id.btn_settings);
        mKyc = view.findViewById(R.id.btn_kyc);
        mFxTrade = view.findViewById(R.id.btn_forex);

        mPaymentRequestBadge = view.findViewById(R.id.badge_payment_request);
        mWalletRequestBadge = view.findViewById(R.id.badge_wallet_request);


        token = getArguments().getString("token");

        AppUtils.showLog(TAG, "token" + token);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mTransactionHistory.setOnClickListener(this);
        mPaymentRequest.setOnClickListener(this);
        mAutopayments.setOnClickListener(this);
        mWalletRequest.setOnClickListener(this);
        mSettings.setOnClickListener(this);
        mLogout.setOnClickListener(this);
        mKyc.setOnClickListener(this);
        mFxTrade.setOnClickListener(this);

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();

        updateBadges();

    }

    private void updateBadges() {
        int paymentRequestBadgeCount = mPrefs.getInt(Constants.FUNDREQUESTCREATED, 0);
        int walletRequestBadgeCount = mPrefs.getInt(Constants.WALLETREQUESTCREATED, 0);

        AppUtils.showLog(TAG, "paymentBadge: " + paymentRequestBadgeCount);
        AppUtils.showLog(TAG, "walletBadge: " + walletRequestBadgeCount);

        if (paymentRequestBadgeCount != 0)
            mPaymentRequestBadge.setText(String.valueOf(paymentRequestBadgeCount));
        else mPaymentRequestBadge.setVisibility(View.GONE);
        if (walletRequestBadgeCount != 0)
            mWalletRequestBadge.setText(String.valueOf(walletRequestBadgeCount));
        else mWalletRequestBadge.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_logout:
                logout();
                break;

            case R.id.btn_transaction_history:
                startActivity(new Intent(getActivity(), TransactionHistory.class));
                break;

            case R.id.btn_auto_payments:
                startActivity(new Intent(getActivity(), AutoPayments.class));
                break;

            case R.id.btn_settings:
                startActivity(new Intent(getActivity(), Settings.class));
                break;

        }
    }


    private void logout() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
        builder1.setMessage("Are you sure you want to logout?");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (!NetworkUtils.isNetworkConnected(getActivity())) {
                            Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        endpoints.logout(token).enqueue(new Callback<ReqResProto.BaseResponse>() {
                            @Override
                            public void onResponse(Call<ReqResProto.BaseResponse> call, Response<ReqResProto.BaseResponse> response) {
                                ReqResProto.BaseResponse baseResponse = response.body();
                                if (baseResponse == null) {
                                    logoutFail(null);
                                    return;
                                }

                                if (baseResponse.getError()) {
                                    logoutFail(baseResponse.getMsg());
                                    return;
                                }


                                logoutSuccess();
                            }

                            @Override
                            public void onFailure(Call<ReqResProto.BaseResponse> call, Throwable t) {
                                logoutFail(t.toString());
                            }
                        });
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        final AlertDialog alert11 = builder1.create();
        alert11.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                alert11.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(
                        getResources().getColor(android.R.color.holo_blue_light));

                alert11.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(
                        getResources().getColor(android.R.color.holo_red_light));

            }
        });
        alert11.show();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    /**
     * clear shared preferences and database on logout
     */
    public void logoutSuccess() {
        AppUtils.showLog(TAG, "Logout success");


        clearSharedPrefsAndRealm();

        Intent logoutIntent = new Intent(getActivity(), LoginActivity.class);
        logoutIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(logoutIntent);
        getActivity().finish();
    }

    private void clearSharedPrefsAndRealm() {
        //clear shared preferences and realm after logout
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.clear();
        editor.apply();

        final Realm realm = RealmDatabase.getInstance().getRealm();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });
    }

    public void logoutFail(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }
}
