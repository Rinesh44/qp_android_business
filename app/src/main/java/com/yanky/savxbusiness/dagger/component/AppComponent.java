package com.yanky.savxbusiness.dagger.component;

import com.yanky.savxbusiness.activities.account.LoginActivity;
import com.yanky.savxbusiness.activities.account.RegisterActivity;
import com.yanky.savxbusiness.activities.account.password.ChangePassword;
import com.yanky.savxbusiness.activities.account.password.RequestResetActivity;
import com.yanky.savxbusiness.activities.account.password.ResetPasswordActivity;
import com.yanky.savxbusiness.activities.autopayments.AutoPaymentDetails;
import com.yanky.savxbusiness.activities.autopayments.AutoPayments;
import com.yanky.savxbusiness.activities.dashboard.DashboardActivity;
import com.yanky.savxbusiness.activities.invoice.AutoPaymentDialog;
import com.yanky.savxbusiness.activities.invoice.PayInvoice;
import com.yanky.savxbusiness.activities.keys.EnterPasscodeActivity;
import com.yanky.savxbusiness.activities.keys.GenerateKeyActivity;
import com.yanky.savxbusiness.activities.paynow.Pay;
import com.yanky.savxbusiness.activities.paynow.RequestPayment;
import com.yanky.savxbusiness.activities.paynow.mqttpay.ConnectWithServer;
import com.yanky.savxbusiness.activities.profile.AddAddress;
import com.yanky.savxbusiness.activities.profile.UpdatePersonalInfo;
import com.yanky.savxbusiness.activities.profile.UserProfileActivity;
import com.yanky.savxbusiness.activities.search.SearchUser;
import com.yanky.savxbusiness.activities.synccontacts.SyncContacts;
import com.yanky.savxbusiness.activities.transactionhistory.TransactionHistory;
import com.yanky.savxbusiness.activities.verification.VerificationActivity;
import com.yanky.savxbusiness.activities.waitinglist.EnterCredential;
import com.yanky.savxbusiness.activities.waitinglist.RegisterSuccess;
import com.yanky.savxbusiness.activities.waitinglist.RegisterWaitingUser;
import com.yanky.savxbusiness.activities.wallet.ForSelf;
import com.yanky.savxbusiness.activities.wallet.WalletDetails;
import com.yanky.savxbusiness.activities.wallet.fund.LoadFund;
import com.yanky.savxbusiness.activities.wallet.fund.TransferFund;
import com.yanky.savxbusiness.activities.wallet.fund.TransferToBank;
import com.yanky.savxbusiness.activities.wallet.fund.TransferToUser;

import com.yanky.savxbusiness.dagger.component.modules.AppModule;
import com.yanky.savxbusiness.dagger.component.modules.NetModule;
import com.yanky.savxbusiness.fragments.More;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Leesa Shakya on 27/09/18.
 * leezshk@gmail.com
 */

@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface AppComponent {
    void inject(RegisterActivity signUpActivity);

    void inject(LoginActivity loginActivity);

    void inject(VerificationActivity verificationActivity);

    void inject(RequestResetActivity requestResetActivity);

    void inject(ResetPasswordActivity resetPasswordActivity);

    void inject(GenerateKeyActivity generateKeyActivity);

    void inject(EnterPasscodeActivity enterPasscodeActivity);

    void inject(DashboardActivity dashboardActivity);

    void inject(ForSelf forSelf);

    void inject(UserProfileActivity userProfileActivity);

    void inject(UpdatePersonalInfo updatePersonalInfo);

    void inject(AddAddress addAddress);

    void inject(TransferFund transferFund);

    void inject(More more);

    void inject(Pay pay);

    void inject(RequestPayment requestPayment);

    void inject(ChangePassword changePassword);

    void inject(TransactionHistory transactionHistory);

    void inject(LoadFund loadFund);


    void inject(AutoPayments autoPayments);


    void inject(PayInvoice payInvoice);

    void inject(SyncContacts syncContacts);

    void inject(ConnectWithServer connectWithServer);

    void inject(AutoPaymentDialog autoPaymentDialog);

    void inject(AutoPaymentDetails autoPaymentDetails);

    void inject(EnterCredential enterCredential);

    void inject(RegisterWaitingUser registerWaitingUser);

    void inject(TransferToUser transferToUser);

    void inject(TransferToBank transferToBank);

    void inject(SearchUser searchUser);

    void inject(WalletDetails walletDetails);

    void inject(RegisterSuccess registerSuccess);

}
