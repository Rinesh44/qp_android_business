package com.yanky.savxbusiness.realm.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class SyncedContacts extends RealmObject {
    @PrimaryKey
    private String phone;
    private String name;


    public SyncedContacts(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }

    public SyncedContacts() {
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
