package com.yanky.savxbusiness.realm.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.yanky.savxbusiness.entities.WalletProto;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class UnpaidInvoice extends RealmObject implements Parcelable {
    @PrimaryKey
    private String invoiceId;
    private String senderVatNo;
    private String senderUserId;
    private String senderName;
    private String receiverVatNo;
    private String receiverUserId;
    private String receiverName;
    private String totalAmountInWords;
    private String anchorRef;
    private String notes;
    private String forUserId;
    private String invoiceHeading;
    private String dueDate;
    private Double totalAmountToPay;
    private String taxableAmount;
    private String vatPercent;
    private String disPercent;
    private String invoiceDate;
    private String shippingCharge;
    private String signature;
    private String signaturePubKey;
    private String currency;
    private RealmList<InvoiceItem> invoiceItemList;

    public UnpaidInvoice() {
    }

    public UnpaidInvoice(String invoiceId, String senderVatNo, String senderUserId, String senderName,
                         String receiverVatNo, String receiverUserId, String receiverName,
                         String totalAmountInWords, String anchorRef, String notes,
                         String forUserId, String invoiceHeading,
                         String dueDate, Double totalAmountToPay, String taxableAmount,
                         String vatPercent, String disPercent, String invoiceDate, String shippingCharge,
                         String signature, String signaturePubKey, String currency,
                         RealmList<InvoiceItem> invoiceItemList) {
        this.invoiceId = invoiceId;
        this.senderVatNo = senderVatNo;
        this.senderUserId = senderUserId;
        this.senderName = senderName;
        this.receiverVatNo = receiverVatNo;
        this.receiverUserId = receiverUserId;
        this.receiverName = receiverName;
        this.totalAmountInWords = totalAmountInWords;
        this.anchorRef = anchorRef;
        this.notes = notes;
        this.forUserId = forUserId;
        this.invoiceHeading = invoiceHeading;
        this.dueDate = dueDate;
        this.totalAmountToPay = totalAmountToPay;
        this.taxableAmount = taxableAmount;
        this.vatPercent = vatPercent;
        this.disPercent = disPercent;
        this.invoiceDate = invoiceDate;
        this.shippingCharge = shippingCharge;
        this.signature = signature;
        this.signaturePubKey = signaturePubKey;
        this.currency = currency;
        this.invoiceItemList = invoiceItemList;


    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getSignaturePubKey() {
        return signaturePubKey;
    }

    public void setSignaturePubKey(String signaturePubKey) {
        this.signaturePubKey = signaturePubKey;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getShippingCharge() {
        return shippingCharge;
    }

    public void setShippingCharge(String shippingCharge) {
        this.shippingCharge = shippingCharge;
    }

    public Double getTotalAmountToPay() {
        return totalAmountToPay;
    }

    public void setTotalAmountToPay(Double totalAmountToPay) {
        this.totalAmountToPay = totalAmountToPay;
    }

    public String getTaxableAmount() {
        return taxableAmount;
    }

    public void setTaxableAmount(String taxableAmount) {
        this.taxableAmount = taxableAmount;
    }

    public String getVatPercent() {
        return vatPercent;
    }

    public void setVatPercent(String vatPercent) {
        this.vatPercent = vatPercent;
    }

    public String getDisPercent() {
        return disPercent;
    }

    public void setDisPercent(String disPercent) {
        this.disPercent = disPercent;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public RealmList<InvoiceItem> getInvoiceItemList() {
        return invoiceItemList;
    }

    public void setInvoiceItemList(RealmList<InvoiceItem> invoiceItemList) {
        this.invoiceItemList = invoiceItemList;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getSenderVatNo() {
        return senderVatNo;
    }

    public void setSenderVatNo(String senderVatNo) {
        this.senderVatNo = senderVatNo;
    }

    public String getSenderUserId() {
        return senderUserId;
    }

    public void setSenderUserId(String senderUserId) {
        this.senderUserId = senderUserId;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getReceiverVatNo() {
        return receiverVatNo;
    }

    public void setReceiverVatNo(String receiverVatNo) {
        this.receiverVatNo = receiverVatNo;
    }

    public String getReceiverUserId() {
        return receiverUserId;
    }

    public void setReceiverUserId(String receiverUserId) {
        this.receiverUserId = receiverUserId;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getTotalAmountInWords() {
        return totalAmountInWords;
    }

    public void setTotalAmountInWords(String totalAmountInWords) {
        this.totalAmountInWords = totalAmountInWords;
    }

    public String getAnchorRef() {
        return anchorRef;
    }

    public void setAnchorRef(String anchorRef) {
        this.anchorRef = anchorRef;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getForUserId() {
        return forUserId;
    }

    public void setForUserId(String forUserId) {
        this.forUserId = forUserId;
    }

    public String getInvoiceHeading() {
        return invoiceHeading;
    }

    public void setInvoiceHeading(String invoiceHeading) {
        this.invoiceHeading = invoiceHeading;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(invoiceHeading);
        parcel.writeString(senderVatNo);
        parcel.writeString(senderUserId);
        parcel.writeString(senderName);
        parcel.writeString(receiverVatNo);
        parcel.writeString(receiverUserId);
        parcel.writeString(receiverName);
        parcel.writeString(totalAmountInWords);
        parcel.writeString(anchorRef);
        parcel.writeString(notes);
        parcel.writeString(forUserId);
        parcel.writeString(invoiceId);
        parcel.writeString(dueDate);
        parcel.writeDouble(totalAmountToPay);
        parcel.writeString(taxableAmount);
        parcel.writeString(vatPercent);
        parcel.writeString(disPercent);
        parcel.writeString(invoiceDate);
        parcel.writeString(shippingCharge);
        parcel.writeString(signature);
        parcel.writeString(signaturePubKey);
        parcel.writeString(currency);
        parcel.writeList(invoiceItemList);
    }

    private UnpaidInvoice(Parcel in) {
        this.invoiceHeading = in.readString();
        this.senderVatNo = in.readString();
        this.senderUserId = in.readString();
        this.senderName = in.readString();
        this.receiverVatNo = in.readString();
        this.receiverUserId = in.readString();
        this.receiverName = in.readString();
        this.totalAmountInWords = in.readString();
        this.anchorRef = in.readString();
        this.notes = in.readString();
        this.forUserId = in.readString();
        this.invoiceId = in.readString();
        this.dueDate = in.readString();
        this.totalAmountToPay = in.readDouble();
        this.taxableAmount = in.readString();
        this.vatPercent = in.readString();
        this.disPercent = in.readString();
        this.invoiceDate = in.readString();
        this.shippingCharge = in.readString();
        this.signature = in.readString();
        this.signaturePubKey = in.readString();
        this.currency = in.readString();
        invoiceItemList = new RealmList<>();
        in.readList(invoiceItemList, WalletProto.InvoiceItem.class.getClassLoader());
    }


    public static final Parcelable.Creator<UnpaidInvoice> CREATOR = new Parcelable.Creator<UnpaidInvoice>() {
        @Override
        public UnpaidInvoice createFromParcel(Parcel source) {
            return new UnpaidInvoice(source);
        }

        @Override
        public UnpaidInvoice[] newArray(int size) {
            return new UnpaidInvoice[size];
        }
    };


}
