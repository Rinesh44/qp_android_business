package com.yanky.savxbusiness.realm.models;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Autopayment extends RealmObject implements Parcelable {
    @PrimaryKey
    private String paymentId;
    private String amount;
    private String businessUserId;
    private String countryCode;
    private String currency;
    private long date;
    private String desc;
    private String fromUserId;
    private String fromUserName;
    private String fromWalletAddress;
    private String invoiceId;
    private String signature;
    private int status;
    private long timeStamp;
    private String toUsername;
    private String toUserId;
    private String toWalletAddress;
    private String senderName;


    public Autopayment(String amount, String businessUserId, String countryCode, String currency, long date, String desc, String fromUserId, String fromUserName, String fromWalletAddress, String invoiceId, String paymentId, String signature, int status, long timeStamp, String toUsername, String toUserId, String toWalletAddress, String senderName) {
        this.amount = amount;
        this.businessUserId = businessUserId;
        this.countryCode = countryCode;
        this.currency = currency;
        this.date = date;
        this.desc = desc;
        this.fromUserId = fromUserId;
        this.fromUserName = fromUserName;
        this.fromWalletAddress = fromWalletAddress;
        this.invoiceId = invoiceId;
        this.paymentId = paymentId;
        this.signature = signature;
        this.status = status;
        this.timeStamp = timeStamp;
        this.toUsername = toUsername;
        this.toUserId = toUserId;
        this.toWalletAddress = toWalletAddress;
        this.senderName = senderName;
    }

    public Autopayment() {
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBusinessUserId() {
        return businessUserId;
    }

    public void setBusinessUserId(String businessUserId) {
        this.businessUserId = businessUserId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(String fromUserId) {
        this.fromUserId = fromUserId;
    }

    public String getFromUserName() {
        return fromUserName;
    }

    public void setFromUserName(String fromUserName) {
        this.fromUserName = fromUserName;
    }

    public String getFromWalletAddress() {
        return fromWalletAddress;
    }

    public void setFromWalletAddress(String fromWalletAddress) {
        this.fromWalletAddress = fromWalletAddress;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getToUsername() {
        return toUsername;
    }

    public void setToUsername(String toUsername) {
        this.toUsername = toUsername;
    }

    public String getToUserId() {
        return toUserId;
    }

    public void setToUserId(String toUserId) {
        this.toUserId = toUserId;
    }

    public String getToWalletAddress() {
        return toWalletAddress;
    }

    public void setToWalletAddress(String toWalletAddress) {
        this.toWalletAddress = toWalletAddress;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(amount);
        parcel.writeString(businessUserId);
        parcel.writeString(countryCode);
        parcel.writeString(currency);
        parcel.writeLong(date);
        parcel.writeString(desc);
        parcel.writeString(fromUserId);
        parcel.writeString(fromUserName);
        parcel.writeString(fromWalletAddress);
        parcel.writeString(invoiceId);
        parcel.writeString(paymentId);
        parcel.writeString(signature);
        parcel.writeInt(status);
        parcel.writeLong(timeStamp);
        parcel.writeString(toUsername);
        parcel.writeString(toUserId);
        parcel.writeString(toWalletAddress);
        parcel.writeString(senderName);
    }


    private Autopayment(Parcel in) {
        this.amount = in.readString();
        this.businessUserId = in.readString();
        this.countryCode = in.readString();
        this.currency = in.readString();
        this.date = in.readLong();
        this.desc = in.readString();
        this.fromUserId = in.readString();
        this.fromUserName = in.readString();
        this.fromWalletAddress = in.readString();
        this.invoiceId = in.readString();
        this.paymentId = in.readString();
        this.signature = in.readString();
        this.status = in.readInt();
        this.timeStamp = in.readLong();
        this.toUsername = in.readString();
        this.toUserId = in.readString();
        this.toWalletAddress = in.readString();
        this.senderName = in.readString();
    }


    public static final Parcelable.Creator<Autopayment> CREATOR = new Parcelable.Creator<Autopayment>() {
        @Override
        public Autopayment createFromParcel(Parcel source) {
            return new Autopayment(source);
        }

        @Override
        public Autopayment[] newArray(int size) {
            return new Autopayment[size];
        }
    };

}
