package com.yanky.savxbusiness.realm.models;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class    TransactionOnHoldHolder extends RealmObject implements Parcelable {
    @PrimaryKey
    private String transactionId;
    private String signature;
    private String refId;
    private String fromUserId;
    private String toUserId;
    private String fromUserFullName;
    private String fromUserProfilePicUrl;
    private String toUserFullName;
    private String toUserProfilePicUrl;
    private String signaturePubKey;
    private String toWalletAddress;
    private String sourceId;
    private String walletName;
    private String mcc;
    private String accountNumber;
    private String accountType;
    private String bankAccountName;
    private String nameOnAccount;
    private String routingNumber;
    private String transactiontype;
    private long timestamp;
    private String amount;
    private String toPubKey;
    private String fromPubKey;
    private String toCurrency;
    private String fromCurrency;
    private String fromCountryCode;
    private String toCountryCode;


    public TransactionOnHoldHolder() {
    }

    public TransactionOnHoldHolder(String transactionId, String signature, String refId, String fromUserId, String toUserId, String fromUserFullName, String fromUserProfilePicUrl, String toUserFullName, String toUserProfilePicUrl, String signaturePubKey, String toWalletAddress, String sourceId, String walletName, String mcc, String accountNumber,
                                   String accountType, String bankAccountName, String nameOnAccount, String routingNumber, String transactiontype, long timestamp, String amount,
                                   String toPubKey, String fromPubKey, String toCurrency, String fromCurrency, String fromCountryCode, String toCountryCode) {
        this.transactionId = transactionId;
        this.signature = signature;
        this.refId = refId;
        this.fromUserId = fromUserId;
        this.toUserId = toUserId;
        this.fromUserFullName = fromUserFullName;
        this.fromUserProfilePicUrl = fromUserProfilePicUrl;
        this.toUserFullName = toUserFullName;
        this.toUserProfilePicUrl = toUserProfilePicUrl;
        this.signaturePubKey = signaturePubKey;
        this.toWalletAddress = toWalletAddress;
        this.sourceId = sourceId;
        this.walletName = walletName;
        this.mcc = mcc;
        this.accountNumber = accountNumber;
        this.accountType = accountType;
        this.bankAccountName = bankAccountName;
        this.nameOnAccount = nameOnAccount;
        this.routingNumber = routingNumber;
        this.transactiontype = transactiontype;
        this.timestamp = timestamp;
        this.amount = amount;
        this.toPubKey = toPubKey;
        this.fromPubKey = fromPubKey;
        this.toCurrency = toCurrency;
        this.fromCurrency = fromCurrency;
        this.fromCountryCode = fromCountryCode;
        this.toCountryCode = toCountryCode;
    }

    public String getFromCountryCode() {
        return fromCountryCode;
    }

    public void setFromCountryCode(String fromCountryCode) {
        this.fromCountryCode = fromCountryCode;
    }

    public String getToCountryCode() {
        return toCountryCode;
    }

    public void setToCountryCode(String toCountryCode) {
        this.toCountryCode = toCountryCode;
    }

    public String getToCurrency() {
        return toCurrency;
    }

    public void setToCurrency(String toCurrency) {
        this.toCurrency = toCurrency;
    }

    public String getFromCurrency() {
        return fromCurrency;
    }

    public void setFromCurrency(String fromCurrency) {
        this.fromCurrency = fromCurrency;
    }

    public String getTransactiontype() {
        return transactiontype;
    }

    public void setTransactiontype(String transactiontype) {
        this.transactiontype = transactiontype;
    }

    public String getToPubKey() {
        return toPubKey;
    }

    public void setToPubKey(String toPubKey) {
        this.toPubKey = toPubKey;
    }

    public String getFromPubKey() {
        return fromPubKey;
    }

    public void setFromPubKey(String fromPubKey) {
        this.fromPubKey = fromPubKey;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getBankAccountName() {
        return bankAccountName;
    }

    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName;
    }

    public String getNameOnAccount() {
        return nameOnAccount;
    }

    public void setNameOnAccount(String nameOnAccount) {
        this.nameOnAccount = nameOnAccount;
    }

    public String getRoutingNumber() {
        return routingNumber;
    }

    public void setRoutingNumber(String routingNumber) {
        this.routingNumber = routingNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(String fromUserId) {
        this.fromUserId = fromUserId;
    }

    public String getToUserId() {
        return toUserId;
    }

    public void setToUserId(String toUserId) {
        this.toUserId = toUserId;
    }

    public String getFromUserFullName() {
        return fromUserFullName;
    }

    public void setFromUserFullName(String fromUserFullName) {
        this.fromUserFullName = fromUserFullName;
    }

    public String getFromUserProfilePicUrl() {
        return fromUserProfilePicUrl;
    }

    public void setFromUserProfilePicUrl(String fromUserProfilePicUrl) {
        this.fromUserProfilePicUrl = fromUserProfilePicUrl;
    }

    public String getToUserFullName() {
        return toUserFullName;
    }

    public void setToUserFullName(String toUserFullName) {
        this.toUserFullName = toUserFullName;
    }

    public String getToUserProfilePicUrl() {
        return toUserProfilePicUrl;
    }

    public void setToUserProfilePicUrl(String toUserProfilePicUrl) {
        this.toUserProfilePicUrl = toUserProfilePicUrl;
    }

    public String getSignaturePubKey() {
        return signaturePubKey;
    }

    public void setSignaturePubKey(String signaturePubKey) {
        this.signaturePubKey = signaturePubKey;
    }

    public String getToWalletAddress() {
        return toWalletAddress;
    }

    public void setToWalletAddress(String toWalletAddress) {
        this.toWalletAddress = toWalletAddress;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getWalletName() {
        return walletName;
    }

    public void setWalletName(String walletName) {
        this.walletName = walletName;
    }

    public String getMcc() {
        return mcc;
    }

    public void setMcc(String mcc) {
        this.mcc = mcc;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(transactionId);
        parcel.writeString(signature);
        parcel.writeString(refId);
        parcel.writeString(fromUserId);
        parcel.writeString(toUserId);
        parcel.writeString(fromUserFullName);
        parcel.writeString(fromUserProfilePicUrl);
        parcel.writeString(toUserFullName);
        parcel.writeString(toUserProfilePicUrl);
        parcel.writeString(signaturePubKey);
        parcel.writeString(toWalletAddress);
        parcel.writeString(sourceId);
        parcel.writeString(walletName);
        parcel.writeString(mcc);
        parcel.writeString(accountNumber);
        parcel.writeString(accountType);
        parcel.writeString(bankAccountName);
        parcel.writeString(nameOnAccount);
        parcel.writeString(routingNumber);
        parcel.writeString(transactiontype);
        parcel.writeLong(timestamp);
        parcel.writeString(amount);
        parcel.writeString(toPubKey);
        parcel.writeString(fromPubKey);
        parcel.writeString(toCurrency);
        parcel.writeString(fromCurrency);
        parcel.writeString(fromCountryCode);
        parcel.writeString(toCountryCode);

    }

    private TransactionOnHoldHolder(Parcel in) {
        this.transactionId = in.readString();
        this.signature = in.readString();
        this.refId = in.readString();
        this.fromUserId = in.readString();
        this.toUserId = in.readString();
        this.fromUserFullName = in.readString();
        this.fromUserProfilePicUrl = in.readString();
        this.toUserFullName = in.readString();
        this.toUserProfilePicUrl = in.readString();
        this.signaturePubKey = in.readString();
        this.toWalletAddress = in.readString();
        this.sourceId = in.readString();
        this.walletName = in.readString();
        this.mcc = in.readString();
        this.accountNumber = in.readString();
        this.accountType = in.readString();
        this.bankAccountName = in.readString();
        this.nameOnAccount = in.readString();
        this.routingNumber = in.readString();
        this.transactiontype = in.readString();
        this.timestamp = in.readLong();
        this.amount = in.readString();
        this.toPubKey = in.readString();
        this.fromPubKey = in.readString();
        this.toCurrency = in.readString();
        this.fromCurrency = in.readString();
        this.fromCountryCode = in.readString();
        this.toCountryCode = in.readString();
    }

    public static final Parcelable.Creator<TransactionOnHoldHolder> CREATOR = new Parcelable.Creator<TransactionOnHoldHolder>() {
        @Override
        public TransactionOnHoldHolder createFromParcel(Parcel source) {
            return new TransactionOnHoldHolder(source);
        }

        @Override
        public TransactionOnHoldHolder[] newArray(int size) {
            return new TransactionOnHoldHolder[size];
        }
    };
}
