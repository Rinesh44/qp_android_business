package com.yanky.savxbusiness.realm.models;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class InvoiceItem extends RealmObject implements Parcelable {
    @PrimaryKey
    private String id;
    private String description;
    private String quantity;
    private String unitPrice;

    public InvoiceItem() {
    }

    public InvoiceItem(String description, String quantity, String unitPrice, String id) {
        this.description = description;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(description);
        parcel.writeString(quantity);
        parcel.writeString(unitPrice);
    }

    private InvoiceItem(Parcel in) {
        this.id = in.readString();
        this.description = in.readString();
        this.quantity = in.readString();
        this.unitPrice = in.readString();
    }

    public static final Parcelable.Creator<InvoiceItem> CREATOR = new Parcelable.Creator<InvoiceItem>() {
        @Override
        public InvoiceItem createFromParcel(Parcel source) {
            return new InvoiceItem(source);
        }

        @Override
        public InvoiceItem[] newArray(int size) {
            return new InvoiceItem[size];
        }
    };


}
