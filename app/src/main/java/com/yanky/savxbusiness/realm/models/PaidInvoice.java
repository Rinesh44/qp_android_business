package com.yanky.savxbusiness.realm.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.yanky.savxbusiness.entities.WalletProto;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class PaidInvoice extends RealmObject implements Parcelable {
    @PrimaryKey
    private String invoiceId;
    private String senderVatNo;
    private String senderUserId;
    private String senderName;
    private String receiverVatNo;
    private String receiverUserId;
    private String receiverName;
    private String totalAmountInWords;
    private String anchorRef;
    private String notes;
    private String forUserId;
    private String invoiceHeading;
    private String dueDate;
    private String totalAmountToPay;
    private String taxableAmount;
    private String vatPercent;
    private String disPercent;
    private String invoiceDate;
    private String signature;
    private RealmList<InvoiceItem> invoiceItemList;

    public PaidInvoice() {
    }

    public PaidInvoice(String invoiceId, String senderVatNo, String senderUserId, String senderName,
                       String receiverVatNo, String receiverUserId, String receiverName,
                       String totalAmountInWords, String anchorRef, String notes, String forUserId,
                       String invoiceHeading,
                       String dueDate, String totalAmountToPay, String taxableAmount,
                       String vatPercent, String disPercent, String invoiceDate, String signature, RealmList<InvoiceItem> invoiceItemList) {
        this.invoiceId = invoiceId;
        this.senderVatNo = senderVatNo;
        this.senderUserId = senderUserId;
        this.senderName = senderName;
        this.receiverVatNo = receiverVatNo;
        this.receiverUserId = receiverUserId;
        this.receiverName = receiverName;
        this.totalAmountInWords = totalAmountInWords;
        this.anchorRef = anchorRef;
        this.notes = notes;
        this.forUserId = forUserId;
        this.invoiceHeading = invoiceHeading;
        this.dueDate = dueDate;
        this.taxableAmount = taxableAmount;
        this.vatPercent = vatPercent;
        this.disPercent = disPercent;
        this.invoiceDate = invoiceDate;
        this.totalAmountToPay = totalAmountToPay;
        this.signature = signature;
        this.invoiceItemList = invoiceItemList;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public List<InvoiceItem> getInvoiceItemList() {
        return invoiceItemList;
    }

    public void setInvoiceItemList(RealmList<InvoiceItem> invoiceItemList) {
        this.invoiceItemList = invoiceItemList;
    }

    public String getTaxableAmount() {
        return taxableAmount;
    }

    public void setTaxableAmount(String taxableAmount) {
        this.taxableAmount = taxableAmount;
    }

    public String getVatPercent() {
        return vatPercent;
    }

    public void setVatPercent(String vatPercent) {
        this.vatPercent = vatPercent;
    }

    public String getDisPercent() {
        return disPercent;
    }

    public void setDisPercent(String disPercent) {
        this.disPercent = disPercent;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getTotalAmountToPay() {
        return totalAmountToPay;
    }

    public void setTotalAmountToPay(String totalAmountToPay) {
        this.totalAmountToPay = totalAmountToPay;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getSenderVatNo() {
        return senderVatNo;
    }

    public void setSenderVatNo(String senderVatNo) {
        this.senderVatNo = senderVatNo;
    }

    public String getSenderUserId() {
        return senderUserId;
    }

    public void setSenderUserId(String senderUserId) {
        this.senderUserId = senderUserId;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getReceiverVatNo() {
        return receiverVatNo;
    }

    public void setReceiverVatNo(String receiverVatNo) {
        this.receiverVatNo = receiverVatNo;
    }

    public String getReceiverUserId() {
        return receiverUserId;
    }

    public void setReceiverUserId(String receiverUserId) {
        this.receiverUserId = receiverUserId;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getTotalAmountInWords() {
        return totalAmountInWords;
    }

    public void setTotalAmountInWords(String totalAmountInWords) {
        this.totalAmountInWords = totalAmountInWords;
    }

    public String getAnchorRef() {
        return anchorRef;
    }

    public void setAnchorRef(String anchorRef) {
        this.anchorRef = anchorRef;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getForUserId() {
        return forUserId;
    }

    public void setForUserId(String forUserId) {
        this.forUserId = forUserId;
    }

    public String getInvoiceHeading() {
        return invoiceHeading;
    }

    public void setInvoiceHeading(String invoiceHeading) {
        this.invoiceHeading = invoiceHeading;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(invoiceHeading);
        parcel.writeString(senderVatNo);
        parcel.writeString(senderUserId);
        parcel.writeString(senderName);
        parcel.writeString(receiverVatNo);
        parcel.writeString(receiverUserId);
        parcel.writeString(receiverName);
        parcel.writeString(totalAmountInWords);
        parcel.writeString(anchorRef);
        parcel.writeString(notes);
        parcel.writeString(forUserId);
        parcel.writeString(invoiceId);
        parcel.writeString(dueDate);
        parcel.writeString(totalAmountToPay);
        parcel.writeString(taxableAmount);
        parcel.writeString(vatPercent);
        parcel.writeString(disPercent);
        parcel.writeString(invoiceDate);
        parcel.writeString(signature);
        parcel.writeList(invoiceItemList);
    }

    private PaidInvoice(Parcel in) {
        this.invoiceHeading = in.readString();
        this.senderVatNo = in.readString();
        this.senderUserId = in.readString();
        this.senderName = in.readString();
        this.receiverVatNo = in.readString();
        this.receiverUserId = in.readString();
        this.receiverName = in.readString();
        this.totalAmountInWords = in.readString();
        this.anchorRef = in.readString();
        this.notes = in.readString();
        this.forUserId = in.readString();
        this.invoiceId = in.readString();
        this.dueDate = in.readString();
        this.totalAmountToPay = in.readString();
        this.taxableAmount = in.readString();
        this.vatPercent = in.readString();
        this.disPercent = in.readString();
        this.invoiceDate = in.readString();
        this.signature = in.readString();
        invoiceItemList = new RealmList<>();
        in.readList(invoiceItemList, WalletProto.InvoiceItem.class.getClassLoader());
    }

    public static final Parcelable.Creator<PaidInvoice> CREATOR = new Parcelable.Creator<PaidInvoice>() {
        @Override
        public PaidInvoice createFromParcel(Parcel source) {
            return new PaidInvoice(source);
        }

        @Override
        public PaidInvoice[] newArray(int size) {
            return new PaidInvoice[size];
        }
    };
}

