package com.yanky.savxbusiness.realm.models;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Wallet extends RealmObject implements Parcelable {
    @PrimaryKey
    private String walletAddress;
    private String userId;
    private long timestamp;
    private String walletName;
    private double amount;
    private boolean isDefault;
    private long lastActive;
    private String currency;
    private String countryCode;
    private boolean createdBySelf;

    public Wallet() {
    }

    public Wallet(String walletAddress, String userId, long timestamp, String walletName, double amount, boolean isDefault, long lastActive,
                  String currency, String countryCode, boolean createdBySelf) {
        this.walletAddress = walletAddress;
        this.userId = userId;
        this.timestamp = timestamp;
        this.walletName = walletName;
        this.amount = amount;
        this.isDefault = isDefault;
        this.lastActive = lastActive;
        this.currency = currency;
        this.countryCode = countryCode;
        this.createdBySelf = createdBySelf;
    }

    public String getWalletAddress() {
        return walletAddress;
    }

    public void setWalletAddress(String walletAddress) {
        this.walletAddress = walletAddress;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getWalletName() {
        return walletName;
    }

    public void setWalletName(String walletName) {
        this.walletName = walletName;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public void setDefault(boolean aDefault) {
        isDefault = aDefault;
    }

    public long getLastActive() {
        return lastActive;
    }

    public void setLastActive(long lastActive) {
        this.lastActive = lastActive;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public boolean isCreatedBySelf() {
        return createdBySelf;
    }

    public void setCreatedBySelf(boolean createdBySelf) {
        this.createdBySelf = createdBySelf;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(walletAddress);
        parcel.writeString(userId);
        parcel.writeLong(timestamp);
        parcel.writeString(walletName);
        parcel.writeDouble(amount);
        parcel.writeByte((byte) (isDefault ? 1 : 0));
        parcel.writeLong(lastActive);
        parcel.writeString(currency);
        parcel.writeString(countryCode);
        parcel.writeByte((byte) (createdBySelf ? 1 : 0));
    }

    private Wallet(Parcel in) {
        this.walletAddress = in.readString();
        this.userId = in.readString();
        this.timestamp = in.readLong();
        this.walletName = in.readString();
        this.amount = in.readDouble();
        this.isDefault = in.readByte() != 0;
        this.lastActive = in.readLong();
        this.currency = in.readString();
        this.countryCode = in.readString();
        this.createdBySelf = in.readByte() != 0;
    }

    public static final Parcelable.Creator<Wallet> CREATOR = new Parcelable.Creator<Wallet>() {
        @Override
        public Wallet createFromParcel(Parcel source) {
            return new Wallet(source);
        }

        @Override
        public Wallet[] newArray(int size) {
            return new Wallet[size];
        }
    };
}


