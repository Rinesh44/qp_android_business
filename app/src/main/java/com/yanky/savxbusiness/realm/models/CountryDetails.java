package com.yanky.savxbusiness.realm.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class CountryDetails extends RealmObject {
    @PrimaryKey
    private String countryCode;
    private String dialCode;
    private String name;

    public CountryDetails(String countryCode, String dialCode, String name) {
        this.countryCode = countryCode;
        this.dialCode = dialCode;
        this.name = name;
    }

    public CountryDetails() {
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getDialCode() {
        return dialCode;
    }

    public void setDialCode(String dialCode) {
        this.dialCode = dialCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
