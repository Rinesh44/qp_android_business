package com.yanky.savxbusiness.realm.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class WaitingUser extends RealmObject {
    @PrimaryKey
    String emailPhone;
    String name;
    String pushToken;
    String deviceInfo;
    String deviceId;

    public WaitingUser() {
    }

    public WaitingUser(String emailPhone, String name, String pushToken, String deviceInfo, String deviceId) {
        this.emailPhone = emailPhone;
        this.name = name;
        this.pushToken = pushToken;
        this.deviceInfo = deviceInfo;
        this.deviceId = deviceId;
    }

    public String getEmailPhone() {
        return emailPhone;
    }

    public void setEmailPhone(String emailPhone) {
        this.emailPhone = emailPhone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPushToken() {
        return pushToken;
    }

    public void setPushToken(String pushToken) {
        this.pushToken = pushToken;
    }

    public String getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(String deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
