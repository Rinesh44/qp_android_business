package com.yanky.savxbusiness.realm.models;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Leesa Shakya on 02/10/18.
 * leezshk@gmail.com
 */

public class User extends RealmObject implements Parcelable {
    public final static String USER_ID = "userId";
    @PrimaryKey
    private String userId;
    private String fullName;
    private String username;
    private String userType;
    private String userStatus;
    private String gender;
    private String profilePicUrl;
    private boolean verifiedProfile;
    private boolean isKeyBackup;
    private long joinedTimestamp;
    private String dob;
    private String emailPhone;
    private String address;
    private String country;

    public User() {
    }

    public User(String userId, String fullName, String username, String userType, String userStatus, String gender, String profilePicUrl, boolean verifiedProfile, boolean isKeyBackup, long joinedTimestamp, String dob, String emailPhone, String address, String country) {
        this.userId = userId;
        this.fullName = fullName;
        this.username = username;
        this.userType = userType;
        this.userStatus = userStatus;
        this.gender = gender;
        this.profilePicUrl = profilePicUrl;
        this.verifiedProfile = verifiedProfile;
        this.isKeyBackup = isKeyBackup;
        this.joinedTimestamp = joinedTimestamp;
        this.dob = dob;
        this.emailPhone = emailPhone;
        this.address = address;
        this.country = country;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getProfilePicUrl() {
        return profilePicUrl;
    }

    public void setProfilePicUrl(String profilePicUrl) {
        this.profilePicUrl = profilePicUrl;
    }

    public boolean isVerifiedProfile() {
        return verifiedProfile;
    }

    public void setVerifiedProfile(boolean verifiedProfile) {
        this.verifiedProfile = verifiedProfile;
    }

    public long getJoinedTimestamp() {
        return joinedTimestamp;
    }

    public void setJoinedTimestamp(long joinedTimestamp) {
        this.joinedTimestamp = joinedTimestamp;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getEmailPhone() {
        return emailPhone;
    }

    public void setEmailPhone(String emailPhone) {
        this.emailPhone = emailPhone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public boolean isKeyBackup() {
        return isKeyBackup;
    }

    public void setKeyBackup(boolean keyBackup) {
        isKeyBackup = keyBackup;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(userId);
        parcel.writeString(fullName);
        parcel.writeString(username);
        parcel.writeString(userType);
        parcel.writeString(userStatus);
        parcel.writeString(gender);
        parcel.writeString(profilePicUrl);
        parcel.writeByte((byte) (verifiedProfile ? 1 : 0));
        parcel.writeByte((byte) (isKeyBackup ? 1 : 0));
        parcel.writeLong(joinedTimestamp);
        parcel.writeString(dob);
        parcel.writeString(emailPhone);
        parcel.writeString(address);
        parcel.writeString(country);

    }

    private User(Parcel in) {
        this.userId = in.readString();
        this.fullName = in.readString();
        this.username = in.readString();
        this.userType = in.readString();
        this.userStatus = in.readString();
        this.gender = in.readString();
        this.profilePicUrl = in.readString();
        this.verifiedProfile = in.readByte() != 0;
        this.isKeyBackup = in.readByte() != 0;
        this.joinedTimestamp = in.readLong();
        this.dob = in.readString();
        this.emailPhone = in.readString();
        this.address = in.readString();
        this.country = in.readString();
    }


    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

}
