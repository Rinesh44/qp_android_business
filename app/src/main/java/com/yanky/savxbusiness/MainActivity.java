package com.yanky.savxbusiness;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yanky.savxbusiness.activities.waitinglist.EnterCredential;
import com.yanky.savxbusiness.adapters.SliderAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Leesa Shakya on 25/09/18.
 * leezshk@gmail.com
 */

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    @BindView(R.id.view_pager)
    ViewPager mViewPager;
    @BindView(R.id.ll_dot_layout)
    LinearLayout mDotLayout;
    @BindView(R.id.btn_nav)
    Button mNav;

    private SliderAdapter mAdapter;
    private TextView[] mDots;

    private int mCurrentPage;
    private int lastPageCheck = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        mAdapter = new SliderAdapter(this);
        mViewPager.setAdapter(mAdapter);

        addDotsIndicator(0);

        mViewPager.addOnPageChangeListener(viewListener);

        mNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(mCurrentPage + 1);

                if (lastPageCheck > mDots.length && mNav.getText().toString().equals("finish")) {
                    startActivity(new Intent(MainActivity.this, EnterCredential.class));
                    return;
                }

                if (mCurrentPage <= mDots.length - 1) {
                    lastPageCheck = mCurrentPage + 1;
                }

                lastPageCheck++;

            }
        });
    }


    public void addDotsIndicator(int position) {
        mDots = new TextView[4];
        mDotLayout.removeAllViews();

        for (int i = 0; i < mDots.length; i++) {
            mDots[i] = new TextView(this);
            mDots[i].setText(Html.fromHtml("&#8226"));
            mDots[i].setTextSize(40);
            mDots[i].setPadding(0, 0, 25, 0);
            mDots[i].setTextColor(getResources().getColor(R.color.pelorous_light));

            mDotLayout.addView(mDots[i]);
        }

        if (mDots.length > 0) {
            mDots[position].setTextColor(getResources().getColor(R.color.pelorous));
        }
    }


    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            addDotsIndicator(position);

            mCurrentPage = position;

            if (position == mDots.length - 1) {
                mNav.setText("finish");

            } else {
                mNav.setText("next");
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };
}
