package com.yanky.savxbusiness.model;

public class Email {
    private String name;
    private String email;


    public Email(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public Email() {
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
