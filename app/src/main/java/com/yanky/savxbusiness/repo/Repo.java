package com.yanky.savxbusiness.repo;

import android.os.Looper;

import io.realm.Realm;

/**
 * Created by Leesa Shakya on 02/10/18.
 * leezshk@gmail.com
 */

public class Repo {
    void close(Realm realm){
        if(Thread.currentThread() != Looper.getMainLooper().getThread()){
            if(null != realm){
                realm.close();
            }
        }
    }

    public interface Callback<T>{
        void success(T t);
        void fail();
    }
}
