package com.yanky.savxbusiness.repo;

import com.yanky.savxbusiness.realm.RealmDatabase;
import com.yanky.savxbusiness.realm.models.CountryDetails;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

public class CountryDetailsRepo extends Repo {
    private static final CountryDetailsRepo countryDetailRepo;

    static {
        countryDetailRepo = new CountryDetailsRepo();
    }

    public static CountryDetailsRepo getInstance() {
        return countryDetailRepo;
    }


    public void saveCountryDetails(final List<CountryDetails> countryDetailsList, final Repo.Callback callback) {
        final Realm realm = RealmDatabase.getInstance().getRealm();

        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(countryDetailsList);
                    callback.success(true);
                }
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            callback.fail();
        } finally {
            close(realm);
        }
    }


    public List<CountryDetails> getAllCountryDetails() {
        Realm realm = RealmDatabase.getInstance().getRealm();
        try {
            List<CountryDetails> countryDetailsList = new ArrayList<>(realm.where(CountryDetails.class).findAll());
            return countryDetailsList;

        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return null;
        } finally {
            close(realm);
        }
    }

}
