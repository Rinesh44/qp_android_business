package com.yanky.savxbusiness.repo;

import com.yanky.savxbusiness.realm.RealmDatabase;
import com.yanky.savxbusiness.realm.models.WaitingUser;
import com.yanky.savxbusiness.utils.AppUtils;

import io.realm.Realm;
import io.realm.RealmResults;

public class WaitingUserRepo extends Repo {
    private static final String TAG = "WaitingUserRepo";

    private static final WaitingUserRepo waitingUserRepo;

    static {
        waitingUserRepo = new WaitingUserRepo();
    }

    public static WaitingUserRepo getInstance() {
        return waitingUserRepo;
    }


    public void saveWaitingUser(final WaitingUser waitingUser, final Repo.Callback callback) {
        final Realm realm = RealmDatabase.getInstance().getRealm();

        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(waitingUser);
                    callback.success(true);
                }
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            AppUtils.showLog(TAG, "throwable: " + throwable.toString());
            callback.fail();
        } finally {
            close(realm);
        }
    }

    public void deleteWaitingUser(final Callback callback) {
        final Realm realm = RealmDatabase.getInstance().getRealm();

        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<WaitingUser> waitingUser = realm.where(WaitingUser.class).findAll();
                    waitingUser.deleteAllFromRealm();
                    callback.success(true);
                }
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            callback.fail();
        } finally {
            close(realm);
        }
    }


    public WaitingUser getWaitingUser() {
        final Realm realm = RealmDatabase.getInstance().getRealm();
        try {
            WaitingUser waitingUser = realm.where(WaitingUser.class).findFirst();
            return waitingUser;

        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return null;
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

}
