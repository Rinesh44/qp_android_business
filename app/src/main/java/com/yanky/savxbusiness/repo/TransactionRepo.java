package com.yanky.savxbusiness.repo;

import com.yanky.savxbusiness.realm.RealmDatabase;
import com.yanky.savxbusiness.realm.models.TransactionHolder;
import com.yanky.savxbusiness.realm.models.TransactionOnHoldHolder;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class TransactionRepo extends Repo {
    private static final TransactionRepo transactionRepo;

    static {
        transactionRepo = new TransactionRepo();
    }

    public static TransactionRepo getInstance() {
        return transactionRepo;
    }

    public void saveTransactionList(final List<TransactionHolder> transactionHolderList, final Callback callback) {
        final Realm realm = RealmDatabase.getInstance().getRealm();

        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(transactionHolderList);
                    callback.success(true);
                }
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            callback.fail();
        } finally {
            close(realm);
        }
    }

    public void saveTransactionsOnHold(final List<TransactionOnHoldHolder> transactionHolderList, final Callback callback) {
        final Realm realm = RealmDatabase.getInstance().getRealm();

        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(transactionHolderList);
                    callback.success(true);
                }
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            callback.fail();
        } finally {
            close(realm);
        }
    }


    public List<TransactionOnHoldHolder> getAllTransactionHoldersOnHold() {
        Realm realm = RealmDatabase.getInstance().getRealm();
        try {
            List<TransactionOnHoldHolder> trasactionHolderList = new ArrayList<>(realm.where(TransactionOnHoldHolder.class).findAll());
            return trasactionHolderList;

        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return null;
        } finally {
            close(realm);
        }

    }


    public List<TransactionHolder> getAllTransactionHolders() {
        Realm realm = RealmDatabase.getInstance().getRealm();
        try {
            List<TransactionHolder> trasactionHolderList = new ArrayList<>(realm.where(TransactionHolder.class).findAll());
            return trasactionHolderList;

        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return null;
        } finally {
            close(realm);
        }

    }

    public List<TransactionHolder> getTransactionsByWalletAddress(String walletAddress) {
        Realm realm = RealmDatabase.getInstance().getRealm();
        try {
            List<TransactionHolder> transactionHolderList = new ArrayList<>(realm.where(TransactionHolder.class).equalTo("fromPubKey", walletAddress).findAll());
            return transactionHolderList;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return null;

        } finally {
            close(realm);
        }
    }


    public void deleteTransactionOnHoldById(String transactionId, final Callback callback) {
        final Realm realm = RealmDatabase.getInstance().getRealm();

        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<TransactionOnHoldHolder> result = realm.where(TransactionOnHoldHolder.class).equalTo("transactionId", transactionId).findAll();
                    result.deleteAllFromRealm();
                    callback.success(true);
                }
            });

        } catch (Throwable throwable) {
            throwable.printStackTrace();
            callback.fail();
        } finally {
            close(realm);
        }
    }


}
