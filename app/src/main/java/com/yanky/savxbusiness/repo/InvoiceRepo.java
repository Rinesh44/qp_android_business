package com.yanky.savxbusiness.repo;

import com.yanky.savxbusiness.realm.RealmDatabase;
import com.yanky.savxbusiness.realm.models.PaidInvoice;
import com.yanky.savxbusiness.realm.models.UnpaidInvoice;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

public class InvoiceRepo extends Repo {
    private static final InvoiceRepo invoiceRepo;

    static {
        invoiceRepo = new InvoiceRepo();
    }

    public static InvoiceRepo getInstance() {
        return invoiceRepo;
    }

    public void saveUnPaidInvoiceList(List<UnpaidInvoice> invoiceList, final Callback callback) {
        final Realm realm = RealmDatabase.getInstance().getRealm();

        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(invoiceList);
                    callback.success(true);
                }
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            callback.fail();
        } finally {
            close(realm);
        }
    }


    public List<UnpaidInvoice> getAllUnpaidInvoices() {
        Realm realm = RealmDatabase.getInstance().getRealm();
        try {
            List<UnpaidInvoice> invoiceList = new ArrayList<>(realm.where(UnpaidInvoice.class).findAll());
            return invoiceList;

        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return null;
        } finally {
            close(realm);
        }
    }


    public void savePaidInvoiceList(List<PaidInvoice> invoiceList, final Callback callback) {
        final Realm realm = RealmDatabase.getInstance().getRealm();

        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(invoiceList);
                    callback.success(true);
                }
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            callback.fail();
        } finally {
            close(realm);
        }
    }


    public void saveUnpaidInvoiceFromBarcode(UnpaidInvoice unpaidInvoice, final Callback callback) {
        final Realm realm = RealmDatabase.getInstance().getRealm();

        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(unpaidInvoice);
                    callback.success(true);
                }
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            callback.fail();
        } finally {
            close(realm);
        }
    }

    public List<PaidInvoice> getAllPaidInvoices() {
        Realm realm = RealmDatabase.getInstance().getRealm();
        try {
            List<PaidInvoice> invoiceList = new ArrayList<>(realm.where(PaidInvoice.class).findAll());
            return invoiceList;

        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return null;
        } finally {
            close(realm);
        }
    }
}
