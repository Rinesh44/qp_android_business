package com.yanky.savxbusiness.repo;

import com.yanky.savxbusiness.realm.RealmDatabase;
import com.yanky.savxbusiness.realm.models.SyncedContacts;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

public class SyncedContactsRepo extends Repo {
    private static final SyncedContactsRepo contactsRepo;

    static {
        contactsRepo = new SyncedContactsRepo();
    }

    public static SyncedContactsRepo getInstance() {
        return contactsRepo;
    }

    public void saveContacts(final List<SyncedContacts> syncedContactsList, final Callback callback) {
        final Realm realm = RealmDatabase.getInstance().getRealm();

        try {

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(syncedContactsList);
                    callback.success(true);
                }
            });

        } catch (Throwable throwable) {
            throwable.printStackTrace();
            callback.fail();
        } finally {
            close(realm);
        }
    }


    public List<SyncedContacts> getSyncedContacts() {
        final Realm realm = RealmDatabase.getInstance().getRealm();

        try {
            return new ArrayList<>(realm.where(SyncedContacts.class).findAll());

        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return null;
        } finally {
            close(realm);
        }
    }

}
