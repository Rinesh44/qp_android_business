package com.yanky.savxbusiness.repo;

import com.yanky.savxbusiness.realm.RealmDatabase;
import com.yanky.savxbusiness.realm.models.Wallet;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class WalletsRepo extends Repo {
    private static final WalletsRepo walletsRepo;

    static {
        walletsRepo = new WalletsRepo();
    }

    public static WalletsRepo getInstance() {
        return walletsRepo;
    }

    public void saveWalletList(final List<Wallet> walletList, final Callback callback) {
        final Realm realm = RealmDatabase.getInstance().getRealm();

        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(walletList);
                    callback.success(true);
                }
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            callback.fail();
        } finally {
            close(realm);
        }
    }

    public void deleteAllWallets(final Callback callback) {
        final Realm realm = RealmDatabase.getInstance().getRealm();

        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<Wallet> wallets = realm.where(Wallet.class).findAll();
                    wallets.deleteAllFromRealm();
                    callback.success(true);
                }
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            callback.fail();
        } finally {
            close(realm);
        }
    }


    public List<Wallet> getAllWallets() {
        final Realm realm = RealmDatabase.getInstance().getRealm();
        try {
            List<Wallet> walletList = new ArrayList<>(realm.where(Wallet.class).findAll());
            return walletList;

        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return null;
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public List<Wallet> getWalletsByCurrency(String currency) {
        final Realm realm = RealmDatabase.getInstance().getRealm();
        try {
            List<Wallet> walletList = new ArrayList<>(realm.where(Wallet.class).equalTo("currency", currency).findAll());
            return walletList;

        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return null;
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }


    public List<Wallet> getWalletsByMe() {
        final Realm realm = RealmDatabase.getInstance().getRealm();
        try {
            List<Wallet> walletList = new ArrayList<>(realm.where(Wallet.class).equalTo("createdBySelf", true).findAll());
            return walletList;

        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return null;
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }


    public List<Wallet> getWalletsByOthers() {
        final Realm realm = RealmDatabase.getInstance().getRealm();
        try {
            List<Wallet> walletList = new ArrayList<>(realm.where(Wallet.class).notEqualTo("createdBySelf", true).findAll());
            return walletList;

        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return null;
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }


    public Wallet getDefaultWallet() {
        final Realm realm = RealmDatabase.getInstance().getRealm();
        try {
            Wallet wallet = realm.where(Wallet.class).equalTo("isDefault", true).findFirst();
            return wallet;

        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return null;
        } finally {
            close(realm);
        }
    }


    public String getWalletNameForAddress(String walletAddress) {
        final Realm realm = RealmDatabase.getInstance().getRealm();

        try {
            Wallet wallet = realm.where(Wallet.class).equalTo("walletAddress", walletAddress)
                    .findFirst();
            return wallet.getWalletName();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return null;
        } finally {
            close(realm);
        }

    }


    public Wallet getWalletForAddress(String walletAddress) {
        final Realm realm = RealmDatabase.getInstance().getRealm();

        try {
            Wallet wallet = realm.where(Wallet.class).equalTo("walletAddress", walletAddress)
                    .findFirst();
            return wallet;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return null;
        } finally {
            close(realm);
        }

    }


    public String getWalletAddressForName(String walletName) {
        final Realm realm = RealmDatabase.getInstance().getRealm();

        try {
            Wallet wallet = realm.where(Wallet.class).equalTo("walletName", walletName)
                    .findFirst();
            return wallet.getWalletAddress();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return null;
        } finally {
            close(realm);
        }

    }


    public List<Wallet> getWalletsForWalletRate(String currency) {
        final Realm realm = RealmDatabase.getInstance().getRealm();

        try {
            List<Wallet> walletList = realm.where(Wallet.class).notEqualTo("currency", currency)
                    .findAll();
            return walletList;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return null;
        } finally {
            close(realm);
        }
    }


    public void deleteWalletById(String walletId, final Callback callback) {
        final Realm realm = RealmDatabase.getInstance().getRealm();

        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<Wallet> result = realm.where(Wallet.class).equalTo("walletAddress", walletId).findAll();
                    result.deleteAllFromRealm();
                    callback.success(true);
                }
            });

        } catch (Throwable throwable) {
            throwable.printStackTrace();
            callback.fail();
        } finally {
            close(realm);
        }
    }


}
