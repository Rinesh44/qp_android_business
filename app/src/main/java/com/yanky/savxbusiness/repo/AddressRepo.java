package com.yanky.savxbusiness.repo;

import com.yanky.savxbusiness.realm.RealmDatabase;
import com.yanky.savxbusiness.realm.models.Address;
import com.yanky.savxbusiness.utils.AppUtils;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class AddressRepo extends Repo {
    private static final String TAG = "AddressRepo";
    private static final AddressRepo addressRepo;

    static {
        addressRepo = new AddressRepo();
    }

    public static AddressRepo getInstance() {
        return addressRepo;
    }

    public void saveAddressList(final List<Address> addressList, final Callback callback) {
        final Realm realm = RealmDatabase.getInstance().getRealm();

        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(addressList);
                    callback.success(true);
                }
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            callback.fail();
        } finally {
            close(realm);
        }
    }

    public List<Address> getAllAddresses() {
        Realm realm = RealmDatabase.getInstance().getRealm();
        try {
            List<Address> addressList = new ArrayList<>(realm.where(Address.class).findAll());
            return addressList;

        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return null;
        } finally {
            close(realm);
        }
    }

    public void deleteAddress(String addressId, final Callback callback) {
        final Realm realm = RealmDatabase.getInstance().getRealm();

        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<Address> addresses = realm.where(Address.class).equalTo("addressId", addressId).findAll();
                    addresses.deleteAllFromRealm();
                    callback.success(true);
                }
            });

        } catch (Throwable throwable) {
            AppUtils.showLog(TAG, String.valueOf(throwable.toString()));
            throwable.printStackTrace();
            callback.fail();
        } finally {
            close(realm);
        }
    }

    public void deleteAllAddresses(final Callback callback) {
        final Realm realm = RealmDatabase.getInstance().getRealm();

        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<Address> addresses = realm.where(Address.class).findAll();
                    addresses.deleteAllFromRealm();
                    callback.success(true);
                }
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            callback.fail();
        } finally {
            close(realm);
        }
    }


}
