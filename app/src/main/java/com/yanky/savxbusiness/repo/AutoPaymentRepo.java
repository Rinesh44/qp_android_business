package com.yanky.savxbusiness.repo;

import com.yanky.savxbusiness.realm.RealmDatabase;
import com.yanky.savxbusiness.realm.models.Autopayment;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class AutoPaymentRepo extends Repo {

    private static final AutoPaymentRepo autoPaymentRepo;

    static {
        autoPaymentRepo = new AutoPaymentRepo();
    }

    public static AutoPaymentRepo getInstance() {
        return autoPaymentRepo;
    }

    public void saveAutoPayments(final List<Autopayment> autopaymentList, final Repo.Callback callback) {
        final Realm realm = RealmDatabase.getInstance().getRealm();

        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(autopaymentList);
                    callback.success(true);
                }
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            callback.fail();
        } finally {
            close(realm);
        }
    }

    public List<Autopayment> getAllAutoPayments() {
        Realm realm = RealmDatabase.getInstance().getRealm();
        try {
            List<Autopayment> autopaymentList = new ArrayList<>(realm.where(Autopayment.class).findAll());
            return autopaymentList;

        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return null;
        } finally {
            close(realm);
        }
    }

    public void deleteAllAutoPayments(final Callback callback) {
        final Realm realm = RealmDatabase.getInstance().getRealm();

        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<Autopayment> autopayments = realm.where(Autopayment.class).findAll();
                    autopayments.deleteAllFromRealm();
                    callback.success(true);
                }
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            callback.fail();
        } finally {
            close(realm);
        }
    }

    public void deleteAutoPaymentsById(String paymentId, final Callback callback) {
        final Realm realm = RealmDatabase.getInstance().getRealm();

        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<Autopayment> result = realm.where(Autopayment.class).equalTo("paymentId", paymentId).findAll();
                    result.deleteAllFromRealm();
                    callback.success(true);
                }
            });

        } catch (Throwable throwable) {
            throwable.printStackTrace();
            callback.fail();
        } finally {
            close(realm);
        }
    }
}
